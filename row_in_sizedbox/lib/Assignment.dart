import 'package:flutter/material.dart';

class Assignment extends StatelessWidget {
  const Assignment({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(("App_name")),
        ),
        body: SizedBox(
          height: double.infinity,
         width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 200,
                      width: 150,
                      color: Colors.orange,
                    ),
                    Container(
                      height: 200,
                      width: 150,
                      color: Colors.orange,
                    ),
                    Container(
                      height: 200,
                      width: 150,
                      color: Colors.orange,
                    )
                  ],
                ),
              ),
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 200,
                      width: 150,
                      color: Colors.orange,
                    ),
                    Container(
                      height: 200,
                      width: 150,
                      color: Colors.orange,
                    ),
                    Container(
                      height: 200,
                      width: 150,
                      color: Colors.orange,
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
