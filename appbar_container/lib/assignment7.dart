import 'package:flutter/material.dart';

class Assignment7 extends StatelessWidget {
  const Assignment7({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("App_name"),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            
            Image.network(
                "https://imgd.aeplcdn.com/1920x1080/n/cw/ec/55215/defender-exterior-right-front-three-quarter-3.jpeg?isig=0&q=80&q=80",
                height: 300,
                width: 150),
            const Padding(padding: EdgeInsets.only(right: 20)),
            Image.network(
                "https://imgd.aeplcdn.com/1920x1080/n/cw/ec/55215/defender-exterior-right-front-three-quarter-3.jpeg?isig=0&q=80&q=80",
                height: 300,
                width: 150),
            const Padding(padding: EdgeInsets.only(right: 20)),
            Image.network(
                "https://imgd.aeplcdn.com/1920x1080/n/cw/ec/55215/defender-exterior-right-front-three-quarter-3.jpeg?isig=0&q=80&q=80",
                height: 300,
                width: 150),
            const Padding(padding: EdgeInsets.only(right: 20)),
            Image.network(
                "https://imgd.aeplcdn.com/1920x1080/n/cw/ec/55215/defender-exterior-right-front-three-quarter-3.jpeg?isig=0&q=80&q=80",
                height: 300,
                width: 150),
            const Padding(padding: EdgeInsets.only(right: 20)),
            Image.network(
                "https://imgd.aeplcdn.com/1920x1080/n/cw/ec/55215/defender-exterior-right-front-three-quarter-3.jpeg?isig=0&q=80&q=80",
                height: 300,
                width: 150),
                
            
          ],
        ),
      ),
    );
  }
}

