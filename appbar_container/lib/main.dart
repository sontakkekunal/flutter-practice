import 'package:appbar_container/assignment1.dart';
import 'package:appbar_container/assignment2.dart';
import 'package:appbar_container/assignment3.dart';
import 'package:appbar_container/assignment4.dart';
import 'package:appbar_container/assignment5.dart';
import 'package:appbar_container/assignment6.dart';
import 'package:appbar_container/assignment7.dart';
import 'package:flutter/material.dart';

import 'assignment10.dart';
import 'assignment8.dart';
import 'assignment9.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Assignment10(),
      //home:Assigment9(),
      //home: Assignment8(),
      //home: Assignment7(),
      //home:Assignment6(),
      //home:Assignment5(),
      //home:Assignment4(),
      //home: Assignment3(),
      //home: Assignment2(),
      //home: Assignment1(),
      debugShowCheckedModeBanner: false,
    );
  }
}
