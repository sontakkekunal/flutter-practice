import 'package:flutter/material.dart';

class Assignment5 extends StatelessWidget {
  const Assignment5({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("app_Name"),
      ),
      body:SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: [
            Image.asset("images/img1.jpg",
            height: 150,
            width: 150,
            ),
            const SizedBox(
              width: 20,
            ),
            Image.asset("images/img2.jpg",
            height: 150,
            width: 150,
            ),
            const SizedBox(
              width: 20,
            ),
            Image.asset("images/img3.jpg",
            height: 150,
            width: 150,
            ),
          ],
        ),
      )
    );
  }
}
