import 'package:flutter/material.dart';

class Assignment8 extends StatelessWidget {
  const Assignment8({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("App_Name"),
        ),
        body: Center(
          child: Container(
            height: 300,
            width: 300,
            color: Colors.red,
            //padding: const EdgeInsets.all(20),
            child: Container(
              alignment: Alignment.center,
              margin: const EdgeInsets.all(20),
              color: Colors.lime,
              height: double.infinity,
              width: double.infinity,
            ),
          ),
        ));
  }
}
