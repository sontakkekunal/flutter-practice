import 'package:flutter/material.dart';

class Assignment1 extends StatelessWidget {
  const Assignment1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Appbar_Name"),
        actions: const [
          Icon(Icons.search),
          SizedBox(
            width: 20,
          ),
          Icon(Icons.menu_book_rounded),
          SizedBox(
            width: 20,
          )
        ],
        backgroundColor: Colors.deepOrange,
      ),
    );
  }
}
