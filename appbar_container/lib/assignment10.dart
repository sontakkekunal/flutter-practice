import 'package:flutter/material.dart';

class Assignment10 extends StatelessWidget {
  const Assignment10({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("App_Name"),),
      body: Center(
        child: Container(
          height: 300,
          width: 300,
          decoration: const BoxDecoration(color: Colors.red,borderRadius: BorderRadius.only(topLeft:Radius.circular(20) ,bottomRight:Radius.circular(20))),
        ),
      ),
    );
  }
}
