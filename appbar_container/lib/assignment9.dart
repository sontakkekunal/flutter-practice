import 'package:flutter/material.dart';

class Assigment9 extends StatelessWidget {
  const Assigment9({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("App_name"),
      ),
      body: Center(
          child: Container(
        alignment: Alignment.center,
        height: 320,
        width: 320,
        decoration: BoxDecoration(
            color: Colors.red, borderRadius: BorderRadius.circular(20)),
        child: Container(
          alignment: Alignment.center,
          height: 300,
          width: 300,
          //color: Color.fromARGB(255, 83, 210, 74),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: Colors.greenAccent),
          margin: const EdgeInsets.all(20),
        ),
      )),
    );
  }
}
