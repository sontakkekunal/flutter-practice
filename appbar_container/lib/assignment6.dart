import 'package:flutter/material.dart';

class Assignment6 extends StatelessWidget {
  const Assignment6({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("App_Name"),
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
           children: [
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            width: 300,
            height: 300,
            color: Colors.purple,
          ),
        ]),
      ),
    );
  }
}
