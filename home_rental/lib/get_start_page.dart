import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:home_rental/home_page.dart';
import 'package:page_transition/page_transition.dart';

class GetStart extends StatefulWidget {
  const GetStart({super.key});

  @override
  State<StatefulWidget> createState() {
    return _GetStartState();
  }
}

class _GetStartState extends State {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Column(
        children: [
          Animate(
            effects: const [
              SlideEffect(
                  curve: Curves.easeInOut,
                  end: Offset(0, 0),
                  begin: Offset(0, 9),
                  duration: Duration(
                    milliseconds: 500,
                  )),
              FadeEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(
                    milliseconds: 600,
                  ))
            ],
            child: Image.asset(
              'images/homeImg.png',
              width: double.infinity,
              height: height * 0.74,
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: height * 0.025),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('Lets find your Paradise',
                          style: GoogleFonts.poppins(
                              fontSize: 22, fontWeight: FontWeight.w600))
                      .animate(
                    effects: const [
                      SlideEffect(
                          curve: Curves.easeInOut,
                          end: Offset(0, 0),
                          begin: Offset(-9, 0),
                          duration: Duration(
                            milliseconds: 700,
                          )),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 800,
                          ))
                    ],
                  ),
                  SizedBox(
                    height: height * 0.05,
                    width: width * 0.5,
                    child: Text(
                            'Find your perfect dream space with just a few clicks',
                            textAlign: TextAlign.center,
                            style: GoogleFonts.poppins(
                                fontSize: 15,
                                fontWeight: FontWeight.w600,
                                color: const Color.fromRGBO(101, 101, 101, 1)))
                        .animate(
                      effects: const [
                        SlideEffect(
                            curve: Curves.easeInOut,
                            end: Offset(0, 0),
                            begin: Offset(9, 0),
                            duration: Duration(
                              milliseconds: 900,
                            )),
                        FadeEffect(
                            curve: Curves.easeInOut,
                            duration: Duration(
                              milliseconds: 1000,
                            ))
                      ],
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(PageTransition(
                          child: const HomePage(),
                          curve: Curves.easeInOut,
                          duration: const Duration(milliseconds: 210),
                          type: PageTransitionType.bottomToTop));
                    },
                    child: Container(
                      height: height * 0.06,
                      width: width * 0.48,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(40),
                          color: const Color.fromRGBO(32, 169, 247, 1)),
                      alignment: Alignment.center,
                      child: Text('Get Started',
                          style: GoogleFonts.poppins(
                              fontSize: 22,
                              fontWeight: FontWeight.w400,
                              color: Colors.white)),
                    ).animate(
                      effects: const [
                        SlideEffect(
                            curve: Curves.easeInOut,
                            end: Offset(0, 0),
                            begin: Offset(-9, 0),
                            duration: Duration(
                              milliseconds: 1000,
                            )),
                        FadeEffect(
                            curve: Curves.easeInOut,
                            duration: Duration(
                              milliseconds: 1100,
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
