import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:home_rental/detail_page.dart';
import 'package:home_rental/villa_model.dart';
import 'package:page_transition/page_transition.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

class _HomePageState extends State {
  bool animationOver = false;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      setState(() {});
    });
  }

  List<Villa> popVillaList = [
    Villa(
        imgPath: 'images/nightHillVilla.png',
        name: 'Night Hill Villa',
        location: 'London,Night Hill',
        rent: 5900,
        rating: 4.9,
        bathroomCount: 6,
        bedroomCount: 5,
        area: 7000,
        description:
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet"),
    Villa(
        imgPath: 'images/nightVilla.png',
        name: 'Night Villa',
        location: 'London,New Park',
        rent: 4900,
        rating: 4.2,
        bathroomCount: 6,
        bedroomCount: 5,
        area: 6000,
        description:
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet")
  ];
  List<Villa> nearbyVillaList = [
    Villa(
        imgPath: 'images/jumeriah.png',
        name: 'Jumeriah Golf Estates Villa',
        location: 'London,Area Plam Jumeriah',
        rent: 4900,
        rating: 4.2,
        bathroomCount: 5,
        bedroomCount: 4,
        area: 6000,
        description:
            "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet. Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit. Exercitation veniam consequat sunt nostrud amet")
  ];
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: const Color.fromRGBO(237, 237, 237, 1),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 22, top: 50, right: 15),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    'Hey Dravid,',
                    style: GoogleFonts.poppins(
                        fontSize: 14,
                        fontWeight: FontWeight.w500,
                        color: const Color.fromRGBO(101, 101, 101, 1)),
                  ).animate(
                    effects: const [
                      SlideEffect(
                          curve: Curves.easeInOut,
                          end: Offset(0, 0),
                          begin: Offset(-9, 0),
                          duration: Duration(
                            milliseconds: 600,
                          )),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 700,
                          ))
                    ],
                  ),
                  const Spacer(),
                  Image.asset('images/profile.png').animate(
                    effects: const [
                      RotateEffect(
                          curve: Curves.easeInOut,
                          begin: 1.2,
                          end: 0,
                          duration: Duration(milliseconds: 600)),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 700,
                          ))
                    ],
                  )
                ],
              ),
              SizedBox(
                height: height * 0.01,
              ),
              SizedBox(
                height: height * 0.07,
                width: width * 0.6,
                child: Text(
                  'Let’s find your best residence ',
                  style: GoogleFonts.poppins(
                    fontSize: 20,
                    fontWeight: FontWeight.w500,
                  ),
                ).animate(
                  effects: const [
                    SlideEffect(
                        curve: Curves.easeInOut,
                        end: Offset(0, 0),
                        begin: Offset(-9, 0),
                        duration: Duration(
                          milliseconds: 600,
                        )),
                    FadeEffect(
                        curve: Curves.easeInOut,
                        duration: Duration(
                          milliseconds: 700,
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              TextFormField(
                decoration: InputDecoration(
                    filled: true,
                    contentPadding: const EdgeInsets.only(left: 5, top: 30),
                    prefixIcon: const Icon(Icons.search),
                    hintStyle: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500, fontSize: 16),
                    hintText: 'Search your favourite paradise',
                    fillColor: const Color.fromRGBO(255, 255, 255, 1),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(15),
                        borderSide: BorderSide.none)),
              ).animate(
                effects: const [
                  SlideEffect(
                      curve: Curves.easeInOut,
                      end: Offset(0, 0),
                      begin: Offset(9, 0),
                      duration: Duration(
                        milliseconds: 600,
                      )),
                  FadeEffect(
                      curve: Curves.easeInOut,
                      duration: Duration(
                        milliseconds: 700,
                      ))
                ],
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Row(
                children: [
                  Text(
                    'Most popular',
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600, fontSize: 22),
                  ).animate(
                    effects: const [
                      SlideEffect(
                          curve: Curves.easeInOut,
                          end: Offset(0, 0),
                          begin: Offset(-9, 0),
                          duration: Duration(
                            milliseconds: 600,
                          )),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 700,
                          ))
                    ],
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'See all',
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: const Color.fromRGBO(32, 169, 247, 1)),
                    ),
                  ).animate(
                    effects: const [
                      SlideEffect(
                          curve: Curves.easeInOut,
                          end: Offset(0, 0),
                          begin: Offset(9, 0),
                          duration: Duration(
                            milliseconds: 600,
                          )),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 700,
                          ))
                    ],
                  )
                ],
              ),
              SizedBox(
                height: height * 0.01,
              ),
              SizedBox(
                width: double.infinity,
                height: height * 0.335,
                child: ListView.separated(
                  separatorBuilder: (context, i) => SizedBox(
                    width: width * 0.05,
                  ),
                  itemCount: popVillaList.length,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(PageTransition(
                            child: DetailPage(villaObj: popVillaList[index]),
                            type: PageTransitionType.fade,
                            duration: const Duration(milliseconds: 130),
                            curve: Curves.easeInOut));
                      },
                      child: AnimatedOpacity(
                        opacity: animationOver ? 1 : 0,
                        duration: Duration(milliseconds: 750 + (index * 170)),
                        child: AnimatedContainer(
                          duration: Duration(milliseconds: 325 + (index * 150)),
                          transform: Matrix4.translationValues(
                              animationOver ? 0 : 300, 0, 0),
                          height: height * 0.3,
                          width: width * 0.52,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15)),
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Hero(
                                tag: popVillaList[index].imgPath,
                                child: Container(
                                  height: height * 0.21,
                                  width: width * 0.5,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                          image: AssetImage(
                                              popVillaList[index].imgPath),
                                          fit: BoxFit.cover)),
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: height * 0.025,
                                    width: width * 0.1,
                                    padding: const EdgeInsets.all(2),
                                    margin: const EdgeInsets.only(
                                        top: 10, right: 10),
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(15),
                                        color: const Color.fromRGBO(
                                            32, 169, 247, 1)),
                                    alignment: Alignment.center,
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.star_rounded,
                                          size: 13.65,
                                          color:
                                              Color.fromRGBO(251, 227, 12, 1),
                                        ),
                                        Text(
                                          "${popVillaList[index].rating}",
                                          style: GoogleFonts.poppins(
                                              fontWeight: FontWeight.w500,
                                              fontSize: 12,
                                              color: Colors.white),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                              Hero(
                                tag: popVillaList[index].name,
                                child: Text(
                                  popVillaList[index].name,
                                  style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 16,
                                  ),
                                ),
                              ),
                              Hero(
                                tag: popVillaList[index].location,
                                child: Text(
                                  popVillaList[index].location,
                                  style: GoogleFonts.poppins(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 12,
                                      color:
                                          const Color.fromRGBO(72, 72, 72, 1)),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Hero(
                                    tag: popVillaList[index].hashCode,
                                    child: Text(
                                      "\$${popVillaList[index].rent}",
                                      style: GoogleFonts.poppins(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w600,
                                          color: const Color.fromRGBO(
                                              32, 169, 247, 1)),
                                    ),
                                  ),
                                  Text(
                                    ' /Month',
                                    style: GoogleFonts.poppins(
                                        fontSize: 12,
                                        fontWeight: FontWeight.w500,
                                        color: const Color.fromRGBO(
                                            72, 72, 72, 1)),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 3,
                              )
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                ),
              ),
              SizedBox(
                height: height * 0.01,
              ),
              Row(
                children: [
                  Text(
                    'Nearby your location',
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600, fontSize: 22),
                  ).animate(
                    effects: const [
                      SlideEffect(
                          curve: Curves.easeInOut,
                          end: Offset(0, 0),
                          begin: Offset(-9, 0),
                          duration: Duration(
                            milliseconds: 600,
                          )),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 700,
                          ))
                    ],
                  ),
                  const Spacer(),
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'More',
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500,
                          fontSize: 16,
                          color: const Color.fromRGBO(32, 169, 247, 1)),
                    ),
                  ).animate(
                    effects: const [
                      SlideEffect(
                          curve: Curves.easeInOut,
                          end: Offset(0, 0),
                          begin: Offset(9, 0),
                          duration: Duration(
                            milliseconds: 600,
                          )),
                      FadeEffect(
                          curve: Curves.easeInOut,
                          duration: Duration(
                            milliseconds: 700,
                          ))
                    ],
                  )
                ],
              ),
              SizedBox(
                height: height * 0.01,
              ),
              SizedBox(
                  height: height * 0.2,
                  width: double.infinity,
                  child: ListView.separated(
                    padding: EdgeInsets.zero,
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: height * 0.05,
                      );
                    },
                    itemCount: nearbyVillaList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                DetailPage(villaObj: nearbyVillaList[index]),
                          ));
                        },
                        child: AnimatedOpacity(
                          opacity: animationOver ? 1 : 0,
                          duration: Duration(milliseconds: 750 + (index * 170)),
                          child: AnimatedContainer(
                            transform: Matrix4.translationValues(
                                0, animationOver ? 0 : height, 0),
                            duration:
                                Duration(milliseconds: 360 + (index * 150)),
                            height: height * 0.135,
                            width: width,
                            padding: const EdgeInsets.all(10),
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(20)),
                            child: Row(
                              children: [
                                ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Hero(
                                      tag: nearbyVillaList[index].imgPath,
                                      child: Image.asset(
                                        nearbyVillaList[index].imgPath,
                                        height: height * 0.12,
                                        width: width * 0.22,
                                        fit: BoxFit.cover,
                                      ),
                                    )),
                                SizedBox(
                                  width: width * 0.04,
                                ),
                                Container(
                                  width: width * 0.6,
                                  padding:
                                      const EdgeInsets.only(top: 5, bottom: 2),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          const Icon(
                                            Icons.location_on_sharp,
                                            color:
                                                Color.fromRGBO(90, 90, 90, 1),
                                            size: 20,
                                          ),
                                          Hero(
                                            tag:
                                                nearbyVillaList[index].location,
                                            child: Text(
                                              nearbyVillaList[index].location,
                                              style: GoogleFonts.poppins(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 11,
                                                  color: const Color.fromRGBO(
                                                      90, 90, 90, 1)),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          const Icon(
                                            Icons.chair_rounded,
                                            color:
                                                Color.fromRGBO(90, 90, 90, 1),
                                            size: 20,
                                          ),
                                          Text(
                                            "  ${nearbyVillaList[index].bedroomCount} Bedrooms  ",
                                            style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 11,
                                                color: const Color.fromRGBO(
                                                    90, 90, 90, 1)),
                                          ),
                                          const Icon(
                                            Icons.bathtub_sharp,
                                            color:
                                                Color.fromRGBO(90, 90, 90, 1),
                                            size: 20,
                                          ),
                                          Text(
                                            "  ${nearbyVillaList[index].bathroomCount} Bedrooms",
                                            style: GoogleFonts.poppins(
                                                fontWeight: FontWeight.w600,
                                                fontSize: 11,
                                                color: const Color.fromRGBO(
                                                    90, 90, 90, 1)),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        children: [
                                          Hero(
                                            tag:
                                                nearbyVillaList[index].hashCode,
                                            child: Text(
                                              "\$${nearbyVillaList[index].rent}",
                                              style: GoogleFonts.poppins(
                                                  fontSize: 14,
                                                  fontWeight: FontWeight.w600,
                                                  color: const Color.fromRGBO(
                                                      32, 169, 247, 1)),
                                            ),
                                          ),
                                          Text(
                                            ' /Month',
                                            style: GoogleFonts.poppins(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                                color: const Color.fromRGBO(
                                                    72, 72, 72, 1)),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  ))
            ],
          ),
        ),
      ),
    );
  }
}
