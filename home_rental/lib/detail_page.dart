import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:home_rental/villa_model.dart';
import 'package:readmore/readmore.dart';

class DetailPage extends StatefulWidget {
  Villa villaObj;
  DetailPage({super.key, required this.villaObj});

  @override
  State<StatefulWidget> createState() {
    return _DetailPageState();
  }
}

class _DetailPageState extends State<DetailPage> {
  bool animationOver = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    List<ShortVillaInfo> infoList = [
      ShortVillaInfo(
          icon: const Icon(
            Icons.chair_rounded,
            color: Color.fromRGBO(90, 90, 90, 1),
            size: 35,
          ),
          name: Text(
            "Bedrooms",
            style: GoogleFonts.poppins(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: const Color.fromRGBO(90, 90, 90, 1)),
          ),
          area: Text(
            '5',
            style:
                GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w600),
          )),
      ShortVillaInfo(
          icon: const Icon(
            Icons.bathtub_sharp,
            color: Color.fromRGBO(90, 90, 90, 1),
            size: 35,
          ),
          name: Text(
            "Bathrooms",
            style: GoogleFonts.poppins(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: const Color.fromRGBO(90, 90, 90, 1)),
          ),
          area: Text(
            '6',
            style:
                GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w600),
          )),
      ShortVillaInfo(
          icon: const Icon(
            Icons.aspect_ratio_sharp,
            color: Color.fromRGBO(90, 90, 90, 1),
            size: 35,
          ),
          name: Text(
            "Square ft",
            style: GoogleFonts.poppins(
                fontWeight: FontWeight.w600,
                fontSize: 14,
                color: const Color.fromRGBO(90, 90, 90, 1)),
          ),
          area: Text(
            '7,000 sq ft',
            style:
                GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w600),
          )),
    ];
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: const Color.fromRGBO(237, 237, 237, 1),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        centerTitle: true,
        title: Text(
          'Details',
          style: GoogleFonts.poppins(fontSize: 20, fontWeight: FontWeight.w500),
        ).animate(
          effects: const [
            SlideEffect(
                curve: Curves.easeInOut,
                end: Offset(0, 0),
                begin: Offset(9, 0),
                duration: Duration(
                  milliseconds: 500,
                )),
            FadeEffect(
                curve: Curves.easeInOut,
                duration: Duration(
                  milliseconds: 550,
                ))
          ],
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 22),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Hero(
              tag: widget.villaObj.imgPath,
              child: Container(
                  height: 244,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      image: DecorationImage(
                          image: AssetImage(widget.villaObj.imgPath),
                          fit: BoxFit.cover)),
                  alignment: Alignment.topLeft,
                  child: Container(
                    height: height * 0.03,
                    width: width * 0.13,
                    padding: const EdgeInsets.all(4),
                    margin: const EdgeInsets.only(top: 10, left: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: const Color.fromRGBO(32, 169, 247, 1)),
                    alignment: Alignment.center,
                    child: Row(
                      children: [
                        const Icon(
                          Icons.star_rounded,
                          size: 21.65,
                          color: Color.fromRGBO(251, 227, 12, 1),
                        ),
                        Text(
                          "${widget.villaObj.rating}",
                          style: GoogleFonts.poppins(
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                              color: Colors.white),
                        )
                      ],
                    ),
                  )),
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Hero(
                      tag: widget.villaObj.name,
                      child: SizedBox(
                        height: height * 0.035,
                        width: width * 0.6,
                        child: ReadMoreText(
                          widget.villaObj.name,
                          trimMode: TrimMode.Line,
                          trimLines: 2,
                          style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w600,
                            fontSize: 22,
                          ),
                        ),
                      ),
                    ),
                    Hero(
                      tag: widget.villaObj.location,
                      child: Text(
                        widget.villaObj.location,
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: const Color.fromRGBO(72, 72, 72, 1)),
                      ),
                    ),
                  ],
                ),
                const Spacer(),
                Hero(
                  tag: widget.villaObj.hashCode,
                  child: Text(
                    "\$${widget.villaObj.rent}",
                    style: GoogleFonts.poppins(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                        color: const Color.fromRGBO(32, 169, 247, 1)),
                  ),
                ),
                Text(
                  ' /Month',
                  style: GoogleFonts.poppins(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                      color: const Color.fromRGBO(72, 72, 72, 1)),
                ).animate(
                  effects: const [
                    SlideEffect(
                        curve: Curves.easeInOut,
                        end: Offset(0, 0),
                        begin: Offset(9, 0),
                        duration: Duration(
                          milliseconds: 500,
                        )),
                    FadeEffect(
                        curve: Curves.easeInOut,
                        duration: Duration(
                          milliseconds: 550,
                        ))
                  ],
                )
              ],
            ),
            SizedBox(
              height: height * 0.16,
              width: double.infinity,
              child: ListView.separated(
                separatorBuilder: (context, index) => const SizedBox(
                  width: 20,
                ),
                scrollDirection: Axis.horizontal,
                itemCount: infoList.length,
                itemBuilder: (context, index) {
                  return AnimatedOpacity(
                    opacity: animationOver ? 1 : 0,
                    duration: Duration(milliseconds: 450 + (index * 150)),
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 400 + (index * 130)),
                      transform: Matrix4.translationValues(
                          0, animationOver ? 0 : width, 0),
                      height: height * 0.16,
                      width: width * 0.28,
                      padding: const EdgeInsets.symmetric(
                          vertical: 15, horizontal: 12),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          infoList[index].icon,
                          infoList[index].name,
                          infoList[index].area
                        ],
                      ),
                    ),
                  );
                },
              ),
            ),
            Text(
              widget.villaObj.description,
              style: GoogleFonts.poppins(
                  fontSize: 17.4,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(0, 0, 0, 1)),
            ).animate(
              effects: const [
                SlideEffect(
                    curve: Curves.easeInOut,
                    end: Offset(0, 0),
                    begin: Offset(-9, 0),
                    duration: Duration(
                      milliseconds: 500,
                    )),
                FadeEffect(
                    curve: Curves.easeInOut,
                    duration: Duration(
                      milliseconds: 550,
                    ))
              ],
            ),
            AnimatedOpacity(
              duration: const Duration(milliseconds: 550),
              opacity: animationOver ? 1 : 0,
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 500),
                transform:
                    Matrix4.translationValues(0, animationOver ? 0 : height, 0),
                height: height * 0.07,
                width: width * 0.5,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(40),
                    color: const Color.fromRGBO(32, 169, 247, 1)),
                alignment: Alignment.center,
                child: Text(
                  'Rent Now',
                  style: GoogleFonts.poppins(
                      fontSize: 22,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(255, 255, 255, 1)),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
