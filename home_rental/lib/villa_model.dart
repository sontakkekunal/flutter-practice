import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Villa {
  String imgPath;
  String name;
  String location;
  int rent;
  double rating;
  int bedroomCount;
  int bathroomCount;
  int area;
  String description;
  Villa(
      {required this.imgPath,
      required this.name,
      required this.location,
      required this.rent,
      required this.rating,
      required this.bathroomCount,
      required this.bedroomCount,
      required this.area,
      required this.description});
}

class ShortVillaInfo {
  Icon icon;
  Text name;
  Text area;
  ShortVillaInfo({required this.icon,required this.name,required this.area});
}
