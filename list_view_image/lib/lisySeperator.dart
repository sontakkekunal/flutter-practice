import 'package:flutter/material.dart';

class CricketMatch1 extends StatelessWidget {
  const CricketMatch1({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CricketMatch(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class CricketMatch extends StatefulWidget {
  const CricketMatch({super.key});
  @override
  State createState() => _CricketMatchState();
}

class _CricketMatchState extends State<CricketMatch> {
  List<Map<String, List<Map>>> cricket = [
    {
      "T20": [
        {
          "player": "Virat",
          "link":
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"
        },
        {
          "player": "Rohit",
          "link":
              "https://wallpapers.com/images/hd/rohit-sharma-cricket-hitman-si5a3eqvyd6s0re4.jpg"
        },
        {
          "player": "KL Rahul",
          "link": "https://images.indianexpress.com/2023/09/Rahul-15.jpg"
        }
      ],
    },
    {
      "Test": [
        {
          "player": "Virat",
          "link":
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"
        },
        {
          "player": "Virat",
          "link":
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"
        },
        {
          "player": "Virat",
          "link":
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"
        }
      ],
    },
    {
      "WorldCup": [
        {
          "player": "Rohit Sharma",
          "link":
              "https://assets.gqindia.com/photos/6211d04a0685056c20f85d00/master/pass/Rohit%20Sharma.jpg"
        },
        {
          "player": "Shami",
          "link":
              "https://c.ndtvimg.com/2023-11/3hokis78_mohammad-shami-_625x300_16_November_23.jpeg"
        },
        {
          "player": "Bumrah",
          "link":
              "https://assets.telegraphindia.com/telegraph/2023/Aug/1692451517_1690866366_1690818985_jasprit-bumrah.jpg"
        }
      ],
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 87, 163, 226),
          title: const Text("Cricket player"),
          centerTitle: true,
        ),
        body: SizedBox(
            height: double.infinity,
            width: double.infinity,
            child: ListView.separated(
              itemCount: cricket.length,
              padding: const EdgeInsets.all(10),
              scrollDirection: Axis.vertical,
              separatorBuilder: (BuildContext context, int index) {
                return const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("-x-x-x-x-x-x-x--x-x-x-x-x-x--x-x-x-x-"),
                    SizedBox(
                      height: 80,
                    )
                  ],
                );
              },
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 40,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.red,
                              style: BorderStyle.solid,
                              width: 1.5),
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.amber),
                      child: Center(
                        child: Text(
                          cricket[index].entries.first.key,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 360,
                      width: double.infinity,
                      child: ListView.separated(
                        itemCount: cricket[index].entries.first.value.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (BuildContext context, int index2) {
                          return const SizedBox(
                            width: 10,
                          );
                        },
                        itemBuilder: (BuildContext context, int index3) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  height: 40,
                                  width: 300,
                                  margin: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.purple,
                                        style: BorderStyle.solid,
                                        width: 1.5),
                                    borderRadius: BorderRadius.circular(20),
                                    color: Color.fromARGB(255, 241, 212, 127),
                                  ),
                                  child: Center(
                                    child: Text(
                                        "${cricket[index].entries.first.value[index3]["player"]}",
                                        style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700)),
                                  )),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.network(
                                  "${cricket[index].entries.first.value[index3]["link"]}",
                                  height: 300,
                                  width: 300,
                                  fit: BoxFit.cover,
                                ),
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            )));
  }
}
