import 'package:flutter/material.dart';

class ListViewNum extends StatefulWidget {
  const ListViewNum({super.key});

  @override
  State createState() => _ListViewNumState();
}

class _ListViewNumState extends State<ListViewNum> {
  int count = 0;
  int val = 0;
  List<int> numList = [];
  @override
  Widget build(BuildContext contexty) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
              title: const Text("ListViewNum"),
              centerTitle: true,
              backgroundColor: Colors.purple),
          body: ListView.builder(
              itemCount: numList.length,
              itemBuilder: (context, index) {
                return Container(
                  height: 100,
                  width: 100,
                  padding: const EdgeInsets.all(15),
                  margin: const EdgeInsets.all(20),
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                      color: Color.fromARGB(255, 225, 220, 166)),
                  child: Text(
                    "${numList[index]}",
                    style: const TextStyle(
                        fontSize: 25, fontWeight: FontWeight.w500),
                  ),
                );
              }),
          floatingActionButton: FloatingActionButton(
              onPressed: () {
                setState(() {
                  numList.add(++count);
                });
              },
              child: const Icon(Icons.arrow_forward_rounded))),
    );
  }
}
