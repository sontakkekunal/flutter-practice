import 'package:flutter/material.dart';

class ListViewImage extends StatefulWidget {
  const ListViewImage({super.key});

  @override
  State createState() => _ListViewImageState();
}

class _ListViewImageState extends State<ListViewImage> {
  List<String> image = [
    "https://cdni.autocarindia.com/Utils/ImageResizer.ashx?n=https://cdni.autocarindia.com/ExtraImages/20210602025723_Mercedes_Maybach_GLS_600.jpg",
    "https://imgd.aeplcdn.com/1280x720/n/cw/ec/43658/mercedes-benz--exterior-0.jpeg",
    "https://cdni.autocarindia.com/Utils/ImageResizer.ashx?n=https://cdni.autocarindia.com/ExtraImages/20210602025723_Mercedes_Maybach_GLS_600.jpg"
  ];
  @override
  Widget build(BuildContext contexty) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          appBar: AppBar(
              title: const Text("ListViewImage App"),
              centerTitle: true,
              backgroundColor: Color.fromARGB(255, 200, 121, 214)),
          body: ListView.builder(
            itemCount: image.length,
            itemBuilder: (context, index) {
              return Container(
                  margin: const EdgeInsets.all(8),
                  color: Color.fromARGB(255, 225, 151, 146),
                  height: 300,
                  width: 100,
                  child: Image.network(image[index]));
            },
          ),
        ));
  }
}
