import 'package:flutter/material.dart';

class CricketMatch2 extends StatelessWidget {
  const CricketMatch2({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: CricketMatch(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class CricketMatch extends StatefulWidget {
  const CricketMatch({super.key});
  @override
  State createState() => _CricketMatchState();
}

class MatchModel {
  final String matchName;
  final List<PlayerProfile> playersProfileList;

  const MatchModel({required this.matchName, required this.playersProfileList});
}

class PlayerProfile {
  final String playerName;
  final String playerPhotoUrl;
  const PlayerProfile({required this.playerName, required this.playerPhotoUrl});
}

class _CricketMatchState extends State<CricketMatch> {
  List<MatchModel> cricket = [
    const MatchModel(matchName: "T20", playersProfileList: [
      PlayerProfile(
          playerName: "Virat",
          playerPhotoUrl:
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"),
      PlayerProfile(
          playerName: "Rohit",
          playerPhotoUrl:
              "https://wallpapers.com/images/hd/rohit-sharma-cricket-hitman-si5a3eqvyd6s0re4.jpg"),
      PlayerProfile(
          playerName: "KL Rahul",
          playerPhotoUrl:
              "https://images.indianexpress.com/2023/09/Rahul-15.jpg")
    ]),
    const MatchModel(matchName: "Text", playersProfileList: [
      PlayerProfile(
          playerName: "Virat",
          playerPhotoUrl:
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"),
      PlayerProfile(
          playerName: "Rohit",
          playerPhotoUrl:
              "https://wallpapers.com/images/hd/rohit-sharma-cricket-hitman-si5a3eqvyd6s0re4.jpg"),
      PlayerProfile(
          playerName: "KL Rahul",
          playerPhotoUrl:
              "https://images.indianexpress.com/2023/09/Rahul-15.jpg")
    ]),
    const MatchModel(matchName: "Text", playersProfileList: [
      PlayerProfile(
          playerName: "Virat",
          playerPhotoUrl:
              "https://img.etimg.com/thumb/width-1600,height-900,imgsize-47368,resizemode-75,msid-104999679/news/sports/when-virat-kohli-battled-depression-his-coach-recalls-the-toughest-phase-of-the-cricketers-career.jpg"),
      PlayerProfile(
          playerName: "Rohit",
          playerPhotoUrl:
              "https://wallpapers.com/images/hd/rohit-sharma-cricket-hitman-si5a3eqvyd6s0re4.jpg"),
      PlayerProfile(
          playerName: "KL Rahul",
          playerPhotoUrl:
              "https://images.indianexpress.com/2023/09/Rahul-15.jpg")
    ])
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 87, 163, 226),
          title: const Text("Cricket player"),
          centerTitle: true,
        ),
        body: SizedBox(
            height: double.infinity,
            width: double.infinity,
            child: ListView.separated(
              itemCount: cricket.length,
              padding: const EdgeInsets.all(10),
              scrollDirection: Axis.vertical,
              separatorBuilder: (BuildContext context, int index) {
                return const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("-x-x-x-x-x-x-x--x-x-x-x-x-x--x-x-x-x-"),
                    SizedBox(
                      height: 80,
                    )
                  ],
                );
              },
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 40,
                      width: double.infinity,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Colors.red,
                              style: BorderStyle.solid,
                              width: 1.5),
                          borderRadius: BorderRadius.circular(20),
                          color: Colors.amber),
                      child: Center(
                        child: Text(
                          cricket[index].matchName,
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 360,
                      width: double.infinity,
                      child: ListView.separated(
                        itemCount: cricket[index].playersProfileList.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (BuildContext context, int index2) {
                          return const SizedBox(
                            width: 10,
                          );
                        },
                        itemBuilder: (BuildContext context, int index3) {
                          return Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Container(
                                  height: 40,
                                  width: 300,
                                  margin: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Colors.purple,
                                        style: BorderStyle.solid,
                                        width: 1.5),
                                    borderRadius: BorderRadius.circular(20),
                                    color: Color.fromARGB(255, 241, 212, 127),
                                  ),
                                  child: Center(
                                    child: Text(
                                        cricket[index]
                                            .playersProfileList[index3]
                                            .playerName,
                                        style: const TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w700)),
                                  )),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(20),
                                child: Image.network(
                                  cricket[index]
                                      .playersProfileList[index3]
                                      .playerPhotoUrl,
                                  height: 300,
                                  width: 300,
                                  fit: BoxFit.cover,
                                ),
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                );
              },
            )));
  }
}
