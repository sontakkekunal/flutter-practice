import 'package:flutter/material.dart';

import 'padding.dart';

void main() {
  runApp(PaddingApp());
}

class PaddingApp extends StatelessWidget {
  const PaddingApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home:PaddingAssignment()
    );
  }
}
