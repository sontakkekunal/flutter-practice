import 'package:flutter/material.dart';

class PaddingAssignment extends StatelessWidget {
  const PaddingAssignment({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Padding_App"),
      ),
      body: Center(
        child: Container(
          color: Colors.brown,
          child: Container(
            color: Colors.lightBlueAccent,
            height: 250,
            width: 250,
            padding: const EdgeInsets.all(20),
            margin: const EdgeInsets.all(20),
            child: Image.network("https://static-cse.canva.com/blob/825910/ComposeStunningImages6.jpg"),
          ),
        ),
      ),
    );
  }
}
//"https://static-cse.canva.com/blob/825910/ComposeStunningImages6.jpg",
            