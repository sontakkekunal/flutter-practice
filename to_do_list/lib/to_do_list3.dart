import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ToDoListApp extends StatefulWidget {
  const ToDoListApp({super.key});

  @override
  State createState() => _ToDoListAppState();
}

class Task {
  String title;
  String description;
  String date;
  int imgIndex;
  Task(
      {required this.title,
      required this.description,
      required this.date,
      required this.imgIndex});
}

class _ToDoListAppState extends State {
  List<Task> taskList = [
    Task(
        title: "Lorem Ipsum is simply setting industry. ",
        description:
            "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
        date: "10 July 2023",
        imgIndex: 0),
  ];
  List<Color> colorList = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];
  int submitCount = 0;
  List<Map> inputTask = [
    {
      "taskName": "Title",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Description",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Date",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Networl Image URl(Optional)",
      "controller": TextEditingController(),
    }
  ];
  List<String> imgList = [
    "images/profile.png",
    "images/task1.webp",
    "images/task2.jpg",
    "images/task3.webp",
    "images/task4.png",
    "images/task5.jpg",
  ];
  int selectedInd = 0;
  bool flag1 = false;
  bool flag2 = false;
  bool flag3 = false;
  taskModification(String modifiType,
      {String? title, String? description, String? date, int editIndex = -1}) {
    if (title != null && description != null && date != null) {
      inputTask[0]["controller"].text = title;
      inputTask[1]["controller"].text = description;
      inputTask[2]["controller"].text = date;
    } else {
      for (int i = 0; i < inputTask.length; i++) {
        inputTask[i]["controller"].clear();
      }
      selectedInd = 0;
    }
    flag1 = false;
    flag2 = false;
    flag3 = false;
    submitCount = 0;

    showModalBottomSheet(
        backgroundColor: Colors.white,
        barrierColor: Colors.transparent,
        isScrollControlled: true,
        
        context: context,
        builder: (BuildContext build) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              width: 375,
              decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.1),
                        blurRadius: double.infinity,
                        spreadRadius: 2)
                  ],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(42),
                      topRight: Radius.circular(42))),
              padding: const EdgeInsets.all(10),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      "$modifiType Task ",
                      style: GoogleFonts.quicksand(
                          fontSize: 22, fontWeight: FontWeight.w600),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 275,
                      width: 300,
                      child: ListView.separated(
                        itemCount: inputTask.length,
                        separatorBuilder: (BuildContext context, int index) =>
                            const SizedBox(
                          height: 10,
                        ),
                        itemBuilder: (BuildContext context, int index) {
                          return Column(
                            children: [
                              TextField(
                                controller: inputTask[index]["controller"],
                                maxLength: (index == 0)
                                    ? 50
                                    : (index == 1)
                                        ? 250
                                        : (index == 2)
                                            ? 20
                                            : 200,
                                cursorWidth: 2,
                                cursorHeight: 30,
                                onChanged: (String val) {
                                  setState(() {
                                    val;
                                  });
                                },
                                cursorColor:
                                    const Color.fromARGB(255, 105, 9, 122),
                                cursorRadius: const Radius.circular(50),
                                cursorOpacityAnimates: true,
                                textAlign: TextAlign.center,
                                textAlignVertical: TextAlignVertical.center,
                                readOnly: (index == 2) ? true : false,
                                onTap: (index == 2)
                                    ? () async {
                                        //pick the date from datePicker
                                        DateTime? pickdate =
                                            await showDatePicker(
                                                context: context,
                                                initialDate: DateTime.now(),
                                                firstDate: DateTime.now(),
                                                lastDate: DateTime(2050));

                                        //Format the date into the requierd
                                        //format of the date i.e. year month date
                                        String formatedDate = DateFormat.yMMMd()
                                            .format(pickdate!);

                                        setState(() {
                                          inputTask[index]["controller"].text =
                                              formatedDate;
                                        });
                                      }
                                    : null,
                                autofocus: true,
                                decoration: InputDecoration(
                                  hintText:
                                      "Enter ${inputTask[index]["taskName"]}",
                                  hintStyle:
                                      GoogleFonts.quicksand(fontSize: 14),
                                  labelText: inputTask[index]["taskName"],
                                  labelStyle:
                                      GoogleFonts.quicksand(fontSize: 16),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: (index == 3)
                                              ? (Colors.yellow)
                                              : (submitCount == 0)
                                                  ? Colors.black
                                                  : (inputTask[index]
                                                                  ["controller"]
                                                              .text
                                                              .length >
                                                          0)
                                                      ? Colors.purple
                                                      : Colors.red,
                                          width: 2),
                                      gapPadding: 10,
                                      borderRadius: BorderRadius.circular(20)),
                                  suffixIcon: (index == 2)
                                      ? const Icon(
                                          Icons.calendar_month_outlined,
                                        )
                                      : null,
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: (index == 3)
                                              ? (Colors.yellow)
                                              : (submitCount == 0)
                                                  ? Color.fromARGB(
                                                      255, 6, 204, 235)
                                                  : (inputTask[index]
                                                                  ["controller"]
                                                              .text
                                                              .length >
                                                          0)
                                                      ? Color.fromARGB(
                                                          255, 34, 236, 3)
                                                      : Colors.red,
                                          width: 2),
                                      gapPadding: 3,
                                      borderRadius: BorderRadius.circular(20)),
                                ),
                              ),
                              (submitCount > 0 &&
                                      inputTask[0]["controller"].text.length ==
                                          0 &&
                                      index == 0)
                                  ? Text(
                                      "Please enter valid title",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.red),
                                    )
                                  : const SizedBox(),
                              (submitCount > 0 &&
                                      inputTask[1]["controller"].text.length ==
                                          0 &&
                                      index == 1)
                                  ? Text(
                                      "Please enter valid desciption",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.red),
                                    )
                                  : const SizedBox(),
                              (submitCount > 0 &&
                                      inputTask[2]["controller"].text.length ==
                                          0 &&
                                      index == 2)
                                  ? Text(
                                      "Please enter valid date",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.red),
                                    )
                                  : const SizedBox()
                            ],
                          );
                        },
                      ),
                    ),
                    Text("-------------OR--------------",
                        style: GoogleFonts.quicksand(
                            fontSize: 22, fontWeight: FontWeight.w600)),
                    Text("Choose Img :",
                        style: GoogleFonts.quicksand(
                            fontSize: 22, fontWeight: FontWeight.w600)),
                    const SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 50,
                      width: 300,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: imgList.length,
                        separatorBuilder: (BuildContext context, int indexZ) =>
                            const SizedBox(
                          width: 20,
                        ),
                        itemBuilder: (BuildContext context, int indexY) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                selectedInd = indexY;
                              });
                            },
                            child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: (selectedInd == indexY)
                                      ? Border.all(color: Colors.blue, width: 3)
                                      : null),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(60),
                                  child: Image.asset(
                                    imgList[indexY],
                                    height: 60,
                                    width: 60,
                                  )),
                            ),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(90, 50),
                              foregroundColor: Colors.white,
                              backgroundColor: Color.fromRGBO(0, 139, 148, 1)),
                          onPressed: () {
                            for (int i = 0; i < inputTask.length - 1; i++) {
                              inputTask[i]["controller"].text =
                                  inputTask[i]["controller"].text.trim();
                            }
                            if (inputTask[0]["controller"].text.length > 0 &&
                                inputTask[1]["controller"].text.length > 0 &&
                                inputTask[2]["controller"].text.length > 0) {
                              if (editIndex == -1) {
                                taskList.add(Task(
                                    title: inputTask[0]["controller"].text,
                                    description:
                                        inputTask[1]["controller"].text,
                                    date: inputTask[2]["controller"].text,
                                    imgIndex: selectedInd));
                              } else {
                                taskList[editIndex].title =
                                    inputTask[0]["controller"].text;
                                taskList[editIndex].description =
                                    inputTask[1]["controller"].text;
                                taskList[editIndex].date =
                                    inputTask[2]["controller"].text;
                              }
                              flag1 = false;
                              flag2 = false;
                              flag3 = false;
                              submitCount = 0;
                              Navigator.pop(context);
                            } else {
                              if (inputTask[0]["controller"].text.length <= 0) {
                                flag1 = true;
                              } else {
                                flag1 = false;
                              }

                              if (inputTask[1]["controller"].text.length <= 0) {
                                flag2 = true;
                              } else {
                                flag2 = false;
                              }

                              if (inputTask[2]["controller"].text.length <= 0) {
                                flag3 = true;
                              } else {
                                flag3 = false;
                              }
                            }
                            submitCount++;
                            setState(() {});
                          },
                          child: Text(
                            "Save",
                            style: GoogleFonts.inter(
                                fontSize: 20, fontWeight: FontWeight.w700),
                          ),
                        ),
                        const SizedBox(
                          width: 15,
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                minimumSize: const Size(90, 50),
                                foregroundColor: Colors.white,
                                backgroundColor:
                                    Color.fromRGBO(0, 139, 148, 1)),
                            onPressed: () {
                              for (int i = 0; i < inputTask.length; i++) {
                                inputTask[i]["controller"].clear();
                              }
                              selectedInd = 0;
                              setState(() {});
                            },
                            child: Text(
                              "ClearAll",
                              style: GoogleFonts.inter(
                                  fontSize: 20, fontWeight: FontWeight.w700),
                            )),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        foregroundColor: Color.fromRGBO(255, 255, 255, 1),
        backgroundColor: Color.fromRGBO(2, 167, 177, 1),
        title: Text("To-do list",
            style: GoogleFonts.quicksand(
                fontSize: 26, fontWeight: FontWeight.w700)),
      ),
      body: (taskList.length > 0)
          ? ListView.builder(
              itemCount: taskList.length,
              itemBuilder: (BuildContext context, int index) {
                return Padding(
                    padding: const EdgeInsets.all(11.5),
                    child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: colorList[index % colorList.length],
                            boxShadow: const [
                              BoxShadow(
                                  blurRadius: 7,
                                  spreadRadius: 1,
                                  color: Color.fromRGBO(0, 0, 0, 0.1))
                            ]),
                        child: Padding(
                          padding: const EdgeInsets.all(10),
                          child: Column(
                            children: [
                              //Row1
                              Row(
                                children: [
                                  Container(
                                      height: 52,
                                      width: 52,
                                      decoration: const BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.white,
                                          boxShadow: [
                                            BoxShadow(
                                                color: Color.fromRGBO(
                                                    0, 0, 0, 0.1),
                                                blurRadius: 2)
                                          ]),
                                      child: Image.asset(
                                          "${imgList[taskList[index].imgIndex]}")),
                                  const SizedBox(
                                    width: 15,
                                  ),
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          taskList[index].title,
                                          style: GoogleFonts.quicksand(
                                              fontSize: 12,
                                              fontWeight: FontWeight.w600),
                                        ),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          taskList[index].description,
                                          style: GoogleFonts.quicksand(
                                              fontSize: 10,
                                              fontWeight: FontWeight.w500),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 6,
                              ),
                              //Row2
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    taskList[index].date,
                                    style: GoogleFonts.quicksand(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w500,
                                        color:
                                            Color.fromRGBO(132, 132, 132, 1)),
                                  ),
                                  const Spacer(),
                                  GestureDetector(
                                    child: const Icon(
                                      Icons.edit_outlined,
                                      color: Color.fromRGBO(0, 139, 148, 1),
                                      size: 15,
                                    ),
                                    onTap: () {
                                      taskModification("Edit",
                                          title: taskList[index].title,
                                          description:
                                              taskList[index].description,
                                          date: taskList[index].date,
                                          editIndex: index);
                                    },
                                  ),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  GestureDetector(
                                    child: const Icon(
                                      Icons.delete_outline_outlined,
                                      color: Color.fromRGBO(0, 139, 148, 1),
                                      size: 15,
                                    ),
                                    onTap: () {
                                      taskList.removeAt(index);
                                      setState(() {});
                                    },
                                  )
                                ],
                              )
                            ],
                          ),
                        )));
              })
          : Center(
              child: Text("No task Avaiable",
                  style: GoogleFonts.quicksand(
                      fontSize: 23, fontWeight: FontWeight.w500)),
            ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.plus_one_outlined),
        onPressed: () {
          taskModification("Create");
        },
      ),
    );
  }
}
