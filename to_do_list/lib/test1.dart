import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Text1 extends StatefulWidget {
  const Text1({super.key});
  @override
  State createState() => _Text1State();
}

class _Text1State extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Cirualar img",
            style: GoogleFonts.quicksand(fontSize: 20),
          ),
        ),
        body: Center(
          child: Container(
              height: 200,
              width: 200,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                boxShadow: const [
                  BoxShadow(
                    color: Colors.red,
                    offset: Offset(-8, 8),
                    blurRadius: 10,
                  )
                ],
                border: Border.all(width: 5, color: Colors.orange),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Image.network(
                  "https://imgeng.jagran.com/images/2023/nov/Rohit-Sharma-profile1700142481090.jpg",
                  fit: BoxFit.cover,
                ),
              )),
        ));
  }
}
