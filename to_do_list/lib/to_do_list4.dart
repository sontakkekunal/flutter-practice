import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

class ToDoListApp extends StatefulWidget {
  const ToDoListApp({super.key});

  @override
  State createState() => _ToDoListAppState();
}

class Task {
  String title;
  String description;
  String date;
  int imgIndex;
  String? imgURl;
  Task({
    required this.title,
    required this.description,
    required this.date,
    required this.imgIndex,
    this.imgURl,
  });
}

class _ToDoListAppState extends State {
  List<Task> taskList = [];
  List<Color> colorList = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];
  List<Color> colorListDark = const [
    Color.fromRGBO(225, 132, 132, 0.616),
    Color.fromRGBO(165, 184, 234, 0.639),
    Color.fromRGBO(236, 233, 165, 0.627),
    Color.fromRGBO(230, 152, 230, 0.667)
  ];

  List<Map> inputTask = [
    {
      "taskName": "Title",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Description",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Date",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Networl Image URl(Optional)",
      "controller": TextEditingController(),
    }
  ];
  List<String> imgList = [
    "images/profile.png",
    "images/meeting.gif",
    "images/task1.jpg",
    "images/bill.gif",
    "images/task2.jpg",
    "images/car.gif",
    "images/task3.webp",
    "images/doctor.gif",
    "images/task4.png",
    "images/shopping.gif",
    "images/task5.jpg",
  ];
  final GlobalKey<FormFieldState> _key1 = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _key2 = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _key3 = GlobalKey<FormFieldState>();

  int pageIndex = 0;
  Image imgChoice(String? imgUrl, int i) {
    if (imgUrl != null && imgUrl.isNotEmpty) {
      return Image.network(
        imgUrl,
        height: 50,
        width: 50,
        fit: BoxFit.cover,
      );
    } else {
      return Image.asset(imgList[taskList[i].imgIndex]);
    }
  }

  bool themeColor = true;
  taskModification(bool editable, {Task? taskObj}) {
    int selectedInd = 0;
    int submitCount = 0;
    if (editable == false) {
      for (int i = 0; i < inputTask.length; i++) {
        inputTask[i]["controller"].clear();
      }
    } else if (taskObj != null) {
      inputTask[0]["controller"].text = taskObj.title;
      inputTask[1]["controller"].text = taskObj.description;
      inputTask[2]["controller"].text = taskObj.date;
      if (taskObj.imgURl != null) {
        inputTask[3]["controller"].text = taskObj.imgURl;
      }
      selectedInd = taskObj.imgIndex;
    }

    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: (themeColor == false) ? Colors.black87 : null,
        builder: (BuildContext context) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                    color: (themeColor) ? Colors.black87 : Colors.white,
                    width: 1),
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
              ),
              width: (FocusScope.of(context).hasPrimaryFocus)
                  ? 340
                  : double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 30,
                    width: double.infinity,
                    child: Icon(
                      Icons.horizontal_rule_rounded,
                      size: 60,
                      color: (themeColor == false) ? Colors.white : null,
                    ),
                  ),
                  Text(
                    (editable == false) ? "Create task" : "Edit task",
                    style: GoogleFonts.quicksand(
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                        color: (themeColor == false) ? Colors.white : null),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 310,
                    width: 300,
                    child: ListView.separated(
                      separatorBuilder: (BuildContext context, int index) =>
                          const SizedBox(
                        height: 8,
                      ),
                      itemCount: inputTask.length,
                      itemBuilder: (BuildContext context, int index) {
                        return TextFormField(
                          key: (index == 0)
                              ? _key1
                              : (index == 1)
                                  ? (_key2)
                                  : (index == 2)
                                      ? _key3
                                      : null,
                          controller: inputTask[index]["controller"],
                          maxLength: (index == 0)
                              ? 50
                              : (index == 1)
                                  ? 250
                                  : (index == 2)
                                      ? 20
                                      : 200,
                          cursorWidth: 2,
                          cursorHeight: 30,
                          onChanged: (submitCount > 0)
                              ? (String val) {
                                  _key1.currentState?.validate();
                                  _key2.currentState?.validate();
                                  _key3.currentState?.validate();
                                }
                              : null,
                          cursorColor: const Color.fromARGB(255, 105, 9, 122),
                          cursorRadius: const Radius.circular(50),
                          cursorOpacityAnimates: true,
                          textAlign: TextAlign.center,
                          validator: (String? value) {
                            if ((value == null || value.isEmpty) &&
                                index == 0) {
                              return "Please enter valid title";
                            } else if ((value == null || value.isEmpty) &&
                                index == 1) {
                              return "Please enter valid desciption";
                            } else if ((value == null || value.isEmpty) &&
                                index == 2) {
                              return "Please enter valid date";
                            } else {
                              return null;
                            }
                          },
                          textAlignVertical: TextAlignVertical.center,
                          readOnly: (index == 2) ? true : false,
                          onTap: (index == 2)
                              ? () async {
                                  //pick the date from datePicker
                                  DateTime? pickdate = await showDatePicker(
                                      context: context,
                                      initialDate: DateTime.now(),
                                      firstDate: DateTime.now(),
                                      lastDate: DateTime(2050));

                                  //Format the date into the requierd
                                  //format of the date i.e. year month date
                                  String formatedDate =
                                      DateFormat.yMMMd().format(pickdate!);

                                  setState(() {
                                    inputTask[index]["controller"].text =
                                        formatedDate;
                                  });
                                  if (submitCount > 0) {
                                    _key3.currentState!.validate();
                                  }
                                }
                              : null,
                          style: GoogleFonts.quicksand(
                              fontSize: 18,
                              color:
                                  (themeColor == false) ? Colors.white : null),
                          decoration: InputDecoration(
                            hintText: "Enter ${inputTask[index]["taskName"]}",
                            hintStyle: GoogleFonts.quicksand(
                                fontSize: 14,
                                color: (themeColor == false)
                                    ? Colors.white
                                    : null),
                            labelText: inputTask[index]["taskName"],
                            labelStyle: GoogleFonts.quicksand(
                                fontSize: 16,
                                color: (themeColor == false)
                                    ? Colors.white
                                    : null),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: (index == 3)
                                        ? Colors.yellow
                                        : (submitCount > 0)
                                            ? Colors.purple
                                            : (themeColor)
                                                ? Colors.black87
                                                : Colors.white,
                                    width: 2),
                                gapPadding: 10,
                                borderRadius: BorderRadius.circular(20)),
                            suffixIcon: (index == 2)
                                ? const Icon(
                                    Icons.calendar_month_outlined,
                                    color: Color.fromRGBO(2, 167, 177, 1),
                                  )
                                : null,
                            errorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide:
                                    const BorderSide(color: Colors.red)),
                            focusedErrorBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide:
                                    const BorderSide(color: Colors.red)),
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: (index == 3)
                                        ? Colors.yellow
                                        : (submitCount > 0)
                                            ? Colors.green
                                            : Colors.blue,
                                    width: 2),
                                gapPadding: 3,
                                borderRadius: BorderRadius.circular(20)),
                          ),
                        );
                      },
                    ),
                  ),
                  Text("--------provide network path OR----------",
                      style: GoogleFonts.quicksand(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: (themeColor == false) ? Colors.white : null)),
                  Text("Choose gif :",
                      style: GoogleFonts.quicksand(
                          fontSize: 16,
                          fontWeight: FontWeight.w600,
                          color: (themeColor == false) ? Colors.white : null)),
                  const SizedBox(
                    height: 10,
                  ),
                  SizedBox(
                    height: 45,
                    width: 300,
                    child: ListView.separated(
                      scrollDirection: Axis.horizontal,
                      itemCount: imgList.length,
                      separatorBuilder: (BuildContext context, int index) {
                        return const SizedBox(
                          width: 25,
                        );
                      },
                      itemBuilder: (BuildContext context, int index) {
                        return GestureDetector(
                          onTap: () {
                            setState(() {
                              selectedInd = index;

                              if (FocusScope.of(context).hasPrimaryFocus ==
                                  false) {
                                FocusScope.of(context).unfocus();
                              }
                            });
                          },
                          child: Container(
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                  border: (index == selectedInd)
                                      ? Border.all(color: Color.fromARGB(255, 191, 13, 222), width: 4)
                                      : null,
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.7),
                                      blurRadius: 2,
                                    )
                                  ]),
                              child: ClipRRect(
                                  borderRadius: BorderRadius.circular(25),
                                  child: Image.asset(
                                    imgList[index],
                                    height: 40,
                                    width: 40,
                                  ))),
                        );
                      },
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: const Size(90, 50),
                          foregroundColor: Colors.white,
                          backgroundColor: Color.fromRGBO(0, 139, 148, 1)),
                      onPressed: () {
                        for (int i = 0; i < inputTask.length; i++) {
                          inputTask[i]["controller"].text =
                              inputTask[i]["controller"].text.trim();
                        }
                        if (inputTask[0]["controller"].text.length > 0 &&
                            inputTask[1]["controller"].text.length > 0 &&
                            inputTask[2]["controller"].text.length > 0) {
                          if (editable == false) {
                            taskList.add(Task(
                                title: inputTask[0]["controller"].text,
                                description: inputTask[1]["controller"].text,
                                date: inputTask[2]["controller"].text,
                                imgIndex: selectedInd,
                                imgURl:
                                    (inputTask[3]["controller"].text.isEmpty)
                                        ? null
                                        : inputTask[3]["controller"].text));
                          } else {
                            taskObj?.title = inputTask[0]["controller"].text;
                            taskObj?.description =
                                inputTask[1]["controller"].text;
                            taskObj?.date = inputTask[2]["controller"].text;
                            taskObj?.imgURl = inputTask[3]["controller"].text;
                            taskObj?.imgIndex = selectedInd;
                          }
                          setState(() {});
                          Navigator.of(context).pop();
                        } else {
                          submitCount++;
                          _key1.currentState!.validate();
                          _key2.currentState!.validate();
                          _key3.currentState!.validate();
                        }
                      },
                      child: Text(
                        "Save",
                        style: GoogleFonts.inter(
                            fontSize: 20, fontWeight: FontWeight.w700),
                      ),
                    ),
                    const SizedBox(
                      width: 20,
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: const Size(90, 50),
                          foregroundColor: Colors.white,
                          backgroundColor: Color.fromRGBO(0, 139, 148, 1)),
                      onPressed: () {
                        for (int i = 0; i < inputTask.length; i++) {
                          inputTask[i]["controller"].clear();
                        }
                        if (submitCount > 0) {
                          _key1.currentState!.validate();
                          _key2.currentState!.validate();
                          _key3.currentState!.validate();
                        }
                        //FocusScope.of(context).unfocus();
                      },
                      child: Text(
                        "ClearAll",
                        style: GoogleFonts.inter(
                            fontSize: 20, fontWeight: FontWeight.w700),
                      ),
                    )
                  ])
                ],
              ),
            ),
          );
        });
  }

  bool viewpassword1 = true;
  bool viewpassword2 = true;

  List<Map> loginInputList = [
    {
      "name": "UserName",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Confirm Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    }
  ];
  List userDate = [
    {"username": "kunal", "password": "kunal@123", "taskList": <Task>[]}
  ];
  int sigUpCnt = 0;
  bool userNameChecker(String value) {
    for (int i = 0; i < userDate.length; i++) {
      if (userDate[i]["username"] == value) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    if (pageIndex == 0 || pageIndex == 1) {
      return Scaffold(
        body: Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("images/backgroundToDoListApp.png"),
                  fit: BoxFit.cover)),
          height: double.infinity,
          width: double.infinity,
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 8, sigmaY: 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 40,
                  width: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white.withOpacity(0.4),
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.7),
                            blurRadius: 5,
                            spreadRadius: 1)
                      ]),
                  child: Center(
                    child: Text(
                      "To-Do List App",
                      style: GoogleFonts.quicksand(
                          fontSize: 20,
                          fontWeight: FontWeight.w700,
                          fontStyle: FontStyle.italic),
                    ),
                  ),
                ),
                Container(
                  height: 350,
                  width: 350,
                  padding: const EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white.withOpacity(0.4),
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.7),
                            blurRadius: 5,
                            spreadRadius: 1)
                      ]),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        (pageIndex == 0) ? "Login" : "Sigin",
                        style: GoogleFonts.quicksand(
                            fontSize: 25,
                            fontWeight: FontWeight.w700,
                            fontStyle: FontStyle.italic),
                      ),
                      SizedBox(
                        width: double.infinity,
                        height: 200,
                        child: ListView.separated(
                          itemCount: (pageIndex == 0)
                              ? loginInputList.length - 1
                              : loginInputList.length,
                          separatorBuilder: (BuildContext context, int index) =>
                              const SizedBox(
                            height: 10,
                          ),
                          itemBuilder: (BuildContext context, int index) {
                            return TextFormField(
                              key: loginInputList[index]["key"],
                              cursorWidth: 2,
                              controller: loginInputList[index]["controller"],
                              cursorHeight: 30,
                              obscureText: (index == 1)
                                  ? viewpassword1
                                  : (index == 2)
                                      ? viewpassword2
                                      : false,
                              obscuringCharacter: '#',
                              onChanged: (String val) {
                                if (pageIndex == 1 && sigUpCnt > 0) {
                                  for (int i = 0;
                                      i < loginInputList.length;
                                      i++) {
                                    loginInputList[i]["key"]
                                        .currentState!
                                        .validate();
                                  }
                                }
                              },
                              cursorColor:
                                  const Color.fromARGB(255, 105, 9, 122),
                              cursorRadius: const Radius.circular(50),
                              cursorOpacityAnimates: true,
                              textAlign: TextAlign.center,
                              validator: (String? value) {
                                if (value != null) {
                                  value = value.trim();
                                }

                                if (pageIndex == 1) {
                                  if (index == 0) {
                                    if (value == null || value.isEmpty) {
                                      return "Invaild username";
                                    } else if (value.contains(" ")) {
                                      return "Invaild username";
                                    } else if (userNameChecker(value)) {
                                      return "username already taken";
                                    }
                                  } else if (index == 1 &&
                                      (value == null || value.isEmpty)) {
                                    return "Invaild password";
                                  } else if (index == 2) {
                                    if (index == 2 &&
                                        (value == null || value.isEmpty)) {
                                      return "Please re-enter password";
                                    } else if (loginInputList[1]["controller"]
                                            .text !=
                                        loginInputList[2]["controller"].text) {
                                      return "Incorrect password";
                                    }
                                  }
                                }
                                return null;
                              },
                              textAlignVertical: TextAlignVertical.center,
                              style: GoogleFonts.quicksand(
                                fontSize: 18,
                              ),
                              decoration: InputDecoration(
                                hintText:
                                    "Enter ${loginInputList[index]["name"]}",
                                hintStyle: GoogleFonts.quicksand(
                                  fontSize: 14,
                                ),
                                suffixIcon: (index == 1 || index == 2)
                                    ? GestureDetector(
                                        child: Icon((index == 1)
                                            ? (viewpassword1)
                                                ? Icons.visibility_off_outlined
                                                : Icons.visibility_outlined
                                            : (index == 2)
                                                ? (viewpassword2)
                                                    ? Icons
                                                        .visibility_off_outlined
                                                    : Icons.visibility_outlined
                                                : null),
                                        onTap: () {
                                          if (index == 1) {
                                            viewpassword1 = !viewpassword1;
                                          }
                                          if (index == 2) {
                                            viewpassword2 = !viewpassword2;
                                          }
                                          setState(() {});
                                        },
                                      )
                                    : null,
                                labelText: loginInputList[index]["name"],
                                labelStyle: GoogleFonts.quicksand(
                                  fontSize: 16,
                                ),
                                enabledBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: 2),
                                    gapPadding: 10,
                                    borderRadius: BorderRadius.circular(20)),
                                focusedBorder: OutlineInputBorder(
                                    borderSide: BorderSide(width: 2),
                                    gapPadding: 3,
                                    borderRadius: BorderRadius.circular(20)),
                              ),
                            );
                          },
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          if (pageIndex == 0) {
                            bool hasData = false;
                            for (int i = 0; i < userDate.length; i++) {
                              if (userDate[i]["username"] ==
                                      loginInputList[0]["controller"].text &&
                                  userDate[i]["password"] ==
                                      loginInputList[1]["controller"].text) {
                                taskList = userDate[i]["taskList"];
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                  content: Text(
                                    "Login Sucessfully",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.white),
                                  ),
                                  backgroundColor: Colors.green,
                                ));
                                hasData = true;
                                setState(() {
                                  pageIndex = 3;

                                  viewpassword1 = true;
                                  viewpassword2 = true;
                                  sigUpCnt = 0;
                                  for (int i = 0;
                                      i < loginInputList.length;
                                      i++) {
                                    loginInputList[i]["controller"].clear();
                                  }
                                });
                                break;
                              }
                            }
                            if (!hasData) {
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text(
                                  "Invaild username or password",
                                  style: GoogleFonts.quicksand(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                                backgroundColor: Colors.red,
                              ));
                            }
                          } else if (pageIndex == 1) {
                            bool val = false;
                            for (int i = 0; i < loginInputList.length; i++) {
                              val = loginInputList[i]["key"]
                                  .currentState!
                                  .validate();
                            }
                            sigUpCnt++;
                            if (val &&
                                !userNameChecker(
                                    loginInputList[0]["controller"].text)) {
                              List<Task> temp = [];
                              userDate.add({
                                "username":
                                    loginInputList[0]["controller"].text,
                                "password":
                                    loginInputList[1]["controller"].text,
                                "taskList": temp
                              });
                              ScaffoldMessenger.of(context)
                                  .showSnackBar(SnackBar(
                                content: Text(
                                  "Account created succesfully",
                                  style: GoogleFonts.quicksand(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.white),
                                ),
                                backgroundColor: Colors.green,
                              ));
                              setState(() {
                                taskList = temp;
                                pageIndex = 3;
                                sigUpCnt = 0;
                                viewpassword1 = true;
                                viewpassword2 = true;

                                for (int i = 0;
                                    i < loginInputList.length;
                                    i++) {
                                  loginInputList[i]["controller"].clear();
                                }
                              });
                            }
                          }
                        },
                        child: Container(
                          height: 40,
                          width: 180,
                          decoration: BoxDecoration(
                              color: Color.fromRGBO(0, 139, 148, 1),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.7),
                                    blurRadius: 5,
                                    spreadRadius: 1)
                              ],
                              borderRadius: BorderRadius.circular(30)),
                          child: Center(
                              child: Text(
                            (pageIndex == 0) ? "Login" : "Sigin up",
                            style: GoogleFonts.quicksand(
                                fontSize: 18, fontWeight: FontWeight.w600),
                          )),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white.withOpacity(0.6),
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.7),
                            blurRadius: 5,
                            spreadRadius: 1)
                      ]),
                  child: (pageIndex == 0)
                      ? TextButton(
                          onPressed: () {
                            setState(() {
                              pageIndex = 1;
                              sigUpCnt = 0;
                              for (int i = 0; i < loginInputList.length; i++) {
                                loginInputList[i]["controller"].clear();
                              }
                            });
                          },
                          child: Text("I don't have account? Sign Up",
                              style: GoogleFonts.quicksand(
                                  fontSize: 14, fontWeight: FontWeight.w600)))
                      : TextButton(
                          onPressed: () {
                            setState(() {
                              pageIndex = 0;
                              sigUpCnt = 0;
                              for (int i = 0; i < loginInputList.length; i++) {
                                loginInputList[i]["controller"].clear();
                              }
                            });
                          },
                          child: Text("I have have account? Login",
                              style: GoogleFonts.quicksand(
                                  fontSize: 14, fontWeight: FontWeight.w600))),
                )
              ],
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
        backgroundColor: (themeColor == false) ? Colors.black : null,
        appBar: AppBar(
          toolbarHeight: 80,
          elevation: 0,
          flexibleSpace: Container(
            decoration: BoxDecoration(
                color: (themeColor)
                    ? Color.fromRGBO(2, 167, 177, 1)
                    : Colors.black,
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(25),
                    bottomRight: Radius.circular(25))),
          ),
          foregroundColor: Color.fromRGBO(255, 255, 255, 1),
          backgroundColor: Colors.transparent,
          title: Text("To-do list",
              style: GoogleFonts.quicksand(
                  fontSize: 26, fontWeight: FontWeight.w700)),
          actions: [
            IconButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text(
                      "Logout succesfully",
                      style: GoogleFonts.quicksand(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: Colors.white),
                    ),
                    backgroundColor: Colors.green,
                  ));
                  setState(() {
                    viewpassword1 = true;
                    viewpassword2 = true;
                    
                    sigUpCnt = 0;
                    pageIndex = 0;
                  });
                },
                icon: Icon(
                  Icons.logout_outlined,
                  color: (themeColor)
                      ? const Color.fromARGB(255, 222, 21, 6)
                      : Colors.white,
                )),
            IconButton(
              icon: Icon(
                Icons.lightbulb,
                color: (themeColor) ? Colors.yellow : Colors.white,
              ),
              onPressed: () {
                setState(() {
                  themeColor = !themeColor;
                });
              },
            )
          ],
        ),
        body: (taskList.isNotEmpty)
            ? ListView.builder(
                itemCount: taskList.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                      padding: const EdgeInsets.all(11.5),
                      child: Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: (themeColor)
                                  ? colorList[index % colorList.length]
                                  : colorListDark[index % colorListDark.length],
                              boxShadow: const [
                                BoxShadow(
                                    blurRadius: 7,
                                    spreadRadius: 1,
                                    color: Color.fromRGBO(0, 0, 0, 0.1))
                              ]),
                          child: Padding(
                            padding: const EdgeInsets.all(10),
                            child: Column(
                              children: [
                                //Row1
                                Row(
                                  children: [
                                    Container(
                                        height: 52,
                                        width: 52,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            color: Colors.white,
                                            boxShadow: [
                                              BoxShadow(
                                                  color: (themeColor)
                                                      ? Color.fromRGBO(
                                                          0, 0, 0, 0.1)
                                                      : Colors.white,
                                                  blurRadius: 2)
                                            ]),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(25),
                                          child: imgChoice(
                                              taskList[index].imgURl, index),
                                        )),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "• ${taskList[index].title}",
                                            style: GoogleFonts.quicksand(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w600,
                                                color: (themeColor == false)
                                                    ? Colors.white
                                                    : null),
                                          ),
                                          const SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            "• ${taskList[index].description}",
                                            style: GoogleFonts.quicksand(
                                                fontSize: 10,
                                                fontWeight: FontWeight.w500,
                                                color: (themeColor == false)
                                                    ? Colors.white
                                                    : null),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 6,
                                ),
                                //Row2
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Text(
                                      taskList[index].date,
                                      style: GoogleFonts.quicksand(
                                          fontSize: 10,
                                          fontWeight: FontWeight.w500,
                                          color: (themeColor)
                                              ? Color.fromRGBO(132, 132, 132, 1)
                                              : Colors.white),
                                    ),
                                    const Spacer(),
                                    GestureDetector(
                                      child: Icon(
                                        Icons.edit_outlined,
                                        color: (themeColor)
                                            ? Color.fromRGBO(0, 139, 148, 1)
                                            : Colors.white,
                                        size: 15,
                                      ),
                                      onTap: () {
                                        taskModification(true,
                                            taskObj: taskList[index]);
                                      },
                                    ),
                                    const SizedBox(
                                      width: 10,
                                    ),
                                    GestureDetector(
                                      child: Icon(
                                        Icons.delete_outline_outlined,
                                        color: (themeColor)
                                            ? Color.fromRGBO(0, 139, 148, 1)
                                            : Colors.white,
                                        size: 15,
                                      ),
                                      onTap: () {
                                        taskList.remove(taskList[index]);
                                        setState(() {});
                                      },
                                    )
                                  ],
                                )
                              ],
                            ),
                          )));
                })
            : Center(
                child: Text("No task available",
                    style: GoogleFonts.quicksand(
                      
                        fontSize: 23, fontWeight: FontWeight.w500)),
              ),
        floatingActionButton: FloatingActionButton(
          backgroundColor: (themeColor == false)
              ? Color.fromRGBO(0, 148, 158, 1)
              : Color.fromRGBO(0, 138, 148, 0.508),
          foregroundColor: (themeColor) ? Colors.white : Colors.black,
          child: const Icon(
            Icons.plus_one_outlined,
            size: 35,
          ),
          onPressed: () {
            taskModification(false);
          },
        ),
      );
    }
  }
}
