import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ToDoList extends StatefulWidget {
  const ToDoList({super.key});
  @override
  State createState() => _ToDoListState();
}

List<Map> taskList = [
  {
    "title": "Lorem Ipsum is simply setting industry. ",
    "description":
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "date": "10 July 2023"
  },
  {
    "title": "Lorem Ipsum is simply setting industry. ",
    "description":
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "date": "10 July 2023"
  },
  {
    "title": "Lorem Ipsum is simply setting industry. ",
    "description":
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "date": "10 July 2023"
  },
  {
    "title": "Lorem Ipsum is simply setting industry. ",
    "description":
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "date": "10 July 2023"
  },
  {
    "title": "Lorem Ipsum is simply setting industry. ",
    "description":
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "date": "10 July 2023"
  },
  {
    "title": "Lorem Ipsum is simply setting industry. ",
    "description":
        "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
    "date": "10 July 2023"
  }
];
List<Color> colorList = const [
  Color.fromRGBO(250, 232, 232, 1),
  Color.fromRGBO(232, 237, 250, 1),
  Color.fromRGBO(250, 249, 232, 1),
  Color.fromRGBO(250, 232, 250, 1)
];

class _ToDoListState extends State {
  TextEditingController _textEditingController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        foregroundColor: const Color.fromRGBO(255, 255, 255, 1),
        title: Text(
          "TTo-do list",
          style:
              GoogleFonts.quicksand(fontSize: 26, fontWeight: FontWeight.w700),
        ),
      ),
      body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Padding(
            padding: const EdgeInsets.all(15),
            child: ListView.builder(
              itemCount: taskList.length,
              itemBuilder: (BuildContext build, int index) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 117,
                    width: 330,
                    margin: const EdgeInsets.only(bottom: 25),
                    decoration: BoxDecoration(
                      color: colorList[index % colorList.length],
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: const [
                        BoxShadow(
                            spreadRadius: 1,
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            offset: Offset(0, 7),
                            blurRadius: 20)
                      ],
                    ),
                    child: Column(
                      children: [
                        Row(
                          children: [
                            Container(
                                margin: const EdgeInsets.only(left: 10, top: 15),
                                height: 52,
                                width: 52,
                                decoration: const BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Color.fromRGBO(255, 255, 255, 1),
                                    boxShadow: [
                                      BoxShadow(
                                          blurRadius: 10,
                                          color: Color.fromRGBO(0, 0, 0, 0.07))
                                    ]),
                                child: Image.asset("images/profile.png")),
                            const SizedBox(
                              width: 25,
                            ),
                            Expanded(
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        taskList[index]["title"],
                                        style: GoogleFonts.quicksand(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w600),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    taskList[index]["description"],
                                    style: GoogleFonts.quicksand(
                                        fontSize: 10,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                            )
                          ],
                        ),
                        Row(
                          children: [
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              taskList[index]["date"],
                              style: GoogleFonts.quicksand(
                                  fontSize: 10, fontWeight: FontWeight.w500),
                            ),
                            const Spacer(),
                            IconButton(
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.edit,
                                  size: 18,
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                )),
                            IconButton(
                                onPressed: () {},
                                icon: const Icon(
                                  Icons.delete,
                                  size: 18,
                                  color: Color.fromRGBO(0, 139, 148, 1),
                                )),
                          ],
                        )
                      ],
                    ),
                  ),
                );
              },
            ),
          )),
      floatingActionButton: FloatingActionButton(
        child: const Icon(
          Icons.add,
          size: 35,
        ),
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  width: double.infinity,
                  height: double.infinity,
                  padding: const EdgeInsets.all(22),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 1,
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            offset: Offset(0, 7),
                            blurRadius: 20),
                      ],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Create Task",
                        style: GoogleFonts.quicksand(
                            fontSize: 25, fontWeight: FontWeight.w700),
                      ),
                      TextField(
                        controller: _textEditingController,
                        decoration: InputDecoration(
                            hintText: "Enter title",
                            labelText: "Title",
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.green,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide:
                                    const BorderSide(color: Colors.blue))),
                      ),
                      TextField(
                        //controller: _textEditingController,
                        decoration: InputDecoration(
                            hintText: "Enter Description",
                            labelText: "Description",
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.green,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide:
                                    const BorderSide(color: Colors.blue))),
                      ),
                      TextField(
                        //controller: _textEditingController,
                        decoration: InputDecoration(
                            hintText: "Enter Description",
                            labelText: "Description",
                            
                            icon: Icon(Icons.calendar_month_rounded),
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.green,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide:
                                    const BorderSide(color: Colors.blue))),
                      ),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(300, 50),
                              backgroundColor: Color.fromRGBO(0, 139, 148, 1),
                              foregroundColor: Colors.white),
                          onPressed: () {},
                          child: Text(
                            "Submit",
                            style: GoogleFonts.quicksand(
                                fontSize: 20, fontWeight: FontWeight.w700),
                          )),
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}
