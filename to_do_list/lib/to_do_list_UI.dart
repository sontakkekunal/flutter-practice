import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ToDoListUI extends StatefulWidget {
  const ToDoListUI({super.key});

  @override
  State createState() => _ToDoListUIState();
}

class _ToDoListUIState extends State {
  List<Color> colorBox = const [
    Color.fromRGBO(250, 232, 232, 1),
    Color.fromRGBO(232, 237, 250, 1),
    Color.fromRGBO(250, 249, 232, 1),
    Color.fromRGBO(250, 232, 250, 1)
  ];
  List<Map> taskList = [
    {
      "title": "Lorem Ipsum is simply setting industry.",
      "description":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
      "assetsImg": "",
      "networkImg":
          "https://imgeng.jagran.com/images/2023/nov/Rohit-Sharma-profile1700142481090.jpg"
    },
    {
      "title": "Lorem Ipsum is simply setting industry.",
      "description":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
      "assetsImg": "images/Vector.jpg",
      "networkImg": ""
    },
    {
      "title": "Lorem Ipsum is simply setting industry.",
      "description":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
      "assetsImg": "images/profile.png",
      "networkImg": ""
    },
    {
      "title": "Lorem Ipsum is simply setting industry.",
      "description":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
      "assetsImg": "images/myGif.gif",
      "networkImg": ""
    },
    {
      "title": "Lorem Ipsum is simply setting industry.",
      "description":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
      "assetsImg": "",
      "networkImg":
          "https://m.media-amazon.com/images/M/MV5BMjQ5OTY4MjYtMjBkNi00ZWFjLWI1YmItMTkxZTk0ODYwNDZiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_.jpg"
    },
    {
      "title": "Lorem Ipsum is simply setting industry.",
      "description":
          "Simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s",
      "date": "10 July 2023",
      "assetsImg": "images/myGifK.gif",
      "networkImg": ""
    },
  ];
  List<Map> inputTask = [
    {"name": "Title", "controller": TextEditingController()},
    {"name": "Deciption", "controller": TextEditingController()},
    {"name": "Date", "controller": TextEditingController()},
    {"name": "Assets Image path", "controller": TextEditingController()},
    {"name": "Network Image path", "controller": TextEditingController()}
  ];
  bool themeFlag = false;
  bool addFlag = false;
  int editIndex = -1;
  Image img(int index) {
    if (taskList[index]["assetsImg"].length > 0) {
      return Image.asset("${taskList[index]["assetsImg"]}");
    } else if (taskList[index]["networkImg"].length > 0) {
      return Image.network(
        "${taskList[index]["networkImg"]}",
        fit: BoxFit.cover,
      );
    } else {
      return Image.asset("images/profile.png");
    }
  }

  @override
  Widget build(BuildContext context) {
    if (addFlag == false) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: (themeFlag == false)
              ? const Color.fromRGBO(2, 167, 177, 1)
              : Color.fromARGB(255, 4, 4, 4),
          actions: [
            IconButton(
              color: Colors.white,
              icon: Icon(
                  (themeFlag == false) ? Icons.dark_mode : Icons.light_mode),
              onPressed: () {
                themeFlag = !themeFlag;
                setState(() {});
              },
            ),
          ],
          title: Text(
            "To-do List",
            style: GoogleFonts.quicksand(
                fontWeight: FontWeight.w700, fontSize: 26, color: Colors.white),
          ),
        ),
        backgroundColor: (themeFlag == false) ? Colors.white : Colors.black,
        body: (taskList.isNotEmpty)
            ? SizedBox(
                height: double.infinity,
                width: double.infinity,
                child: ListView.builder(
                  itemCount: taskList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                        height: 122,
                        width: 330,
                        decoration: BoxDecoration(
                            boxShadow: const [
                              BoxShadow(
                                  blurRadius: 20,
                                  color: Color.fromRGBO(0, 0, 0, 0.1))
                            ],
                            color: (themeFlag == false)
                                ? colorBox[index % colorBox.length]
                                : Color.fromARGB(255, 116, 73, 73),
                            borderRadius: BorderRadius.circular(10)),
                        margin:
                            const EdgeInsets.only(top: 20, left: 15, right: 15),
                        child: Column(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                    height: 52,
                                    width: 52,
                                    margin: const EdgeInsets.only(
                                      top: 20,
                                      left: 10,
                                      right: 10,
                                    ),
                                    decoration: BoxDecoration(
                                        boxShadow: const [
                                          BoxShadow(
                                              color:
                                                  Color.fromRGBO(0, 0, 0, 0.07),
                                              blurRadius: 10)
                                        ],
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(26)),
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(26),
                                        child: img(index))
                                    //child: (inputTask[index]["assetsImg"].length>0)?Image.asset("${inputTask[index]["assetsImg"]}"):(inputTask[index]["networkImg"].length>0)?Image.network("${inputTask[index]["networkImg"]}"):Image.asset("images/profile.png"),
                                    ),
                                SizedBox(
                                  width: 280,
                                  child: Column(
                                    children: [
                                      const SizedBox(
                                        height: 7,
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            taskList[index]["title"],
                                            style: GoogleFonts.quicksand(
                                                fontSize: 12,
                                                fontWeight: FontWeight.w700,
                                                color: (themeFlag == false)
                                                    ? null
                                                    : Colors.white),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        taskList[index]["description"],
                                        style: GoogleFonts.quicksand(
                                            fontSize: 10,
                                            color: (themeFlag == false)
                                                ? null
                                                : Colors.white),
                                      )
                                    ],
                                  ),
                                )
                              ],
                            ),
                            Row(
                              children: [
                                const SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  taskList[index]["date"],
                                  style: GoogleFonts.quicksand(
                                      fontSize: 10,
                                      color: (themeFlag == false)
                                          ? null
                                          : Colors.white),
                                ),
                                const Spacer(),
                                IconButton(
                                    onPressed: () {
                                      addFlag = true;
                                      editIndex = index;
                                      inputTask[0]["controller"].text =
                                          taskList[index]["title"];
                                      inputTask[1]["controller"].text =
                                          taskList[index]["description"];
                                      inputTask[2]["controller"].text =
                                          taskList[index]["date"];
                                      inputTask[3]["controller"].text =
                                          taskList[index]["assetsImg"];
                                      inputTask[4]["controller"].text =
                                          taskList[index]["networkImg"];
                                      setState(() {});
                                    },
                                    icon: Icon(
                                      Icons.edit,
                                      color: (themeFlag == false)
                                          ? Color.fromRGBO(0, 139, 148, 1)
                                          : Colors.white,
                                      size: 18,
                                    )),
                                IconButton(
                                    onPressed: () {
                                      taskList.removeAt(index);
                                      setState(() {});
                                    },
                                    icon: Icon(
                                      Icons.delete,
                                      size: 18,
                                      color: (themeFlag == false)
                                          ? Color.fromRGBO(0, 139, 148, 1)
                                          : Colors.white,
                                    ))
                              ],
                            )
                          ],
                        ));
                  },
                ),
              )
            : Center(
                child: Text(
                  "No Task avaiable",
                  style: GoogleFonts.quicksand(
                      fontSize: 18,
                      fontWeight: FontWeight.w300,
                      color: (themeFlag == false) ? null : Colors.white),
                ),
              ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            addFlag = true;
            setState(() {});
          },
          child: const Icon(Icons.add),
        ),
      );
    } else {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: (themeFlag == false)
              ? Color.fromRGBO(0, 139, 148, 1)
              : Colors.black,
          title: Text(
            "To-do List",
            style: GoogleFonts.quicksand(
                fontWeight: FontWeight.w700, fontSize: 26, color: Colors.white),
          ),
          actions: [
            IconButton(
              color: Colors.white,
              icon: Icon(
                  (themeFlag == false) ? Icons.dark_mode : Icons.light_mode),
              onPressed: () {
                themeFlag = !themeFlag;
                setState(() {});
              },
            ),
          ],
          leading: IconButton(
            icon: Icon(Icons.arrow_back,
                color: (themeFlag == false) ? null : Colors.white),
            onPressed: () {
              addFlag = !addFlag;
              editIndex = -1;
              for (int i = 0; i < inputTask.length; i++) {
                inputTask[i]["controller"].clear();
              }
              setState(() {});
            },
          ),
        ),
        backgroundColor: (themeFlag == false) ? Colors.white : Colors.black,
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                (editIndex == -1) ? "Enter task Details" : "Edit task Details",
                style: GoogleFonts.quicksand(
                    fontSize: 25,
                    color: (themeFlag == false) ? null : Colors.white),
              ),
              Container(
                  height: 350,
                  padding: const EdgeInsets.all(25),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: (themeFlag == false)
                          ? Color.fromRGBO(250, 229, 201, 1)
                          : Color.fromARGB(255, 111, 85, 83),
                      boxShadow: const [
                        BoxShadow(
                            color: Color.fromARGB(255, 177, 154, 154),
                            blurRadius: 10),
                        BoxShadow(
                            color: Color.fromARGB(255, 199, 132, 132),
                            blurRadius: 10,
                            offset: Offset(8, 8))
                      ]),
                  width: 320,
                  child: ListView.separated(
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox(
                        height: 15,
                      );
                    },
                    itemCount: inputTask.length,
                    itemBuilder: (BuildContext context, int index) {
                      return TextField(
                        textAlignVertical: TextAlignVertical.center,
                        cursorColor: Colors.blue,
                        cursorHeight: 30,
                        cursorWidth: 5,
                        keyboardAppearance: (themeFlag == false)
                            ? Brightness.light
                            : Brightness.dark,
                        keyboardType: (index == 3 || index == 4)
                            ? TextInputType.url
                            : (index != 2)
                                ? TextInputType.name
                                : TextInputType.datetime,
                        maxLength: (index == 0)
                            ? 50
                            : (index == 1)
                                ? 150
                                : (index == 2)
                                    ? 20
                                    : null,
                        controller: inputTask[index]["controller"],
                        onChanged: (String val) {
                          setState(() {});
                        },
                        decoration: InputDecoration(
                            hintText: "Enter ${inputTask[index]["name"]}",
                            hintStyle: GoogleFonts.quicksand(
                                fontSize: 15,
                                color:
                                    (themeFlag == false) ? null : Colors.white),
                            labelText: inputTask[index]["name"],
                            labelStyle: GoogleFonts.quicksand(
                                fontSize: 15,
                                color:
                                    (themeFlag == false) ? null : Colors.white),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: const BorderSide(
                                    color: Colors.blue, width: 2)),
                            enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(
                                    color: (themeFlag == false)
                                        ? Colors.blueGrey
                                        : Colors.white))),
                      );
                    },
                  )),
              Text(
                "Note: Enter assets path or network url,if both entered then assets path will be considered.If neither one is entered then default image will be added",
                style: GoogleFonts.quicksand(color: Colors.red, fontSize: 12),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: const Size(90, 50)),
                      onPressed: (inputTask[0]["controller"].text.length > 0)
                          ? () {
                              if (editIndex == -1) {
                                taskList.add({
                                  "title": inputTask[0]["controller"].text,
                                  "description":
                                      inputTask[1]["controller"].text,
                                  "date": inputTask[2]["controller"].text,
                                  "assetsImg": inputTask[3]["controller"].text,
                                  "networkImg": inputTask[4]["controller"].text
                                });
                              } else {
                                taskList[editIndex]["title"] =
                                    inputTask[0]["controller"].text;
                                taskList[editIndex]["description"] =
                                    inputTask[1]["controller"].text;
                                taskList[editIndex]["date"] =
                                    inputTask[2]["controller"].text;
                                taskList[editIndex]["assetsImg"] =
                                    inputTask[3]["controller"].text;
                                taskList[editIndex]["networkImg"] =
                                    inputTask[4]["controller"].text;
                                editIndex = -1;
                              }
                              addFlag = false;
                              for (int i = 0; i < inputTask.length; i++) {
                                inputTask[i]["controller"].clear();
                              }
                              setState(() {});
                            }
                          : () {},
                      child: Text(
                        (editIndex == -1) ? "Submit" : "Save",
                        style: const TextStyle(fontSize: 18),
                      )),
                  const SizedBox(
                    width: 15,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          minimumSize: const Size(90, 50)),
                      onPressed: () {
                        for (int i = 0; i < inputTask.length; i++) {
                          inputTask[i]["controller"].clear();
                        }
                        setState(() {});
                      },
                      child: const Text(
                        "clearAll",
                        style: TextStyle(fontSize: 18),
                      )),
                ],
              ),
            ],
          ),
        ),
      );
    }
  }
}
