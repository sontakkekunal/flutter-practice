import "package:flutter/material.dart";
import "package:google_fonts/google_fonts.dart";

class BottomShitDemo extends StatefulWidget {
  const BottomShitDemo({super.key});
  @override
  State createState() => _BottomShitDemoState();
}

class _BottomShitDemoState extends State {
  TextEditingController _textEditingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Bottom ShitDemo"),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_a_photo_rounded),
        onPressed: () {
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  width: double.infinity,
                  height: double.infinity,
                  padding: const EdgeInsets.all(20),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                            spreadRadius: 1,
                            color: Color.fromRGBO(0, 0, 0, 0.1),
                            offset: Offset(0, 7),
                            blurRadius: 20),
                      ],
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        "Create Task",
                        style: GoogleFonts.quicksand(
                            fontSize: 25, fontWeight: FontWeight.w700),
                      ),
                      TextField(
                        controller: _textEditingController,
                        decoration: InputDecoration(
                            hintText: "Enter title",
                            labelText: "Title",
                            
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide(
                                color: Colors.green,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(color: Colors.blue))),
                      ),
                      TextField(
                        controller: _textEditingController,
                        decoration: InputDecoration(
                            hintText: "Enter Description",
                            labelText: "Description",
                            
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide(
                                color: Colors.green,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(color: Colors.blue))),
                      ),
                      TextField(
                        controller: _textEditingController,
                        decoration: InputDecoration(
                            hintText: "Enter Description",
                            labelText: "Description",
                            
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide(
                                color: Colors.green,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(20),
                                borderSide: BorderSide(color: Colors.blue))),
                      )
                    ],
                  ),
                );
              });
        },
      ),
    );
  }
}
