import "package:flutter/material.dart";

class NetflixApp extends StatefulWidget {
  const NetflixApp({super.key});

  @override
  State<StatefulWidget> createState() => _NetflixApp();
}

class _NetflixApp extends State<NetflixApp> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Netflix ",
          style: TextStyle(
              fontStyle: FontStyle.italic,
              fontSize: 35,
              color: Color.fromARGB(255, 255, 17, 1)),
        ),
        backgroundColor: const Color.fromARGB(255, 221, 147, 51),
        
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Row(
              children: [
                Text(
                  "Movies",
                  style: TextStyle(fontSize: 30),
                )
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Image.network(
                    "https://assets-in.bmscdn.com/discovery-catalog/events/tr:w-400,h-600,bg-CCCCCC/et00311762-lmdexnggxy-portrait.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://www.gamespot.com/a/uploads/original/1179/11799911/3719922-screenshot2020-08-12at10.28.27pm.png",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://cdn.gulte.com/wp-content/uploads/2023/11/F-tQWpkWAAAU9aY-scaled.jpeg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://m.media-amazon.com/images/I/81v1NMJBrlL._AC_UF894,1000_QL80_.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://m.media-amazon.com/images/I/81XXxWVKbOL._AC_UF1000,1000_QL80_.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const Row(
              children: [
                Text(
                  "Web Series",
                  style: TextStyle(fontSize: 30),
                )
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                 Image.network(
                    "https://rukminim2.flixcart.com/image/850/1000/l0h1g280/poster/w/g/d/small-mirzapur-web-series-poster-multicolor-photopaper-print-12-original-imagc95dpwwmqcbg.jpeg?q=90&crop=false",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://lumiere-a.akamaihd.net/v1/images/p_disneyplusoriginals_loki_s2_finale_poster_v1_613_36711b22.jpeg?region=0%2C0%2C540%2C810",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://upload.wikimedia.org/wikipedia/en/c/c9/Loki_season_1_poster.jpeg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://i.ytimg.com/vi/GtnQvSdkDJQ/sddefault.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://cdn.marvel.com/content/1x/halfstack_busshelter_48x70_moonknight_v7_lg_0.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            const Row(
              children: [
                Text(
                  "Most popular",
                  style: TextStyle(fontSize: 30),
                )
              ],
            ),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                 Image.network(
                    "https://mir-s3-cdn-cf.behance.net/projects/404/a230cf164031123.Y3JvcCwzMDAwLDIzNDYsMCwxMjYy.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://upload.wikimedia.org/wikipedia/en/4/4c/The_Night_Manager_%28Indian_TV_series%29.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://m.media-amazon.com/images/M/MV5BZDI4OTM1ZjMtOWQxMC00OTY5LTg3NjQtMjlhMWVjODFlYTY4XkEyXkFqcGdeQXVyMTYzMTU3Njgx._V1_.jpg",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://akm-img-a-in.tosshub.com/indiatoday/inline-images/WhatsApp%20Image%202023-05-18%20at%2010.05.13%20AM.jpeg?VersionId=UmLQKWlWF3XqQg9T7wl_ldpMP6OgQ_oP",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                  const SizedBox(width: 20,),
                   Image.network(
                    "https://i0.wp.com/boxofficeworldwide.com/wp-content/uploads/2023/07/Gadar-2.webp?fit=640%2C1024&ssl=1",
                    height: 250,
                    width: 175,
                    fit: BoxFit.cover,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
