import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';

Widget getCircleContOrange({
  required double height,
  required double width,
}) {
  return Container(
    height: height,
    width: width,
    decoration: const BoxDecoration(
      color: Color.fromRGBO(253, 194, 40, 1),
      shape: BoxShape.circle,
    ),
  );
}

Widget getLogo({required double boxHeight,required double boxWidth,required Color fontColor,required double fontSize,required FontWeight fontWeight}){
return Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
            height: boxHeight,
            width: boxWidth,
          ),
          Text(
            'Tap’nPay',
            style: GoogleFonts.sora(
                  fontSize: fontSize,
                  color: fontColor,
                  fontWeight: fontWeight),
          ),
          Positioned(
              top: 0,
              right: 0,
              child: getCircleContOrange(
                  height: boxHeight * 0.328, width: boxWidth * 0.089)),
          Positioned(
              top: 0,
              right: 10,
              child: getCircleContOrange(
                  height: boxHeight * 0.164, width: boxWidth * 0.044)),
        ],
      );
}