import 'package:flutter/material.dart';
import 'package:wallet_app/View/commanWidget.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        body: Center(
          child: getLogo(
              boxHeight: height * 0.062,
              boxWidth: width * 0.49,
              fontColor: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.w600),
        ));
  }
}
