import 'package:flutter/material.dart';
import 'package:wallet_app/View/commanWidget.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Color.fromRGBO(247, 244, 255, 1),
      body: Column(
        children: [
          getLogo(
              boxHeight: height * 0.027,
              boxWidth: width * 0.0244,
              fontColor: Theme.of(context).primaryColor,
              fontSize: 16,
              fontWeight: FontWeight.w600),
          Image.asset('images/tapPay.png'),
          Expanded(
              child: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Column(
              children: [],
            ),
          ))
        ],
      ),
    );
  }
}
