import 'package:bicycle_ui/Model/Product.dart';
import 'package:bicycle_ui/Model/ProductTypeModel.dart';
import 'package:flutter/material.dart';

class ProductTypeController extends ChangeNotifier {
  List<ProductTypeModel> productTypeList = [
    ProductTypeModel(
        typeImgPath: 'images/onlineImage/cycleType.png',
        productList: [
          Product(
              imgPath: [
                'images/onlineImage/kicrossImg1.png',
                'images/onlineImage/bicycle1.png',
                'images/onlineImage/bicycle2.png'
              ],
              name: 'Kicross',
              type: 'Road Bike',
              price: 1599.99,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.'),
          Product(
              imgPath: [
                'images/onlineImage/gtBikeImg1.png',
                'images/onlineImage/bicycle3.png',
                'images/onlineImage/bicycle4.png'
              ],
              name: 'GT Bike',
              type: 'Road Bike',
              price: 2599.99,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.'),
          Product(
              imgPath: [
                'images/onlineImage/scotImg1.png',
                'images/onlineImage/bicycle5.png',
                'images/onlineImage/bicycle1.png'
              ],
              name: 'Scott',
              type: 'Road Bike',
              price: 2399.99,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.'),
          Product(
              imgPath: [
                'images/onlineImage/rossImg1.png',
                'images/onlineImage/bicycle6.png',
                'images/onlineImage/bicycle2.png'
              ],
              name: 'Ross',
              type: 'Road Bike',
              price: 1999.99,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.')
        ]),
    ProductTypeModel(
        typeImgPath: 'images/onlineImage/hemletType.png',
        productList: [
          Product(
              imgPath: [
                'images/onlineImage/helmet1.png',
                'images/onlineImage/helmet2.png',
                'images/onlineImage/helmet3.png'
              ],
              name: 'Helmet 1',
              type: 'Road Helmet',
              price: 250.33,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.'),
          Product(
              imgPath: [
                'images/onlineImage/helmet4.png',
                'images/onlineImage/helmet2.png',
                'images/onlineImage/helmet5.png'
              ],
              name: 'Helmet 2',
              type: 'Road Helmet',
              price: 220.40,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.')
        ]),
    ProductTypeModel(
        typeImgPath: 'images/onlineImage/glowesType.png',
        productList: [
          Product(
              imgPath: [
                'images/onlineImage/glows1.png',
                'images/onlineImage/glows2.png',
                'images/onlineImage/glows3.png'
              ],
              name: 'Glows 1',
              type: 'Road Glows',
              price: 10.42,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.')
        ]),
    ProductTypeModel(
        typeImgPath: 'images/onlineImage/bottleType.png',
        productList: [
          Product(
              imgPath: [
                'images/onlineImage/bottle1.png',
                'images/onlineImage/bottle2.png',
                'images/onlineImage/bottle3.png'
              ],
              name: 'Bottle 1',
              type: 'Road Bottle',
              price: 100.12,
              description:
                  'Lorem ipsum dolor sit amet. Ab tenetur molestias vel rerum cupiditate qui dolores consequatur et asperiores sunt ea magnam dolorem qui consectetur omnis. Ut error voluptas qui tempora provident aut necessitatibus voluptas rem eveniet nulla ut accusantium dignissimos aut facilis perspiciatis a natus quia.')
        ]),
  ];
  void refreash() {
    notifyListeners();
  }
}
