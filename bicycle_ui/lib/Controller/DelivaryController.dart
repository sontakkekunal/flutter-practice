import 'package:bicycle_ui/Model/Product.dart';
import 'package:flutter/material.dart';

class Delivarycontroller extends ChangeNotifier {
  double delivaryFree = 0;
  List<Product> delivaryProductList = [];
  bool checkOut = false;
  bool isDelivaryFree = false;
  void refreah() {
    notifyListeners();
  }

  double subtotal = 0;
  double discount = 0;
  void add(Product product) {
    if (delivaryProductList.contains(product)) {
      product.buyCount++;
    } else {
      product.buyCount++;
      delivaryProductList.add(product);
    }
  }

  void remove(Product product) {
    if (product.buyCount == 1) {
      delivaryProductList.remove(product);
    }
    product.buyCount--;
  }

  double total = 0;
  void makeBill() {
    getSubTotal();
    discount = delivaryProductList.length * 10;
    delivaryFree = 0;

    if (!isDelivaryFree) {
      for (int i = 0; i < delivaryProductList.length; i++) {
        delivaryFree += delivaryProductList[i].buyCount * 5;
      }
    }
    if (checkOut) {
      total = subtotal * ((100 - discount) / 100);
    } else {
      total = subtotal;
    }
  }

  double getSubTotal() {
    subtotal = 0;
    for (int i = 0; i < delivaryProductList.length; i++) {
      subtotal = subtotal +
          (delivaryProductList[i].price * delivaryProductList[i].buyCount);
    }
    return subtotal;
  }
}
