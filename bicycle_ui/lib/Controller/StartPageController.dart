import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';

class StartPageController extends ChangeNotifier {
  bool displayImg = true;
  final player = AudioPlayer();

  void playAudio({required String path}) async {
    await player.play(AssetSource(path), volume: 10000);
  }

  void changeDisplay({required bool makeChange}) {
    if (makeChange) {
      playAudio(path: 'bell1.mp3');
    } else {
      playAudio(path: 'bell2.mp3');
    }

    displayImg = makeChange;
    notifyListeners();
  }
}
