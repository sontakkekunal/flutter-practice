import 'dart:developer';

import 'package:bicycle_ui/Controller/ProductTypeController.dart';
import 'package:bicycle_ui/Controller/StartPageController.dart';
import 'package:bicycle_ui/Model/Product.dart';
import 'package:bicycle_ui/view/Navbar.dart';
import 'package:bicycle_ui/view/ProductDetail.dart';
import 'package:bicycle_ui/view/commanWidget.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class ProductsPage extends StatefulWidget {
  const ProductsPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ProductsPageState();
  }
}

class _ProductsPageState extends State {
  int _selectedIndex = 0;
  void changeProductList(int index, ProductTypeController obj) {
    _selectedIndex = index;
    obj.refreash();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    log('Product Page build');
    return CustomBackGround(
      bottomWidgetColor: Theme.of(context).primaryColor,
      bottomNavigationBar: Navbar(selectedIndex: 0, context: context),
      child: Padding(
        padding: EdgeInsets.only(
            left: width * 0.041, right: width * 0.041, top: height * 0.053),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    'Choose Your Bicycle',
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                ),
                SizedBox(
                  width: width * 0.33,
                ),
                Container(
                    height: height * 0.053,
                    width: width * 0.098,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: getTwoGradiantMix()),
                    alignment: Alignment.center,
                    child: Image.asset('images/onlineImage/search.png'))
              ],
            ),
            SizedBox(
              height: height * 0.020,
            ),
            Container(
              height: height * 0.20,
              width: width * 0.91,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  gradient: getTwoGradiant_2(),
                  boxShadow: const [
                    BoxShadow(
                        offset: Offset(4, 7),
                        blurRadius: 10,
                        color: Color.fromRGBO(0, 0, 0, 0.5))
                  ],
                  border: Border.all(
                      width: 2, color: const Color.fromRGBO(0, 0, 0, 0))),
              alignment: Alignment.center,
              child: Row(
                children: [
                  Image.asset('images/onlineImage/offerImg.png'),
                  Container(
                    margin: const EdgeInsets.symmetric(vertical: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'EXTREME',
                          style: GoogleFonts.allertaStencil(
                              fontSize: 32,
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(255, 255, 255, 1)),
                        ),
                        Text(
                          '30% OFF',
                          style: Theme.of(context).textTheme.displayLarge,
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: height * 0.018,
            ),
            SizedBox(
                height: height * 0.053,
                width: width * 0.65,
                child: Consumer<ProductTypeController>(
                  builder: (context, ProductTypeController value, child) {
                    return ListView.separated(
                      separatorBuilder: (context, index) => SizedBox(
                        width: width * 0.065,
                      ),
                      scrollDirection: Axis.horizontal,
                      itemCount: value.productTypeList.length,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            if (index == 0) {
                              Provider.of<StartPageController>(context,
                                      listen: false)
                                  .playAudio(path: 'bell1.mp3');
                            } else if (index == 1) {
                              Provider.of<StartPageController>(context,
                                      listen: false)
                                  .playAudio(path: 'helmet.mp3');
                            } else if (index == 2) {
                              Provider.of<StartPageController>(context,
                                      listen: false)
                                  .playAudio(path: 'glows.mp3');
                            } else if (index == 3) {
                              Provider.of<StartPageController>(context,
                                      listen: false)
                                  .playAudio(path: 'bottle.mp3');
                            }
                            changeProductList(index, value);
                          },
                          child: Container(
                              height: height * 0.053,
                              width: width * 0.098,
                              padding: const EdgeInsets.all(4),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1,
                                      color: (_selectedIndex == index)
                                          ? const Color.fromRGBO(
                                              255, 255, 255, 0.2)
                                          : const Color.fromRGBO(0, 0, 0, 0.2)),
                                  borderRadius: BorderRadius.circular(10),
                                  gradient: (_selectedIndex == index)
                                      ? getTwoGradiantMix()
                                      : const LinearGradient(colors: [
                                          Color.fromRGBO(255, 255, 255, 1),
                                          Color.fromRGBO(161, 161, 161, 1)
                                        ])),
                              alignment: Alignment.center,
                              child: Image.asset(
                                  value.productTypeList[index].typeImgPath)),
                        );
                      },
                    );
                  },
                )),
            SizedBox(
              height: height * 0.018,
            ),
            Expanded(child: Consumer<ProductTypeController>(
              builder: (context, ProductTypeController value, child) {
                List<Product> productList =
                    value.productTypeList[_selectedIndex].productList;
                return GridView.builder(
                  padding: EdgeInsets.zero,
                  itemCount: productList.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisSpacing: width * 0.039,
                      childAspectRatio: (height / width) * 0.35,
                      mainAxisSpacing: height * 0.0182,
                      crossAxisCount: width ~/ 150),
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        if (_selectedIndex == 0) {
                          Provider.of<StartPageController>(context,
                                  listen: false)
                              .playAudio(path: 'bell2.mp3');
                        } else if (_selectedIndex == 1) {
                          Provider.of<StartPageController>(context,
                                  listen: false)
                              .playAudio(path: 'helmet.mp3');
                        } else if (_selectedIndex == 2) {
                          Provider.of<StartPageController>(context,
                                  listen: false)
                              .playAudio(path: 'glows.mp3');
                        } else if (_selectedIndex == 3) {
                          Provider.of<StartPageController>(context,
                                  listen: false)
                              .playAudio(path: 'bottle.mp3');
                        }
                        Navigator.of(context)
                            .push(PageTransition(
                                child: ProductDetail(
                                    productIndex: index,
                                    productTypeIndex: _selectedIndex),
                                type: PageTransitionType.fade))
                            .then((value) {
                          Provider.of<ProductTypeController>(context,
                                  listen: false)
                              .refreash();
                        });
                        //             Navigator.of(context).push(MaterialPageRoute(
                        //               builder: (context) {
                        //                 return ProductDetail(
                        //                     productIndex: index,
                        //                     productTypeIndex: _selectedIndex);
                        //               },
                        //             )).then((value){
                        //               Provider.of<ProductTypeController>(context,listen: false).refreash();
                        //             });
                      },
                      child: Container(
                        padding: EdgeInsets.symmetric(
                            vertical: height * 0.0128,
                            horizontal: width * 0.014),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            gradient: getTwoGradiant_2(),
                            border: Border.all(
                                width: 1,
                                color:
                                    const Color.fromRGBO(255, 255, 255, 0.2))),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Hero(
                              tag: productList[index].imgPath,
                              child: Image.asset(
                                productList[index].imgPath[0],
                                height: height * 0.167,
                                width: width * 0.935,
                                alignment: Alignment.center,
                              ),
                            ),
                            Text(
                              productList[index].type,
                              style: Theme.of(context).textTheme.labelSmall,
                            ),
                            Hero(
                              tag: productList[index].name,
                              child: Text(
                                productList[index].name,
                                style: Theme.of(context).textTheme.labelMedium,
                              ),
                            ),
                            Hero(
                              tag: productList[index].price,
                              child: Text(
                                "\$${getFormatedNum(num: productList[index].price)}",
                                style:
                                    Theme.of(context).textTheme.headlineSmall,
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                );
              },
            ))
          ],
        ),
      ),
    );
  }
}
