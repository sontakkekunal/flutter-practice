import 'package:bicycle_ui/Controller/DelivaryController.dart';
import 'package:bicycle_ui/Model/Product.dart';
import 'package:bicycle_ui/view/MyShape.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';

final NumberFormat numberFormat = NumberFormat.decimalPattern('hi');
String getFormatedNum({required double num}) {
  return numberFormat.format(num);
}

class CustomBackGround extends StatelessWidget {
  Color bottomWidgetColor;
  Widget? floatingActionButton;
  Widget? bottomNavigationBar;
  Color? textColor;
  Widget child;
  CustomBackGround(
      {super.key,
      this.textColor,
      required this.bottomWidgetColor,
      required this.child,
      this.bottomNavigationBar,
      this.floatingActionButton});
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: CustomPaint(
        painter: MyShape(bottomWidgetColor: bottomWidgetColor),
        child: SizedBox(
          height: double.maxFinite,
          width: double.maxFinite,
          child: Stack(
            alignment: Alignment.topCenter,
            clipBehavior: Clip.none,
            children: [
              Positioned(
                right: -width * 0.085,
                top: height * 0.150,
                child: RotatedBox(
                  quarterTurns: 1,
                  child: Text(
                    'EXTREME',
                    style: GoogleFonts.allertaStencil(
                        fontSize: 150,
                        fontWeight: FontWeight.w400,
                        color: textColor ??
                            const Color.fromRGBO(255, 255, 255, 0.5)),
                  ),
                ),
              ),
              child
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomNavigationBar,
      floatingActionButton: floatingActionButton,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

LinearGradient getTwoGradiantMix() {
  return const LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      stops: [
        0.2,
        0.75,
        2
      ],
      colors: [
        Color.fromRGBO(55, 182, 233, 1),
        Color.fromRGBO(72, 92, 236, 1),
        Color.fromRGBO(75, 76, 237, 1),
      ]);
}

LinearGradient getTwoGradiant_2() {
  return const LinearGradient(
      begin: Alignment.topLeft,
      end: Alignment.bottomRight,
      colors: [
        Color.fromRGBO(53, 63, 84, 0.6),
        Color.fromRGBO(34, 40, 52, 0.6)
      ]);
}

Widget getBackBar({
  required String title,
  required BuildContext context,
}) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Hero(
        tag: title,
        child: Text(
          title,
          style: Theme.of(context).textTheme.headlineLarge,
        ),
      ),
      GestureDetector(
        onTap: () => Navigator.of(context).pop(),
        child: Container(
            height: height * 0.053,
            width: width * 0.098,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: getTwoGradiantMix()),
            alignment: Alignment.center,
            child: const Icon(
              Icons.arrow_back_ios_new_sharp,
              color: Colors.white,
            )),
      )
    ],
  );
}

Widget choiceCont(
    {required String title,
    required double height,
    required isSelected,
    required BuildContext context}) {
  return Container(
    height: height * 0.037,
    padding: const EdgeInsets.symmetric(horizontal: 5),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
            width: 1, color: const Color.fromRGBO(255, 255, 255, 0.5)),
        color: isSelected
            ? Theme.of(context).primaryColor
            : Theme.of(context).scaffoldBackgroundColor,
        boxShadow: isSelected
            ? const [
                BoxShadow(
                  offset: Offset(0, 4),
                  color: Color.fromRGBO(0, 0, 0, 0.25),
                  blurRadius: 5,
                )
              ]
            : null),
    alignment: Alignment.center,
    child: Text(
      title,
      style: Theme.of(context)
          .textTheme
          .headlineSmall!
          .copyWith(color: Colors.white),
    ),
  );
}

Widget addRemove(
    {required Product product,
    required Delivarycontroller obj,
    required BuildContext context}) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 5),
    decoration: BoxDecoration(
        color: const Color.fromRGBO(30, 30, 30, 0.8),
        borderRadius: BorderRadius.circular(6),
        boxShadow: const [
          BoxShadow(
              offset: Offset(0, 1),
              blurRadius: 3,
              color: Color.fromRGBO(0, 0, 0, 0.5))
        ]),
    width: width * 0.155,
    height: height * 0.033,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        GestureDetector(
          onTap: () {
            product.buyCount++;
            obj.refreah();
          },
          child: Container(
            height: height * 0.024,
            width: width * 0.046,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(3),
              gradient: const LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [
                    0.2,
                    0.75,
                  ],
                  colors: [
                    Color.fromRGBO(55, 182, 233, 1),
                    Color.fromRGBO(75, 76, 237, 1),
                  ]),
            ),
            alignment: Alignment.center,
            child: const Icon(
              Icons.add,
              color: Colors.white,
              size: 20,
            ),
          ),
        ),
        Text(
          '${product.buyCount}',
          style: GoogleFonts.allertaStencil(
              fontSize: 10,
              fontWeight: FontWeight.w400,
              color: const Color.fromRGBO(255, 255, 255, 1)),
        ),
        GestureDetector(
          onTap: () {
            obj.remove(product);
            obj.refreah();
          },
          child: Container(
            height: height * 0.024,
            width: width * 0.046,
            //padding: EdgeInsets.only(left: 1),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              gradient: const LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  stops: [
                    0.2,
                    0.75,
                  ],
                  colors: [
                    Color.fromRGBO(255, 255, 255, 0.8),
                    Color.fromRGBO(195, 195, 195, 0.2),
                  ]),
            ),
            alignment: Alignment.center,
            child: const Icon(
              Icons.remove,
              color: Colors.white,
              size: 20,
            ),
          ),
        )
      ],
    ),
  );
}
