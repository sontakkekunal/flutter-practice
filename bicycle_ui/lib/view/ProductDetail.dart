import 'dart:developer';

import 'package:bicycle_ui/Controller/DelivaryController.dart';
import 'package:bicycle_ui/Controller/ProductTypeController.dart';
import 'package:bicycle_ui/Model/Product.dart';
import 'package:bicycle_ui/view/CartScreen.dart';
import 'package:bicycle_ui/view/commanWidget.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ProductDetail extends StatefulWidget {
  int productTypeIndex;
  int productIndex;
  ProductDetail(
      {super.key, required this.productIndex, required this.productTypeIndex});

  @override
  State<StatefulWidget> createState() {
    return _ProductDetailState();
  }
}

class _ProductDetailState extends State<ProductDetail> {
  final CarouselSliderController _carouselSliderController =
      CarouselSliderController();

  void changeImage({required int index, required ProductTypeController obj}) {
    activeImageIndex = index;

    obj.refreash();
  }

  bool isdesc = false;
  int activeImageIndex = 0;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    log('In product Detail page');
    return CustomBackGround(
      bottomWidgetColor: Theme.of(context).primaryColor,
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        onPressed: () {},
        shape: const RoundedRectangleBorder(),
        label: Container(
          width: width,
          height: height * 0.4,
          padding: EdgeInsets.symmetric(horizontal: width * 0.05),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Consumer<ProductTypeController>(
                builder: (context, value, child) {
                  Product product = value
                      .productTypeList[widget.productTypeIndex]
                      .productList[widget.productIndex];
                  return Text(
                    '\$${getFormatedNum(num: product.price)}',
                    style: Theme.of(context).textTheme.headlineLarge,
                  );
                },
              ),
              GestureDetector(
                onTap: () {
                  Provider.of<Delivarycontroller>(context, listen: false).add(
                      Provider.of<ProductTypeController>(context, listen: false)
                          .productTypeList[widget.productTypeIndex]
                          .productList[widget.productIndex]);
                  Navigator.of(context).push(PageTransition(
                      child: const CartScreen(),
                      type: PageTransitionType.theme));
                },
                child: Container(
                  width: width * 0.43,
                  height: height * 0.0537,
                  decoration: BoxDecoration(
                      gradient: const LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          stops: [
                            0.2,
                            0.75,
                          ],
                          colors: [
                            Color.fromRGBO(55, 182, 233, 1),
                            Color.fromRGBO(75, 76, 237, 1),
                          ]),
                      borderRadius: BorderRadius.circular(20)),
                  alignment: Alignment.center,
                  child: Text(
                    'Buy Now',
                    style: Theme.of(context).textTheme.headlineLarge,
                  ),
                ),
              )
            ],
          ),
        ),
      ),
      child: Column(
        children: [
          Padding(
              padding: EdgeInsets.only(
                  left: width * 0.0465,
                  right: width * 0.0465,
                  top: height * 0.065),
              child: Column(
                children: [
                  Consumer<ProductTypeController>(
                    builder: (context, ProductTypeController value, child) {
                      Product product = value
                          .productTypeList[widget.productTypeIndex]
                          .productList[widget.productIndex];
                      return Column(
                        children: [
                          getBackBar(title: product.name, context: context),
                          SizedBox(
                            height: height * 0.032,
                          ),
                          CarouselSlider.builder(
                              carouselController: _carouselSliderController,
                              itemCount: product.imgPath.length,
                              itemBuilder: (context, index, realIndex) {
                                return Hero(
                                    tag: product.imgPath,
                                    child: Image.asset(
                                      product.imgPath[index],
                                      height: height * 0.42,
                                    ));
                              },
                              options: CarouselOptions(
                                  onPageChanged: (index, reason) {
                                    changeImage(index: index, obj: value);
                                  },
                                  aspectRatio: 0.922,
                                  autoPlay: true,
                                  enlargeCenterPage: true,
                                  pageSnapping: false,
                                  enableInfiniteScroll: false,
                                  viewportFraction: 1)),
                          AnimatedSmoothIndicator(
                            activeIndex: activeImageIndex,
                            onDotClicked: (index) {
                              _carouselSliderController.animateToPage(index,
                                  duration: const Duration(milliseconds: 300));
                            },
                            count: product.imgPath.length,
                            effect: JumpingDotEffect(
                                jumpScale: 3,
                                dotHeight: height * 0.0107,
                                dotWidth: width * 0.023,
                                activeDotColor:
                                    const Color.fromRGBO(0, 0, 0, 1),
                                dotColor: Colors.white),
                          ),
                        ],
                      );
                    },
                  ),
                ],
              )),
          SizedBox(
            height: height * 0.021,
          ),
          Expanded(
            child: Container(
                padding: EdgeInsets.all(width * 0.05),
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20)),
                    gradient: getTwoGradiant_2(),
                    border: Border.all(
                        width: 2,
                        color: const Color.fromRGBO(255, 255, 255, 0.2))),
                alignment: Alignment.topCenter,
                child: Consumer<ProductTypeController>(
                  builder: (context, value, child) {
                    Product product = value
                        .productTypeList[widget.productTypeIndex]
                        .productList[widget.productIndex];
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                isdesc = !isdesc;
                                value.refreash();
                              },
                              child: choiceCont(
                                  title: 'Description',
                                  height: height,
                                  isSelected: isdesc,
                                  context: context),
                            ),
                            GestureDetector(
                              onTap: () {
                                isdesc = !isdesc;
                                value.refreash();
                              },
                              child: choiceCont(
                                  title: 'Specification',
                                  height: height,
                                  isSelected: !isdesc,
                                  context: context),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: height * 0.037,
                        ),
                        Text(
                          isdesc ? '' : product.description,
                          style: Theme.of(context).textTheme.displaySmall,
                        )
                      ],
                    );
                  },
                )),
          )
        ],
      ),
    );
  }
}
