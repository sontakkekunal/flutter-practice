import 'package:flutter/widgets.dart';

class MyShape extends CustomPainter {
  Color bottomWidgetColor;
  MyShape({required this.bottomWidgetColor});
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint();
    paint.color = bottomWidgetColor;

    Path path_1 = Path();
    path_1.moveTo(size.width * -0.0005814, size.height * 0.5586159);
    path_1.lineTo(size.width * 0.9988380, size.height * 0.3577546);
    path_1.lineTo(size.width * 0.9988380, size.height * 0.9987641);
    path_1.lineTo(size.width * -0.0043010, size.height * 0.9987641);
    path_1.lineTo(size.width * -0.0005814, size.height * 0.5586159);
    canvas.drawPath(path_1, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
