import 'package:bicycle_ui/Controller/StartPageController.dart';
import 'package:bicycle_ui/view/ProductsPage.dart';
import 'package:bicycle_ui/view/commanWidget.dart';
import 'package:flutter/material.dart';
import 'dart:developer';

import 'package:flutter_cube/flutter_cube.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class StartPage extends StatefulWidget {
  const StartPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _StartPageState();
  }
}

class _StartPageState extends State {
  Object bicycleObj = Object(fileName: 'assets/bicycle/Bike.obj');

  Widget getDisplay({required bool val, required width}) {
    if (val) {
      return Stack(
        children: [
          Positioned(
            left: 0,
            right: -width * 0.232,
            top: 0,
            bottom: 0,
            //right: -width * 0.110,
            child: Transform.flip(
                flipX: true,
                child: Image.asset(
                  'images/offlineImage/startImg.png',
                  // height: height * 0.536,
                  // width: width,
                  alignment: Alignment.center,

                  //fit: BoxFit.cover,
                )),
          )
        ],
      );
    } else {
      return Cube(
        onSceneCreated: (Scene scene) {
          scene.world.add(bicycleObj);
          scene.camera.zoom = 10;
        },
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    log('In StartPage build');
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return CustomBackGround(
        bottomWidgetColor: Theme.of(context).primaryColor,
        child: Padding(
          padding: EdgeInsets.only(top: height * 0.053),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset('images/offlineImage/bicycleIcon.png'),
              SizedBox(
                height: height * 0.195,
              ),
              SizedBox(
                  height: height * 0.536,
                  width: double.maxFinite,
                  child: Consumer<StartPageController>(
                    builder: (context, value, child) {
                      return GestureDetector(
                          onTap: () async {
                            value.changeDisplay(makeChange: !value.displayImg);
                          },
                          child:
                              getDisplay(val: value.displayImg, width: width));
                    },
                  )),
              SizedBox(
                height: height * 0.035,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      Provider.of<StartPageController>(context, listen: false)
                          .playAudio(path: 'bell2.mp3');
                      // Navigator.of(context).push(MaterialPageRoute(
                      //   builder: (context) => const ProductsPage(),
                      // ));
                      Navigator.of(context).push(PageTransition(
                          child: const ProductsPage(),
                          type: PageTransitionType.rightToLeft));
                    },
                    child: Container(
                      height: height * 0.0815,
                      width: width * 0.63,
                      decoration: BoxDecoration(
                          color: Theme.of(context).scaffoldBackgroundColor,
                          borderRadius: const BorderRadius.only(
                              topLeft: Radius.circular(38),
                              bottomLeft: Radius.circular(38))),
                      child: Row(
                        children: [
                          Container(
                            height: height * 0.075,
                            width: width * 0.16,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    width: width * 0.01,
                                    color:
                                        const Color.fromRGBO(239, 186, 51, 1)),
                                shape: BoxShape.circle,
                                color: Theme.of(context).primaryColor),
                            alignment: Alignment.center,
                            child: const Icon(
                              Icons.arrow_forward_ios_sharp,
                              color: Colors.white,
                            ),
                          ),
                          SizedBox(
                            width: width * 0.10,
                          ),
                          Text(
                            'Get Start',
                            style: Theme.of(context).textTheme.labelLarge,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ));
  }
}
