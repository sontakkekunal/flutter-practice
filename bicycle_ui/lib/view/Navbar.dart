import 'package:bicycle_ui/Controller/DelivaryController.dart';
import 'package:bicycle_ui/view/commanWidget.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

import 'CartScreen.dart';
import 'ProductsPage.dart';

class Navbar extends StatelessWidget {
  int selectedIndex;
  BuildContext context;
  Navbar({super.key, required this.selectedIndex, required this.context});

  @override
  Widget build(BuildContext context) {
    bool checkBadge({required int index}) {
      if (index == 0) {
        return false;
      } else if (index == 1) {
        if (Provider.of<Delivarycontroller>(context)
            .delivaryProductList
            .isNotEmpty) {
          return true;
        }
        return false;
      } else {
        return false;
      }
    }

    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    List<String> iconPath = [
      'images/onlineImage/house.png',
      'images/onlineImage/bag.png',
      'images/onlineImage/walletadd.png',
      'images/onlineImage/user.png'
    ];
    return NavigationBar(
        backgroundColor: Theme.of(context).scaffoldBackgroundColor,
        destinations: List.generate(4, (index) {
          return GestureDetector(
              onTap: () {
                if (index == 0 && selectedIndex != 0) {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ProductsPage(),
                  ));
                } else if (index == 1 && selectedIndex != 1) {
                  Navigator.of(context).push(PageTransition(
                      child: const CartScreen(),
                      type: PageTransitionType.fade));
                }
              },
              child: Stack(
                children: [
                  Container(
                    width: width * 0.10,
                    margin: EdgeInsets.only(
                        left: width * 0.05,
                        right: width * 0.05,
                        top: height * 0.02),
                    height: double.maxFinite,
                    decoration: BoxDecoration(
                        gradient: (index == selectedIndex)
                            ? const LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                stops: [
                                    0.2,
                                    0.75,
                                  ],
                                colors: [
                                    Color.fromRGBO(55, 182, 233, 1),
                                    Color.fromRGBO(75, 76, 237, 1)
                                  ])
                            : null,
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15))),
                    child: Image.asset(iconPath[index]),
                  ),
                  (checkBadge(index: index))
                      ? Positioned(
                          right: width * 0.11,
                          top: height * 0.025,
                          child: Container(
                            height: height * 0.015,
                            width: width * 0.015,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              gradient: getTwoGradiantMix(),
                            ),
                          ))
                      : const SizedBox()
                ],
              ));
        }));
  }
}
