import 'dart:developer';

import 'package:bicycle_ui/Controller/DelivaryController.dart';
import 'package:bicycle_ui/Controller/StartPageController.dart';
import 'package:bicycle_ui/view/commanWidget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../Model/Product.dart';
class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CardScreenState();
  }
}

class _CardScreenState extends State {
  final GlobalKey<FormFieldState> _globalKey = GlobalKey<FormFieldState>();
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    Provider.of<StartPageController>(context).playAudio(path: 'money.mp3');
    log('In CardScreen build');
    return CustomBackGround(
        textColor: Colors.white12,
        bottomWidgetColor: const Color.fromARGB(255, 0, 0, 0),
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          child: Container(
            color: Theme.of(context).scaffoldBackgroundColor.withOpacity(0.5),
            child: SingleChildScrollView(
              child: Padding(
                padding: EdgeInsets.only(
                    left: width * 0.0465,
                    right: width * 0.0465,
                    top: height * 0.065),
                child: Column(
                  children: [
                    getBackBar(title: 'My Shopping Cart', context: context),
                    SizedBox(
                      height: height * 0.5,
                      width: width,
                      child: Consumer<Delivarycontroller>(
                        builder: (context, Delivarycontroller value, child) {
                          List<Product> delivaryProductList =
                              value.delivaryProductList;
                          return ListView.builder(
                            padding: const EdgeInsets.only(top: 8, bottom: 8),
                            itemCount: delivaryProductList.length,
                            itemBuilder: (context, index) {
                              Product product = delivaryProductList[index];
                              return Container(
                                height: height * 0.193,
                                width: width,
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    vertical: width * 0.05),
                                decoration: const BoxDecoration(
                                    //color: Colors.red,
                                    border: Border(
                                        bottom: BorderSide(
                                            width: 1.4,
                                            color: Color.fromRGBO(
                                                255, 255, 255, 0.7)))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Container(
                                      height: height * 0.145,
                                      width: width * 0.32,
                                      padding: const EdgeInsets.symmetric(
                                          horizontal: 10),
                                      decoration: BoxDecoration(
                                          border: Border.all(
                                              width: 2,
                                              color: const Color.fromRGBO(
                                                  255, 255, 255, 0.2)),
                                          boxShadow: const [
                                            BoxShadow(
                                                offset: Offset(5, 6),
                                                blurRadius: 10,
                                                spreadRadius: 1,
                                                color: Color.fromRGBO(
                                                    0, 0, 0, 0.25)),
                                            BoxShadow(
                                                offset: Offset(6, 6),
                                                blurRadius: 5,
                                                spreadRadius: 0,
                                                color: Colors.transparent
                                                //color: Color.fromRGBO(0, 0, 0, 0.25)
                                                )
                                          ],
                                          borderRadius:
                                              BorderRadius.circular(15),
                                          gradient: const LinearGradient(
                                              begin: Alignment.centerLeft,
                                              end: Alignment.centerRight,
                                              colors: [
                                                Color.fromRGBO(53, 63, 84, 0.6),
                                                Color.fromRGBO(34, 40, 54, 0)
                                              ])),
                                      child: Hero(
                                          tag: product.imgPath,
                                          child:
                                              Image.asset(product.imgPath[0])),
                                    ),
                                    SizedBox(
                                      width: width * 0.09,
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(
                                          vertical: 10),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Hero(
                                            tag: product.name,
                                            child: Text(
                                              product.name,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .labelLarge,
                                            ),
                                          ),
                                          Row(
                                            children: [
                                              Hero(
                                                tag: product.price,
                                                child: Text(
                                                  "\$${getFormatedNum(num: product.price)}",
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .labelMedium!
                                                      .copyWith(
                                                          color: const Color
                                                              .fromRGBO(
                                                              55, 182, 233, 1),
                                                          fontSize: 18),
                                                ),
                                              ),
                                              SizedBox(
                                                width: width * 0.085,
                                              ),
                                              addRemove(
                                                  product: product,
                                                  obj: value,
                                                  context: context)
                                            ],
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              );
                            },
                          );
                        },
                      ),
                    ),
                    Text(
                      'Your Bag Qualifies for Free Shipping',
                      style: Theme.of(context)
                          .textTheme
                          .labelSmall!
                          .copyWith(fontWeight: FontWeight.w300),
                    ),
                    SizedBox(
                      height: height * 0.02,
                    ),
                    SizedBox(
                      height: height * 0.05,
                      child: TextFormField(
                        key: _globalKey,
                        validator: (val) {
                          if (val == null || val.isEmpty) {
                            return "";
                          } else if (val != 'kunal') {
                            return 'Invalid Code';
                          }
                        },
                        style: Theme.of(context).textTheme.titleMedium,
                        decoration: InputDecoration(
                          hintText: '6Affg5',
                          hintStyle: Theme.of(context)
                              .textTheme
                              .labelLarge!
                              .copyWith(
                                  fontSize: 22,
                                  color:
                                      const Color.fromRGBO(142, 142, 142, 1)),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              if (_globalKey.currentState!.validate() && Provider.of<Delivarycontroller>(context,
                                  listen: false).delivaryProductList.isNotEmpty) {
                                Provider.of<Delivarycontroller>(context,
                                        listen: false)
                                    .isDelivaryFree = true;
                                Provider.of<Delivarycontroller>(context,
                                        listen: false)
                                    .refreah();
                                if (MediaQuery.of(context).viewInsets.bottom !=
                                    0) {
                                  FocusScope.of(context).unfocus();
                                }
                              }
                            },
                            child: Container(
                              height: height * 0.047,
                              width: width * 0.31,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(8),
                                gradient: const LinearGradient(
                                    begin: Alignment.centerLeft,
                                    end: Alignment.centerRight,
                                    stops: [
                                      0.2,
                                      0.75,
                                    ],
                                    colors: [
                                      Color.fromRGBO(55, 182, 233, 1),
                                      Color.fromRGBO(75, 76, 237, 1),
                                    ]),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                'Apply',
                                style: Theme.of(context)
                                    .textTheme
                                    .displayLarge!
                                    .copyWith(fontSize: 22),
                              ),
                            ),
                          ),
                          filled: true,
                          fillColor: const Color.fromRGBO(36, 44, 59, 0.6),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  color: Colors.red, width: 1)),
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  color: Colors.red, width: 1)),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.3),
                                  width: 1)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10),
                              borderSide: const BorderSide(
                                  color: Color.fromRGBO(0, 0, 0, 0.3),
                                  width: 1)),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: height * 0.035,
                    ),
                    SizedBox(
                      height: height * 0.15,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Subtotal',
                                style:
                                    Theme.of(context).textTheme.displayMedium,
                              ),
                              Text(
                                'Delivery fee',
                                style:
                                    Theme.of(context).textTheme.displayMedium,
                              ),
                              Text(
                                'Discount',
                                style:
                                    Theme.of(context).textTheme.displayMedium,
                              ),
                              Text(
                                'Total',
                                style:
                                    Theme.of(context).textTheme.displayMedium,
                              ),
                            ],
                          ),
                          Consumer<Delivarycontroller>(
                            builder: (context, value, child) {
                              value.makeBill();
                              return Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    '\$${value.subtotal}',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                  Text(
                                    '\$${value.delivaryFree}',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                  Text(
                                    '${value.discount}%',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                  Text(
                                    '\$${value.total}',
                                    style:
                                        Theme.of(context).textTheme.titleMedium,
                                  ),
                                ],
                              );
                            },
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      height: height * 0.030,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        GestureDetector(
                          onTap: () {
                            Provider.of<Delivarycontroller>(context,
                                    listen: false)
                                .checkOut = !Provider.of<Delivarycontroller>(
                                    context,
                                    listen: false)
                                .checkOut;
                            Provider.of<Delivarycontroller>(context,
                                    listen: false)
                                .refreah();
                          },
                          child: Container(
                            height: height * 0.0321,
                            width: width * 0.069,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              gradient: const LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  stops: [
                                    0.2,
                                    0.75,
                                  ],
                                  colors: [
                                    Color.fromRGBO(55, 182, 233, 1),
                                    Color.fromRGBO(75, 76, 237, 1),
                                  ]),
                            ),
                            alignment: Alignment.center,
                            child: Consumer<Delivarycontroller>(
                              builder: (context, value, child) {
                                return value.checkOut
                                    ? const Icon(
                                        Icons.check,
                                        color: Colors.white,
                                      )
                                    : const SizedBox();
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          width: width * 0.05,
                        ),
                        Text(
                          'Check Out',
                          style: Theme.of(context).textTheme.labelMedium,
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
