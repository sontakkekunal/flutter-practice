import 'package:bicycle_ui/Controller/DelivaryController.dart';
import 'package:bicycle_ui/Controller/ProductTypeController.dart';
import 'package:bicycle_ui/Controller/StartPageController.dart';
import 'package:bicycle_ui/view/startPage.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => StartPageController()),
        ChangeNotifierProvider(create: (context) => ProductTypeController()),
        ChangeNotifierProvider(create: (context) => Delivarycontroller())
      ],
      child: MaterialApp(
        theme: ThemeData(
            textTheme: TextTheme(
              headlineLarge: GoogleFonts.poppins(
                  fontSize: 30,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
              headlineSmall: GoogleFonts.poppins(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: const Color.fromRGBO(195, 195, 195, 1)),
              labelLarge: GoogleFonts.poppins(
                  fontSize: 25,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
              labelMedium: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
              labelSmall: GoogleFonts.poppins(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
              displayLarge: GoogleFonts.poppins(
                  fontSize: 32,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
              displaySmall: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.w500,
                  color: Colors.white),
              displayMedium: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
              titleMedium: GoogleFonts.poppins(
                  fontSize: 20,
                  fontWeight: FontWeight.w600,
                  color: const Color.fromARGB(255, 37, 176, 231)),
            ),
            primaryColor: const Color.fromRGBO(75, 76, 237, 1),

            scaffoldBackgroundColor: const Color.fromRGBO(36, 44, 59, 1)),
        debugShowCheckedModeBanner: false,
        home: const StartPage(),
      ),
    );
  }
}
