class Product {
  List<String> imgPath;
  String name;
  String type;
  double price;
  String description;
  int buyCount = 0;
  Product(
      {required this.imgPath,
      required this.name,
      required this.type,
      required this.price,
      required this.description});
}
