import 'package:bicycle_ui/Model/Product.dart';

class ProductTypeModel {
  String typeImgPath;
  List<Product> productList;
  ProductTypeModel({required this.typeImgPath, required this.productList});
}
