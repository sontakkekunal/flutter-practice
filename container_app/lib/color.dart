import 'package:flutter/material.dart';

class ColorsContainer extends StatelessWidget {
  const ColorsContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("ConatinerColor"),
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          color: Colors.red,
        ),
      ),
    );
  }
}
