import 'package:flutter/material.dart';

class MyBoxShadow extends StatelessWidget {
  const MyBoxShadow({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ConatinerBoxShadow"),
      ),
      body: Center(
        child: Container(
          height: 200,
          width: 200,
          decoration: const BoxDecoration(
            color: Colors.amber,
            borderRadius: BorderRadius.all(Radius.circular(20)),
            boxShadow: [
              BoxShadow(
                color: Colors.purple,offset: Offset(30,30),blurRadius: 8
              ),
              BoxShadow(
                color: Colors.red,offset: Offset(30,30),blurRadius: 8
              ),
              BoxShadow(
                color: Colors.green,offset: Offset(30,30),blurRadius: 8
              )
            ]

        ),
          ),
      ),
    );
  }
}
