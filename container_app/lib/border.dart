import 'package:flutter/material.dart';

class MyBorder extends StatelessWidget {
  const MyBorder({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ConatinerBorder"),
      ),
      body: Center(
        child: Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.yellow, width: 5),
              color: Colors.blueGrey),
        ),
      ),
    );
  }
}
