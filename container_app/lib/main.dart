import 'package:container_app/border.dart';
import 'package:container_app/boxShadow.dart';
import 'package:container_app/color.dart';
import 'package:container_app/gradient.dart';
import 'package:container_app/margin.dart';
import 'package:container_app/padding.dart';
import 'package:container_app/propertionWidget.dart';
import 'package:container_app/radius.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      //home:ColorsContainer()
      //home:PaddingC()
      //home: MyBorder(),
      //home:MyRadius()
     // home: MyMargin(),
     //home:MyBoxShadow()
     //home:MyGradient()
     home:Proportion()
    );
  }
}
