import 'package:flutter/material.dart';

class MyGradient extends StatelessWidget {
  const MyGradient({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ConatinerGradient"),
      ),
      body: Center(
        child: Container(
            height: 100,
            width: 100,
            decoration: BoxDecoration(
                color: Colors.amber,
                borderRadius: const BorderRadius.all(Radius.circular(20)),
                border: Border.all(
                    color: Colors.blue, width: 5, style: BorderStyle.solid),
                gradient: const LinearGradient(
                    stops: [0.5, 0.7], colors: [Colors.red, Colors.green]))),
      ),
    );
  }
}
