import 'package:flutter/material.dart';

class PaddingC extends StatelessWidget {
  const PaddingC({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ConatinerPadding"),
      ),
      body: Center(
        child: Container(
            height: 100,
            width: 100,
            color: Colors.blue,
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Container(
              height: 100,
              width: 100,
              color: Colors.red,
            )),
      ),
    );
  }
}
