import 'package:flutter/material.dart';

class Proportion extends StatelessWidget {
  const Proportion({super.key});
  
  @override
  Widget build(BuildContext context) {
    double height=MediaQuery.of(context).size.height;
    double width=MediaQuery.of(context).size.width;
    return Scaffold(
      body: Center(child: Row(
        children: [
          Container(
            height:50,
            width: width*0.6,
            color: Colors.red,
          ),
          Container(
            height:50,
            width: width*0.3,
            color: Colors.green,
          ),
          Container(
            height:50,
            width: width*0.1,
            color: Colors.blue,
          )
        ],
      ),),
    );
  }
}
