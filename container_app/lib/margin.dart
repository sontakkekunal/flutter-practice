import 'package:flutter/material.dart';

class MyMargin extends StatelessWidget {
  const MyMargin({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("ConatinerMargin"),
      ),
      body: Center(
        child: Container(
            height: 100,
            width: 100,
            color: Colors.blue,           
            child: Container(
              color: Colors.yellow,
              margin: const EdgeInsets.only(top:10,bottom: 10,left: 10),
              height: 50,
              width: 50,
            )),
      ),
    );
  }
}
