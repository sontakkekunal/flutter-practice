import 'package:flutter/material.dart';

class PaymentModel {
  String hint;
  GlobalKey<FormFieldState> key = GlobalKey<FormFieldState>();
  List<BankType> bankList;
  String val = ' ';
  PaymentModel({required this.hint, required this.bankList});
}

class BankType {
  String name;
  String? iconImage;
  Color backGroundColor;
  BankType({required this.name, this.iconImage, required this.backGroundColor});
}
