class ProductModel {
  String compyName;
  String productName;
  String color;
  int buyCount;
  int price;
  String productDetail;
  String sellerDeatil;
  int orderId;
  ProductModel(
      {required this.compyName,
      required this.productName,
      required this.color,
      required this.buyCount,
      required this.price,
      required this.productDetail,
      required this.sellerDeatil,
      required this.orderId});
}
