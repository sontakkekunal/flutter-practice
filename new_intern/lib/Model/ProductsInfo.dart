import 'package:new_intern/Model/ProductMinInfo.dart';

class ProductsInfo {
  String name;
  List<ProductMinInfo> productList;
  ProductsInfo({required this.name, required this.productList});
}
