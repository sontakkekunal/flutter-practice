class ProductMinInfo {
  String date;
  String compyName;
  String productName;
  String spec;
  String status;
  ProductMinInfo(
      {required this.date,
      required this.compyName,
      required this.productName,
      required this.spec,
      required this.status});
}
