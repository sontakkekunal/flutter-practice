import 'package:flutter/material.dart';
import 'package:new_intern/Controller/ProductController.dart';
import 'package:new_intern/Controller/ProductHistoryController.dart';
import 'package:new_intern/Model/AddressModel.dart';
import 'package:new_intern/view/CartScreeen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(
          create: (context) => AddressModel(),
        ),
        ChangeNotifierProvider(create: (context) => ProductController()),
        ChangeNotifierProvider(create: (context) => Producthistorycontroller())
      ],
      child: MaterialApp(
        theme: ThemeData(
            iconTheme:
                const IconThemeData(color: Color.fromRGBO(207, 207, 207, 1)),
            scaffoldBackgroundColor: const Color.fromRGBO(13, 13, 13, 1),
            appBarTheme: const AppBarTheme(
                backgroundColor: Color.fromRGBO(13, 13, 13, 1),
                centerTitle: true,
                titleTextStyle: TextStyle(
                    color: Color.fromRGBO(207, 207, 207, 1),
                    fontSize: 18,
                    fontWeight: FontWeight.w400)),
            primaryColor: const Color.fromRGBO(20, 20, 20, 1),
            primaryTextTheme: const TextTheme(
                labelLarge: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(151, 151, 151, 1)),
                labelMedium: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(207, 207, 207, 1)),
                titleLarge: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: Color.fromRGBO(207, 207, 207, 1)),
                titleMedium: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w400,
                    color: Color.fromRGBO(207, 207, 207, 1)))),
        debugShowCheckedModeBanner: false,
        home: const CartScreen(),
      ),
    );
  }
}
