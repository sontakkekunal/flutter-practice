import 'package:flutter/material.dart';
import 'package:new_intern/view/OrderConfirmation.dart';

class ProcessingScreen extends StatefulWidget {
  const ProcessingScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ProcessingScreenState();
  }
}

class _ProcessingScreenState extends State {
  getDelay() async {
    await Future.delayed(const Duration(milliseconds: 1500));
    Navigator.of(context).pushReplacement(MaterialPageRoute(
      builder: (context) => const OrderConfirmation(),
    ));
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getDelay();
  }

  @override
  Widget build(BuildContext context) {

    return const Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 75,
              width: 75,
              child: CircularProgressIndicator(
                strokeWidth: 8,
                semanticsValue: '10',
                color: Color.fromRGBO(243, 116, 19, 1),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Text(
              'Processing...',
              style: TextStyle(
                color: Color.fromRGBO(243, 116, 19, 1),
                fontWeight: FontWeight.w600,
                fontSize: 22,
              ),
            ),
            //getDelay()
          ],
        ),
      ),
    );
  }
}
