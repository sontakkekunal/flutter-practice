import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:new_intern/view/ProcessingScreen.dart';

class OTPScreen extends StatefulWidget {
  const OTPScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _OTPScreenState();
  }
}

class _OTPScreenState extends State {
  final List<GlobalKey<FormFieldState>> _otpKeys = [
    GlobalKey<FormFieldState>(),
    GlobalKey<FormFieldState>(),
    GlobalKey<FormFieldState>(),
    GlobalKey<FormFieldState>()
  ];
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back_ios_new_sharp,
            color: Color.fromRGBO(207, 207, 207, 1),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Enter Pin',
              style: Theme.of(context).primaryTextTheme.labelLarge!.copyWith(
                  fontSize: 22,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
            SizedBox(
              height: height * 0.02,
            ),
            SizedBox(
              width: width * 0.65,
              child: Text(
                  'Please enter the 4 digit Pin code to Complete the payment',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).primaryTextTheme.titleMedium),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 74,
                  width: width * 0.825,
                  margin: const EdgeInsets.only(top: 50),
                  child: ListView.separated(
                    itemCount: 4,
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (context, index) => const SizedBox(
                      width: 32,
                    ),
                    itemBuilder: (context, index) {
                      return Container(
                        height: 95,
                        width: 67,
                        padding: const EdgeInsets.all(1),

                        // decoration: BoxDecoration(
                        //   border: Border.all(width: 1,color: Color.fromRGBO(255, 255, 255, 1))
                        // ),
                        child: TextFormField(
                          key: _otpKeys[index],
                          textAlign: TextAlign.center,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(1),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          keyboardAppearance: Brightness.dark,
                          autofocus: true,
                          onChanged: (String value) {
                            if (index != 3 && value.isNotEmpty) {
                              FocusScope.of(context).nextFocus();
                            }
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              return "";
                            } else {}
                          },
                          keyboardType: TextInputType.phone,
                          style: Theme.of(context).primaryTextTheme.titleLarge,
                          decoration: InputDecoration(
                              filled: true,
                              hintStyle:
                                  Theme.of(context).primaryTextTheme.titleLarge,
                              fillColor: Colors.transparent,
                              hintText: "${index + 1}",
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1, color: Colors.red)),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1, color: Colors.red)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(255, 255, 255, 1))),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1,
                                      color:
                                          Color.fromRGBO(255, 255, 255, 1)))),
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        backgroundColor: const Color.fromRGBO(243, 116, 19, 1),
        label: SizedBox(
            width: width * 0.84,
            height: height * 0.07,
            child: Center(
                child: Text(
              'Confirm',
              style: Theme.of(context)
                  .primaryTextTheme
                  .labelMedium!
                  .copyWith(color: Colors.white),
            ))),
        onPressed: () {
          bool nav = true;
          for (int i = 0; i < _otpKeys.length; i++) {
            if (_otpKeys[i].currentState!.validate() == false) {
              nav = false;
            }
          }
          if (nav) {
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => const ProcessingScreen(),
            ));
          }
        },
      ),
    );
  }
}
