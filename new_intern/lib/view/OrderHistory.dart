import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:new_intern/Controller/ProductHistoryController.dart';
import 'package:new_intern/Model/ProductMinInfo.dart';
import 'package:provider/provider.dart';

class OrderHistory extends StatefulWidget {
  const OrderHistory({super.key});

  @override
  State<StatefulWidget> createState() {
    return _OrderHistoryState();
  }
}

class _OrderHistoryState extends State {
  int _selectedIndex = 0;
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios_new_sharp,
              color: Color.fromRGBO(207, 207, 207, 1),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: const Text('Order History'),
          actions: const [
            Icon(
              Icons.more_vert_sharp,
              color: Color.fromRGBO(207, 207, 207, 1),
            ),
          ]),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            Container(
              height: height * 0.066,
              width: width,
              decoration: const BoxDecoration(
                  border: Border(bottom: BorderSide(width: 1))),
              alignment: Alignment.center,
              child: Consumer<Producthistorycontroller>(
                builder: (context, value, child) {
                  return ListView.separated(
                    separatorBuilder: (context, index) => SizedBox(
                      width: width * 0.12,
                    ),
                    scrollDirection: Axis.horizontal,
                    itemCount: value.productsInfoList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          _selectedIndex = index;
                          value.refreash();
                        },
                        child: Text(
                          value.productsInfoList[index].name,
                          style: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.w600,
                              color: _selectedIndex == index
                                  ? const Color.fromARGB(243, 116, 19, 1)
                                  : const Color.fromRGBO(111, 111, 111, 1)),
                        ),
                      );
                    },
                  );
                },
              ),
            ),
            Expanded(child: Consumer<Producthistorycontroller>(
              builder: (context, value, child) {
                List<ProductMinInfo> productList =
                    value.productsInfoList[_selectedIndex].productList;
                return ListView.separated(
                  separatorBuilder: (context, index) => SizedBox(
                    height: height * 0.025,
                  ),
                  itemCount: productList.length,
                  itemBuilder: (context, index) {
                    return Container(
                      height: height * 0.167,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: const Color.fromRGBO(53, 53, 53, 0.5),
                              width: 1),
                          borderRadius: BorderRadius.circular(12)),
                      child: Column(
                        children: [
                          Container(
                            height: 50,
                            margin: const EdgeInsets.symmetric(horizontal: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  productList[index].status,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .labelMedium,
                                ),
                                Text(
                                  productList[index].date,
                                  style: Theme.of(context)
                                      .primaryTextTheme
                                      .labelLarge,
                                )
                              ],
                            ),
                          ),
                          Expanded(
                            child: Container(
                              padding: const EdgeInsets.symmetric(vertical: 10),
                              decoration: const BoxDecoration(
                                borderRadius: BorderRadius.vertical(
                                    bottom: Radius.circular(12)),
                                color: Color.fromRGBO(53, 53, 53, 0.5),
                              ),
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: width * 0.2,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceAround,
                                    children: [
                                      Text(
                                        productList[index].compyName,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .labelLarge,
                                      ),
                                      Text(
                                        productList[index].productName,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .labelMedium,
                                      ),
                                      Text(
                                        productList[index].spec,
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .titleMedium,
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  },
                );
              },
            ))
          ],
        ),
      ),
    );
  }
}
