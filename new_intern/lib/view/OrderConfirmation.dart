import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_animate/flutter_animate.dart';

class OrderConfirmation extends StatefulWidget {
  const OrderConfirmation({super.key});

  @override
  State<StatefulWidget> createState() {
    return _OrderConfirmationState();
  }
}

class _OrderConfirmationState extends State {
  List<Map<String, dynamic>> info = [
    {
      'color': null,
      'gradient': const LinearGradient(colors: [
        Color.fromRGBO(207, 207, 207, 1),
        Color.fromRGBO(13, 13, 13, 1)
      ], stops: [
        0.25,
        0.75
      ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
      'child': null,
    },
    {
      'color': const Color.fromRGBO(53, 53, 53, 0.5),
      'gradient': null,
      'child': const Column(
        children: [
          SizedBox(
            height: 180,
          ),
          Icon(Icons.add),
          Text(
            'Add review',
            style: TextStyle(color: Colors.white),
          )
        ],
      ),
    }
  ];
  delay() async {
    super.initState();
    await Future.delayed(const Duration(milliseconds: 1400));
    setState(() {
      done = true;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    delay();
  }

  Widget display(double height, double width) {
    if (isConfirm == false) {
      return Column(
        children: [
          Image.asset(
              done == false ? 'images/loading1.png' : 'images/loading2.png'),
          SizedBox(
            height: height * 0.02,
          ),
          SizedBox(width: width * 0.7, child: getData()),
          SizedBox(
            height: (done == false) ? height * 0.21 : height * 0.15,
          ),
          done == false
              ? const SizedBox()
              : GestureDetector(
                  onTap: () {
                    done = false;
                    isConfirm = true;
                    setState(() {});
                  },
                  child: Container(
                    height: height * 0.07,
                    width: width * 0.83,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 18, vertical: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: const Color.fromRGBO(53, 53, 53, 0.5),
                        border: Border.all(
                          color: const Color.fromRGBO(68, 68, 68, 1),
                        )),
                    child: Row(
                      children: [
                        Image.asset('images/coin.png'),
                        SizedBox(
                          width: width * 0.09,
                        ),
                        Expanded(
                            child: Text(
                          'Record product review video and win cashback',
                          style: Theme.of(context).primaryTextTheme.titleLarge,
                        ))
                      ],
                    ),
                  ),
                )
        ],
      );
    } else {
      return Animate(
        effects: const [
          FadeEffect(
              begin: 0.1, end: 0.8, duration: Duration(milliseconds: 600)),
          SlideEffect(
              begin: Offset(0, 3), duration: Duration(milliseconds: 600)),
        ],
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: height * 0.1,
            ),
            SizedBox(
              height: height * 0.5,
              width: width,
              child: CarouselSlider.builder(
                  itemCount: info.length,
                  itemBuilder: (context, index, realIndex) {
                    return Container(
                      height: height,
                      width: width * 0.6,
                      decoration: BoxDecoration(
                          color: info[index]['color'],
                          gradient: info[index]['gradient'],
                          borderRadius: BorderRadius.circular(16)),
                      alignment: Alignment.center,
                      child: Center(
                        child: info[index]['child'],
                      ),
                    );
                  },
                  options: CarouselOptions(
                      viewportFraction: 0.7,
                      enableInfiniteScroll: true,
                      autoPlay: true,
                      aspectRatio: 1,
                      enlargeFactor: 0.4,
                      enlargeCenterPage: true,
                      enlargeStrategy: CenterPageEnlargeStrategy.height)),
            ),
            SizedBox(
              height: height * 0.06,
            ),
            Container(
              height: height * 0.07,
              width: width * 0.83,
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: const Color.fromRGBO(53, 53, 53, 0.5),
                  border: Border.all(
                    color: const Color.fromRGBO(68, 68, 68, 1),
                  )),
              child: Row(
                children: [
                  Image.asset('images/rupee.png'),
                  SizedBox(
                    width: width * 0.09,
                  ),
                  Expanded(
                    child: RichText(
                        text: TextSpan(children: <TextSpan>[
                      TextSpan(
                        text:
                            'Earn cashback as you enjoy your purchase. Know more ',
                        style: Theme.of(context).primaryTextTheme.titleLarge,
                      ),
                      TextSpan(
                          text: 'here ',
                          style: Theme.of(context)
                              .primaryTextTheme
                              .titleLarge!
                              .copyWith(color: Colors.red))
                    ])),
                  )
                ],
              ),
            ),
          ],
        ),
      );
    }
  }

  bool done = false;
  bool isConfirm = false;
  Widget getData() {
    if (done == false) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text(
            'View',
            style: TextStyle(
                color: Color.fromRGBO(151, 151, 151, 1), fontSize: 15),
          ),
          TextButton(
              onPressed: () {},
              child: const Text('transaction history',
                  style: TextStyle(
                      color: Color.fromRGBO(243, 116, 19, 1), fontSize: 15))),
          const Text('in your UnBox.',
              style: TextStyle(
                  color: Color.fromRGBO(151, 151, 151, 1), fontSize: 15)),
        ],
      );
    } else {
      return const Text(
        'Earn cashback as you enjoy your purchase ',
        style: TextStyle(color: Color.fromRGBO(151, 151, 151, 1), fontSize: 15),
        textAlign: TextAlign.center,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: SizedBox(
        height: height,
        width: width,
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: isConfirm == false ? height * 0.38 : height * 0.1,
            ),
            const Text(
              'Congratulations',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontSize: 20,
                  fontWeight: FontWeight.w700),
            ),
            const Text(
              'Your order has been placed!',
              style: TextStyle(
                  color: Color.fromRGBO(255, 255, 255, 1),
                  fontSize: 16,
                  fontWeight: FontWeight.w500),
            ),
            SizedBox(
              height: height * 0.02,
            ),
            display(height, width)
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: height * 0.05,
            width: width * 0.44,
            decoration: BoxDecoration(
                border: Border.all(
                    color: const Color.fromRGBO(243, 116, 19, 1), width: 1.4),
                borderRadius: BorderRadius.circular(8)),
            alignment: Alignment.center,
            child: const Text(
              'Record Review',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w400),
            ),
          ),
          Container(
            height: height * 0.05,
            width: width * 0.44,
            decoration: BoxDecoration(
                color: const Color.fromRGBO(243, 116, 19, 1),
                borderRadius: BorderRadius.circular(8)),
            alignment: Alignment.center,
            child: const Text(
              'Go Back Home',
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                  fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
