import 'package:flutter/material.dart';
import 'package:new_intern/Model/AddressModel.dart';
import 'package:provider/provider.dart';

Widget getAddress({required BuildContext context}) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return Container(
      height: height * 0.136,
      width: width,
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Theme.of(context).primaryColor,
      ),
      child: Consumer<AddressModel>(
        builder: (context, AddressModel value, child) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Shipping Address',
                style: Theme.of(context).primaryTextTheme.titleMedium,
              ),
              Text(
                value.name,
                style: Theme.of(context).primaryTextTheme.titleLarge,
              ),
              Row(
                children: [
                  Expanded(
                    child: Text(
                      value.address,
                      style: Theme.of(context).primaryTextTheme.titleMedium,
                    ),
                  ),
                  SizedBox(
                    width: width * 0.465,
                  ),
                  Image.asset('images/edit.png')
                ],
              ),
              Text(
                'Mobile: ${value.phoneNo}',
                style: Theme.of(context).primaryTextTheme.titleMedium,
              ),
            ],
          );
        },
      ));
}
