import 'package:flutter/material.dart';
import 'package:new_intern/Controller/ProductController.dart';
import 'package:new_intern/Model/AddressModel.dart';
import 'package:new_intern/Model/ProductModel.dart';
import 'package:new_intern/view/OrderHistory.dart';
import 'package:new_intern/view/PaymentScreen.dart';
import 'package:new_intern/view/commanWidget.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CartScreenState();
  }
}

class _CartScreenState extends State {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(
          Icons.arrow_back_ios_new_sharp,
          color: Color.fromRGBO(207, 207, 207, 1),
        ),
        title: const Text('UnBox'),
        actions: const [
          Icon(
            Icons.more_vert_sharp,
            color: Color.fromRGBO(207, 207, 207, 1),
          )
        ],
      ),
      body: Column(
        children: [
          getAddress(context: context),
          const SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(
              children: [
                Row(
                  children: [
                    Consumer<ProductController>(
                      builder: (context, value, child) {
                        return IconButton(
                            onPressed: () {
                              value.offer = !value.offer;
                              value.refreash();
                            },
                            icon: Icon(value.offer
                                ? Icons.check_box_outlined
                                : Icons.check_box_outline_blank_rounded));
                      },
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text(
                      'Get ₹ 1500 cashback',
                      style: Theme.of(context).primaryTextTheme.labelMedium,
                    ),
                    const Spacer(),
                    const Icon(
                      Icons.arrow_forward_ios_sharp,
                      color: Color.fromRGBO(207, 207, 207, 1),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
                SizedBox(
                  width: width,
                  height: height / 2,
                  child: Consumer<ProductController>(
                    builder: (context, ProductController value, child) {
                      List<ProductModel> delivayProductList =
                          value.delivaryProductList;
                      return ListView.builder(
                        itemCount: delivayProductList.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              Container(
                                height: height * 0.47,
                                width: width * 0.91,
                                padding: const EdgeInsets.symmetric(
                                    vertical: 20, horizontal: 2),
                                decoration: BoxDecoration(
                                    color:
                                        const Color.fromRGBO(53, 53, 53, 0.5),
                                    borderRadius: BorderRadius.circular(15)),
                                child: Column(
                                  children: [
                                    SizedBox(
                                      height: height * 0.142,
                                      width: double.maxFinite,
                                      child: Row(
                                        children: [
                                          SizedBox(
                                            width: width * 0.3,
                                          ),
                                          Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                delivayProductList[index]
                                                    .compyName,
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .labelLarge,
                                              ),
                                              Text(
                                                delivayProductList[index]
                                                    .productName,
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .labelMedium,
                                              ),
                                              Text(
                                                delivayProductList[index].color,
                                                style: Theme.of(context)
                                                    .primaryTextTheme
                                                    .titleMedium,
                                              ),
                                              Row(
                                                children: [
                                                  Text(
                                                    "₹ ${delivayProductList[index].price * delivayProductList[index].buyCount}",
                                                    style: Theme.of(context)
                                                        .primaryTextTheme
                                                        .titleMedium,
                                                  ),
                                                  SizedBox(
                                                    width: width * 0.2,
                                                  ),
                                                  IconButton(
                                                    onPressed: () {
                                                      delivayProductList[index]
                                                          .buyCount++;
                                                      value.refreash();
                                                    },
                                                    icon: const Icon(
                                                        Icons.add_box_outlined),
                                                  ),
                                                  Text(
                                                    "${delivayProductList[index].buyCount}",
                                                    style: Theme.of(context)
                                                        .primaryTextTheme
                                                        .titleMedium,
                                                  ),
                                                  IconButton(
                                                      onPressed: () {
                                                        if (delivayProductList[
                                                                    index]
                                                                .buyCount ==
                                                            1) {
                                                          delivayProductList[
                                                                  index]
                                                              .buyCount--;
                                                          delivayProductList
                                                              .removeAt(index);
                                                        } else {
                                                          delivayProductList[
                                                                  index]
                                                              .buyCount--;
                                                        }

                                                        value.refreash();
                                                      },
                                                      icon: const Icon(Icons
                                                          .indeterminate_check_box_outlined))
                                                ],
                                              ),
                                            ],
                                          )
                                        ],
                                      ),
                                    ),
                                    Container(
                                      height: height * 0.05,
                                      decoration: const BoxDecoration(
                                        border: Border.symmetric(
                                            horizontal: BorderSide(
                                                color: Color.fromRGBO(
                                                    151, 151, 151, 1))),
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text(
                                            'Total:',
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .titleMedium,
                                          ),
                                          Text(
                                            "₹ ${delivayProductList[index].price * delivayProductList[index].buyCount}",
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .titleMedium!
                                                .copyWith(
                                                    fontSize: 20,
                                                    fontWeight: FontWeight.w700,
                                                    color: const Color.fromRGBO(
                                                        243, 116, 19, 1)),
                                          ),
                                        ],
                                      ),
                                    ),
                                    SizedBox(
                                      height: height * 0.02,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Product Detail',
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .titleMedium,
                                        ),
                                        SizedBox(
                                          width: width * 0.1,
                                        ),
                                        Expanded(
                                          child: Text(
                                            delivayProductList[index]
                                                .productDetail,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .titleMedium,
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: height * 0.05,
                                    ),
                                    Row(
                                      children: [
                                        Text(
                                          'Seller',
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .titleMedium,
                                        ),
                                        SizedBox(
                                          width: width * 0.25,
                                        ),
                                        Expanded(
                                          child: Text(
                                            delivayProductList[index]
                                                .sellerDeatil,
                                            style: Theme.of(context)
                                                .primaryTextTheme
                                                .titleMedium,
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Order Status Update',
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .titleMedium!
                                        .copyWith(
                                            fontSize: 20,
                                            fontWeight: FontWeight.w600),
                                  ),
                                  const Icon(Icons.arrow_forward_ios_sharp)
                                ],
                              ),
                              SizedBox(
                                height: height * 0.02,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Order ID: ${delivayProductList[index].orderId}',
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .titleMedium,
                                  ),
                                  Text(
                                    'Cancel',
                                    style: Theme.of(context)
                                        .primaryTextTheme
                                        .titleMedium!
                                        .copyWith(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w500,
                                            color: const Color.fromRGBO(
                                                187, 63, 63, 1)),
                                  ),
                                ],
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Transction History',
                      style: Theme.of(context)
                          .primaryTextTheme
                          .titleMedium!
                          .copyWith(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => OrderHistory(),
                          ));
                        },
                        child: const Text('See all'))
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) {
                        return const PaymentScreen();
                      },
                    ));
                  },
                  child: Container(
                    height: height * 0.05,
                    width: width * 0.91,
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(243, 116, 19, 1),
                        borderRadius: BorderRadius.circular(10)),
                    alignment: Alignment.center,
                    child: Text(
                      'Checkout',
                      style: Theme.of(context)
                          .primaryTextTheme
                          .titleMedium!
                          .copyWith(fontSize: 20, fontWeight: FontWeight.w600),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
