import 'package:flutter/material.dart';
import 'package:new_intern/Controller/ProductController.dart';
import 'package:new_intern/Model/PaymentModel.dart';
import 'package:new_intern/view/OTPScreen.dart';
import 'package:new_intern/view/commanWidget.dart';
import 'package:provider/provider.dart';

import '../Model/ProductModel.dart';

class PaymentScreen extends StatefulWidget {
  const PaymentScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PaymentScreenState();
  }
}

class _PaymentScreenState extends State {
  List<DropdownMenuItem> getDropDownItemList(
      PaymentModel payObj, double width) {
    List<DropdownMenuItem> list = [];
    List<String> aplha = ['A', 'B', 'C', 'D'];
    for (int i = 0; i < payObj.bankList.length; i++) {
      list.add(DropdownMenuItem(
          value: aplha[i],
          child: Row(
            children: [
              Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: payObj.bankList[i].backGroundColor),
                alignment: Alignment.center,
                child: (payObj.bankList[i].iconImage != null)
                    ? Image.asset(payObj.bankList[i].iconImage!)
                    : null,
              ),
              SizedBox(
                width: width * 0.03,
              ),
              Text(
                payObj.bankList[i].name,
                style: Theme.of(context).primaryTextTheme.titleMedium,
              ),

              // const Icon(Icons.radio_button_checked_sharp)
            ],
          )));
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    int selectedIndex = 1;
    String? val1;

    return Scaffold(
      appBar: AppBar(
          leading: IconButton(
            icon: const Icon(
              Icons.arrow_back_ios_new_sharp,
              color: Color.fromRGBO(207, 207, 207, 1),
            ),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          title: const Text('Payment'),
          actions: const [
            Icon(
              Icons.more_vert_sharp,
              color: Color.fromRGBO(207, 207, 207, 1),
            ),
          ]),
      body: SingleChildScrollView(
        child: Column(
          children: [
            getAddress(context: context),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: height * 0.03,
                  ),
                  Text(
                    'Delivery Type',
                    style: Theme.of(context).primaryTextTheme.titleMedium,
                  ),
                  SizedBox(
                    width: width,
                    height: height * 0.06,
                    child: Consumer<ProductController>(
                      builder: (context, value, child) {
                        List<String> delivaryType = value.delivaryType;
                        return ListView.separated(
                          separatorBuilder: (context, index) => SizedBox(
                            width: width * 0.1,
                          ),
                          scrollDirection: Axis.horizontal,
                          itemCount: delivaryType.length,
                          itemBuilder: (context, index) {
                            return SizedBox(
                              width: width * 0.3,
                              child: GestureDetector(
                                onTap: () {
                                  selectedIndex = index;
                                  value.refreash();
                                },
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Icon(
                                      selectedIndex == index
                                          ? Icons.radio_button_on_sharp
                                          : Icons.radio_button_off_sharp,
                                      color: selectedIndex == index
                                          ? Colors.orange
                                          : Colors.white,
                                    ),
                                    Text(delivaryType[index],
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .titleLarge)
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                  Text(
                    'Payment Method',
                    style: Theme.of(context).primaryTextTheme.labelLarge,
                  ),
                  SizedBox(
                    height: height*0.02,
                  ),
                  SizedBox(
                      height: height * 0.35,
                      width: width,
                      child: Consumer<ProductController>(
                        builder: (context, value, child) {
                          List<PaymentModel> paymentList = value.paymentList;
                          return ListView.separated(
                            separatorBuilder: (context, index) => SizedBox(
                              height: height * 0.03,
                            ),
                            itemCount: paymentList.length,
                            itemBuilder: (context, index) {
                              return DropdownButtonFormField(
                                key: paymentList[index].key,
                                  value: val1,
                                  validator: (val){
                                  if(val==null || val.isEmpty){
                                    return 'Feild required';
                                  }
                                  },
                                  autovalidateMode:AutovalidateMode.onUserInteraction,
                                  decoration: InputDecoration(
                                      hintStyle: const TextStyle(
                                          color:
                                              Color.fromRGBO(151, 151, 151, 1)),
                                      enabledBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          borderSide: const BorderSide(
                                              width: 2,
                                              color: Color.fromRGBO(
                                                  207, 207, 207, 1))),
                                      errorBorder: OutlineInputBorder(
                                          borderRadius:
                                          BorderRadius.circular(8),
                                          borderSide: const BorderSide(
                                              width: 2,
                                              color: Colors.red)),
                                      focusedBorder: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(8),
                                          borderSide: const BorderSide(
                                              width: 2,
                                              color: Color.fromRGBO(
                                                  207, 207, 207, 1))),
                                      //hoverColor: Colors.transparent,
                                      hintText: paymentList[index].hint,
                                      // border: OutlineInputBorder(
                                      //     borderRadius:
                                      //         BorderRadius.circular(8),
                                      //     borderSide: const BorderSide(
                                      //         width: 2,
                                      //         color: Color.fromRGBO(
                                      //             207, 207, 207, 1))
                                      // )
                                  ),
                                  focusColor: Colors.transparent,
                                  iconEnabledColor: Colors.white,
                                  iconDisabledColor: Colors.white,
                                  icon: Container(
                                    margin: const EdgeInsets.only(right: 10),
                                    child: const Icon(
                                        Icons.arrow_forward_ios_sharp),
                                  ),
                                  dropdownColor:
                                      Theme.of(context).scaffoldBackgroundColor,
                                  borderRadius: BorderRadius.circular(8),
                                  items:
                                      getDropDownItemList(paymentList[index], width),
                                  onChanged: (value) {});
                            },
                          );
                        },
                      )),
                  SizedBox(
                    height: height * 0.05,
                  ),
                  SizedBox(
                    width: width,
                    height: height * 0.48,
                    child: Consumer<ProductController>(
                      builder: (context, ProductController value, child) {
                        List<ProductModel> delivayProductList =
                            value.delivaryProductList;
                        return ListView.builder(
                          itemCount: delivayProductList.length,
                          itemBuilder: (context, index) {
                            return Container(
                              height: height * 0.47,
                              width: width * 0.91,
                              padding: const EdgeInsets.symmetric(
                                  vertical: 20, horizontal: 2),
                              decoration: BoxDecoration(
                                  color: const Color.fromRGBO(53, 53, 53, 0.5),
                                  borderRadius: BorderRadius.circular(15)),
                              child: Column(
                                children: [
                                  SizedBox(
                                    height: height * 0.142,
                                    width: double.maxFinite,
                                    child: Row(
                                      children: [
                                        SizedBox(
                                          width: width * 0.3,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              delivayProductList[index]
                                                  .compyName,
                                              style: Theme.of(context)
                                                  .primaryTextTheme
                                                  .labelLarge,
                                            ),
                                            Text(
                                              delivayProductList[index]
                                                  .productName,
                                              style: Theme.of(context)
                                                  .primaryTextTheme
                                                  .labelMedium,
                                            ),
                                            Text(
                                              delivayProductList[index].color,
                                              style: Theme.of(context)
                                                  .primaryTextTheme
                                                  .titleMedium,
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "₹ ${delivayProductList[index].price * delivayProductList[index].buyCount}",
                                                  style: Theme.of(context)
                                                      .primaryTextTheme
                                                      .titleMedium,
                                                ),
                                                SizedBox(
                                                  width: width * 0.2,
                                                ),
                                                IconButton(
                                                  onPressed: () {
                                                    delivayProductList[index]
                                                        .buyCount++;
                                                    value.refreash();
                                                  },
                                                  icon: const Icon(
                                                      Icons.add_box_outlined),
                                                ),
                                                Text(
                                                  "${delivayProductList[index].buyCount}",
                                                  style: Theme.of(context)
                                                      .primaryTextTheme
                                                      .titleMedium,
                                                ),
                                                IconButton(
                                                    onPressed: () {
                                                      if (delivayProductList[
                                                                  index]
                                                              .buyCount ==
                                                          1) {
                                                        delivayProductList[
                                                                index]
                                                            .buyCount--;
                                                        delivayProductList
                                                            .removeAt(index);
                                                      } else {
                                                        delivayProductList[
                                                                index]
                                                            .buyCount--;
                                                      }

                                                      value.refreash();
                                                    },
                                                    icon: const Icon(Icons
                                                        .indeterminate_check_box_outlined))
                                              ],
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                  Container(
                                    height: height * 0.05,
                                    decoration: const BoxDecoration(
                                      border: Border.symmetric(
                                          horizontal: BorderSide(
                                              color: Color.fromRGBO(
                                                  151, 151, 151, 1))),
                                    ),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          'Total:',
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .titleMedium,
                                        ),
                                        Text(
                                          "₹ ${delivayProductList[index].price * delivayProductList[index].buyCount}",
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .titleMedium!
                                              .copyWith(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.w700,
                                                  color: const Color.fromRGBO(
                                                      243, 116, 19, 1)),
                                        ),
                                      ],
                                    ),
                                  ),
                                  SizedBox(
                                    height: height * 0.02,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Product Detail',
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .titleMedium,
                                      ),
                                      SizedBox(
                                        width: width * 0.1,
                                      ),
                                      Expanded(
                                        child: Text(
                                          delivayProductList[index]
                                              .productDetail,
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .titleMedium,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: height * 0.05,
                                  ),
                                  Row(
                                    children: [
                                      Text(
                                        'Seller',
                                        style: Theme.of(context)
                                            .primaryTextTheme
                                            .titleMedium,
                                      ),
                                      SizedBox(
                                        width: width * 0.25,
                                      ),
                                      Expanded(
                                        child: Text(
                                          delivayProductList[index]
                                              .sellerDeatil,
                                          style: Theme.of(context)
                                              .primaryTextTheme
                                              .titleMedium,
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            );
                          },
                        );
                      },
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton.extended(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
        backgroundColor: const Color.fromRGBO(243, 116, 19, 1),
        label: SizedBox(
            width: width * 0.84,
            height: height * 0.07,
            child: Center(
                child: Text(
              'Pay now',
              style: Theme.of(context).primaryTextTheme.labelMedium,
            ))),
        onPressed: () {
          bool nav=true;
          List<PaymentModel> paymentList= Provider.of<ProductController>(context,listen: false).paymentList;
          for(int i=0;i<paymentList.length;i++){
            if(paymentList[i].key.currentState!.validate()==false){
              nav=false;
            }

          }
          if(nav){
            Navigator.of(context).push(MaterialPageRoute(
              builder: (context) {
                return const OTPScreen();
              },
            ));
          }
        },
      ),
    );
  }
}
