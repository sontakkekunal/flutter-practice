import 'package:flutter/material.dart';
import 'package:new_intern/Model/ProductMinInfo.dart';
import 'package:new_intern/Model/ProductsInfo.dart';

class Producthistorycontroller extends ChangeNotifier {
  void refreash() {
    notifyListeners();
  }

  List<ProductsInfo> productsInfoList = [
    ProductsInfo(name: 'All', productList: [
      ProductMinInfo(
          date: 'mar 16, 2023 3:48 pM',
          compyName: 'Apple',
          productName: 'iPhone 14',
          spec: '128 GB Storage',
          status: 'Pending')
    ]),
    ProductsInfo(name: 'Delivered', productList: [
      ProductMinInfo(
          date: 'mar 16, 2023 3:48 pM',
          compyName: 'OnePlus',
          productName: 'OnePlus 11',
          spec: '256 GB Storage',
          status: 'Delivered')
    ]),
    ProductsInfo(name: 'Cancelled', productList: [
      ProductMinInfo(
          date: 'mar 16, 2023 3:48 pM',
          compyName: 'Apple',
          productName: 'iPhone 14',
          spec: '128 GB Storage',
          status: 'Cancelled')
    ]),
    ProductsInfo(name: 'Returned', productList: [
      ProductMinInfo(
          date: 'mar 16, 2023 3:48 pM',
          compyName: 'Apple',
          productName: 'Watch S8',
          spec: 'Beige',
          status: 'Returned')
    ])
  ];
  Producthistorycontroller() {
    List<ProductMinInfo> tempList = productsInfoList[0].productList;
    for (int i = 1; i < productsInfoList.length; i++) {
      tempList.addAll(productsInfoList[i].productList);
    }
  }
}
