import 'package:flutter/material.dart';
import 'package:new_intern/Model/PaymentModel.dart';
import 'package:new_intern/Model/ProductModel.dart';

class ProductController extends ChangeNotifier {
  void refreash() {
    notifyListeners();
  }

  bool offer = false;
  List<String> delivaryType = ['Home Delivery', 'Store Pickup'];
  List<PaymentModel> paymentList = [
    PaymentModel(hint: 'Credit card', bankList: [
      BankType(name: 'HDFC bank (Rupay)', backGroundColor: Colors.white),
      BankType(name: 'Axis Bank (Visa)', backGroundColor: Colors.white),
      BankType(
          name: 'Kotak mahindara bank (Mastercard)',
          backGroundColor: const Color.fromRGBO(0, 56, 116, 1)),
      BankType(
          name: 'Add Payment Method',
          backGroundColor: const Color.fromRGBO(207, 207, 207, 1),
          iconImage: 'images/addPay.png'),
    ]),
    PaymentModel(hint: 'Consumer Durable loan', bankList: [
      BankType(name: 'HDFC bank (Rupay)', backGroundColor: Colors.white),
      BankType(name: 'Axis Bank (Visa)', backGroundColor: Colors.white),
      BankType(
          name: 'Kotak mahindara bank (Mastercard)',
          backGroundColor: const Color.fromRGBO(0, 56, 116, 1)),
      BankType(
          name: 'Add Payment Method',
          backGroundColor: const Color.fromRGBO(207, 207, 207, 1),
          iconImage: 'images/addPay.png'),
    ]),
    PaymentModel(hint: 'Wallets', bankList: [
      BankType(name: 'HDFC bank (Rupay)', backGroundColor: Colors.white),
      BankType(name: 'Axis Bank (Visa)', backGroundColor: Colors.white),
      BankType(
          name: 'Kotak mahindara bank (Mastercard)',
          backGroundColor: const Color.fromRGBO(0, 56, 116, 1)),
      BankType(
          name: 'Add Payment Method',
          backGroundColor: const Color.fromRGBO(207, 207, 207, 1),
          iconImage: 'images/addPay.png'),
    ]),
    PaymentModel(hint: 'UPI Payment', bankList: [
      BankType(name: 'HDFC bank (Rupay)', backGroundColor: Colors.white),
      BankType(name: 'Axis Bank (Visa)', backGroundColor: Colors.white),
      BankType(
          name: 'Kotak mahindara bank (Mastercard)',
          backGroundColor: const Color.fromRGBO(0, 56, 116, 1)),
      BankType(
          name: 'Add Payment Method',
          backGroundColor: const Color.fromRGBO(207, 207, 207, 1),
          iconImage: 'images/addPay.png'),
    ]),
  ];

  List<ProductModel> delivaryProductList = [
    ProductModel(
        orderId: 223576232223,
        compyName: 'Samsung',
        productName: 'S90C OLED',
        color: 'Black Mattee',
        buyCount: 1,
        price: 284990,
        productDetail:
            'Samsung S90C OLED 36.0MP/36.3MP Digital SLR Camera(Black) +SanDisk 128GB,Extreme Pro SDXC UHS-I Card - C10,U3, V30, 4K UHD, SD Card',
        sellerDeatil:
            'Ground Floor, Indraprastha Equinox, 23, 100 Feet Rd, Venkappa Garden, Koramangala, Bangalore - 560095')
  ];
}
