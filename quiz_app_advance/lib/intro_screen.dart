import 'package:flutter/material.dart';
import 'package:quiz_app_advance/subject_choice_screen.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _IntroPageState();
  }
}

class _IntroPageState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: GestureDetector(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const SubjectChoice(),
                ));
          },
          child: Image.asset(
            'images/logo.png',
            height: 117,
            width: 238,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}
