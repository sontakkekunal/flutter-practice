import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:quiz_app_advance/result_screen.dart';
import 'package:quiz_app_advance/subject.dart';

class QuizInfo {
  String question;
  List<String> options;
  int correctOptIndex;
  String explanation;
  QuizInfo(
      {required this.question,
      required this.options,
      required this.correctOptIndex,
      required this.explanation});
}

class QuizPage extends StatefulWidget {
  Subject subjectObj;
  QuizPage({super.key, required this.subjectObj});

  @override
  State<StatefulWidget> createState() {
    return _QuizPageState();
  }
}

class _QuizPageState extends State<QuizPage> {
  int quesNo = 1;
  int solvedQuestion = 0;
  int correctOption = 0;
  int selectedOptionInd = -1;
  List<QuizInfo> quizInfoList = List.generate(
      20,
      (int index) => QuizInfo(
          question:
              'If David’s age is 27 years old in 2011. What was his age in 2003?',
          correctOptIndex: 0,
          options: ['19 year', '37 year', '20 year', '17 year'],
          explanation:
              'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing'));
  bool? checkCorrectOption(int index) {
    if (selectedOptionInd != -1) {
      if (quizInfoList[quesNo - 1].correctOptIndex == index) {
        return true;
      } else if (selectedOptionInd == index) {
        return false;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  Color getOptionColor(int index) {
    bool? val = checkCorrectOption(index);
    if (val == true) {
      return const Color.fromRGBO(26, 181, 134, 1);
    } else if (val == false) {
      return Colors.red;
    } else {
      return const Color.fromRGBO(248, 145, 87, 1);
    }
  }

  Icon getOptionIcon(int index) {
    bool? val = checkCorrectOption(index);
    if (val == true) {
      return const Icon(
        Icons.check,
        color: Colors.white,
      );
    } else if (val == false) {
      return const Icon(
        Icons.stop_circle_sharp,
        color: Colors.white,
      );
    } else {
      return const Icon(
        Icons.arrow_forward_ios_rounded,
        color: Colors.white,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 29),
        child: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            //mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const SizedBox(
                height: 51,
              ),
              Text(
                "${widget.subjectObj.name} Quiz",
                style: GoogleFonts.dmSans(
                    fontSize: 20,
                    fontWeight: FontWeight.w700,
                    color: const Color.fromRGBO(131, 76, 52, 1)),
              ),
              const SizedBox(
                height: 23,
              ),
              LinearPercentIndicator(
                lineHeight: 5,
                percent: quesNo / quizInfoList.length,
                //percent: ((100 * quesNo) / quizInfoList.length) / 100,
                clipLinearGradient: true,
                padding: const EdgeInsets.symmetric(horizontal: 00),
                animation: true,
                backgroundColor: const Color.fromRGBO(245, 216, 186, 1),
                barRadius: const Radius.circular(6),
                animationDuration: 2000,
                animateFromLastPercent: true,
                progressColor: const Color.fromRGBO(42, 135, 63, 1),
              ),
              Row(
                children: [
                  Text(
                    "$quesNo",
                    style: GoogleFonts.dmSans(
                        fontSize: 16, fontWeight: FontWeight.w400),
                  ),
                  Text(
                    "/${quizInfoList.length}",
                    style: GoogleFonts.dmSans(
                        fontSize: 13, fontWeight: FontWeight.w400),
                  )
                ],
              ),
              const SizedBox(
                height: 26,
              ),
              Text(
                quizInfoList[quesNo - 1].question,
                style: GoogleFonts.dmSans(
                    fontSize: 26,
                    fontWeight: FontWeight.w700,
                    color: const Color.fromRGBO(131, 76, 52, 1)),
              ),
              SizedBox(
                  height: 355,
                  width: double.infinity,
                  child: ListView.separated(
                    itemCount: quizInfoList[quesNo - 1].options.length,
                    separatorBuilder: (context, index) {
                      return const SizedBox(
                        height: 15,
                      );
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: (selectedOptionInd == -1)
                            ? () {
                                selectedOptionInd = index;
                                solvedQuestion++;
                                if (index ==
                                    quizInfoList[quesNo - 1].correctOptIndex) {
                                  correctOption++;
                                }
                                setState(() {});
                              }
                            : null,
                        child: Container(
                          height: 70,
                          width: 300,
                          padding: const EdgeInsets.symmetric(horizontal: 25),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: getOptionColor(index)),
                          alignment: Alignment.center,
                          child: Row(
                            children: [
                              Text(
                                quizInfoList[quesNo - 1].options[index],
                                style: GoogleFonts.dmSans(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w700,
                                    color: Colors.white),
                              ),
                              const Spacer(),
                              getOptionIcon(index)
                            ],
                          ),
                        ),
                      );
                    },
                  )),
              const SizedBox(
                height: 26,
              ),
              (selectedOptionInd != -1)
                  ? Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Explanation',
                          style: GoogleFonts.dmSans(
                              fontSize: 16,
                              fontWeight: FontWeight.w700,
                              color: const Color.fromRGBO(131, 76, 52, 1)),
                        ),
                        Text(
                          quizInfoList[quesNo - 1].explanation,
                          style: GoogleFonts.dmSans(
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              color: const Color.fromRGBO(131, 76, 52, 0.8)),
                        )
                      ],
                    )
                  : const SizedBox(
                      height: 10,
                    ),
              const SizedBox(
                height: 20,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    onTap: () {
                      if (quesNo != quizInfoList.length) {
                        selectedOptionInd = -1;
                        quesNo++;
                        setState(() {});
                      } else {
                        widget.subjectObj.correctQuestion = correctOption;
                        widget.subjectObj.solvedQuestion = solvedQuestion;
                        widget.subjectObj.totalQuestion = quizInfoList.length;
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                              builder: (context) => ResultScreen(
                                subjectObj: widget.subjectObj,
                              ),
                            ));
                      }
                    },
                    child: Container(
                      height: 30,
                      width: 87,
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: const Color.fromRGBO(26, 181, 134, 1)),
                      child: (quesNo != quizInfoList.length)
                          ? Row(
                              children: [
                                Text(
                                  'NEXT',
                                  style: GoogleFonts.dmSans(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w700,
                                      color: Colors.white),
                                ),
                                const Spacer(),
                                const Icon(
                                  Icons.arrow_circle_right_rounded,
                                  size: 20,
                                  color: Colors.white,
                                )
                              ],
                            )
                          : Text(
                              "FINISH",
                              style: GoogleFonts.dmSans(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white),
                            ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
