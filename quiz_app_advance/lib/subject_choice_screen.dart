import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz_app_advance/quiz_screen.dart';
import 'package:quiz_app_advance/subject.dart';



class SubjectChoice extends StatefulWidget {
  const SubjectChoice({super.key});
  @override
  State createState() => _SubjectChoiceState();
}

class _SubjectChoiceState extends State {
  List<Subject> subjectList = [
    Subject(
      name: 'Math',
      firstLetter: 'M',
      firstLetterColor: const Color.fromRGBO(200, 60, 0, 1),
    ),
    Subject(
        name: 'History',
        firstLetter: 'H',
        firstLetterColor: const Color.fromRGBO(255, 157, 66, 1)),
    Subject(
      name: 'Geography',
      firstLetter: 'G',
      firstLetterColor: const Color.fromRGBO(3, 163, 134, 1),
    ),
    Subject(
        name: 'Biology',
        firstLetter: 'B',
        firstLetterColor: const Color.fromRGBO(251, 43, 255, 1)),
    Subject(
        name: 'Sports',
        firstLetter: 'S',
        firstLetterColor: const Color.fromRGBO(45, 104, 255, 1))
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 27),
        child: Column(
          children: [
            const SizedBox(
              height: 59,
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Hi Pamela,',
                      style: GoogleFonts.dmSans(
                          fontSize: 25,
                          fontWeight: FontWeight.w700,
                          color: const Color.fromRGBO(131, 76, 52, 1)),
                    ),
                    Text(
                      'Great to see you again!',
                      style: GoogleFonts.dmSans(
                          fontSize: 16,
                          fontWeight: FontWeight.w400,
                          color: const Color.fromRGBO(131, 76, 52, 1)),
                    )
                  ],
                ),
                const Spacer(),
                Container(
                  height: 64,
                  width: 64,
                  decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: Color.fromRGBO(250, 188, 154, 1)),
                )
              ],
            ),
            const SizedBox(
              height: 48,
            ),
            Expanded(
                child: ListView.builder(
              itemCount: subjectList.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) =>
                              QuizPage(subjectObj: subjectList[index]),
                        ));
                  },
                  child: Container(
                    height: 80,
                    width: 306,
                    margin: const EdgeInsets.only(bottom: 20),
                    padding: const EdgeInsets.symmetric(horizontal: 22),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: const Color.fromRGBO(255, 237, 217, 1)),
                    child: Row(
                      children: [
                        Container(
                          height: 45,
                          width: 45,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: const Color.fromRGBO(255, 255, 255, 1)),
                          child: Text(
                            subjectList[index].firstLetter,
                            style: GoogleFonts.dmSans(
                                fontSize: 20,
                                fontWeight: FontWeight.w700,
                                color: subjectList[index].firstLetterColor),
                          ),
                        ),
                        const SizedBox(
                          width: 20,
                        ),
                        Text(
                          subjectList[index].name,
                          style: GoogleFonts.dmSans(
                              fontSize: 20,
                              fontWeight: FontWeight.w700,
                              color: const Color.fromRGBO(131, 76, 52, 1)),
                        ),
                        const Spacer(),
                        const Icon(Icons.arrow_forward_ios_rounded)
                      ],
                    ),
                  ),
                );
              },
            ))
          ],
        ),
      ),
    );
  }
}
