import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:quiz_app_advance/subject.dart';
import 'package:pie_chart/pie_chart.dart';

class ResultScreen extends StatefulWidget {
  Subject subjectObj;
  ResultScreen({super.key, required this.subjectObj});

  @override
  State<StatefulWidget> createState() {
    return _ResultScreenState();
  }
}

class _ResultScreenState extends State<ResultScreen> {
  Map<String, double> getDataMap() {
    return {'score': widget.subjectObj.correctQuestion! + 0.0};
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: const BoxDecoration(
            image: DecorationImage(image: AssetImage("images/resultBg.png"))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 51,
            ),
            Text(
              "Quiz Result",
              style: GoogleFonts.dmSans(
                  fontSize: 20,
                  fontWeight: FontWeight.w700,
                  color: const Color.fromRGBO(131, 76, 52, 1)),
            ),
            Text(
              widget.subjectObj.name,
              style: GoogleFonts.dmSans(
                  fontSize: 15,
                  fontWeight: FontWeight.w400,
                  color: const Color.fromRGBO(131, 76, 52, 1)),
            ),
            Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  height: 311,
                  width: 334,
                  margin: const EdgeInsets.symmetric(vertical: 190),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      color: const Color.fromRGBO(246, 221, 195, 1)),
                ),
                Positioned(
                    top: 150,
                    left: 120,
                    child: Container(
                        height: 85,
                        width: 85,
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Color.fromRGBO(250, 245, 241, 1),
                            boxShadow: [
                              BoxShadow(
                                  blurRadius: 10,
                                  color: Color.fromRGBO(0, 0, 0, 0.1))
                            ]),
                        child: Text(
                          widget.subjectObj.firstLetter,
                          style: GoogleFonts.dmSans(
                              fontSize: 40,
                              fontWeight: FontWeight.w700,
                              color: widget.subjectObj.firstLetterColor),
                        ))),
                Positioned(
                  top: 250,
                  child: Container(
                    height: 105,
                    width: 300,
                    padding: const EdgeInsets.all(15),
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(248, 145, 87, 1),
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 8,
                              color: Color.fromRGBO(0, 0, 0, 0.1))
                        ]),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SizedBox(
                            width: 180,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Total won quiz's till now",
                                  style: GoogleFonts.dmSans(
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Text(
                                  "Lorem Ipsum has been the industry's standard dummy scrambled it to make a type specimen book.",
                                  style: GoogleFonts.dmSans(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                  ),
                                )
                              ],
                            )),
                        const SizedBox(
                          width: 7,
                        ),
                        Container(
                          height: 82,
                          width: 82,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border:
                                  Border.all(color: Colors.white, width: 1)),
                          child: PieChart(
                            totalValue: widget.subjectObj.totalQuestion! + 0.0,
                            chartValuesOptions: const ChartValuesOptions(
                              showChartValueBackground: false,
                              showChartValues: false,
                            ),
                            animationDuration: const Duration(seconds: 3),
                            dataMap: getDataMap(),
                            centerText:
                                '${((widget.subjectObj.correctQuestion! * 100) ~/ widget.subjectObj.totalQuestion!)}%',
                            centerTextStyle: GoogleFonts.dmSans(
                                fontSize: 10,
                                fontWeight: FontWeight.w400,
                                color: Colors.black),
                            ringStrokeWidth: 6,
                            chartRadius: 500,
                            initialAngleInDegree: 0,
                            colorList: const [Colors.white],
                            centerWidget: Container(
                              height: 51,
                              width: 51,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  border: Border.all(
                                      color: Colors.white, width: 1)),
                            ),
                            legendOptions: const LegendOptions(
                                showLegends: false, showLegendsInRow: false),
                            chartType: ChartType.ring,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 380,
                  left: 20,
                  child: Container(
                    height: 95,
                    width: 142,
                    padding: const EdgeInsets.all(13),
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(250, 245, 241, 1),
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 8,
                              color: Color.fromRGBO(0, 0, 0, 0.1))
                        ]),
                    child: Column(
                      children: [
                        Text(
                          'Solved',
                          style: GoogleFonts.dmSans(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                              color: const Color.fromRGBO(131, 76, 52, 0.9)),
                        ),
                        Text(
                          'Questions',
                          style: GoogleFonts.dmSans(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                              color: const Color.fromRGBO(131, 76, 52, 0.9)),
                        ),
                        const Spacer(),
                        Text(
                          '${widget.subjectObj.solvedQuestion}',
                          style: GoogleFonts.dmSans(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: const Color.fromRGBO(131, 76, 52, 1)),
                        )
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: 380,
                  right: 20,
                  child: Container(
                    height: 95,
                    width: 142,
                    padding: const EdgeInsets.all(13),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(26, 181, 134, 1),
                        borderRadius: BorderRadius.circular(15),
                        boxShadow: const [
                          BoxShadow(
                              blurRadius: 8,
                              color: Color.fromRGBO(0, 0, 0, 0.1))
                        ]),
                    child: Column(
                      children: [
                        Text(
                          'Correct',
                          style: GoogleFonts.dmSans(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                        Text(
                          'Questions',
                          style: GoogleFonts.dmSans(
                              fontSize: 12,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        ),
                        const Spacer(),
                        Text(
                          '${widget.subjectObj.correctQuestion}',
                          style: GoogleFonts.dmSans(
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
