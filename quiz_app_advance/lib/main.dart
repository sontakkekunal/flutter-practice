import 'package:flutter/material.dart';
import 'package:quiz_app_advance/intro_screen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color.fromRGBO(250, 245, 241, 1)),
      home:const IntroPage()
    );
  }
}
