import 'package:flutter/material.dart';

class Subject {
  String name;
  String firstLetter;
  Color firstLetterColor;
  int? totalQuestion;
  int? solvedQuestion;
  int? correctQuestion;
  Subject(
      {required this.name,
      required this.firstLetter,
      required this.firstLetterColor});
}