
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}
class MyApp extends StatelessWidget{
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        backgroundColor: Colors.grey[300],
        appBar: AppBar(
          title:  Text("Core2Web",style: TextStyle(color: Colors.blue.shade900,fontSize: 30),),
          flexibleSpace: Image.network("https://media.istockphoto.com/vectors/social-media-blue-seamless-pattern-vector-id690445882?k=20&m=690445882&s=612x612&w=0&h=Y8PXV590DeTKKuOdl5Nwc8xiTOW5Ty1iJfzUDTKjKgc=",fit: BoxFit.cover,),

          actions: const [
            IconButton(onPressed: null, icon: Icon(Icons.qr_code_scanner_rounded),color: Colors.blueAccent)

          ],
          bottom: const PreferredSize(preferredSize: Size.zero,child: Text("You must know code till the core..",style: TextStyle(fontSize: 15,color: Colors.black87),textAlign: TextAlign.left,),),
        ),
        body:
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration:  BoxDecoration(borderRadius: const BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(30)),color: Colors.grey.shade300),
            child:   SingleChildScrollView(
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Row(
                    children: [
                      Text("My Courses",style: TextStyle(fontSize: 25)),
                      Spacer(),
                      TextButton(onPressed: null, child: Text("See all",style: TextStyle(color: Colors.lightBlueAccent),)),
                      SizedBox(
                        height: 35
                      )
                    ],
                  ),
                   SingleChildScrollView(
                     scrollDirection: Axis.horizontal,

                     child: Row(
                       //mainAxisAlignment: MainAxisAlignment.start,
                       //crossAxisAlignment: CrossAxisAlignment.start,
                       children: [
                         const SizedBox(width: 15,),

                         Container(
                           height: 200,
                           width: 225,
                           //alignment: Alignment.centerLeft,
                           decoration:  BoxDecoration(
                               borderRadius: const BorderRadius.all(Radius.circular(20)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.shade300,
                                    offset: const Offset(4.0, 4.0),
                                    blurRadius: 15,
                                    spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                           ),
                           child:Column(

                             children: [

                               ClipRRect(
                                 borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                 child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1690122564635javadsa.jpg"
                                   ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                 )
                               ),
                               const Row(
                                 children: [
                                   Text("   Java DataStructure",style: TextStyle(fontFamily: 'RailWay',fontSize: 15),),
                                 ],
                               ),
                               MyProgressBar(70),
                               const Row(
                                 children: [
                                   Text("   70 % completed "),
                                 ],
                               ),

  
                             ],
                           )
                         ),
                         const SizedBox(width: 21,),
                         Container(
                             height: 200,
                             width: 225,
                             //alignment: Alignment.centerLeft,
                             decoration:  BoxDecoration(
                                 borderRadius: const BorderRadius.all(Radius.circular(20)),color: Colors.grey[300],
                                 boxShadow: [
                                   BoxShadow(
                                       color: Colors.grey.shade300,
                                       offset: const Offset(4.0, 4.0),
                                       blurRadius: 15,
                                       spreadRadius: 1.0
                                   ),
                                   const BoxShadow(
                                       color: Colors.white,
                                       offset: Offset(-4.0, -4.0),
                                       blurRadius: 15,
                                       spreadRadius: 1.0
                                   ),
                                 ]
                             ),
                             child:Column(

                               children: [

                                 ClipRRect(
                                     borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                     child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1687352849855Flutter_Offline.jpg"
                                       ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                     )
                                 ),
                                 const Row(
                                   children: [
                                     Text("   Flutter",style: TextStyle(fontFamily: 'RailWay',fontSize: 15),),
                                   ],
                                 ),
                                 MyProgressBar(49),
                                 const Row(
                                   children: [
                                     Text("    49 % completed "),
                                   ],
                                 ),


                               ],
                             )
                         ),
                         const SizedBox(width: 21,),
                         Container(
                             height: 200,
                             width: 225,
                             //alignment: Alignment.centerLeft,
                             decoration:  BoxDecoration(
                                 borderRadius: const BorderRadius.all(Radius.circular(20)),color: Colors.grey[300],
                                 boxShadow: [
                                   BoxShadow(
                                       color: Colors.grey.shade300,
                                       offset: const Offset(4.0, 4.0),
                                       blurRadius: 15,
                                       spreadRadius: 1.0
                                   ),
                                   const BoxShadow(
                                       color: Colors.white,
                                       offset: Offset(-4.0, -4.0),
                                       blurRadius: 15,
                                       spreadRadius: 1.0
                                   ),
                                 ]
                             ),
                             child:Column(

                               children: [

                                 ClipRRect(
                                     borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                     child: Image.network("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVr9dYkMNecAwdUOlpB5RKjD22aRHvazJbwA&usqp=CAU"
                                       ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                     )
                                 ),
                                 const Row(
                                   children: [
                                     Text("   C, C++ ,DSA",style: TextStyle(fontFamily: 'RailWay',fontSize: 15),),
                                   ],
                                 ),
                                 MyProgressBar(61),
                                 const Row(
                                   children: [
                                     Text("   61 % completed "),
                                   ],
                                 ),


                               ],
                             )
                         ),
                         const SizedBox(width: 15,),
                       ],
                     ),
                    ),
                  const SizedBox(
                      height: 20
                  ),
                  const Row(
                    children: [
                      Text("Online Courses",style: TextStyle(fontSize: 25)),
                      Spacer(),
                      TextButton(onPressed: null, child: Text("See all",style: TextStyle(color: Colors.lightBlueAccent),)),
                      SizedBox(
                          height: 35
                      )
                    ],
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,

                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: 15,),

                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1693399084774Online%20OS%20batch.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  Operating Systems",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   10 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   10 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                 const Row(
                                   children: [
                                        Text("   10 ",style: TextStyle(color: Colors.blue),),
                                      Text("Assignments  "),
                                       Text("   10 ",style: TextStyle(color: Colors.blue),),
                                       Text("MCQ Quiz  "),
                                  ],
                                 ),
                                const Row(
                                  children: [
                                    Text("  ₹ 2000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                 Container(
                                   //color: Colors.blueAccent,
                                   decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueAccent),
                                   child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                        foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Buy Now",style: TextStyle(color: Colors.white))
                                ),
                                 )


                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1694072189723bootcampC.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  C DSA C++ Online",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 5000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueAccent),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Buy Now",style: TextStyle(color: Colors.white))
                                  ),
                                )


                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1702908339680C%20-%20C++.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  C & C++ Programming",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   25 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   25 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   25 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   25 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 2000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueAccent),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Buy Now",style: TextStyle(color: Colors.white))
                                  ),
                                )


                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1694072189723bootcampC.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("   C & Data Structures",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   21 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   21 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   21 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   21 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 4000/-",style: TextStyle(fontSize: 20),),
                                    Text("    50%   -8-0-0-0-",style: TextStyle(color: Colors.blueAccent),)
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueAccent),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Buy Now",style: TextStyle(color: Colors.white))
                                  ),
                                )


                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1702908550961cinner.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("   C Programming",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   12 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   12 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   12 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   12 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 2500/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueAccent),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Buy Now",style: TextStyle(color: Colors.white))
                                  ),
                                )


                              ],
                            )
                        ),
                        const SizedBox(width: 15,),

                      ],
                    ),
                  ),
                  const SizedBox(height: 30,),
                  const Row(
                    children: [
                      Text("Offline Courses",style: TextStyle(fontSize: 25)),
                      Spacer(),
                      TextButton(onPressed: null, child: Text("See all",style: TextStyle(color: Colors.lightBlueAccent),)),
                      SizedBox(
                          height: 35
                      )
                    ],
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,

                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.start,
                      //crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(width: 15,),

                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1702130476788javainnerp.jpeg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  JAVA 2024",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   54 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   54 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   54 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   54 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 10000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueGrey),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Admission Closed",style: TextStyle(color: Colors.white))
                                  ),
                                )


                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/seminar/1697773984570INNERTHUMBNAIL.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  Python & Machine Learning",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   35 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 10000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueGrey),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Admission Closed",style: TextStyle(color: Colors.white))
                                  ),
                                )


                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1687332628078CTHUMBNAIL.png"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  C,DS & C++",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   19 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   19 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   19 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   19 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 10000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueGrey),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Admission Closed",style: TextStyle(color: Colors.white))
                                  ),
                                ),



                              ],
                            )
                        ),
                        const SizedBox(width: 21,),
                        Container(
                            height: 251,
                            width: 225,
                            //alignment: Alignment.centerLeft,
                            decoration:  BoxDecoration(
                                borderRadius: const BorderRadius.all(Radius.circular(17)),color: Colors.grey[300],
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.shade300,
                                      offset: const Offset(4.0, 4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                  const BoxShadow(
                                      color: Colors.white,
                                      offset: Offset(-4.0, -4.0),
                                      blurRadius: 15,
                                      spreadRadius: 1.0
                                  ),
                                ]
                            ),
                            child:Column(

                              children: [

                                ClipRRect(
                                    borderRadius: const BorderRadius.only(topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                                    child: Image.network("https://core2web-do-data.blr1.digitaloceanspaces.com/public/course/1693399084774Online%20OS%20batch.jpg"
                                      ,fit: BoxFit.fill,height: 125,width: double.infinity,
                                    )
                                ),
                                const Row(
                                  children: [
                                    Text("  Operating System",style: TextStyle(fontFamily: 'RailWay',fontSize: 17),),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   6 ",style: TextStyle(color: Colors.blue),),
                                    Text("Topic  "),
                                    Text("   6 ",style: TextStyle(color: Colors.blue),),
                                    Text("Lectures  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("   6 ",style: TextStyle(color: Colors.blue),),
                                    Text("Assignments  "),
                                    Text("   6 ",style: TextStyle(color: Colors.blue),),
                                    Text("MCQ Quiz  "),
                                  ],
                                ),
                                const Row(
                                  children: [
                                    Text("  ₹ 10000/-",style: TextStyle(fontSize: 20),),
                                  ],
                                ),
                                Container(
                                  //color: Colors.blueAccent,
                                  decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(20)),color: Colors.blueGrey),
                                  child: ElevatedButton(
                                      onPressed: null,
                                      style: ElevatedButton.styleFrom(
                                          foregroundColor: Colors.blueAccent,
                                          backgroundColor: Colors.blueAccent,
                                          minimumSize: const Size(250,40)
                                      ), child: const Text("Admission Closed",style: TextStyle(color: Colors.white))
                                  ),
                                ),



                              ],
                            )
                        ),

                        const SizedBox(width: 15,),

                      ],
                    ),
                  ),

                ],
              ),
            ),
      ),
        /*
        bottomNavigationBar: BottomNavigationBar(

          items:  [
            BottomNavigationBarItem(
              icon:Icon(Icons.play_circle_outline_rounded),
              backgroundColor: Colors.blueAccent,
              label: "Home"
            ),

            BottomNavigationBarItem(
              icon:Icon(Icons.mode_comment_outlined),
              backgroundColor: Colors.blueAccent,
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.notifications_none_rounded),
              backgroundColor: Colors.blueAccent,
            ),
            BottomNavigationBarItem(
              icon:Icon(Icons.emoji_emotions_rounded),
              backgroundColor: Colors.blueAccent,
            )


          ],
        ),
        */

      )
    );
  }

}
class MyProgressBar extends StatelessWidget{
  double percent=0;
  MyProgressBar(this.percent,{super.key});


  @override
  Widget build(BuildContext context) {
    return Container(
      height: 10,
      width:200,
      color: Colors.white54,
      //decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(0.11))),
      child: Container(
        height: 10,
        width: 200,
        alignment: Alignment.centerLeft,
        color: Colors.orangeAccent,
        margin: EdgeInsets.only(right: (200-(2*percent))),
        //decoration: const BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(2))),
      ),
    );
  }
}