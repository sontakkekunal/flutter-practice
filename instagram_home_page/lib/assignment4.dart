import 'package:flutter/material.dart';

class Assignment4 extends StatefulWidget {
  const Assignment4({super.key});
  @override
  State<Assignment4> createState() => _Assignment4State();
}

class _Assignment4State extends State<Assignment4> {
  bool flagColor1 = false;
  bool flagColor2 = false;
  bool flagColor3 = false;
  bool flagColor4 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Instagram",
            style: TextStyle(
                fontStyle: FontStyle.italic, color: Colors.black, fontSize: 30),     
          ),
          
          backgroundColor: Colors.black38,
          actions: const [
              Icon(
              Icons.camera_alt_rounded,
              color: Colors.black87,
            ),
            Spacer(),
            IconButton(onPressed: null, icon: Icon(Icons.messenger_outline_rounded)),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Row(
                    children: [
                      Text(
                        "Uesr1 "
                      ),
                      IconButton(onPressed: null, icon:Icon(Icons.account_circle_rounded))
                    ],
                  ),
                  
                  Container(
                      height: 300,
                      width: double.infinity,
                      color: Colors.amber,
                      child: Image.network(
                        "https://i.pinimg.com/originals/58/6c/70/586c7053ade516f544e87a2b2551d2f5.jpg",
                        width: double.infinity,
                        height: double.infinity,
                        
                      ) ,
                      
                      
                      
                      ),
                  Row(
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              flagColor1 = !flagColor1;
                            });
                          },
                          icon: flagColor1
                              ? const Icon(
                                  Icons.favorite_rounded,
                                  color: Colors.red,
                                )
                              : const Icon(Icons.favorite_border_outlined)),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.comment_outlined,
                          )),
                      IconButton(
                          onPressed: () {}, icon: const Icon(Icons.send)),
                      const Spacer(),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.bookmark_outline)),
                      const SizedBox(
                        height: 50,
                      )
                    ],
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Row(
                    children: [
                      Text(
                        "Uesr2 "
                      ),
                      IconButton(onPressed: null, icon:Icon(Icons.account_circle_rounded))
                    ],
                  ),
                  
                  Container(
                      height: 300,
                      width: double.infinity,
                      color: Colors.amber,
                      child: Image.network(
                        "https://i.pinimg.com/originals/58/6c/70/586c7053ade516f544e87a2b2551d2f5.jpg",
                        width: double.infinity,
                        height: double.infinity,
                        
                      ) ,
                      
                      
                      ),
                  Row(
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              flagColor2 = !flagColor2;
                            });
                          },
                          icon: flagColor2
                              ? const Icon(
                                  Icons.favorite_rounded,
                                  color: Colors.red,
                                )
                              : const Icon(Icons.favorite_border_outlined)),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.comment_outlined,
                          )),
                      IconButton(
                          onPressed: () {}, icon: const Icon(Icons.send)),
                      const Spacer(),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.bookmark_outline)),
                      const SizedBox(
                        height: 50,
                      )
                    ],
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Row(
                    children: [
                      Text(
                        "Uesr3 "
                      ),
                      IconButton(onPressed: null, icon:Icon(Icons.account_circle_rounded))
                    ],
                  ),
                  
                  Container(
                      height: 300,
                      width: double.infinity,
                      color: Colors.amber,
                      child: Image.network(
                        "https://i.pinimg.com/originals/58/6c/70/586c7053ade516f544e87a2b2551d2f5.jpg",
                        width: double.infinity,
                        height: double.infinity,
                        
                      ) ,
                      
                      ),
                  Row(
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              flagColor3 = !flagColor3;
                            });
                          },
                          icon: flagColor3
                              ? const Icon(
                                  Icons.favorite_rounded,
                                  color: Colors.red,
                                )
                              : const Icon(Icons.favorite_border_outlined)),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.comment_outlined,
                          )),
                      IconButton(
                          onPressed: () {}, icon: const Icon(Icons.send)),
                      const Spacer(),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.bookmark_outline)),
                      const SizedBox(
                        height: 50,
                      )
                    ],
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Row(
                    children: [
                      Text(
                        "Uesr4 "
                      ),
                      IconButton(onPressed: null, icon:Icon(Icons.account_circle_rounded))
                    ],
                  ),
                  
                  Container(
                      height: 300,
                      width: double.infinity,
                      color: Colors.amber,
                      child: Image.network(
                        "https://i.pinimg.com/originals/58/6c/70/586c7053ade516f544e87a2b2551d2f5.jpg",
                        width: double.infinity,
                        height: double.infinity,
                        
                      ) ,
                      
                      ),
                  Row(
                    children: [
                      IconButton(
                          onPressed: () {
                            setState(() {
                              flagColor4 = !flagColor4;
                            });
                          },
                          icon: flagColor4
                              ? const Icon(
                                  Icons.favorite_rounded,
                                  color: Colors.red,
                                )
                              : const Icon(Icons.favorite_border_outlined)),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(
                            Icons.comment_outlined,
                          )),
                      IconButton(
                          onPressed: () {}, icon: const Icon(Icons.send)),
                      const Spacer(),
                      IconButton(
                          onPressed: () {},
                          icon: const Icon(Icons.bookmark_outline)),
                      const SizedBox(
                        height: 50,
                      )
                    ],
                  )
                ],
              ),
            ],
          ),
        ));
  }
}
