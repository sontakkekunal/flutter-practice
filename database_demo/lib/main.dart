import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

dynamic database;

class Employee {
  final int empId;
  final String empName;
  final double empSal;
  Employee({required this.empId, required this.empName, required this.empSal});
  Map<String, dynamic> getEmployeeMap() {
    return {'empId': empId, 'empName': empName, 'empSal': empSal};
  }

  @override
  String toString() {
    return 'empId: $empId , empName: $empName , empSal: $empSal';
  }
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  database = await openDatabase(
    join(await getDatabasesPath(), "EmployeeDB.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute(
          'CREATE TABLE Employee(empId INTEGER PRIMARY KEY,empName TEXT,empSal REAL)');
    },
  );
  insertEmployeeData(Employee(empId: 1, empName: 'Kunal', empSal: 40000.00));
  insertEmployeeData(Employee(empId: 2, empName: 'Kunal', empSal: 40000.00));
  insertEmployeeData(Employee(empId: 3, empName: 'Kunal3', empSal: 60000.00));
  insertEmployeeData(Employee(empId: 1, empName: 'Kunal', empSal: 40000.00));
  print(await getEmployeeData());
  deleteEmployeeData(Employee(empId: 2, empName: 'Kl', empSal: 40000.00));
  print(await getEmployeeData());
}

Future<void> deleteEmployeeData(Employee empObj) async {
  final localDB = await database;
  localDB.delete('Employee', where: 'empId= ?', whereArgs: [empObj.empId]);
}

Future insertEmployeeData(Employee empObj) async {
  final localDB = await database;
  await database.insert('Employee', empObj.getEmployeeMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<List<Employee>> getEmployeeData() async {
  final localDB = await database;
  List<Map<String, dynamic>> empList = await localDB.query("Employee");
  return List.generate(empList.length, (index) {
    return Employee(
        empId: empList[index]['empId'],
        empName: empList[index]['empName'],
        empSal: empList[index]['empSal']);
  });
}
