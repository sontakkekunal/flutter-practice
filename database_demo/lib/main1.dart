import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';

class Player {
  final String name;
  final int jerNo;
  final int runs;
  final double avg;
  const Player(
      {required this.name,
      required this.jerNo,
      required this.runs,
      required this.avg});
  Map<String, dynamic> getPlayerMap() {
    return {'name': name, 'jerNo': jerNo, 'runs': runs, 'avg': avg};
  }

  @override
  String toString() {
    return "name: $name , jerNo: $jerNo , runs: $runs , avg: $avg";
  }
}

Future insertPlayerData(Player obj) async {
  final localDb = await database;
  await localDb.insert("Player", obj.getPlayerMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<List<Player>> getPlayerData() async {
  final localDB = await database;
  List<Map<String, dynamic>> playerList = await localDB.query("Player");
  return List.generate(playerList.length, (index) {
    return Player(
        avg: playerList[index]['avg'],
        name: playerList[index]['name'],
        jerNo: playerList[index]['jerNo'],
        runs: playerList[index]['runs']);
  });
}

dynamic database;
void main() async {
  runApp(const MainApp());
  database = openDatabase(
    join(await getDatabasesPath(), "PlayerDB.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute(
          'CREATE TABLE Player (name Text,jerNo INTEGER PRIMARY KEY,runs INT ,avg REAL)');
    },
  );
  Player obj1 = const Player(name: "virat", jerNo: 18, runs: 5000, avg: 5.5);
  await insertPlayerData(obj1);
  Player obj2 = const Player(name: "Rohit", jerNo: 45, runs: 60000, avg: 8.9);
  await insertPlayerData(obj2);

  print(await getPlayerData());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: Scaffold(
        body: Center(
          child: Text('Hello World!'),
        ),
      ),
    );
  }
}
