import "dart:ui";

import 'package:flutter/material.dart';
import "package:email_validator/email_validator.dart";
import "package:google_fonts/google_fonts.dart";

class LoginAndSign extends StatefulWidget {
  const LoginAndSign({super.key});
  @override
  State createState() => _LoginAndSignState();
}

class _LoginAndSignState extends State {
  int pageIndex = 0;
  List<Map> loginList = [
    {
      "name": "username",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    }
  ];
  List<Map> userData = [
    {
      "username": "kunal",
      "email": "syz@gmail.com",
      "phone": "1234567890",
      "password": "1234"
    }
  ];
  List<Map> siginList = [
    {
      "name": "Email",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Phone",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Username",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Confirm Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    }
  ];
  Map? account;
  int count = 0;
  bool emailVaildator = false;
  bool phoneNumVerification(String str) {
    if (str.length != 10) {
      return false;
    } else {
      for (int i = 0; i < str.length; i++) {
        int ascii = str.codeUnitAt(i);
        if (ascii < 48 || ascii > 57) {
          return false;
        }
      }
      return true;
    }
  }

  bool usernameVerfication(String username) {
    for (int i = 0; i < userData.length; i++) {
      if (userData[i]["username"] == username) {
        return false;
      }
    }
    return true;
  }

  List<bool> passList = [false, false, false, false, false];
  bool passCritria3(String str) {
    for (int i = 0; i < str.length; i++) {
      int ascii = str.codeUnitAt(i);
      if (ascii >= 48 && ascii <= 57) {
        return true;
      }
    }
    return false;
  }

  bool passCritria2(String str) {
    for (int i = 0; i < str.length; i++) {
      int ascii = str.codeUnitAt(i);
      if (ascii >= 65 && ascii <= 90) {
        return true;
      }
    }
    return false;
  }

  bool passCritria1(String str) {
    for (int i = 0; i < str.length; i++) {
      int ascii = str.codeUnitAt(i);
      if (ascii >= 97 && ascii <= 122) {
        return true;
      }
    }
    return false;
  }

  bool passCritria4(String str) {
    if (str.length >= 8) {
      return true;
    } else {
      return false;
    }
  }

  bool passCritria5(String str) {
    if (str.contains("@") ||
        str.contains("!") ||
        str.contains("\$") ||
        str.contains("%") ||
        str.contains("^") ||
        str.contains("&") ||
        str.contains("*")) {
      return true;
    } else {
      return false;
    }
  }

  bool pass1 = false;
  bool pass2 = false;

  @override
  Widget build(BuildContext context) {
    if (pageIndex == 0) {
      return Scaffold(
          body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: NetworkImage(
                    "https://static.vecteezy.com/system/resources/thumbnails/007/164/537/small/fingerprint-identity-sensor-data-protection-system-podium-hologram-blue-light-and-concept-free-vector.jpg"),
                fit: BoxFit.cover)),
        height: double.infinity,
        width: double.infinity,
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 50,
                width: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white12.withOpacity(0.55),
                ),
                child: Center(
                  child: Text(
                    "Login page",
                    style: GoogleFonts.quicksand(
                        fontSize: 25, fontWeight: FontWeight.w700),
                  ),
                ),
              ),
              Container(
                height: 270,
                width: 380,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white12.withOpacity(0.55)),
                child: ListView.separated(
                  itemCount: loginList.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      height: 20,
                    );
                  },
                  itemBuilder: (BuildContext context, int index) {
                    return TextFormField(
                      controller: loginList[index]["controller"],
                      key: loginList[index]["key"],
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return "Please enter vaild ${loginList[index]["name"]}";
                        }
                        return null;
                      },
                      onChanged: (value) {
                        for (int i = 0;
                            count > 0 && i < loginList.length;
                            i++) {
                          loginList[i]["key"].currentState!.validate();
                        }
                      },
                      decoration: InputDecoration(
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 2.5)),
                          labelText: loginList[index]["name"],
                          hintText: (index == 1)
                              ? "Enter ${loginList[index]["name"]}"
                              : "Enter username,email or phone number",
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                  color: Colors.black87, width: 2.5))),
                    );
                  },
                ),
              ),
              GestureDetector(
                onTap: () {
                  for (int i = 0; i < loginList.length; i++) {
                    loginList[i]["key"].currentState!.validate();
                  }
                  count++;
                  bool hasUser = false;
                  for (int i = 0; i < userData.length; i++) {
                    if ((userData[i]["username"] ==
                                loginList[0]["controller"].text ||
                            userData[i]["email"] ==
                                loginList[0]["controller"].text ||
                            userData[i]["phone"] ==
                                loginList[0]["controller"].text) &&
                        userData[i]["password"] ==
                            loginList[1]["controller"].text) {
                      hasUser = true;
                      account = userData[i];
                      setState(() {
                        pageIndex = 2;
                        emailVaildator = false;
                        for (int i = 0; i < loginList.length; i++) {
                          loginList[i]["controller"].clear();
                        }
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            backgroundColor: Colors.green,
                            content: Text(
                              "Login Succesfully",
                              style: GoogleFonts.quicksand(
                                  fontSize: 16,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.white),
                            )));
                        count = 0;
                      });
                      break;
                    }
                  }
                  if (!hasUser) {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        backgroundColor: Colors.red,
                        content: Text("Inavild username or password",
                            style: GoogleFonts.quicksand(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.white))));
                  }
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white12.withOpacity(0.55),
                      borderRadius: BorderRadius.circular(30)),
                  height: 50,
                  width: 200,
                  child: Center(
                      child: Text(
                    "login",
                    style: GoogleFonts.quicksand(
                        fontSize: 18, fontWeight: FontWeight.w600),
                  )),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white12.withOpacity(0.55)),
                child: TextButton(
                    onPressed: () {
                      setState(() {
                        pageIndex = 1;
                        emailVaildator = false;
                        for (int i = 0; i < loginList.length; i++) {
                          loginList[i]["controller"].clear();
                        }
                        count = 0;
                      });
                    },
                    child: Text(
                      "I don't have account.Sigin?",
                      style: GoogleFonts.quicksand(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: Color.fromARGB(255, 88, 11, 101)),
                    )),
              )
            ],
          ),
        ),
      ));
    } else if (pageIndex == 1) {
      return Scaffold(
        body: Container(
          height: double.infinity,
          width: double.infinity,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      "https://t4.ftcdn.net/jpg/01/19/11/55/360_F_119115529_mEnw3lGpLdlDkfLgRcVSbFRuVl6sMDty.jpg"),
                  fit: BoxFit.cover)),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 15,
                ),
                Container(
                  height: 40,
                  width: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.white12.withOpacity(0.55)),
                  child: Center(
                    child: Text(
                      "Sign in page",
                      style: GoogleFonts.quicksand(
                          fontSize: 25, fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
                Container(
                    height: 320,
                    width: 380,
                    padding:
                        const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
                    decoration: BoxDecoration(
                        color: Colors.white12.withOpacity(0.55),
                        borderRadius: BorderRadius.circular(30)),
                    child: ListView.separated(
                      itemCount: siginList.length,
                      separatorBuilder: (BuildContext context, int index) {
                        if (index == 3) {
                          return Column(
                            children: [
                              const SizedBox(
                                height: 10,
                              ),
                              Row(
                                children: [
                                  Icon(
                                    !passList[0]
                                        ? Icons.circle_outlined
                                        : Icons.check_circle,
                                    color:
                                        passList[0] ? Colors.green : Colors.red,
                                  ),
                                  Text(
                                    "At least one lowercase letter",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: passList[0]
                                            ? Colors.green
                                            : Colors.red),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    !passList[1]
                                        ? Icons.circle_outlined
                                        : Icons.check_circle,
                                    color:
                                        passList[1] ? Colors.green : Colors.red,
                                  ),
                                  Text(
                                    "At least one uppercase letter",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: passList[1]
                                            ? Colors.green
                                            : Colors.red),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    !passList[2]
                                        ? Icons.circle_outlined
                                        : Icons.check_circle,
                                    color:
                                        passList[2] ? Colors.green : Colors.red,
                                  ),
                                  Text(
                                    "At least one number",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: passList[2]
                                            ? Colors.green
                                            : Colors.red),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    !passList[3]
                                        ? Icons.circle_outlined
                                        : Icons.check_circle,
                                    color:
                                        passList[3] ? Colors.green : Colors.red,
                                  ),
                                  Text(
                                    "Be at least 8 character",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: passList[3]
                                            ? Colors.green
                                            : Colors.red),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Icon(
                                    !passList[4]
                                        ? Icons.circle_outlined
                                        : Icons.check_circle,
                                    color:
                                        passList[4] ? Colors.green : Colors.red,
                                  ),
                                  Text(
                                    "Must contains at least 1 special character",
                                    style: GoogleFonts.quicksand(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w500,
                                        color: passList[4]
                                            ? Colors.green
                                            : Colors.red),
                                  )
                                ],
                              ),
                              const SizedBox(
                                height: 10,
                              ),
                            ],
                          );
                        } else {
                          return const SizedBox(
                            height: 10,
                          );
                        }
                      },
                      itemBuilder: (BuildContext context, int index) {
                        return TextFormField(
                          controller: siginList[index]["controller"],
                          key: siginList[index]["key"],
                          obscureText: (!pass1 && index == 3)
                              ? true
                              : (!pass2 && index == 4)
                                  ? true
                                  : false,
                          validator: (String? val) {
                            if (index == 0) {
                              if (val != null) {
                                emailVaildator = EmailValidator.validate(val);

                                if (!emailVaildator) {
                                  return "Please enter vaild email";
                                }
                              } else if (count > 0 && val == null) {
                                return "Please enter vaild email";
                              }
                            } else if (index == 1) {
                              if ((val != null && !phoneNumVerification(val)) ||
                                  (count > 0 && val == null)) {
                                return "Please enter vaild phone number";
                              }
                            } else if (index == 2) {
                              if (val == null || val.isEmpty) {
                                return "Please enter vaild username";
                              } else if (val.contains(" ")) {
                                return "Please enter vaild username";
                              } else if (val.isNotEmpty) {
                                int ascii = val.trim().codeUnitAt(0);
                                if (ascii >= 65 && ascii <= 90) {
                                  return "username must not start with Upperclass letter";
                                } else if (!usernameVerfication(val)) {
                                  return "User already taken";
                                }
                              }
                            } else if (index == 3) {
                              if ((val == null || val.isEmpty) && count > 0) {
                                return "please enter vaild password";
                              }
                            } else if (index == 4) {
                              if ((val != null &&
                                      siginList[3]["controller"].text != val) ||
                                  (count > 0 && (val == null || val.isEmpty))) {
                                return "Password don't match";
                              }
                            }

                            return null;
                          },
                          onChanged: (String val) {
                            if (index == 3) {
                              setState(() {
                                passList[0] = passCritria1(val);
                                passList[1] = passCritria2(val);
                                passList[2] = passCritria3(val);
                                passList[3] = passCritria4(val);
                                passList[4] = passCritria5(val);
                              });
                            }
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          decoration: InputDecoration(
                              hintText: siginList[index]["name"],
                              suffixIcon: (index == 3)
                                  ? IconButton(
                                      icon: Icon((pass1)
                                          ? Icons.visibility
                                          : Icons.visibility_off),
                                      onPressed: () {
                                        pass1 = !pass1;
                                        setState(() {});
                                      },
                                    )
                                  : (index == 4)
                                      ? IconButton(
                                          icon: Icon((pass2)
                                              ? Icons.visibility
                                              : Icons.visibility_off),
                                          onPressed: () {
                                            pass2 = !pass2;
                                            setState(() {});
                                          },
                                        )
                                      : null,
                              labelText: siginList[index]["name"],
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: const BorderSide(
                                      color: Color.fromARGB(255, 243, 33, 33),
                                      width: 2.4)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: const BorderSide(
                                      color: Colors.blue, width: 2.4)),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: const BorderSide(
                                      color: Colors.red, width: 2.4)),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide: const BorderSide(
                                      color: Colors.black, width: 2.4))),
                        );
                      },
                    )),
                GestureDetector(
                  onTap: () {
                    count++;
                    int checkCount = 0;
                    for (int i = 0; i < siginList.length; i++) {
                      if (siginList[i]["key"].currentState!.validate()) {
                        checkCount++;
                      }
                    }
                    if (checkCount == siginList.length &&
                        !passList.contains(false)) {
                      count = 0;
                      pageIndex = 3;
                      account = {
                        "username": siginList[2]["controller"].text,
                        "email": siginList[0]["controller"].text,
                        "phone": siginList[1]["controller"].text,
                        "password": siginList[3]["controller"].text
                      };
                      userData.add(account!);
                      for (int i = 0; i < siginList.length; i++) {
                        siginList[i]["controller"].clear();
                      }
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          backgroundColor: Colors.green,
                          content: Text(
                            "Account created Succesfully",
                            style: GoogleFonts.quicksand(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          )));
                      emailVaildator = false;
                      pass1 = false;
                      pass2 = false;
                      setState(() {});
                    }
                  },
                  child: Container(
                    height: 45,
                    width: 120,
                    decoration: BoxDecoration(
                        color: Colors.white12.withOpacity(0.55),
                        borderRadius: BorderRadius.circular(30)),
                    child: Center(
                      child: Text(
                        "Sign",
                        style: GoogleFonts.quicksand(
                            fontSize: 18, fontWeight: FontWeight.w600),
                      ),
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                      color: Colors.white.withOpacity(0.55),
                      borderRadius: BorderRadius.circular(30)),
                  child: TextButton(
                      onPressed: () {
                        count = 0;
                        pageIndex = 0;
                        for (int i = 0; i < siginList.length; i++) {
                          siginList[i]["controller"].clear();
                        }
                        emailVaildator = false;
                        pass1 = false;
                        pass2 = false;
                        setState(() {});
                      },
                      child: Text("I already have account. Login?",
                          style: GoogleFonts.quicksand(
                              fontSize: 16,
                              fontWeight: FontWeight.w600,
                              color: Color.fromARGB(255, 88, 11, 101)))),
                )
              ],
            ),
          ),
        ),
      );
    } else {
      return Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                  onPressed: () {
                    passList.clear();
                    passList = [false, false, false, false, false];
                    account = null;
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        backgroundColor: Colors.green,
                        content: Text(
                          "Logout succesfully",
                          style: GoogleFonts.quicksand(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: Colors.white),
                        )));
                    setState(() {
                      pageIndex = 0;
                    });
                  },
                  icon: const Icon(
                    Icons.logout_rounded,
                    color: Colors.red,
                  ))
            ],
          ),
          body: Center(
              child: Container(
            height: 500,
            width: 350,
            decoration: BoxDecoration(
                color: Colors.orange.withOpacity(0.4),
                borderRadius: BorderRadius.circular(30)),
            child: Column(
              children: [
                Text("Username:  ${account!["username"]}"),
                Text("email: ${account!["email"]}"),
                Text("phone: ${account!["phone"]}"),
                Text("password: ${account!["password"]} ")
              ],
            ),
          )));
    }
  }
}
