import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:plant_ui/detail_screen.dart';
import 'package:plant_ui/plantClass.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class Home extends StatefulWidget {
  const Home({super.key});

  State createState() => _HomeState();
}

class _HomeState extends State {
  int activeIndex = 0;
  final CarouselController _carouselController = CarouselController();
  List<Map<String, dynamic>> offerList = [
    {'off': 30, 'plantPath': 'images/plant3.png', 'date': '02-23 April'},
    {'off': 30, 'plantPath': 'images/plant3.png', 'date': '02-23 April'},
    {'off': 30, 'plantPath': 'images/plant3.png', 'date': '02-23 April'},
    {'off': 30, 'plantPath': 'images/plant3.png', 'date': '02-23 April'},
    {'off': 30, 'plantPath': 'images/plant3.png', 'date': '02-23 April'}
  ];
  Widget getContainer(Plant plantObj) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => PlantDetail(
            plantObj: plantObj,
          ),
        ));
      },
      child: Container(
        height: 188,
        width: 141,
        padding: const EdgeInsets.all(14),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(9.4),
            color: Colors.white,
            boxShadow: const [
              BoxShadow(
                  offset: Offset(0, 7.52),
                  blurRadius: 18.8,
                  color: Color.fromRGBO(0, 0, 0, 0.06))
            ]),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: Image.asset(
                plantObj.plantImg[0],
                height: 122.8,
                width: 90.24,
                fit: BoxFit.cover,
              ),
            ),
            //const SizedBox(height: 5,),
            Text(
              plantObj.plantName,
              style: GoogleFonts.dmSans(
                  fontWeight: FontWeight.w400, fontSize: 13.16),
            ),
            SizedBox(
              width: 141,
              child: Row(
                children: [
                  Text(
                    "₹ ${plantObj.price}",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w600,
                        fontSize: 16.22,
                        color: Color.fromRGBO(62, 102, 24, 1)),
                  ),
                  const Spacer(),
                  Container(
                    height: 26,
                    width: 26,
                    decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromRGBO(237, 238, 235, 1)),
                    child: Image.asset(
                      'images/shoppingbag2.png',
                      height: 14,
                      width: 14,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(left: 20),
        child: Column(
          children: [
            Row(
              children: [
                const Spacer(),
                Image.asset(
                  "images/design3.png",
                  height: 81,
                  width: 284,
                  fit: BoxFit.cover,
                )
              ],
            ),
            Container(
              decoration: const BoxDecoration(boxShadow: [
                BoxShadow(
                    offset: Offset(10, 10),
                    color: Colors.white,
                    blurRadius: 30,
                    spreadRadius: 15)
              ]),
              child: Row(
                children: [
                  SizedBox(
                    height: 64,
                    width: 176,
                    child: Text(
                      "Find your favorite plants",
                      style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w500,
                        fontSize: 24,
                      ),
                    ),
                  ),
                  const Spacer(),
                  Container(
                    height: 40,
                    width: 40,
                    margin: const EdgeInsets.only(right: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              blurRadius: 16.1,
                              offset: Offset(0, 4))
                        ],
                        borderRadius: BorderRadius.circular(10)),
                    child: Image.asset(
                      "images/shoppingbag.png",
                    ),
                  )
                ],
              ),
            ),
            Expanded(
                child: Container(
              margin: EdgeInsets.only(top: 40),
              //padding: const EdgeInsets.only(top: 40),
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CarouselSlider.builder(
                        itemCount: offerList.length,
                        itemBuilder: (context, index, realIndex) {
                          return Container(
                            height: 109,
                            width: 360,
                            margin: const EdgeInsets.only(right: 15),
                            padding: const EdgeInsets.symmetric(horizontal: 27),
                            decoration: BoxDecoration(
                                color: Color.fromRGBO(204, 231, 185, 1),
                                borderRadius: BorderRadius.circular(10)),
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 102,
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "${offerList[index]['off']}% OFF",
                                        style: GoogleFonts.poppins(
                                            fontWeight: FontWeight.w600,
                                            fontSize: 24),
                                      ),
                                      Text(
                                        "${offerList[index]['date']}",
                                        style: GoogleFonts.poppins(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 14,
                                            color:
                                                Color.fromRGBO(0, 0, 0, 0.6)),
                                      )
                                    ],
                                  ),
                                ),
                                const Spacer(),
                                Image.asset(offerList[index]['plantPath'],
                                    height: 108, width: 120, fit: BoxFit.cover)
                              ],
                            ),
                          );
                        },
                        carouselController: _carouselController,
                        options: CarouselOptions(
                            viewportFraction: 0.92,
                            height: 108,
                            enableInfiniteScroll: false,
                            autoPlay: true,
                            enlargeCenterPage: true,
                            pageSnapping: false,
                            onPageChanged: (index, reason) {
                              setState(() {
                                activeIndex = index;
                              });
                            },
                            enlargeStrategy: CenterPageEnlargeStrategy.zoom,
                            autoPlayAnimationDuration:
                                const Duration(seconds: 2))),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        AnimatedSmoothIndicator(
                            onDotClicked: (index) {
                              _carouselController.animateToPage(index,
                                  duration: const Duration(milliseconds: 300));
                            },
                            effect: const JumpingDotEffect(
                                verticalOffset: 13,
                                dotHeight: 8,
                                dotWidth: 8,

                                //type: SwapType.yRotation,
                                activeDotColor: Color.fromRGBO(62, 102, 24, 1),
                                dotColor: Color.fromRGBO(197, 214, 181, 1)),
                            activeIndex: activeIndex,
                            count: offerList.length),
                      ],
                    ),
                    const SizedBox(
                      height: 28,
                    ),
                    Text(
                      "Indoor",
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500, fontSize: 20),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: 200,
                      width: double.infinity,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: plantList.length,
                        separatorBuilder: (context, index) => const SizedBox(
                          width: 20,
                        ),
                        itemBuilder: (context, index) {
                          return getContainer(plantList[index]);
                        },
                      ),
                    ),
                    Container(
                      width: 360,
                      margin: const EdgeInsets.only(top: 20),
                      decoration: BoxDecoration(
                          border: Border.all(
                        width: 1,
                        color: Color.fromRGBO(204, 211, 196, 1),
                      )),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    SizedBox(
                      height: 100,
                    ),
                    Text(
                      "Outdoor",
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w500, fontSize: 20),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: 200,
                      width: double.infinity,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: plantList.length,
                        separatorBuilder: (context, index) => const SizedBox(
                          width: 20,
                        ),
                        itemBuilder: (context, index) {
                          return getContainer(plantList[index]);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ))
          ],
        ),
      ),
    );
  }
}
