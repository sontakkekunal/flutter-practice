import 'package:flutter/material.dart';
import 'package:plant_ui/GetStartPage.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      theme: ThemeData(scaffoldBackgroundColor: const Color.fromRGBO(251, 247, 248, 1)),
        debugShowCheckedModeBanner: false, home: const GetStart());
  }
}
