List<Plant> plantList = [
  Plant(
      plantImg: ['images/plant4.png', 'images/plant4.png', 'images/plant4.png'],
      plantName: 'Snake Plants',
      price: 350,
      description:
          'Plansts make your life with minimal and happy love the plants more and enjoy life.',
      height1: 30,
      height2: 40,
      temp1: 20,
      temp2: 25,
      pot: 'Ciramic Pot'),
  Plant(
      plantImg: ['images/plant4.png', 'images/plant4.png', 'images/plant4.png'],
      plantName: 'Snake Plants',
      price: 350,
      description:
          'Plansts make your life with minimal and happy love the plants more and enjoy life.',
      height1: 30,
      height2: 40,
      temp1: 20,
      temp2: 25,
      pot: 'Ciramic Pot'),
  Plant(
      plantImg: ['images/plant4.png', 'images/plant4.png', 'images/plant4.png'],
      plantName: 'Snake Plants',
      price: 350,
      description:
          'Plansts make your life with minimal and happy love the plants more and enjoy life.',
      height1: 30,
      height2: 40,
      temp1: 20,
      temp2: 25,
      pot: 'Ciramic Pot'),
  Plant(
      plantImg: ['images/plant4.png', 'images/plant4.png', 'images/plant4.png'],
      plantName: 'Snake Plants',
      price: 350,
      description:
          'Plansts make your life with minimal and happy love the plants more and enjoy life.',
      height1: 30,
      height2: 40,
      temp1: 20,
      temp2: 25,
      pot: 'Ciramic Pot'),
  Plant(
      plantImg: ['images/plant4.png', 'images/plant4.png', 'images/plant4.png'],
      plantName: 'Snake Plants',
      price: 350,
      description:
          'Plansts make your life with minimal and happy love the plants more and enjoy life.',
      height1: 30,
      height2: 40,
      temp1: 20,
      temp2: 25,
      pot: 'Ciramic Pot'),
  Plant(
      plantImg: ['images/plant4.png', 'images/plant4.png', 'images/plant4.png'],
      plantName: 'Snake Plants',
      price: 350,
      description:
          'Plansts make your life with minimal and happy love the plants more and enjoy life.',
      height1: 30,
      height2: 40,
      temp1: 20,
      temp2: 25,
      pot: 'Ciramic Pot'),
];

class Plant {
  List<String> plantImg;
  String plantName;
  int price;
  String description;
  int height1;
  int height2;
  int temp1;
  int temp2;
  String pot;

  Plant(
      {required this.plantImg,
      required this.plantName,
      required this.price,
      required this.description,
      required this.height1,
      required this.height2,
      required this.temp1,
      required this.temp2,
      required this.pot});
}
