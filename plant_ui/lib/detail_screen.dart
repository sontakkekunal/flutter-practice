import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:carousel_slider/utils.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'plantClass.dart';

class PlantDetail extends StatefulWidget {
  Plant plantObj;
  PlantDetail({super.key, required this.plantObj});

  @override
  State<StatefulWidget> createState() {
    _PlantDetailState.plantObj = plantObj;
    return _PlantDetailState();
  }
}

class _PlantDetailState extends State {
  static Plant? plantObj;
  int activeIndex = 0;
  CarouselController _carouselController = CarouselController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(24),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: const Icon(Icons.arrow_back_rounded),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CarouselSlider.builder(
                    itemCount: plantObj!.plantImg.length,
                    itemBuilder: (context, index, realIndex) {
                      return Image.asset(
                        plantObj!.plantImg[index],
                        width: 200,
                        height: double.infinity,
                        fit: BoxFit.cover,
                      );
                    },
                    carouselController: _carouselController,
                    options: CarouselOptions(
                        viewportFraction: 1,
                        height: 320,
                        enableInfiniteScroll: false,
                        autoPlay: true,
                        enlargeCenterPage: true,
                        pageSnapping: false,
                        onPageChanged: (index, reason) {
                          setState(() {
                            activeIndex = index;
                          });
                        },
                        enlargeStrategy: CenterPageEnlargeStrategy.zoom,
                        autoPlayAnimationDuration: const Duration(seconds: 2))),
                const SizedBox(
                  height: 17,
                ),
                AnimatedSmoothIndicator(
                    onDotClicked: (int index) {
                      _carouselController.animateToPage(index,
                          duration: const Duration(milliseconds: 300));
                    },
                    effect: const SwapEffect(
                        dotHeight: 8,
                        dotWidth: 8,
                        type: SwapType.yRotation,
                        activeDotColor: Color.fromRGBO(62, 102, 24, 1),
                        dotColor: Color.fromRGBO(197, 214, 181, 1)),
                    activeIndex: activeIndex,
                    count: plantObj!.plantImg.length),
              ],
            ),

            // Container(
            //   margin: const EdgeInsets.only(left: 82),
            //   child: Image.asset(plantObj!.plantImg[0],
            //       height: 243.75, width: 195, fit: BoxFit.cover),
            // ),

            SizedBox(
              height: 81,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(plantObj!.plantName,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w600, fontSize: 22)),
                  Text(plantObj!.description,
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400, fontSize: 13)),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(11),
              width: double.infinity,
              height: 200,
              decoration: BoxDecoration(
                  color: Color.fromRGBO(118, 152, 75, 1),
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      SizedBox(
                        height: 74,
                        width: 67,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset('images/height.png'),
                            Text("Height",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Colors.white)),
                            Text(
                                "${plantObj!.height1}cm-${plantObj!.height2}cm",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10,
                                    color: Color.fromRGBO(255, 255, 255, 0.6)))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 80,
                        width: 82,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset('images/temp.png'),
                            Text("Temperature",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Colors.white)),
                            Text("${plantObj!.temp1}°C-${plantObj!.temp2}°C",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10,
                                    color: Color.fromRGBO(255, 255, 255, 0.6)))
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 77,
                        width: 67,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset('images/pot.png'),
                            Text("Pot",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 12,
                                    color: Colors.white)),
                            Text(plantObj!.pot,
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 10,
                                    color: Color.fromRGBO(255, 255, 255, 0.6)))
                          ],
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 13),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text("Total Price",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w500,
                                    fontSize: 14,
                                    color: Colors.white)),
                            Text("₹ ${plantObj!.price}",
                                style: GoogleFonts.poppins(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Colors.white))
                          ],
                        ),
                      ),
                      Container(
                        height: 48.39,
                        width: 150,
                        margin: const EdgeInsets.only(right: 8),
                        decoration: BoxDecoration(
                            color: Color.fromRGBO(103, 133, 74, 1),
                            borderRadius: BorderRadius.circular(8.06)),
                        child: Center(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset('images/shoppingbag3.png'),
                              Text("   Add to cart",
                                  style: GoogleFonts.rubik(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500,
                                      fontSize: 14.52))
                            ],
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
