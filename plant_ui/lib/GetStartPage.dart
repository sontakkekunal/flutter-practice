import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:plant_ui/loginPage.dart';

class GetStart extends StatefulWidget {
  const GetStart({super.key});

  @override
  State createState() => _GetStartState();
}

class _GetStartState extends State {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
            margin: const EdgeInsets.only(top: 44),
            child: Image.asset(
              "images/plant1.png",
              height: 464,
              width: 360,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            width: 250,
            margin: const EdgeInsets.only(left: 56.56),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Enjoy your ",
                    style: GoogleFonts.poppins(
                        fontWeight: FontWeight.w400, fontSize: 34.22)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text("life with ",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w400, fontSize: 34.22)),
                    Text("Plants",
                        style: GoogleFonts.poppins(
                            fontWeight: FontWeight.w500, fontSize: 34.22)),
                  ],
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => LoginWithNum(),
              ));
            },
            child: Container(
              width: 320,
              height: 50,
              margin: const EdgeInsets.only(top: 50, left: 45),
              //padding: const EdgeInsets.symmetric(horizontal: 92, vertical: 15),
              decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.15),
                        blurRadius: 40,
                        offset: Offset(0, 20))
                  ],
                  gradient: const LinearGradient(colors: [
                    Color.fromRGBO(124, 180, 70, 1),
                    Color.fromRGBO(62, 102, 24, 1)
                  ], stops: [
                    0,
                    0.9
                  ], begin: Alignment.topCenter, end: Alignment.bottomCenter),
                  borderRadius: BorderRadius.circular(10)),
              child: Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Get Started   ",
                        style: GoogleFonts.rubik(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: Colors.white)),
                    const Icon(
                      Icons.arrow_forward_ios_rounded,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
