import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:plant_ui/home_page.dart';

class VerficationPage extends StatefulWidget {
  const VerficationPage({super.key});

  @override
  State<StatefulWidget> createState() => _VerficationPageState();
}

class _VerficationPageState extends State {
  int _errorCount = 4;
  final List<GlobalKey> _otpKeys = [
    GlobalKey<FormFieldState>(),
    GlobalKey<FormFieldState>(),
    GlobalKey<FormFieldState>(),
    GlobalKey<FormFieldState>()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            children: [
              GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Container(
                    margin: const EdgeInsets.only(left: 10),
                    child: const Icon(Icons.arrow_back_rounded)),
              ),
              const Spacer(),
              Image.asset("images/design2.png")
            ],
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 20),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Verification",
                  style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w700,
                    fontSize: 20,
                  ),
                ),
                SizedBox(
                  height: 46,
                  width: 272,
                  child: Text(
                      "Enter the OTP code from the phone we just sent you.",
                      style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                          color: Color.fromRGBO(0, 0, 0, 0.6))),
                ),
                Container(
                  height: 64,
                  width: double.infinity,
                  margin: const EdgeInsets.only(top: 34),
                  child: ListView.separated(
                    itemCount: 4,
                    scrollDirection: Axis.horizontal,
                    separatorBuilder: (context, index) => const SizedBox(
                      width: 32,
                    ),
                    itemBuilder: (context, index) {
                      return Container(
                        height: 85,
                        width: 67,
                        decoration: const BoxDecoration(
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.06),
                                blurRadius: 20,
                                offset: Offset(0, 8))
                          ],
                        ),
                        child: TextFormField(
                          //key: _otpKeys[index],
                          textAlign: TextAlign.center,
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(1),
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          autofocus: true,
                          onChanged: (String value) {
                            if (index != 3 && value.isNotEmpty) {
                              FocusScope.of(context).nextFocus();
                            } 
                          },
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          validator: (String? value) {
                            if (value == null || value.isEmpty) {
                              _errorCount++;
                              return "";
                            } else {
                              _errorCount--;
                            }
                          },
                          keyboardType: TextInputType.phone,
                          decoration: InputDecoration(
                              hintText: "${index + 1}",
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1, color: Colors.red)),
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1, color: Colors.red)),
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1,
                                      color: Color.fromRGBO(204, 211, 196, 1))),
                              enabledBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(10),
                                  borderSide: const BorderSide(
                                      width: 1,
                                      color:
                                          Color.fromRGBO(204, 211, 196, 1)))),
                        ),
                      );
                    },
                  ),
                ),
                const SizedBox(
                  height: 34,
                ),
                Row(
                  children: [
                    Text("Don’t receive OTP code!",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w400,
                          fontSize: 14,
                        )),
                    Text(" Resend",
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ))
                  ],
                ),
                GestureDetector(
                  onTap: () {
                    if (_errorCount == 0) {
                      Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => const Home(),
                      ));
                    }
                  },
                  child: Container(
                    height: 50,
                    margin: const EdgeInsets.only(top: 10),
                    width: double.infinity,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.15),
                              blurRadius: 40,
                              offset: Offset(0, 20))
                        ],
                        gradient: const LinearGradient(
                            colors: [
                              Color.fromRGBO(124, 180, 70, 1),
                              Color.fromRGBO(62, 102, 24, 1)
                            ],
                            stops: [
                              0,
                              0.9
                            ],
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter),
                        borderRadius: BorderRadius.circular(10)),
                    child: Center(
                      child: Text("Submit",
                          style: GoogleFonts.rubik(
                              fontWeight: FontWeight.w500,
                              fontSize: 18,
                              color: Colors.white)),
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
