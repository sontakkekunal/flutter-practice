import 'package:flutter/material.dart';
import 'package:my_jio/my_jio.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyJio(),
      debugShowCheckedModeBanner: false,
    );
  }
}
