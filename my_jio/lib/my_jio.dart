import 'package:flutter/material.dart';

class MyJio extends StatefulWidget {
  const MyJio({super.key});

  @override
  State createState() => _MyJio();
}

class _MyJio extends State {
  bool expandClick = true;
  List<Map> recharge = [
    {
      "plan": "₹ 239",
      "purchase": "Purchased on 14 Sep,2023 9:36 AM",
    },
    {
      "plan": "₹ 239",
      "purchase": "Purchased on 18 Dec,2023 7:36 AM",
    },
    {
      "plan": "₹ 439",
      "purchase": "Purchased on 14 Jan,2024 9:36 AM",
    },
    {
      "plan": "₹ 239",
      "purchase": "Purchased on 10 Sep,2024 10:01 AM",
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color.fromARGB(255, 245, 243, 243),
        appBar: AppBar(
          backgroundColor: Color.fromARGB(255, 7, 49, 234),
          actions: [
            const SizedBox(
              width: 15,
            ),
            Container(
                height: 30,
                width: 30,
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Image.network(
                  "https://media.imgcdn.org/repo/2023/03/myjio-for-everything-jio/myjio-for-everything-jio-logo.png",
                  fit: BoxFit.cover,
                )),
            const Spacer(),
            const Icon(
              Icons.notification_add_rounded,
              color: Colors.white,
            ),
            const SizedBox(
              width: 15,
            ),
            const Icon(
              Icons.qr_code_scanner_rounded,
              color: Colors.white,
            ),
            const SizedBox(
              width: 15,
            ),
            const Icon(
              Icons.mic,
              color: Colors.white,
            )
          ],
        ),
        body: SizedBox(
            height: double.infinity,
            width: double.infinity,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 75,
                            width: 75,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/5gLogo.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 88,
                            width: 88,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/fiber.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 68,
                            width: 68,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/music.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 70,
                            width: 70,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/tv.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 78,
                            width: 78,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/shop.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 72,
                            width: 72,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/upi.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 68,
                            width: 68,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/games.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 73,
                            width: 73,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/bank.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 78,
                            width: 78,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/play.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 68,
                            width: 68,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/news.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        SizedBox(
                            height: 68,
                            width: 68,
                            //decoration: const BoxDecoration(
                            //     borderRadius:
                            //       BorderRadius.all(Radius.circular(20))),
                            child: Image.asset("images/shorts.jpg",
                                height: 40, width: 40, fit: BoxFit.cover)),
                        const SizedBox(
                          width: 17,
                        ),
                        (expandClick)
                            ? ElevatedButton(
                                onPressed: () {
                                  setState(() {
                                    expandClick = false;
                                  });
                                },
                                child: Image.asset("images/more.jpg",
                                    height: 80, width: 30, fit: BoxFit.cover))
                            : SizedBox(
                                height: 70,
                                width: 70,
                                //decoration: const BoxDecoration(
                                //     borderRadius:
                                //       BorderRadius.all(Radius.circular(20))),
                                child: Image.asset("images/backup.jpg",
                                    height: 40, width: 40, fit: BoxFit.cover),
                              )
                      ],
                    ),
                  ),
                  (expandClick == false)
                      ? SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: [
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 78,
                                  width: 78,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/health.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 72,
                                  width: 72,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/eLearning.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 69,
                                  width: 69,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/movies.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 72,
                                  width: 72,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/events.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                            ],
                          ),
                        )
                      : const SizedBox(),
                  (expandClick == false)
                      ? SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 70,
                                  width: 70,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/jiostore.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 72,
                                  width: 72,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/offers.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                              const SizedBox(
                                width: 17,
                              ),
                              SizedBox(
                                  height: 75,
                                  width: 75,
                                  //decoration: const BoxDecoration(
                                  //     borderRadius:
                                  //       BorderRadius.all(Radius.circular(20))),
                                  child: Image.asset("images/pharmacy.jpg",
                                      height: 40,
                                      width: 40,
                                      fit: BoxFit.cover)),
                            ],
                          ),
                        )
                      : const SizedBox(),
                  (expandClick == false)
                      ? ElevatedButton(
                          onPressed: () {
                            setState(() {
                              expandClick = true;
                            });
                          },
                          child: Image.asset("images/up.jpg",
                              height: 60, width: 30, fit: BoxFit.cover))
                      : const SizedBox(),
                  const SizedBox(
                    height: 20,
                  ),
                  SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: [
                          const SizedBox(
                            width: 15,
                          ),
                          SizedBox(
                              height: 100,
                              width: 200,
                              child: ClipRRect(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(15)),
                                child: Image.network(
                                  "https://images.gizbot.com/webp/img/2021/09/myjio-picture-quiz-desserts-quiz-answers-1631260626.jpg",
                                  fit: BoxFit.cover,
                                ),
                              )),
                          const SizedBox(
                            width: 15,
                          ),
                          SizedBox(
                              height: 100,
                              width: 200,
                              child: ClipRRect(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(15)),
                                child: Image.network(
                                  "https://images.indianexpress.com/2023/03/JioFiber-Backup.jpeg?w=414",
                                  fit: BoxFit.cover,
                                ),
                              )),
                          const SizedBox(
                            width: 15,
                          ),
                          SizedBox(
                              height: 100,
                              width: 200,
                              child: ClipRRect(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(15)),
                                child: Image.network(
                                  "https://onlytech.com/wp-content/uploads/2023/09/Jio-AirFiber-1024x576.jpg.webp",
                                  fit: BoxFit.cover,
                                ),
                              )),
                          const SizedBox(
                            width: 15,
                          ),
                          SizedBox(
                              height: 100,
                              width: 200,
                              child: ClipRRect(
                                borderRadius:
                                    const BorderRadius.all(Radius.circular(15)),
                                child: Image.network(
                                  "https://www.zingoy.com/blog/wp-content/uploads/2021/03/JIO-DTH-1.jpg",
                                  fit: BoxFit.cover,
                                ),
                              ))
                        ],
                      )),
                  const SizedBox(
                    height: 15,
                  ),
                  Row(
                    children: [
                      const SizedBox(
                        width: 10,
                      ),
                      Container(
                        height: 240,
                        width: 359,
                        decoration: BoxDecoration(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(20)),
                            color: Color.fromARGB(255, 255, 255, 255),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.shade300,
                                  offset: const Offset(4.0, 4.0),
                                  blurRadius: 15,
                                  spreadRadius: 1.0),
                              const BoxShadow(
                                  color: Color.fromARGB(255, 215, 196, 196),
                                  offset: Offset(-4.0, -4.0),
                                  blurRadius: 15,
                                  spreadRadius: 1.0),
                            ]),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                const SizedBox(
                                  width: 7,
                                ),
                                Image.network(
                                  "https://images.fonearena.com/blog/wp-content/uploads/2022/12/Jio-5G-supported-phones.jpg",
                                  height: 60,
                                  width: 60,
                                ),
                                const Text(
                                  "    Mobile prepaid 1234567890 ",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500),
                                ),
                                const Spacer(),
                                const Icon(Icons.arrow_right_sharp),
                                const SizedBox(
                                  width: 7,
                                )
                              ],
                            ),
                            const Row(
                              children: [
                                SizedBox(
                                  width: 15,
                                ),
                                Text(
                                  "No data",
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red),
                                ),
                                Spacer(),
                                Text(
                                  "5G Trial  ",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                                Icon(
                                  Icons.info_sharp,
                                  size: 15,
                                  color: Color.fromARGB(255, 84, 85, 84),
                                ),
                                SizedBox(
                                  width: 50,
                                ),
                              ],
                            ),
                            const Row(
                              children: [
                                SizedBox(
                                  width: 15,
                                ),
                                Text(
                                  "0 KB ",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500,
                                      color: Colors.red),
                                ),
                                Spacer(),
                                Text(
                                  "Unlimited  ",
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                                SizedBox(
                                  width: 25,
                                ),
                              ],
                            ),
                            const Row(children: [
                              SizedBox(
                                width: 15,
                              ),
                              Text("left Of 6 GB ",
                                  style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                  )),
                            ]),
                            const SizedBox(
                              height: 7,
                            ),
                            const Row(
                              children: [
                                SizedBox(
                                  width: 15,
                                ),
                                Text("You're on ₹395-84 days-6 GB plan",
                                    style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500,
                                    )),
                              ],
                            ),
                            const Row(
                              children: [
                                SizedBox(
                                  width: 15,
                                ),
                                Text("Expires on 28Mar 2024",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                    )),
                              ],
                            ),
                            ElevatedButton(
                                onPressed: () {},
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.white,
                                    minimumSize: const Size(340, 60)),
                                child: const Text("Recharge",
                                    style: TextStyle(
                                      fontSize: 20,
                                      color: Color.fromARGB(255, 0, 34, 255),
                                      fontWeight: FontWeight.w400,
                                    ))),
                            const SizedBox(
                              height: 10,
                            )
                          ],
                        ),
                      ),
                      const SizedBox(
                        width: 15,
                      )
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Row(
                    children: [
                      SizedBox(
                        width: 10,
                      ),
                      Text("Recharge History",
                          style: TextStyle(
                            fontSize: 20,
                            color: Color.fromARGB(255, 0, 34, 255),
                            fontWeight: FontWeight.w400,
                          )),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  Container(
                      height: 350,
                      width: 360,
                      decoration: BoxDecoration(
                          borderRadius:
                              const BorderRadius.all(Radius.circular(20)),
                          color: Color.fromARGB(255, 255, 255, 255),
                          boxShadow: [
                            BoxShadow(
                                color: Colors.grey.shade300,
                                offset: const Offset(4.0, 4.0),
                                blurRadius: 15,
                                spreadRadius: 1.0),
                            const BoxShadow(
                                color: Color.fromARGB(255, 238, 234, 234),
                                offset: Offset(-4.0, -4.0),
                                blurRadius: 15,
                                spreadRadius: 1.0),
                          ]),
                      child: ListView.builder(
                        itemCount: recharge.length,
                        itemBuilder: (context, index) {
                          return Container(
                              decoration: BoxDecoration(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(20)),
                                  color: Color.fromARGB(255, 255, 255, 255),
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.shade300,
                                        offset: const Offset(4.0, 4.0),
                                        blurRadius: 15,
                                        spreadRadius: 1.0),
                                    const BoxShadow(
                                        color:
                                            Color.fromARGB(255, 229, 230, 236),
                                        offset: Offset(-4.0, -4.0),
                                        blurRadius: 15,
                                        spreadRadius: 1.0),
                                  ]),
                              height: 180,
                              width: 280,
                              child: Column(
                                children: [
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  const Row(
                                    children: [
                                      SizedBox(
                                        width: 12,
                                      ),
                                      Text("PLAN",
                                          style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.w400,
                                          )),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      const SizedBox(
                                        width: 12,
                                      ),
                                      Text("${recharge[index]["plan"]}",
                                          style: const TextStyle(
                                            fontSize: 23,
                                            fontWeight: FontWeight.w700,
                                          )),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      const SizedBox(
                                        width: 12,
                                      ),
                                      Text("${recharge[index]["purchase"]}",
                                          style: const TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w400,
                                          )),
                                    ],
                                  ),
                                  const Row(
                                    children: [
                                      SizedBox(
                                        width: 12,
                                      ),
                                      Text("Payment mode",
                                          style: TextStyle(
                                            fontSize: 13,
                                            fontWeight: FontWeight.w400,
                                          )),
                                      Spacer(),
                                      Text("UPI",
                                          style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600,
                                          )),
                                      SizedBox(
                                        width: 15,
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  ElevatedButton(
                                      onPressed: () {},
                                      style: ElevatedButton.styleFrom(
                                          backgroundColor: Colors.white,
                                          minimumSize: const Size(320, 60)),
                                      child: const Text("View details",
                                          style: TextStyle(
                                            fontSize: 20,
                                            color:
                                                Color.fromARGB(255, 0, 34, 255),
                                            fontWeight: FontWeight.w400,
                                          )))
                                ],
                              ));
                        },
                      ))
                ],
              ),
            )));
  }
}
