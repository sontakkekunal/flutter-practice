import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:login_registration/View/get_start_screen.dart';

import 'package:login_registration/View/signup_screen.dart';
import 'package:login_registration/View/submission_screen.dart';
import 'package:provider/provider.dart';
import 'package:connection_notifier/connection_notifier.dart';
import 'Controller/validation_controller.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (BuildContext context) {
          return ValidationController();
        })
      ],
      child: ConnectionNotifier(
        child: MaterialApp(
            routes: {
              "sigupScreen": (context) {
                return const SignupScreen();
              },
              "submissionScreen": (context) {
                return const SubmissionScreen();
              }
            },
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
                textTheme: TextTheme(
                    headlineSmall: GoogleFonts.quicksand(
                        fontSize: 14,
                        fontWeight: FontWeight.w400,
                        color: Colors.black38),
                    labelLarge: GoogleFonts.quicksand(
                        fontSize: 26, fontWeight: FontWeight.w600),
                    headlineMedium: GoogleFonts.quicksand(
                        fontSize: 20, fontWeight: FontWeight.w500),
                    bodyMedium: GoogleFonts.quicksand(
                        fontSize: 22,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                    bodySmall: GoogleFonts.quicksand(
                      fontSize: 15.5,
                      fontWeight: FontWeight.w500,
                    ),
                    labelSmall: GoogleFonts.quicksand(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.black38),
                    labelMedium: GoogleFonts.quicksand(
                        fontSize: 22, fontWeight: FontWeight.w400),
                    titleSmall: GoogleFonts.quicksand(
                        color: Colors.white,
                        fontSize: 20,
                        fontStyle: FontStyle.italic,
                        decorationThickness: 1.5,
                        decorationColor: Colors.white,
                        decoration: TextDecoration.underline,
                        fontWeight: FontWeight.w400),
                    titleMedium: GoogleFonts.quicksand(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w400),
                    titleLarge: GoogleFonts.quicksand(
                        color: Colors.white,
                        fontSize: 45,
                        fontWeight: FontWeight.w600))),
            home: const GetStartScreen()),
      ),
    );
  }
}
