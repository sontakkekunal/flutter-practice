class RegistrationModel {
  String firstName;
  String lastName;
  String? aadharCard;
  String? panCard;
  String email;
  String mobileNo;
  String role;
  String? password;
  RegistrationModel({
    required this.firstName,
    required this.lastName,
    required this.email,
    required this.mobileNo,
    required this.role,
  });
  @override
  String toString() {
    return "RegistrationModel[\n firstName: $firstName,\n lastName: $lastName,\n aadharCard: $aadharCard,\n panCard: $panCard,\n email: $email,\n mobileNo: $mobileNo,\n role: $role,\n password: $password,\n]";
  }

  Map<String, String>? getMap() {
    if (aadharCard != null && panCard != null && password != null) {
      return {
        'firstName': firstName,
        'lastName': lastName,
        'aadharCard': aadharCard!,
        'panCard': panCard!,
        'email': email,
        'mobileNo': mobileNo,
        'roles': role,
        'password': password!
      };
    } else {
      return null;
    }
  }
}
