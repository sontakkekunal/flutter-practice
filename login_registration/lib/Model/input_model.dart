import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class InputModel {
  String name;
  GlobalKey<FormFieldState> globalKey;
  TextEditingController textEditingController;
  TextInputType textInputType;
  bool obscureText;
  List<TextInputFormatter> inputFormatters;
  IconData? preFixIconData;
  IconData? suffixIconData;
  InputModel(
      {required this.name,
      required this.globalKey,
      required this.textEditingController,
      required this.textInputType,
      this.preFixIconData,
      this.inputFormatters = const [],
      this.suffixIconData,
      this.obscureText = false});
}
