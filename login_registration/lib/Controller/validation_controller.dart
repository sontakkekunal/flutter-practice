import 'dart:convert';
import 'dart:developer';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:login_registration/Model/input_model.dart';
import 'package:login_registration/Model/registration_model.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import '../Model/pass_criteria_model.dart';
import '../View/show_dialog.dart';
import 'package:http/http.dart' as http;

class ValidationController extends ChangeNotifier {
  RegistrationModel? registrationModel;
  bool reSubmissionFlag = true;
  bool clearFeilds = false;
  final Connectivity connectivity = Connectivity();
  Future<bool> checkConnetivity() async {
    List<ConnectivityResult> connectivityResult =
        await connectivity.checkConnectivity();
    return connectivityResult.first != ConnectivityResult.none;
  }

  bool checkAadhar(String val) {
    if (val.length != 12) {
      return false;
    } else {
      for (int i = 0; i < val.length; i++) {
        int ascii = val.codeUnitAt(i);
        if (ascii < 48 || ascii > 57) {
          return false;
        }
      }
    }
    return true;
  }

  bool checkPan(String val) {
    if (val.length != 10) {
      return false;
    } else {
      for (int i = 0; i < val.length; i++) {
        int ascii = val.codeUnitAt(i);
        if ((i > 4 && i < 9) && (ascii < 48 || ascii > 57)) {
          return false;
        } else if ((i < 5 || i == 9) && (ascii < 65 || ascii > 90)) {
          return false;
        }
      }
    }
    return true;
  }

  void refresh() {
    notifyListeners();
  }

  bool checkPass(String pass, List<PassCriteriaModel> list) {
    list[0].val = pass.length > 8 && pass.length < 65;
    int upCount = 0;
    int lowCount = 0;
    int numCount = 0;
    int specCount = 0;
    for (int i = 0; i < pass.length; i++) {
      int ascii = pass.codeUnitAt(i);
      if (ascii >= 65 && ascii <= 90) {
        upCount++;
      }
      if (ascii >= 97 && ascii <= 122) {
        lowCount++;
      }
      if (ascii >= 48 && ascii <= 57) {
        numCount++;
      }
      if ((ascii >= 33 && ascii <= 47) ||
          (ascii >= 58 && ascii <= 64) ||
          (ascii >= 91 && ascii <= 96) ||
          (ascii >= 123 && ascii <= 126)) {
        specCount++;
      }
    }
    list[1].val = upCount > 1;
    list[2].val = lowCount > 1;
    list[3].val = numCount > 1;
    list[4].val = specCount > 1;
    for (int i = 0; i < list.length; i++) {
      if (!list[i].val) {
        return false;
      }
    }
    return true;
  }

  bool confirmPass(String pass1, String pass2) {
    return pass1 == pass2;
  }

  bool checkMobileNum(String num) {
    if (num.length != 10) {
      return false;
    } else {
      for (int i = 0; i < num.length; i++) {
        int ascii = num.codeUnitAt(i);
        if (ascii < 48 || ascii > 57) {
          return false;
        }
      }
    }
    return true;
  }

  Future<void> submit1(
      {required List<InputModel> inputList,
      required bool signupFlag,
      required BuildContext context}) async {
    bool val = await checkConnetivity();
    if (val) {
      if (signupFlag) {
        bool done = true;
        for (int i = 0; i < inputList.length - 1; i++) {
          InputModel action = inputList[i];
          action.textEditingController.text =
              action.textEditingController.text.trim();
          if (!action.globalKey.currentState!.validate()) {
            done = false;
          }
        }
        if (done) {
          registrationModel = RegistrationModel(
            firstName: inputList[0].textEditingController.text.trim(),
            lastName: inputList[1].textEditingController.text.trim(),
            email: inputList[2].textEditingController.text.trim(),
            mobileNo: inputList[3].textEditingController.text.trim(),
            role: inputList[4].textEditingController.text.trim(),
          );
          // for (var action in _inputList) {
          //   action.textEditingController.clear();
          // }

          Navigator.of(context).pushNamed("submissionScreen").then((onValue) {
            for (var action in inputList) {
              if (clearFeilds) {
                action.textEditingController.clear();
              }
              FormFieldState? state = action.globalKey.currentState;
              if (state != null) {
                state.reset();
              }
            }
            clearFeilds = false;

            if (reSubmissionFlag) {
              getAwesomeDialog(
                      title: 'Confirm Resubmission',
                      desc: "",
                      showCloseIcon: false,
                      animType: AnimType.topSlide,
                      dialogType: DialogType.warning,
                      btnOkOnPress: () {
                        Navigator.of(context).pop();
                        Navigator.of(context).pushNamed('sigupScreen');
                      },
                      btnOkColor: Colors.red,
                      btnOkIcon: Icons.cancel_outlined,
                      context: context)
                  .show();
            } else {
              reSubmissionFlag = true;
            }
          });
        }
      } else if (!signupFlag) {
        bool email = inputList[2].globalKey.currentState!.validate();
        bool pass = inputList[5].globalKey.currentState!.validate();
        if (email && pass) {
          Map<String, String> loginMap = {
            "username": inputList[2].textEditingController.text.trim(),
            "password": inputList[5].textEditingController.text.trim()
          };
          Uri url = Uri.parse(
              'https://graceful-solace-production.up.railway.app/jwt/login');
          getProcessingScreen(context: context);
          http.Response response = await http.post(
            url,
            body: jsonEncode(loginMap),
          );
          Map<String, dynamic> respo = json.decode(response.body);

          bool isLogin = respo['message'] == 'success';

          log(respo.toString());
          Navigator.of(context, rootNavigator: true).pop();
          if (isLogin) {
            inputList[2].textEditingController.clear();
          }
          inputList[5].textEditingController.clear();

          getAwesomeDialog(
                  title:
                      isLogin ? 'Login Successfully' : 'Login Unsuccessfully',
                  desc: isLogin ? "" : respo['message']!,
                  showCloseIcon: false,
                  animType: AnimType.bottomSlide,
                  dialogType: isLogin ? DialogType.success : DialogType.error,
                  btnOkOnPress: () {
                    if (isLogin) {
                      inputList[2].globalKey.currentState!.reset();
                    }

                    inputList[5].globalKey.currentState!.reset();
                  },
                  btnOkColor: isLogin ? Colors.green : Colors.red,
                  btnOkIcon:
                      isLogin ? Icons.done_outlined : Icons.cancel_outlined,
                  context: context)
              .show();
        }
      }
    } else {
      getFlutterToast();
    }
  }

  Future<void> submit2(
      {required List<InputModel> inputList,
      required BuildContext context}) async {
    bool val = await checkConnetivity();
    if (val) {
      bool done = true;

      for (var action in inputList) {
        if (!action.globalKey.currentState!.validate()) {
          done = false;
        }
      }
      if (done) {
        reSubmissionFlag = false;
        registrationModel!.aadharCard =
            inputList[0].textEditingController.text.trim();
        registrationModel!.panCard =
            inputList[1].textEditingController.text.trim();
        registrationModel!.password =
            inputList[2].textEditingController.text.trim();
        log("$registrationModel");
        Uri url = Uri.parse(
            "https://graceful-solace-production.up.railway.app/account/register");

        Map<String, String>? registMap = registrationModel!.getMap();

        if (registMap != null) {
          log(registMap.toString());
          getProcessingScreen(context: context);
          http.Response response = await http.post(url,
              body: jsonEncode(registMap),
              headers: {
                "Accept": "application/json",
                "content-type": "application/json"
              });

          Map<String, dynamic> info = json.decode(response.body);
          log(info.toString());
          bool success = info['code'] == 'Successful';
          Navigator.of(context, rootNavigator: true).pop();
          if (success) {
            clearFeilds = true;
          }
          getAwesomeDialog(
                  title: 'Account Created ${info['code']}',
                  desc: success ? "" : info['message']!,
                  showCloseIcon: success ? false : true,
                  dismissOnTouchOutside: success ? false : true,
                  animType: AnimType.bottomSlide,
                  dialogType: success ? DialogType.success : DialogType.error,
                  btnOkOnPress: () {
                    if (success) {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                      Navigator.of(context).pushNamed('sigupScreen');
                    }
                  },
                  btnOkColor: success ? Colors.green : Colors.red,
                  btnOkIcon:
                      success ? Icons.done_rounded : Icons.cancel_outlined,
                  context: context)
              .show();
        }
      }
    } else {
      getFlutterToast();
    }
  }
}
