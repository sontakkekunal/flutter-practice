import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:login_registration/View/my_clip.dart';
import 'package:provider/provider.dart';
import '../Controller/validation_controller.dart';
import '../Model/input_model.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SignupScreenState();
  }
}

class _SignupScreenState extends State {
  bool _signupFlag = false;
  bool _animationOver = false;
  final List<InputModel> _inputList = [
    InputModel(
        name: "First Name",
        globalKey: GlobalKey<FormFieldState>(),
        textEditingController: TextEditingController(),
        preFixIconData: Icons.person_outline_rounded,
        textInputType: TextInputType.name),
    InputModel(
        name: "Last Name",
        globalKey: GlobalKey<FormFieldState>(),
        textEditingController: TextEditingController(),
        preFixIconData: Icons.person_2_outlined,
        textInputType: TextInputType.name),
    InputModel(
        name: "Email",
        globalKey: GlobalKey<FormFieldState>(),
        preFixIconData: Icons.mail_outline_outlined,
        textEditingController: TextEditingController(),
        textInputType: TextInputType.emailAddress),
    InputModel(
        name: "Mobile Number",
        globalKey: GlobalKey<FormFieldState>(),
        inputFormatters: [
          LengthLimitingTextInputFormatter(10),
          FilteringTextInputFormatter.digitsOnly
        ],
        preFixIconData: Icons.phone_android_rounded,
        textEditingController: TextEditingController(),
        textInputType: TextInputType.phone),
    InputModel(
        name: "Role",
        globalKey: GlobalKey<FormFieldState>(),
        preFixIconData: Icons.find_replace_rounded,
        textEditingController: TextEditingController(),
        textInputType: TextInputType.text),
    InputModel(
        name: "Password",
        globalKey: GlobalKey<FormFieldState>(),
        preFixIconData: Icons.key_rounded,
        suffixIconData: Icons.visibility_off_rounded,
        textEditingController: TextEditingController(),
        obscureText: true,
        textInputType: TextInputType.visiblePassword),
  ];
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      _animationOver = true;

      Provider.of<ValidationController>(context, listen: false).refresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    ValidationController validationController =
        Provider.of<ValidationController>(context, listen: false);
    return Scaffold(
      body: SingleChildScrollView(
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 400),
          height: height,
          width: width,
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: _signupFlag ? Alignment.topRight : Alignment.topLeft,
            end: _signupFlag ? Alignment.centerLeft : Alignment.bottomRight,
            //stops: [0.3, 0.7],
            colors: [
              Colors.deepPurpleAccent.shade400,
              Colors.purpleAccent.shade200
            ],
          )),
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: width * 0.03, vertical: height * 0.05),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Hero(
                  tag: "Registration",
                  child: Text(
                    _signupFlag ? "   Registration" : "Login",
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                Consumer<ValidationController>(builder: (context, val1, val2) {
                  return AnimatedContainer(
                    duration: const Duration(milliseconds: 300),
                    transform: Matrix4.translationValues(
                        _animationOver
                            ? _signupFlag
                                ? -width * 0.4
                                : 0
                            : width * 0.6,
                        _animationOver
                            ? _signupFlag
                                ? -height * 0.075
                                : 0
                            : 0,
                        0),
                    child: Icon(
                      Icons.account_circle_outlined,
                      color: Colors.white,
                      size: _signupFlag ? width * 0.15 : width * 0.4,
                    ),
                  );
                }),
                ClipPath(
                  clipper: _signupFlag ? null : MyClip(),
                  child: AnimatedContainer(
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(
                        horizontal: width * 0.05, vertical: height * 0.03),
                    height: _signupFlag ? height * 0.68 : height * 0.35,
                    width: width,
                    decoration: BoxDecoration(
                        boxShadow: const [
                          BoxShadow(
                              spreadRadius: 2,
                              blurRadius: 8,
                              color: Colors.black12)
                        ],
                        borderRadius: BorderRadius.only(
                            topLeft: const Radius.circular(50),
                            bottomRight: _signupFlag
                                ? const Radius.circular(50)
                                : Radius.zero,
                            bottomLeft: _signupFlag
                                ? const Radius.circular(50)
                                : Radius.zero,
                            topRight: const Radius.circular(50)),
                        //color: Colors.transparent.withOpacity(0.1)
                        color: Colors.white12.withOpacity(0.52)),
                    duration: const Duration(milliseconds: 280),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        AnimatedContainer(
                          duration: const Duration(milliseconds: 300),
                          width: width,
                          height: _signupFlag ? height * 0.54 : height * 0.2,
                          child: ListView.separated(
                            separatorBuilder: (context, index) {
                              return SizedBox(
                                height: height * 0.015,
                              );
                            },
                            padding: EdgeInsets.zero,
                            itemCount: _signupFlag ? _inputList.length - 1 : 2,
                            itemBuilder: (context, index) {
                              return Consumer<ValidationController>(
                                  builder: (context, val1, val2) {
                                return AnimatedContainer(
                                  color: _animationOver
                                      ? Colors.transparent
                                      : Colors.transparent.withOpacity(0.1),
                                  transform: Matrix4.translationValues(
                                      _animationOver
                                          ? 0
                                          : index % 2 == 0
                                              ? -width * 0.5
                                              : width * 0.5,
                                      0,
                                      0),
                                  duration: Duration(
                                      milliseconds: 300 + (index * 100)),
                                  child: TextFormField(
                                    inputFormatters: _signupFlag
                                        ? _inputList[index].inputFormatters
                                        : index == 0
                                            ? _inputList[2].inputFormatters
                                            : _inputList[5].inputFormatters,
                                    obscureText: _signupFlag
                                        ? _inputList[index].obscureText
                                        : index == 0
                                            ? _inputList[2].obscureText
                                            : _inputList[5].obscureText,
                                    autovalidateMode:
                                        AutovalidateMode.onUserInteraction,
                                    key: _signupFlag
                                        ? _inputList[index].globalKey
                                        : index == 0
                                            ? _inputList[2].globalKey
                                            : _inputList[5].globalKey,
                                    controller: _signupFlag
                                        ? _inputList[index]
                                            .textEditingController
                                        : index == 0
                                            ? _inputList[2]
                                                .textEditingController
                                            : _inputList[5]
                                                .textEditingController,
                                    style:
                                        Theme.of(context).textTheme.labelMedium,
                                    validator: (String? val) {
                                      if (val != null) {
                                        val = val.trim();
                                      }
                                      if (val == null || val.isEmpty) {
                                        return "Please enter valid ${_signupFlag ? _inputList[index].name : index == 0 ? _inputList[2].name : _inputList[5].name}";
                                      }

                                      if (index == 2 &&
                                          !EmailValidator.validate(val)) {
                                        return "Please enter valid email";
                                      }
                                      if (index == 3 &&
                                          !validationController
                                              .checkMobileNum(val)) {
                                        return "Enter valid Mobile number";
                                      }
                                    },
                                    keyboardType: _signupFlag
                                        ? _inputList[index].textInputType
                                        : index == 0
                                            ? _inputList[2].textInputType
                                            : _inputList[5].textInputType,
                                    decoration: InputDecoration(
                                        suffixIconColor: Colors.black38,
                                        prefixIconColor: Colors.black38,
                                        prefixIcon: Icon(_signupFlag
                                            ? _inputList[index].preFixIconData
                                            : index == 0
                                                ? _inputList[2].preFixIconData
                                                : _inputList[5].preFixIconData),
                                        suffixIcon: GestureDetector(
                                          onTap: () {
                                            if ((!_signupFlag && index == 1) ||
                                                (_signupFlag && index == 5)) {
                                              _inputList[5].obscureText =
                                                  !_inputList[5].obscureText;
                                              if (_inputList[5].obscureText) {
                                                _inputList[5].suffixIconData =
                                                    Icons
                                                        .visibility_off_rounded;
                                              } else {
                                                _inputList[5].suffixIconData =
                                                    Icons.visibility_rounded;
                                              }
                                              Provider.of<ValidationController>(
                                                      context,
                                                      listen: false)
                                                  .refresh();
                                            }
                                          },
                                          child: Icon(_signupFlag
                                              ? _inputList[index].suffixIconData
                                              : index == 0
                                                  ? _inputList[2].suffixIconData
                                                  : _inputList[5]
                                                      .suffixIconData),
                                        ),
                                        labelText: _signupFlag
                                            ? _inputList[index].name
                                            : index == 0
                                                ? "Enter ${_inputList[2].name}"
                                                : "Enter ${_inputList[5].name}",
                                        hintStyle: Theme.of(context)
                                            .textTheme
                                            .labelSmall,
                                        hintText: _signupFlag
                                            ? _inputList[index].name
                                            : index == 0
                                                ? _inputList[2].name
                                                : _inputList[5].name,
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: const BorderSide(
                                                color: Colors.black38,
                                                width: 4)),
                                        focusedErrorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: const BorderSide(
                                                color: Colors.red, width: 4)),
                                        enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: const BorderSide(
                                                color: Colors.black38,
                                                width: 4)),
                                        errorBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            borderSide: const BorderSide(
                                                color: Colors.red, width: 4))),
                                  ).animate(effects: [
                                    FadeEffect(
                                        begin: 0,
                                        end: 1,
                                        duration: Duration(
                                            milliseconds: 300 + (index * 100)))
                                  ]),
                                );
                              });
                            },
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            await validationController.submit1(
                                inputList: _inputList,
                                signupFlag: _signupFlag,
                                context: context);
                          },
                          child: Hero(
                            tag: "45",
                            child: AnimatedContainer(
                              duration: const Duration(milliseconds: 230),
                              height:
                                  _signupFlag ? height * 0.06 : width * 0.14,
                              width: _signupFlag ? width * 0.3 : width * 0.14,
                              decoration: BoxDecoration(
                                // shape: _signupFlag
                                //     ? BoxShape.rectangle
                                //     : BoxShape.circle,
                                borderRadius: _signupFlag
                                    ? BorderRadius.circular(30)
                                    : BorderRadius.circular(30),
                                boxShadow: const [
                                  BoxShadow(
                                      blurRadius: 6,
                                      spreadRadius: 4,
                                      color: Colors.black26)
                                ],
                                gradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  //stops: [0.3, 0.7],
                                  colors: [
                                    Colors.redAccent.shade400,
                                    Colors.pinkAccent.shade100,
                                  ],
                                ),
                              ),
                              alignment: Alignment.center,
                              child: _signupFlag
                                  ? Text(
                                      "Next",
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyMedium,
                                    )
                                  : const Icon(
                                      Icons.arrow_forward_rounded,
                                      color: Colors.white,
                                      size: 42,
                                    ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Text(
                      _signupFlag ? "I have account" : "I don't have account",
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    TextButton(
                      onPressed: () {
                        _signupFlag = !_signupFlag;
                        for (int i = 0; i < _inputList.length; i++) {
                          if (i != 2) {
                            _inputList[i].textEditingController.clear();
                          }
                          // FormFieldState? state =
                          //     _inputList[i].globalKey.currentState;
                          // if (state != null) {
                          //   //_inputList[i].textEditingController.clear();
                          //   state.reset();
                          // }
                        }
                        setState(() {});
                        for (int i = 0; i < _inputList.length; i++) {
                          FormFieldState? state =
                              _inputList[i].globalKey.currentState;
                          if (state != null) {
                            state.reset();
                          }
                        }
                      },
                      child: Text(
                        _signupFlag ? "login" : "Registration",
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
