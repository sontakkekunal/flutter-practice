import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

AwesomeDialog getAwesomeDialog({
  required String title,
  required String desc,
  required DialogType dialogType,
  required AnimType animType,
  required IconData btnOkIcon,
  required Color btnOkColor,
  required var btnOkOnPress,
  bool dismissOnTouchOutside = false,
  bool showCloseIcon = false,
  required BuildContext context,
}) {
  return AwesomeDialog(
    context: context,
    dialogType: dialogType,
    title: title,
    dialogBorderRadius: BorderRadius.circular(30),
    animType: animType,
    btnOkIcon: btnOkIcon,
    btnOkColor: btnOkColor,
    dismissOnTouchOutside: dismissOnTouchOutside,
    btnOkOnPress: btnOkOnPress,
    showCloseIcon: showCloseIcon,
    titleTextStyle: Theme.of(context).textTheme.labelLarge,
    desc: desc,
    descTextStyle: Theme.of(context).textTheme.headlineMedium,
  );
}

void getProcessingScreen({required BuildContext context}) {
  showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) {
        return const Center(
            child: CircularProgressIndicator(
          strokeWidth: 7,
          color: Colors.purpleAccent,
        ));
      });
}

void getFlutterToast() {
  Fluttertoast.showToast(
      msg: "Please check the network connection",
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0);
}
