import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:login_registration/Controller/validation_controller.dart';
import 'package:login_registration/Model/pass_criteria_model.dart';
import 'package:provider/provider.dart';
import '../Model/input_model.dart';

class SubmissionScreen extends StatefulWidget {
  const SubmissionScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SubmissionScreenState();
  }
}

class _SubmissionScreenState extends State {
  bool _animationOver = false;

  final List<PassCriteriaModel> _passCriteria = [
    PassCriteriaModel(criteria: "between 9 to 64 characters", val: false),
    PassCriteriaModel(
        criteria: "at least two Upper case characters", val: false),
    PassCriteriaModel(
        criteria: "at least two Lower case characters", val: false),
    PassCriteriaModel(criteria: "at least two number characters", val: false),
    PassCriteriaModel(criteria: "at least two special characters", val: false),
  ];
  final List<InputModel> _inputList = [
    InputModel(
        name: "Aadhar Number",
        preFixIconData: Icons.document_scanner_rounded,
        globalKey: GlobalKey<FormFieldState>(),
        inputFormatters: [
          LengthLimitingTextInputFormatter(12),
          FilteringTextInputFormatter.digitsOnly
        ],
        textEditingController: TextEditingController(),
        textInputType: TextInputType.phone),
    InputModel(
        name: "Pan Card Number",
        preFixIconData: Icons.edit_document,
        inputFormatters: [
          LengthLimitingTextInputFormatter(10),
        ],
        globalKey: GlobalKey<FormFieldState>(),
        textEditingController: TextEditingController(),
        textInputType: TextInputType.text),
    InputModel(
        name: "Password",
        globalKey: GlobalKey<FormFieldState>(),
        preFixIconData: Icons.key_rounded,
        inputFormatters: [
          LengthLimitingTextInputFormatter(64),
        ],
        suffixIconData: Icons.visibility_off_rounded,
        textEditingController: TextEditingController(),
        textInputType: TextInputType.visiblePassword,
        obscureText: true),
    InputModel(
        name: "Confirm Password",
        globalKey: GlobalKey<FormFieldState>(),
        preFixIconData: Icons.key_rounded,
        inputFormatters: [
          LengthLimitingTextInputFormatter(64),
        ],
        suffixIconData: Icons.visibility_off_rounded,
        textEditingController: TextEditingController(),
        textInputType: TextInputType.visiblePassword,
        obscureText: true),
  ];
  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      _animationOver = true;
      Provider.of<ValidationController>(context, listen: false).refresh();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    ValidationController validationController =
        Provider.of<ValidationController>(context, listen: false);
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: height,
          width: width,
          padding: EdgeInsets.only(top: height * 0.04),
          decoration: BoxDecoration(
              gradient: LinearGradient(
            begin: Alignment.topRight,
            end: Alignment.bottomLeft,
            //stops: [0.3, 0.7],
            colors: [
              Colors.purpleAccent.shade200,
              Colors.deepPurpleAccent.shade400,
            ],
          )),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      padding: EdgeInsets.only(left: width * 0.05),
                      icon: Icon(
                        Icons.arrow_back_ios,
                        color: Colors.white,
                        size: width * 0.07,
                      ))
                ],
              ),
              Hero(
                tag: "Registration",
                child: Text(
                  "Registration",
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
              Container(
                height: height * 0.77,
                width: width,
                padding: EdgeInsets.symmetric(
                    vertical: height * 0.04, horizontal: width * 0.05),
                //duration: Duration(milliseconds: 300),
                decoration: BoxDecoration(
                  boxShadow: const [
                    BoxShadow(
                        blurRadius: 8, spreadRadius: 4, color: Colors.black12)
                  ],
                  color: Colors.white12.withOpacity(0.52),
                  borderRadius:
                      const BorderRadius.vertical(top: Radius.circular(60)),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      height: height * 0.6,
                      width: width,
                      child: ListView.separated(
                        separatorBuilder: (context, index) {
                          if (index == 0) {
                            return Consumer<ValidationController>(
                                builder: (context, val1, val2) {
                              return AnimatedContainer(
                                margin: EdgeInsets.only(
                                    left: width * 0.05,
                                    top: height * 0.004,
                                    bottom: height * 0.01),
                                color: _animationOver
                                    ? Colors.transparent
                                    : Colors.transparent.withOpacity(0.1),
                                transform: Matrix4.translationValues(
                                    _animationOver
                                        ? 0
                                        : index % 2 == 0
                                            ? -width * 0.7
                                            : width * 0.7,
                                    0,
                                    0),
                                duration:
                                    Duration(milliseconds: 300 + (index * 100)),
                                child: Text(
                                  '12 digits',
                                  style:
                                      Theme.of(context).textTheme.headlineSmall,
                                ),
                              );
                            });
                          } else if (index == 1) {
                            return Consumer<ValidationController>(
                                builder: (context, val1, val2) {
                              return AnimatedContainer(
                                margin: EdgeInsets.only(
                                    left: width * 0.05,
                                    top: height * 0.004,
                                    bottom: height * 0.01),
                                color: _animationOver
                                    ? Colors.transparent
                                    : Colors.transparent.withOpacity(0.1),
                                transform: Matrix4.translationValues(
                                    _animationOver
                                        ? 0
                                        : index % 2 == 0
                                            ? -width * 0.7
                                            : width * 0.7,
                                    0,
                                    0),
                                duration:
                                    Duration(milliseconds: 300 + (index * 100)),
                                child: Text('example: XXXXX1234X',
                                    style: Theme.of(context)
                                        .textTheme
                                        .headlineSmall),
                              );
                            });
                          } else if (index == 2) {
                            return Container(
                              alignment: Alignment.center,
                              height: height * 0.19,
                              width: width,
                              padding: const EdgeInsets.all(5),
                              margin: EdgeInsets.zero,
                              child: ListView.separated(
                                padding: EdgeInsets.zero,
                                separatorBuilder: (context, index3) {
                                  return SizedBox(
                                    height: height * 0.0045,
                                  );
                                },
                                itemCount: 5,
                                itemBuilder: (context2, index2) {
                                  return Consumer<ValidationController>(
                                      builder: (context, val1, val2) {
                                    return AnimatedContainer(
                                      margin: EdgeInsets.symmetric(
                                          horizontal: width * 0.03),
                                      color: _animationOver
                                          ? Colors.transparent
                                          : Colors.transparent.withOpacity(0.1),
                                      transform: Matrix4.translationValues(
                                          _animationOver
                                              ? 0
                                              : index2 % 2 == 0
                                                  ? -width * 0.7
                                                  : width * 0.7,
                                          0,
                                          0),
                                      duration: Duration(
                                          milliseconds: 300 + (index2 * 100)),
                                      child: Row(
                                        children: [
                                          Icon(
                                            _passCriteria[index2].val
                                                ? Icons
                                                    .check_circle_outline_rounded
                                                : Icons.circle_outlined,
                                            color: _passCriteria[index2].val
                                                ? Colors.deepPurple
                                                : Colors.red,
                                          ),
                                          SizedBox(width: width * 0.03),
                                          Text(
                                            _passCriteria[index2].criteria,
                                            style: Theme.of(context)
                                                .textTheme
                                                .bodySmall!
                                                .copyWith(
                                                    color: _passCriteria[index2]
                                                            .val
                                                        ? Colors.deepPurple
                                                        : Colors.red),
                                          )
                                        ],
                                      ),
                                    ).animate(effects: [
                                      FadeEffect(
                                          begin: 0,
                                          end: 1,
                                          duration: Duration(
                                              milliseconds:
                                                  300 + (index2 * 110)))
                                    ]);
                                  });
                                },
                              ),
                            );
                          } else {
                            return SizedBox(
                              height: height * 0.015,
                            );
                          }
                        },
                        padding: EdgeInsets.zero,
                        itemCount: _inputList.length,
                        itemBuilder: (context, index) {
                          return Consumer<ValidationController>(
                              builder: (context, val1, val2) {
                            return AnimatedContainer(
                              color: _animationOver
                                  ? Colors.transparent
                                  : Colors.transparent.withOpacity(0.1),
                              transform: Matrix4.translationValues(
                                  _animationOver
                                      ? 0
                                      : index % 2 == 0
                                          ? -width * 0.5
                                          : width * 0.5,
                                  0,
                                  0),
                              duration:
                                  Duration(milliseconds: 300 + (index * 100)),
                              child: TextFormField(
                                inputFormatters:
                                    _inputList[index].inputFormatters,
                                obscureText: _inputList[index].obscureText,
                                key: _inputList[index].globalKey,
                                controller:
                                    _inputList[index].textEditingController,
                                style: Theme.of(context).textTheme.labelMedium,
                                validator: (String? value) {
                                  if (value == null || value.isEmpty) {
                                    return index != 3
                                        ? "Please enter the valid ${_inputList[index].name}"
                                        : "Password doesn't match";
                                  } else if (index == 0 &&
                                      !validationController
                                          .checkAadhar(value)) {
                                    return "Please enter the valid Aadhar number";
                                  } else if (index == 1 &&
                                      !validationController.checkPan(value)) {
                                    return "Please enter the valid Pan number";
                                  } else if (index == 2) {
                                    if (!validationController.checkPass(
                                            value, _passCriteria) ||
                                        value.contains(' ')) {
                                      return "";
                                    }
                                  } else if (index == 3 &&
                                      !validationController.confirmPass(
                                          value,
                                          _inputList[2]
                                              .textEditingController
                                              .text)) {
                                    return "Password doesn't match";
                                  }
                                },
                                onChanged: (String val) {
                                  validationController.refresh();
                                },
                                keyboardType: _inputList[index].textInputType,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                decoration: InputDecoration(
                                    suffixIconColor: Colors.black38,
                                    prefixIconColor: Colors.black38,
                                    suffixIcon: GestureDetector(
                                        onTap: () {
                                          if (index == 2 || index == 3) {
                                            _inputList[index].obscureText =
                                                !_inputList[index].obscureText;
                                            if (_inputList[index].obscureText) {
                                              _inputList[index].suffixIconData =
                                                  Icons.visibility_off_rounded;
                                            } else {
                                              _inputList[index].suffixIconData =
                                                  Icons.visibility_rounded;
                                            }
                                            Provider.of<ValidationController>(
                                                    context,
                                                    listen: false)
                                                .refresh();
                                          }
                                        },
                                        child: Icon(
                                            _inputList[index].suffixIconData)),
                                    prefixIcon:
                                        Icon(_inputList[index].preFixIconData),
                                    labelText: index == 3
                                        ? _inputList[index].name
                                        : "Enter ${_inputList[index].name}",
                                    hintStyle:
                                        Theme.of(context).textTheme.labelSmall,
                                    hintText: _inputList[index].name,
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: const BorderSide(
                                            color: Colors.black38, width: 4)),
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: const BorderSide(
                                            color: Colors.red, width: 4)),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: const BorderSide(
                                            color: Colors.black38, width: 4)),
                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(20),
                                        borderSide: const BorderSide(
                                            color: Colors.red, width: 4))),
                              ),
                            ).animate(effects: [
                              FadeEffect(
                                  begin: 0,
                                  end: 1,
                                  duration: Duration(
                                      milliseconds: 300 + (index * 110)))
                            ]);
                          });
                        },
                      ),
                    ),
                    GestureDetector(
                      onTap: () async {
                        validationController.submit2(
                            inputList: _inputList, context: context);
                      },
                      child: Hero(
                        tag: "45",
                        child: Container(
                            height: height * 0.07,
                            width: width * 0.6,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(40),
                              boxShadow: const [
                                BoxShadow(
                                    blurRadius: 6,
                                    spreadRadius: 4,
                                    color: Colors.black26)
                              ],
                              gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight,
                                //stops: [0.3, 0.7],
                                colors: [
                                  Colors.redAccent.shade400,
                                  Colors.pinkAccent.shade100,
                                ],
                              ),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              "Create Account",
                              style: Theme.of(context).textTheme.bodyMedium,
                            )),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
