import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:login_registration/Controller/validation_controller.dart';
import 'package:login_registration/View/show_dialog.dart';
import 'package:provider/provider.dart';

class GetStartScreen extends StatefulWidget {
  const GetStartScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _GetStartScreenState();
  }
}

class _GetStartScreenState extends State {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: AnimatedContainer(
        padding: EdgeInsets.symmetric(vertical: height * 0.03),
        duration: const Duration(milliseconds: 300),
        height: height,
        width: width,
        decoration: BoxDecoration(
            gradient: LinearGradient(
          begin: Alignment.centerLeft,
          end: Alignment.centerRight,
          //stops: [0.3, 0.7],
          colors: [
            Colors.deepPurpleAccent.shade400,
            Colors.purpleAccent.shade200
          ],
        )),
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaY: 40, sigmaX: 40),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Hero(
                tag: "Registration",
                child: Animate(
                  effects: const [
                    FadeEffect(
                        begin: 0,
                        end: 1,
                        duration: Duration(milliseconds: 350)),
                    SlideEffect(
                        begin: Offset(9, 0),
                        end: Offset(0, 0),
                        duration: Duration(milliseconds: 300))
                  ],
                  child: Text(
                    "Welcome",
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.circular(height * 0.3),
                child: Image.asset(
                  "assets/logo.gif",
                  fit: BoxFit.cover,
                  height: height * 0.45,
                  width: width * 0.9,
                ),
              ).animate(
                effects: const [
                  FadeEffect(
                      begin: 0, end: 1, duration: Duration(milliseconds: 400)),
                  SlideEffect(
                      begin: Offset(-9, 0),
                      end: Offset(0, 0),
                      duration: Duration(milliseconds: 350))
                ],
              ),
              const Text("Assignment Test").animate(
                effects: const [
                  FadeEffect(
                      begin: 0, end: 1, duration: Duration(milliseconds: 450)),
                  SlideEffect(
                      begin: Offset(9, 0),
                      end: Offset(0, 0),
                      duration: Duration(milliseconds: 400))
                ],
              ),
              GestureDetector(
                onTap: () async {
                  bool val = await Provider.of<ValidationController>(context,
                          listen: false)
                      .checkConnetivity();
                  if (val) {
                    Navigator.of(context).pushNamed("sigupScreen");
                  } else {
                    getFlutterToast();
                  }
                },
                child: Hero(
                  tag: "45",
                  child: AnimatedContainer(
                    duration: const Duration(milliseconds: 300),
                    height: height * 0.085,
                    width: width * 0.65,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(40),
                      boxShadow: const [
                        BoxShadow(
                            blurRadius: 6,
                            spreadRadius: 4,
                            color: Colors.black26)
                      ],
                      gradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        //stops: [0.3, 0.7],
                        colors: [
                          Colors.pinkAccent.shade100,
                          Colors.redAccent.shade400,
                        ],
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          "Get Started",
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(fontSize: 26),
                        ),
                        SizedBox(
                          width: width * 0.03,
                        ),
                        Icon(
                          Icons.arrow_forward_rounded,
                          color: Colors.white,
                          size: width * 0.085,
                        )
                      ],
                    ),
                  ),
                ),
              ).animate(
                effects: const [
                  FadeEffect(
                      begin: 0, end: 1, duration: Duration(milliseconds: 500)),
                  SlideEffect(
                      begin: Offset(-9, 0),
                      end: Offset(0, 0),
                      duration: Duration(milliseconds: 450))
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
