
import 'package:flutter/material.dart';

class Assignment1 extends StatefulWidget {
  const Assignment1({super.key});
  @override
  State<StatefulWidget> createState() => _AssignmentState();
}

class _AssignmentState extends State<Assignment1> {
  int? _count = 0;
  void _printTabelValue() {
    setState(() {
      _count = _count! + 2;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Tabel of 2"),
      ),
      body: Center(
        child:Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text("Click button to print table vale"),
              const SizedBox(
                height: 20,
              ),
              Text(
                "$_count"
              ),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                onPressed: _printTabelValue, 
                child: const Text(
                  "Print"
                )
                )
            ],
            
          )

        ),
    );
  }
}
