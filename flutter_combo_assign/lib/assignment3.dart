import 'package:flutter/material.dart';

class Assignment3 extends StatefulWidget {
  const Assignment3({super.key});

  @override
  State<Assignment3> createState() => _Assignment3State();
}

class _Assignment3State extends State<Assignment3> {
  int? selextedIndex = 0;
  final List<String> imageList = [
    "https://images.hindustantimes.com/auto/img/2022/01/12/600x338/2021_Range_Rover_1_1635394942477_1641980643559.jpg",
    "https://stimg.cardekho.com/images/carexteriorimages/630x420/Land-Rover/Range-Rover/9018/1676541159356/front-left-side-47.jpg?tr=w-664",
    "https://i.pinimg.com/originals/58/6c/70/586c7053ade516f544e87a2b2551d2f5.jpg",
    "https://pbs.twimg.com/media/DlH1ZhnXsAAuzG8.jpg:large"
  ];
  void showNextImage() {
    setState(() {
      //int a = imageList.length;
     // print("s: $selextedIndex" );
      //print(" ll: $a");
      if (imageList.length-1 != selextedIndex) {
        selextedIndex = selextedIndex! + 1;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Display tex"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.network(
                imageList[selextedIndex!],
                height: 300,
                width: 300,
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                  onPressed: showNextImage, child: const Text("next")),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      selextedIndex = 0;
                    });
                  },
                  child: const Text("Reset"))
            ],
          ),
        ));
  }
}
