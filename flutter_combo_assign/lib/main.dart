import 'package:flutter/material.dart';
import 'package:flutter_combo_assign/Palindrome.dart';
import 'package:flutter_combo_assign/assignment2.dart';

import 'assignment1.dart';
import 'assignment3.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
       home: Assignment1(),
      // home: Assignement2(),
      //home: Assignment3(),
      //home:PalindromeCheck()
    );
  }
}
