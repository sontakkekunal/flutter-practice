import 'package:flutter/material.dart';

class PalindromeCheck extends StatefulWidget {
  const PalindromeCheck({super.key});

  @override
  State<PalindromeCheck> createState() => _PalindromeCheckAndPrint();
}

class _PalindromeCheckAndPrint extends State<PalindromeCheck> {
  int? cnt = 0;
  List<int> l = [15, 41, 121, 22, 19, 153,145];
  void _palindromeCount() {
    int count = 0;
    int i = 0;
    while (i < l.length) {
      int n = l[i];
      int temp = n;
      int val = 0;
      while (temp != 0) {
        val = val * 10 + (temp % 10);
        temp = temp ~/ 10;
      }
      if (val == n) {
        count++;
      }
      i++;
    }
    setState(() {
      cnt = count;
    });
  }

  void armStrong() {
    int count = 0;
    int i = 0;
    while (i < l.length) {
      int n = l[i];
      int temp = n;
      int numcnt = 0;
      while (temp != 0) {
        numcnt++;
        temp = temp ~/ 10;
      }
      temp = n;
      int total = 0;
      while (temp != 0) {
        int val = temp % 10;
        int product = 1;
        for (int j = 1; j <= numcnt; j++) {
          product = product * val;
        }
        total = total + product;
        temp = temp ~/ 10;
      }
      if (total == n) {
        count++;
      }
      i++;
    }
    setState(() {
      cnt = count;
    });
  }

  void strongNum() {
    int i = 0;
    
    
    int count = 0;
    while (i < l.length) {
      int n = l[i];
      int temp = n;
      int total = 0;
      while (temp != 0) {
        int val = temp % 10;
        int fact = 1;
        while (val != 0) {
          fact = fact * val;
          val--;
        }
        total = total + fact;
        temp = temp ~/ 10;
      }
      if (total == n) {
        count++;
      }
      i++;
    }
    setState(() {
      cnt = count;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Plaindrome count")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text("Click button for checking count of palindrome"),
            const SizedBox(
              height: 20,
            ),
            Text("$cnt"),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: _palindromeCount,
                child: const Text("Palindrome count")),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: armStrong, child: const Text("Amstrong count")
                ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: strongNum, child: const Text("Strong num count")
                ),
          ],
        ),
      ),
    );
  }
}
