import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_gaurdian/view/dashBoardScreen.dart';
import 'package:pet_gaurdian/view/grommingScreen.dart';
import 'package:pet_gaurdian/view/loginScreen.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';
import 'package:pet_gaurdian/view/notificationScreen.dart';
import 'package:pet_gaurdian/view/petShopScreen.dart';
import 'package:pet_gaurdian/view/service.dart';
import 'package:pet_gaurdian/view/splashScreen.dart';
import 'package:pet_gaurdian/view/traningScreen.dart';
import 'package:provider/provider.dart';

void main() {

  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => DoctorController()),
        Provider(create: (context) => TraningModelController())
      ],
      child: MaterialApp(
        title: 'Pet care',
          debugShowCheckedModeBanner: false,
          routes: {
            'login': (context) => const LoginScreen(),
            'dashBoard': (context) => const DashboardScreen(),
            'notification': (context) => const NotificationScreen(),
            'serviceVeterinary': (context) => const ServiceVeterinary(),
            'gromming': (context) => const GrommingScreen(),
            'petShop': (context) => const PetShopScreen(),
            'traning': (context) => const TraningScreen()
          },
          theme: ThemeData(
              shadowColor: const Color.fromRGBO(0, 0, 0, 0.5),
              primaryColor: const Color.fromRGBO(245, 146, 69, 1),
              primaryColorLight: const Color.fromRGBO(252, 219, 193, 1),
              scaffoldBackgroundColor: const Color.fromRGBO(245, 245, 247, 1),
              textTheme: TextTheme(
                headlineLarge: GoogleFonts.poppins(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(31, 32, 41, 1)),
                headlineMedium: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(31, 32, 41, 1)),
                headlineSmall: GoogleFonts.poppins(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                    color: const Color.fromRGBO(31, 32, 41, 1)),
                titleLarge: GoogleFonts.poppins(
                    color: const Color.fromRGBO(194, 195, 204, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
                labelLarge: GoogleFonts.poppins(
                    color: const Color.fromRGBO(245, 245, 247, 1),
                    fontSize: 14,
                    fontWeight: FontWeight.w600),
                labelMedium: GoogleFonts.poppins(
                    color: const Color.fromRGBO(0, 0, 0, 0.3),
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
                labelSmall: GoogleFonts.poppins(
                    color: const Color.fromRGBO(0, 0, 0, 0.6),
                    fontSize: 12,
                    fontWeight: FontWeight.w500),
                bodyMedium: GoogleFonts.poppins(
                    color: const Color.fromRGBO(245, 146, 69, 1),
                    fontSize: 12,
                    fontWeight: FontWeight.w400),
              )),
          home: const SplashScreen()),
    );
  }
}
