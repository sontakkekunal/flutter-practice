import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';

import 'navigationBar.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _DashboardScreenState();
  }
}

class _DashboardScreenState extends State {
  List<Map<String, String>> categoryList = [
    {
      'imgPath': "images/userImg/veterinaryImg.png",
      'name': 'Veterinary',
      'routePage': 'serviceVeterinary'
    },
    {
      'imgPath': "images/userImg/grommingImg.png",
      'name': 'Grooming',
      'routePage': 'gromming'
    },
    {
      'imgPath': "images/userImg/petStoreImg.png",
      'name': 'Pet Store',
      'routePage': 'petShop'
    },
    {
      'imgPath': "images/userImg/traningImg.png",
      'name': 'Training',
      'routePage': 'traning'
    },
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 44),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      Image.asset('images/userImg/profile.png'),
                      const SizedBox(
                        width: 10,
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Text(
                            'Hello, Sarah',
                            style: Theme.of(context).textTheme.headlineSmall,
                          ),
                          const SizedBox(
                            height: 6,
                          ),
                          Text(
                            'Good Morning!',
                            style: Theme.of(context)
                                .textTheme
                                .titleLarge!
                                .copyWith(fontSize: 14),
                          )
                        ],
                      ),
                      const Spacer(),
                      IconButton(
                          onPressed: () {
                            Navigator.of(context).pushNamed('notification');
                          },
                          icon: const Icon(Icons.notifications_none_sharp))
                    ],
                  ),
                  getSearchBox(
                      hint: "Search",
                      icondata: Icons.search_sharp,
                      context: context),
                  Container(
                    height: 99,
                    width: double.infinity,
                    padding:
                        const EdgeInsets.only(top: 16, bottom: 16, left: 16),
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(22, 34, 51, 0.08),
                              offset: Offset(0, 8),
                              spreadRadius: -4,
                              blurRadius: 16)
                        ]),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              'In Love With Pets?',
                              style: Theme.of(context).textTheme.headlineLarge,
                            ),
                            Text(
                              'Get all what you need for them',
                              style: Theme.of(context).textTheme.bodyMedium,
                            )
                          ],
                        ),
                        const Spacer(),
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          child: Image.asset(
                            'images/userImg/petLove.png',
                            height: 97,
                            width: 101,
                            fit: BoxFit.cover,
                          ),
                        )
                      ],
                    ),
                  ),
                  getSectionBar(section: 'Category', context: context),
                  SizedBox(
                    height: 82,
                    width: double.infinity,
                    child: ListView.separated(
                      padding: EdgeInsets.zero,
                      separatorBuilder: (context, index) => SizedBox(
                          width: MediaQuery.of(context).size.width * 0.1),
                      itemCount: 4,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) => GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .pushNamed(categoryList[index]['routePage']!);
                        },
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Image.asset(categoryList[index]['imgPath']!),
                            Text(
                              categoryList[index]['name']!,
                              style: Theme.of(context)
                                  .textTheme
                                  .labelMedium!
                                  .copyWith(color: Colors.black),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                  Text('Event',
                      style: Theme.of(context).textTheme.headlineSmall),
                  getInfoCont(
                      title: 'Let’s Consult The Best DOctor For Your Pets!',
                      imgPath: 'images/userImg/petEventImg.png',
                      context: context),
                  Text('Community',
                      style: Theme.of(context).textTheme.headlineSmall),
                  getInfoCont(
                      title: 'Connect and share with communities! ',
                      imgPath: 'images/userImg/commuintiyImg.png',
                      context: context),
                ],
              ),
            ),
          )
        ],
      ),
      bottomSheet: NavigationbarScreen(
        index: 0,
      ),
      floatingActionButton: getFloatingActionButton(context: context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
