import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State {
  @override
  Widget build(BuildContext context) {
    List<Map<String, dynamic>> inputList = [
      {'name': 'Email', 'hint': 'PetGuardian@gmail.com', 'icon': null},
      {
        'name': 'Label',
        'hint': '**********',
        'icon': Icons.visibility_off_outlined
      },
    ];
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 44),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'Login',
                    style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                        fontSize: 32,
                        fontWeight: FontWeight.w700,
                        color: Theme.of(context).primaryColor),
                  ),
                  getLogo(
                      outerLogoHeight: 194.22,
                      outerLogoWidth: 184,
                      innerLogoHeight: 104.99,
                      innerLogoWidth: 184,
                      outImgPath: 'images/appLogo/outImg2.png',
                      innerImgPath: 'images/appLogo/innerImg2.png',
                      outImgColor: Theme.of(context).primaryColor,
                      innerImgColor: Theme.of(context).scaffoldBackgroundColor),
                  Column(
                    children: [
                      SizedBox(
                        height: 180,
                        width: double.infinity,
                        child: ListView.builder(
                          itemCount: 2,
                          itemBuilder: (context, index) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(inputList[index]['name'],
                                    style:
                                        Theme.of(context).textTheme.labelSmall),
                                getSearchBox(
                                    hint: inputList[index]['hint'],
                                    icondata: inputList[index]['icon'],
                                    context: context)
                                // TextFormField(
                                //   decoration: InputDecoration(
                                //       contentPadding:
                                //           const EdgeInsets.symmetric(
                                //               horizontal: 20, vertical: 0),
                                //       hintStyle: Theme.of(context)
                                //           .textTheme
                                //           .labelMedium,
                                //       hintText: inputList[index]['hint'],
                                //       suffixIcon: inputList[index]['icon'],
                                //       enabledBorder: OutlineInputBorder(
                                //         borderSide: const BorderSide(
                                //             width: 2,
                                //             color: Color.fromRGBO(
                                //                 250, 200, 162, 1)),
                                //         borderRadius: BorderRadius.circular(8),
                                //       ),
                                //       focusedBorder: OutlineInputBorder(
                                //         borderSide: const BorderSide(
                                //             width: 2,
                                //             color: Color.fromRGBO(
                                //                 250, 200, 162, 1)),
                                //         borderRadius: BorderRadius.circular(8),
                                //       ),
                                //       border: OutlineInputBorder(
                                //         borderSide: const BorderSide(
                                //             width: 2,
                                //             color: Color.fromRGBO(
                                //                 250, 200, 162, 1)),
                                //         borderRadius: BorderRadius.circular(8),
                                //       )),
                                // ),
                              ],
                            );
                          },
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Forgot Password ?',
                            style: Theme.of(context)
                                .textTheme
                                .labelMedium!
                                .copyWith(color: Colors.black),
                          ),
                          TextButton(
                              onPressed: () {},
                              child: Text('Click Here',
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelMedium!
                                      .copyWith(
                                          fontWeight: FontWeight.w500,
                                          color: Colors.black)))
                        ],
                      ),
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushNamed('dashBoard');
                    },
                    child: getButton(
                        backGroundColor: Theme.of(context).primaryColor,
                        foreGroundColor:
                            Theme.of(context).scaffoldBackgroundColor,
                        centralWidget: Text(
                          'LOGIN',
                          style: Theme.of(context).textTheme.labelLarge,
                        )),
                  ),
                  Container(
                    height: 1,
                    width: 327,
                    decoration:
                        BoxDecoration(color: Theme.of(context).primaryColor),
                  ),
                  getButton(
                      backGroundColor: Theme.of(context).primaryColor,
                      foreGroundColor:
                          Theme.of(context).scaffoldBackgroundColor,
                      centralWidget: Text(
                        'LOGIN WITH EMAIL',
                        style: Theme.of(context).textTheme.labelLarge,
                      )),
                  getButton(
                      backGroundColor: Theme.of(context).primaryColor,
                      foreGroundColor:
                          Theme.of(context).scaffoldBackgroundColor,
                      centralWidget: Text(
                        'LOGIN WITH FACEBOOK',
                        style: Theme.of(context).textTheme.labelLarge,
                      )),
                  Container(
                    padding: const EdgeInsets.symmetric(horizontal: 70),
                    child: Text(
                      'By continue you agree to our Terms & Privacy Policy',
                      style: Theme.of(context)
                          .textTheme
                          .labelMedium!
                          .copyWith(color: Colors.black),
                      textAlign: TextAlign.center,
                    ),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
