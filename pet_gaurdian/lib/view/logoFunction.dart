import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';

Widget getLogo({
  required double outerLogoHeight,
  required double outerLogoWidth,
  required double innerLogoHeight,
  required double innerLogoWidth,
  required String outImgPath,
  required String innerImgPath,
  required Color outImgColor,
  required Color innerImgColor,
}) {
  return Container(
    height: outerLogoHeight,
    width: outerLogoWidth,
    decoration: BoxDecoration(
        color: innerImgColor,
        image: DecorationImage(
          image: AssetImage(outImgPath),
        )),
    alignment: Alignment.center,
    child: Container(
      height: innerLogoHeight,
      width: innerLogoWidth,
      padding: EdgeInsets.only(top: innerLogoHeight / 2.5),
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(innerImgPath),
      )),
      alignment: Alignment.center,
      child: Icon(
        Icons.add,
        size: 40,
        color: outImgColor,
      ),
    ),
  );
}

Widget getButton(
    {required Color backGroundColor,
    required Color foreGroundColor,
    required Widget centralWidget}) {
  return Container(
    height: 35,
    width: double.infinity,
    decoration: BoxDecoration(
      color: backGroundColor,
      borderRadius: BorderRadius.circular(8),
    ),
    alignment: Alignment.center,
    child: centralWidget,
  );
}

Widget getInfoCont(
    {required String title,
    required String imgPath,
    required BuildContext context}) {
  return Container(
    height: 126,
    width: double.infinity,
    //padding: const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 0),
    decoration: BoxDecoration(
        color: const Color.fromRGBO(255, 255, 255, 1),
        borderRadius: BorderRadius.circular(16),
        boxShadow: const [
          BoxShadow(
              color: Color.fromRGBO(22, 34, 51, 0.08),
              offset: Offset(0, 8),
              spreadRadius: -4,
              blurRadius: 16)
        ]),
    child: Row(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 16, top: 16, bottom: 16),
          width: 191,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                title,
                style: Theme.of(context).textTheme.headlineSmall,
                textAlign: TextAlign.justify,
              ),
              Container(
                height: 34,
                width: 89,
                decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(8),
                ),
                alignment: Alignment.center,
                child: Text(
                  'See More',
                  style: Theme.of(context)
                      .textTheme
                      .labelSmall!
                      .copyWith(color: Colors.white),
                ),
              )
            ],
          ),
        ),
        const Spacer(),
        Container(
          margin: const EdgeInsets.only(top: 8, right: 8),
          child: Image.asset(
            imgPath,
            height: 176,
            width: 104,
            fit: BoxFit.cover,
          ),
        )
      ],
    ),
  );
}

Widget getDayNotification(
    {required String date,
    required DayNotification dayNotifi,
    required BuildContext context}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 23),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          date,
          style: Theme.of(context).textTheme.headlineSmall,
        ),
        const SizedBox(
          height: 10,
        ),
        Container(
          height: dayNotifi.notifiList.length * 57,
          width: double.maxFinite,
          padding: const EdgeInsets.all(8),
          decoration: BoxDecoration(
              color: const Color.fromRGBO(255, 255, 255, 1),
              borderRadius: BorderRadius.circular(
                  8.0 * (dayNotifi.notifiList.length / 2).round()),
              boxShadow: const [
                BoxShadow(
                    color: Color.fromRGBO(22, 34, 51, 0.08),
                    offset: Offset(0, 8),
                    spreadRadius: -4,
                    blurRadius: 16)
              ]),
          child: ListView.separated(
            separatorBuilder: (context, index) => const SizedBox(
              height: 10,
            ),
            itemCount: dayNotifi.notifiList.length,
            itemBuilder: (context, index) {
              return Row(
                children: [
                  Container(
                    height: 40,
                    width: 40,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColorLight,
                        borderRadius: BorderRadius.circular(10)),
                    child: Icon(
                      dayNotifi.notifiList[index].iconData,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  const SizedBox(
                    width: 15,
                  ),
                  Expanded(
                    child: Text(
                      dayNotifi.notifiList[index].data,
                      style: Theme.of(context)
                          .textTheme
                          .labelMedium!
                          .copyWith(color: Colors.black),
                    ),
                  )
                ],
              );
            },
          ),
        )
      ],
    ),
  );
}

Widget getofferCont(
    {required Text title,
    Text? description,
    required String imgPath,
    required BuildContext context}) {
  return Card(
    color: Theme.of(context).primaryColor,
    //height: 99,
    //width: double.maxFinite,
    //padding: EdgeInsets.zero,

    margin: EdgeInsets.zero,
    // decoration: BoxDecoration(
    //     color: Theme.of(context).primaryColor,
    //     borderRadius: BorderRadius.circular(16),
    //     boxShadow: const [
    //       BoxShadow(
    //           color: Color.fromRGBO(22, 34, 51, 0.08),
    //           offset: Offset(0, 8),
    //           spreadRadius: -4,
    //           blurRadius: 16)
    //     ]),
    // alignment: Alignment.bottomCenter,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: Container(
            margin:
                const EdgeInsets.only(left: 16, top: 16, bottom: 16, right: 55),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                title,
                description ?? const SizedBox(),
              ],
            ),
          ),
        ),
        Image.asset(imgPath)
        //Spacer(),
        // Container(
        //   height: 95,
        //   width: 95,
        //   decoration: BoxDecoration(
        //       shape: BoxShape.rectangle,
        //       image: DecorationImage(
        //           image: AssetImage(imgPath),
        //           fit: BoxFit.cover,
        //           filterQuality: FilterQuality.high),
        //       boxShadow: [
        //         BoxShadow(
        //             offset: Offset(0, 11),
        //             blurRadius: 25,
        //             spreadRadius: 0,
        //             color: Color.fromRGBO(22, 34, 51, 0.08)),
        //         BoxShadow(
        //             offset: Offset(0, 4),
        //             blurRadius: 8,
        //             spreadRadius: -4,
        //             color: Color.fromRGBO(22, 34, 51, 0.08))
        //       ]),
        //)
      ],
    ),
  );
}

Widget getSearchBox(
    {required String hint,
    required IconData? icondata,
    Color? fillColor,
    required BuildContext context}) {
  return TextFormField(
    decoration: InputDecoration(
        contentPadding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
        hintStyle: Theme.of(context).textTheme.labelMedium,
        hintText: hint,
        //hoverColor: Colors.white,
        fillColor: fillColor,
        filled: fillColor == null ? false : true,
        suffixIcon: (icondata != null)
            ? Icon(
                icondata,
                size: 32,
                color: Theme.of(context).primaryColor,
              )
            : null,
        enabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              width: 2, color: Color.fromRGBO(250, 200, 162, 1)),
          borderRadius: BorderRadius.circular(8),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: const BorderSide(
              width: 2, color: Color.fromRGBO(250, 200, 162, 1)),
          borderRadius: BorderRadius.circular(8),
        ),
        border: OutlineInputBorder(
          borderSide: const BorderSide(
              width: 2, color: Color.fromRGBO(250, 200, 162, 1)),
          borderRadius: BorderRadius.circular(8),
        )),
  );
}

Widget getSectionBar({required String section, required BuildContext context}) {
  return Row(
    children: [
      Text(
        section,
        style: Theme.of(context).textTheme.headlineSmall,
      ),
      const Spacer(),
      TextButton(
          onPressed: () {},
          child: Text(
            'See All',
            style: Theme.of(context).textTheme.titleLarge,
          ))
    ],
  );
}

Widget getFloatingActionButton({required BuildContext context}) {
  return FloatingActionButton.extended(
    shape: const CircleBorder(),
    onPressed: () {
      if (ModalRoute.of(context)?.settings.name != 'petShop') {
        Navigator.of(context).pushNamed('petShop');
      }
    },
    label: Container(
      height: 66,
      width: 66,
      decoration: BoxDecoration(
          color: Theme.of(context).primaryColor, shape: BoxShape.circle),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Icon(
            Icons.shopping_cart_outlined,
            size: 22,
            color: Colors.white,
          ),
          Text(
            'Shop',
            style: Theme.of(context)
                .textTheme
                .labelMedium!
                .copyWith(color: Colors.white),
          )
        ],
      ),
    ),
  );
}

AppBar getbackAppBar(
    {Color? appbarColor,
    required String title,
    required BuildContext context}) {
//   return AppBar(
//     leadingWidth:,
//     leading: Container(
//
// padding: EdgeInsets.only(top: 8),
//       decoration: BoxDecoration(
//       image: DecorationImage(
//
//           image: AssetImage('images/userImg/buttonBoard.png')),
//
//     ),
//     alignment:Alignment.topCenter,
//       child: Icon(Icons.chevron_left),
//       ),
//   );
  return AppBar(

    backgroundColor: appbarColor ?? Colors.transparent.withOpacity(0),
    automaticallyImplyLeading: false,
    toolbarHeight: 70,
    centerTitle: true,
    flexibleSpace: Container(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      margin: const EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Container(
              height: 30,
              width: 25,
              decoration: BoxDecoration(
                  color: appbarColor == null
                      ? Theme.of(context).primaryColor
                      : Colors.white,
                  borderRadius: BorderRadius.circular(8)),
              alignment: Alignment.center,
              child: Icon(
                Icons.chevron_left,
                color: appbarColor != null
                    ? Theme.of(context).primaryColor
                    : Colors.white,
              ),
            ),
          ),
          const Spacer(),
          Text(
            title,
            style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                color: appbarColor != null ? Colors.white : Colors.black),
          ),
          const Spacer(),
        ],
      ),
    ),
  );
}
