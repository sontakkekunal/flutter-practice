import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';

class NotificationScreen extends StatefulWidget {
  const NotificationScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _NotificationScreenState();
  }
}

class _NotificationScreenState extends State {
  List<DayNotification> dayNotifiList = [
    DayNotification(date: 'Today', notifiList: [
      Notifications(
          data: 'Your checkout is successfull, product is on tne way',
          iconData: Icons.shopping_bag_outlined),
      Notifications(
          data: 'Appointment request accepted', iconData: Icons.check_sharp)
    ]),
    DayNotification(date: '25 September', notifiList: [
      Notifications(
          data: 'Your checkout is successfull, product is on tne way',
          iconData: Icons.shopping_bag_outlined),
      Notifications(
          data: 'Appointment request accepted', iconData: Icons.check_sharp),
      Notifications(
          data: 'Vaccinate your pet timely',
          iconData: Icons.favorite_border_sharp)
    ]),
    DayNotification(date: '15 September', notifiList: [
      Notifications(
          data: 'Your checkout is successfull, product is on tne way',
          iconData: Icons.shopping_bag_outlined),
      Notifications(
          data: 'Appointment request accepted', iconData: Icons.check_sharp),
      Notifications(
          data: 'Vaccinate your pet timely',
          iconData: Icons.favorite_border_sharp)
    ]),
    DayNotification(date: '21 October', notifiList: [
      Notifications(
          data: 'Your checkout is successfull, product is on tne way',
          iconData: Icons.shopping_bag_outlined),
      Notifications(
          data: 'Appointment request accepted', iconData: Icons.check_sharp)
    ]),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: getbackAppBar(title: 'Notifications', context: context),
      body: ListView.separated(
        separatorBuilder: (context, index) => const SizedBox(
          height: 10,
        ),
        itemCount: dayNotifiList.length,
        itemBuilder: (context, index) {
          return getDayNotification(
              date: dayNotifiList[index].date,
              dayNotifi: dayNotifiList[index],
              context: context);
        },
      ),
    );
  }
}
