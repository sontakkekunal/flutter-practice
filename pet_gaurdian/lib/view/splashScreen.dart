import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State with SingleTickerProviderStateMixin{
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersive);
    Future.delayed(const Duration(seconds: 2),(){
      Navigator.of(context).pushNamed('login');
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,overlays: SystemUiOverlay.values);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed('login');
        },
        child: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              getLogo(
                  outerLogoHeight: 182.41,
                  outerLogoWidth: 192.51,
                  innerLogoHeight: 98.72,
                  innerLogoWidth: 104,
                  outImgPath: 'images/appLogo/logoImg.png',
                  innerImgPath: 'images/appLogo/inLogoImg.png',
                  outImgColor: Theme.of(context).scaffoldBackgroundColor,
                  innerImgColor: Theme.of(context).primaryColor),
              const SizedBox(
                height: 20,
              ),
              Text(
                'PetGuardian',
                style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                    fontSize: 32,
                    fontWeight: FontWeight.w600,
                    color: Colors.white),
              ),
              const SizedBox(
                height: 10,
              ),
              Text(
                "“Your Pets' Lifelong Protector\"",
                style: Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(color: Colors.white),
              )
            ],
          ),
        ),
      ),
    );
  }
}
