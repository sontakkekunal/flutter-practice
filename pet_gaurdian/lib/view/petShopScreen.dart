import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';
import 'package:pet_gaurdian/view/navigationBar.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class PetShopScreen extends StatefulWidget {
  const PetShopScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PetShopScreenState();
  }
}

class _PetShopScreenState extends State {
  final GrommingServicesController _grommingServicesController =
      GrommingServicesController(grommingServicesList: [
    GrommingServices(name: 'Pets', imgPath: 'images/userImg/shopPet.png'),
    GrommingServices(name: 'Foods', imgPath: 'images/userImg/foodImg.png'),
    GrommingServices(name: 'Healthy', imgPath: 'images/userImg/healthImg.png'),
    GrommingServices(name: 'Toys', imgPath: 'images/userImg/toyImg.png'),
    GrommingServices(
        name: 'Accessories', imgPath: 'images/userImg/accesoriesImg.png'),
    GrommingServices(name: 'Clothes', imgPath: 'images/userImg/clothesImg.png'),
  ]);
  @override
  Widget build(BuildContext context) {
    List<GrommingServices> petShop =
        _grommingServicesController.grommingServicesList;
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 168,
        automaticallyImplyLeading: false,
        elevation: 0,
        flexibleSpace: Stack(
          children: [
            const SizedBox(
              height: 168,
              width: double.maxFinite,
            ),
            Container(
              height: 148,
              alignment: Alignment.center,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(32),
                      bottomRight: Radius.circular(32))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Hello Sarah',
                        style: Theme.of(context)
                            .textTheme
                            .headlineSmall!
                            .copyWith(color: Colors.white),
                      ),
                      Text(
                        'Find your lovable Pets',
                        style: Theme.of(context)
                            .textTheme
                            .headlineLarge!
                            .copyWith(
                                color: Colors.white,
                                fontWeight: FontWeight.w700),
                      )
                    ],
                  ),
                  const Spacer(),
                  const Icon(
                    Icons.shopping_cart_outlined,
                    color: Colors.white,
                    size: 26,
                  )
                ],
              ),
            ),
            Positioned(
              top: 125,
              left: 0,
              child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 40),
                  height: 40,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: getSearchBox(
                      hint: 'Search Something Here...',
                      icondata: Icons.search,
                      fillColor: Colors.white,
                      context: context)),
            )
          ],
        ),
      ),
      body: MasonryGridView.builder(
        padding: const EdgeInsets.symmetric(horizontal: 38),
        itemCount: petShop.length,
        gridDelegate: const SliverSimpleGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
        ),
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        itemBuilder: (context, index) {
          //print(petShop);
          return Container(
              padding: const EdgeInsets.symmetric(vertical: 8),
              decoration: BoxDecoration(
                  color: Theme.of(context).primaryColor,
                  borderRadius: BorderRadius.circular(16)),
              alignment: Alignment.center,
              child: Stack(
                clipBehavior: Clip.none,
                alignment: Alignment.topLeft,
                children: [
                  Image.asset(
                    petShop[index].imgPath,
                    fit: BoxFit.cover,
                  ),
                  Positioned(
                    top: 15,
                    left: -7,
                    //left: -MediaQuery.of(context).size.width * 0.6,
                    child: Container(
                      height: 29,
                      width: 106,
                      alignment: Alignment.center,
                      decoration: const BoxDecoration(
                          color: Color.fromRGBO(245, 245, 247, 1),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(50),
                              bottomRight: Radius.circular(50))),
                      child: Text(
                        petShop[index].name,
                        style: Theme.of(context)
                            .textTheme
                            .bodyMedium!
                            .copyWith(fontWeight: FontWeight.w500),
                      ),
                    ),
                  )
                ],
              ));
        },
      ),
      bottomSheet: NavigationbarScreen(index: 3),
      floatingActionButton: getFloatingActionButton(context: context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
