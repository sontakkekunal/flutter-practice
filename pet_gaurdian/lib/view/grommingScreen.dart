import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';

class GrommingScreen extends StatefulWidget {
  const GrommingScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _GrommingScreenState();
  }
}

class _GrommingScreenState extends State {
  final GrommingServicesController _grommingServicesControllerObj =
      GrommingServicesController(grommingServicesList: [
    GrommingServices(
        name: 'Bathing & Drying', imgPath: 'images/userImg/bathDryImg.png'),
    GrommingServices(
        name: 'Hair Triming', imgPath: 'images/userImg/hairDryImg.png'),
    GrommingServices(
        name: 'Nail Trimming', imgPath: 'images/userImg/hailTrimImg.png'),
    GrommingServices(
        name: 'Ear Cleaning', imgPath: 'images/userImg/earCleanImg.png'),
    GrommingServices(
        name: 'Teeth Brushing', imgPath: 'images/userImg/teethBrushImg.png'),
    GrommingServices(
        name: 'Flea Treatment', imgPath: 'images/userImg/fleaTreatImg.png')
  ]);
  @override
  Widget build(BuildContext context) {
    List<GrommingServices> grommingServicesList =
        _grommingServicesControllerObj.grommingServicesList;
    return Scaffold(
      appBar: getbackAppBar(title: 'Gromming', context: context),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            getofferCont(
                title: Text(
                  '60% OFF',
                  style: Theme.of(context).textTheme.headlineLarge!.copyWith(
                      fontSize: 24,
                      fontWeight: FontWeight.w700,
                      color: Colors.white),
                ),
                description: Text('On hair & spa treatment',
                    style: Theme.of(context)
                        .textTheme
                        .labelMedium!
                        .copyWith(color: Colors.white)),
                imgPath: 'images/userImg/grommingOffer.png',
                context: context),
            getSearchBox(
                hint: 'Search', icondata: Icons.search, context: context),
            getSectionBar(section: 'Our Services', context: context),
            SizedBox(
              width: double.maxFinite,
              height: MediaQuery.of(context).size.height * 0.56,
              child: GridView.builder(
                padding: EdgeInsets.zero,
                itemCount: grommingServicesList.length,
                gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 16,
                  mainAxisSpacing: 16,
                  crossAxisCount: 2,
                  childAspectRatio: 0.9
                  //childAspectRatio: 169 / 154
                ),
                itemBuilder: (context, index) {
                  return Container(
                    width: double.infinity,
                    height: 200,
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    decoration: BoxDecoration(
                        color: const Color.fromRGBO(255, 255, 255, 1),
                        borderRadius: BorderRadius.circular(16),
                        boxShadow: const [
                          BoxShadow(
                              color: Color.fromRGBO(22, 34, 51, 0.08),
                              offset: Offset(0, 8),
                              spreadRadius: -4,
                              blurRadius: 16)
                        ]),
                    alignment: Alignment.center,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(grommingServicesList[index].imgPath),
                        Text(
                          grommingServicesList[index].name,
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall!
                              .copyWith(
                                  color: const Color.fromRGBO(9, 29, 14, 1)),
                        )
                      ],
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }
}
