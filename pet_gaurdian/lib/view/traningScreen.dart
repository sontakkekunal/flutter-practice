import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';
import 'package:provider/provider.dart';

class TraningScreen extends StatefulWidget {
  const TraningScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _TraningScreenState();
  }
}

class _TraningScreenState extends State {
  List<TraningModel> traningModelList = [];
  @override
  @override
  Widget build(BuildContext context) {
    Provider.of<TraningModelController>(context).traningModelList = [
      TraningModel(
          imgPath: 'images/userImg/obedienceCoursesImg.png',
          title: 'Obedience Courses',
          subtitle: 'By Jhon Smith',
          rating: 4.9,
          count: 335),
      TraningModel(
          imgPath: 'images/userImg/SpecialtyClassesImg.png',
          title: 'Specialty Classes & Workshops',
          subtitle: 'By Duke Fuzzington',
          rating: 5.0,
          count: 500),
      TraningModel(
          imgPath: 'images/userImg/puppyKindergantenImg.png',
          title: 'Puppy Kinderganten and Playgroups',
          subtitle: 'By Sir Fluffington',
          rating: 5.0,
          count: 500),
      TraningModel(
          imgPath: 'images/userImg/canineGoodImg.png',
          title: 'Canine Good Citizen Test',
          subtitle: 'By Baron Fuzzypaws',
          rating: 4.8,
          count: 200),
      TraningModel(
          imgPath: 'images/userImg/theraphyDogsImg.png',
          title: 'Theraphy Dogs',
          subtitle: 'By Duke Fuzzington',
          rating: 5.0,
          count: 500),
      TraningModel(
          imgPath: 'images/userImg/socializationImg.png',
          title: 'Obedience Courses',
          subtitle: 'By Jhon Smith',
          rating: 4.9,
          count: 335),
    ];
    traningModelList =
        Provider.of<TraningModelController>(context).traningModelList!;
    return Scaffold(
      appBar: getbackAppBar(title: 'Training', context: context),
      body: ListView.builder(
        itemCount: traningModelList.length,
        itemBuilder: (context, index) {
          return Card(
            margin: const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
            color: Colors.white,
            elevation: 7,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
            child: Container(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  Container(
                    height: 90,
                    width: 90,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image:
                                AssetImage(traningModelList[index].imgPath))),
                    child: Image.asset('images/userImg/playImg.png'),
                  ),
                  const SizedBox(
                    width: 20,
                  ),
                  SizedBox(
                    height: 90,
                    width: 180,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          traningModelList[index].title,
                          style: Theme.of(context).textTheme.headlineSmall,
                        ),
                        Text(
                          traningModelList[index].subtitle,
                          style: Theme.of(context)
                              .textTheme
                              .labelMedium!
                              .copyWith(
                                  color: const Color.fromRGBO(31, 32, 41, 1)),
                        ),
                        Row(
                          children: [
                            Icon(
                              Icons.star_outline_rounded,
                              color: Theme.of(context).primaryColor,
                              size: 24,
                            ),
                            Text(
                              " ${traningModelList[index].rating} (${traningModelList[index].count})",
                              style: Theme.of(context).textTheme.titleLarge,
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
