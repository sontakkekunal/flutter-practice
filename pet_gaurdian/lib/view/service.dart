import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/doctorDetailScreen.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';
import 'package:pet_gaurdian/view/navigationBar.dart';
import 'package:pet_gaurdian/view/notifiModel.dart';
import 'package:provider/provider.dart';

class ServiceVeterinary extends StatefulWidget {
  const ServiceVeterinary({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ServiceVeterinaryState();
  }
}

class _ServiceVeterinaryState extends State {
  List<Map<String, String>> serviceList = [
    {'name': 'Vaccinations', 'imgPath': 'images/userImg/vaccinationsImg.png'},
    {'name': 'Operations', 'imgPath': 'images/userImg/operationImg.png'},
    {'name': 'Behaviorals', 'imgPath': 'images/userImg/behavioralsImg.png'},
    {'name': 'Dentistry', 'imgPath': 'images/userImg/dentistryImg.png'}
  ];

  @override
  Widget build(BuildContext context) {
    List<DoctorInfo> doctorInfoList =
        Provider.of<DoctorController>(context).doctorInfoList;
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          child: Padding(
            padding: const EdgeInsets.only(left: 24,right: 24,top:14),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Row(
                  children: [
                    Icon(
                      Icons.location_on_outlined,
                      color: Theme.of(context).primaryColor,
                      size: 32,
                    ),
                    Text(
                      'London, UK',
                      style: Theme.of(context)
                          .textTheme
                          .headlineSmall!
                          .copyWith(color: Theme.of(context).shadowColor),
                    ),
                  ],
                ),
                getofferCont(
                    title: Text(
                      'Lets Find Specialist Doctor for Your Pet!',
                      style: Theme.of(context)
                          .textTheme
                          .headlineLarge!
                          .copyWith(
                              color: Theme.of(context).scaffoldBackgroundColor),
                    ),
                    imgPath: 'images/userImg/offerDoct.png',
                    context: context),
                getSearchBox(
                    hint: "Search",
                    icondata: Icons.search_sharp,
                    context: context),
                getSectionBar(section: 'Our Services', context: context),
                SizedBox(
                  height: 82,
                  width: double.infinity,
                  child: ListView.separated(
                    padding: EdgeInsets.zero,
                    separatorBuilder: (context, index) => SizedBox(
                      width: MediaQuery.of(context).size.width * 0.08,
                    ),
                    itemCount: 4,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, index) => Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(serviceList[index]['imgPath']!),
                        Text(
                          serviceList[index]['name']!,
                          style: Theme.of(context)
                              .textTheme
                              .labelSmall!
                              .copyWith(color: Theme.of(context).primaryColor),
                        )
                      ],
                    ),
                  ),
                ),
                Text(
                  'Best Specialists Nearby',
                  style: Theme.of(context).textTheme.headlineSmall,
                ),
                SizedBox(
                  height: 145.0 * 2.2,
                  child: ListView.builder(
                    padding: EdgeInsets.zero,
                    itemCount: doctorInfoList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                DoctorDetailScreen(index: index),
                          ));
                        },
                        child: Container(
                          height: 122,
                          width: double.infinity,
                          margin: const EdgeInsets.only(bottom: 20),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              borderRadius: BorderRadius.circular(16),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color.fromRGBO(22, 34, 51, 0.08),
                                    offset: Offset(0, 8),
                                    spreadRadius: -4,
                                    blurRadius: 16)
                              ]),
                          child: Row(
                            children: [
                              Hero(
                                tag: "img-${doctorInfoList[index].imgpath}",
                                child: Image.asset(
                                  doctorInfoList[index].imgpath,
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.symmetric(
                                    vertical: 16, horizontal: 4),
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text("Dr. ${doctorInfoList[index].name}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .headlineSmall),
                                    Text(
                                      doctorInfoList[index].type,
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleLarge,
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.star_border_rounded,
                                          color: Theme.of(context)
                                              .primaryColor
                                              .withOpacity(0.7),
                                          size: 19,
                                        ),
                                        Text(
                                          " ${doctorInfoList[index].rating}",
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleLarge,
                                        ),
                                        const SizedBox(
                                          width: 7,
                                        ),
                                        Icon(
                                          Icons.location_on_outlined,
                                          color: Theme.of(context)
                                              .primaryColor
                                              .withOpacity(0.7),
                                          size: 19,
                                        ),
                                        Text(
                                          " ${doctorInfoList[index].distance}",
                                          style: Theme.of(context)
                                              .textTheme
                                              .titleLarge,
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        )
      ]),
      bottomNavigationBar: NavigationbarScreen(index: 1),
      floatingActionButton: getFloatingActionButton(context: context),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
