import 'package:flutter/material.dart';

class NavigationbarScreen extends StatefulWidget {
  int? index;
  NavigationbarScreen({super.key, required this.index});

  @override
  State<StatefulWidget> createState() {
    return _NavigationbarScreenState();
  }
}

class _NavigationbarScreenState extends State<NavigationbarScreen> {
  @override
  Widget build(BuildContext context) {
    int? selectedIndex = widget.index;

    return NavigationBar(
        height: 82,
        elevation: 0,
        selectedIndex: selectedIndex ?? 8,
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        indicatorColor: Colors.transparent,
        surfaceTintColor: Colors.transparent,
        indicatorShape: const CircleBorder(),
        onDestinationSelected: (value) {
          if (selectedIndex != value && value == 0) {
            Navigator.of(context).pushNamed('dashBoard');
          } else if (selectedIndex != value && value == 1) {
            Navigator.of(context).pushNamed('serviceVeterinary');
          }
        },
        labelBehavior: NavigationDestinationLabelBehavior.alwaysShow,
        destinations: [
          NavigationDestination(
            icon: const Icon(
              Icons.home_outlined,
            ),
            label: 'Home',
            selectedIcon: Icon(
              Icons.home_outlined,
              color: Theme.of(context).primaryColor,
            ),
          ),
          NavigationDestination(
            icon: const Icon(Icons.favorite_outline_sharp),
            label: 'Service',
            selectedIcon: Icon(
              Icons.favorite_outline_sharp,
              color: Theme.of(context).primaryColor,
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          NavigationDestination(
            icon: const Icon(
              Icons.access_time,
            ),
            label: 'History',
            selectedIcon: Icon(
              Icons.access_time,
              color: Theme.of(context).primaryColor,
            ),
          ),
          NavigationDestination(
            icon: const Icon(Icons.person_3_outlined),
            label: 'profile',
            selectedIcon: Icon(
              Icons.person_3_outlined,
              color: Theme.of(context).primaryColor,
            ),
          )
        ]);
  }
}
