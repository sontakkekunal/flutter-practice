import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pet_gaurdian/view/grommingScreen.dart';

class DayNotification {
  String date;
  List<Notifications> notifiList;
  DayNotification({required this.date, required this.notifiList});
}

class Notifications {
  String data;
  IconData iconData;
  Notifications({required this.data, required this.iconData});
}

class DoctorInfo {
  String imgpath;

  String name;
  String type;
  double rating;
  double distance;
  double location;
  double experence;
  int price;
  String about;
  String? imgpath2;
  DoctorInfo(
      {required this.imgpath,
      required this.name,
      required this.type,
      required this.rating,
      required this.distance,
      required this.location,
      required this.experence,
      required this.price,
      required this.about,
      this.imgpath2});
}

class GrommingServices {
  String name;
  String imgPath;
  GrommingServices({required this.name, required this.imgPath});
}

class GrommingServicesController {
  List<GrommingServices> grommingServicesList;
  GrommingServicesController({required this.grommingServicesList});
}

class DoctorController {
  List<DoctorInfo> doctorInfoList = [
    DoctorInfo(
        imgpath: 'images/userImg/annaImg.png',
        imgpath2: 'images/userImg/annaImg2.png',
        name: 'Anna Johanson',
        type: 'Veterinary Behavioral',
        rating: 4.8,
        distance: 1,
        location: 2.5,
        experence: 11,
        price: 250,
        about:
            'Dr. Maria Naiis is a highly experienced veterinarian with 11 years of dedicated practice, showcasing a pro...'),
    DoctorInfo(
        imgpath: 'images/userImg/vernonImg.png',
        name: 'Vernon Chwe',
        type: 'Veterinary Surgery',
        rating: 4.9,
        distance: 1.5,
        location: 2.5,
        experence: 11,
        price: 270,
        about:
            'Dr. Maria Naiis is a highly experienced veterinarian with 11 years of dedicated practice, showcasing a pro...'),
    DoctorInfo(
        imgpath: 'images/userImg/vernonImg.png',
        name: 'Vernon Chwe',
        type: 'Veterinary Surgery',
        rating: 4.9,
        distance: 1.5,
        location: 2.5,
        experence: 11,
        price: 270,
        about:
            'Dr. Maria Naiis is a highly experienced veterinarian with 11 years of dedicated practice, showcasing a pro...'),
  ];
}

class TraningModel {
  String imgPath;
  String title;
  String subtitle;
  double rating;
  int count;
  TraningModel(
      {required this.imgPath,
      required this.title,
      required this.subtitle,
      required this.rating,
      required this.count});
}

class TraningModelController {
  List<TraningModel>? traningModelList;
  TraningModelController({this.traningModelList});
}
