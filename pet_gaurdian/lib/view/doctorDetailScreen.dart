import 'dart:ffi';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pet_gaurdian/view/logoFunction.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';

import 'notifiModel.dart';

class DoctorDetailScreen extends StatefulWidget {
  int index;
  DoctorDetailScreen({super.key, required this.index});

  @override
  State<StatefulWidget> createState() {
    return _DoctorDetailScreenState();
  }
}

class _DoctorDetailScreenState extends State<DoctorDetailScreen> {
  void getdates() {
    DateTime tempdate = _dateTime.copyWith();
    for (int i = 0; i < 5; i++) {
      dateList
          .add({'date': DateFormat('EE , d').format(tempdate), 'value': false});
      tempdate = tempdate.add(const Duration(days: 1));
    }
  }

  DateTime _dateTime = DateTime.now();
  List<Map<String, dynamic>> timeList = [
    {'time': '09:00', 'value': true},
    {'time': '15:00', 'value': false},
    {'time': '19:00', 'value': false},
  ];
  List<Map<String, dynamic>> dateList = [];
  @override
  Widget build(BuildContext context) {
    DoctorInfo doctorInfoObj =
        Provider.of<DoctorController>(context).doctorInfoList[widget.index];
    List<Map<String, String>> skillList = [
      {'name': 'Experience', 'value': "${doctorInfoObj.experence} year"},
      {'name': 'Price', 'value': "\$${doctorInfoObj.price}"},
      {'name': 'Location', 'value': "${doctorInfoObj.location} km"},
    ];
    getdates();
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: getbackAppBar(
          title: 'Veterinary',
          appbarColor: Theme.of(context).primaryColor,
          context: context),
      body: Column(
        children: [
          ClipRRect(
            borderRadius: BorderRadius.circular(40),
            child: Hero(
              tag: "img-${doctorInfoObj.imgpath}",
              child: Image.asset(
                doctorInfoObj.imgpath2 == null
                    ? doctorInfoObj.imgpath
                    : doctorInfoObj.imgpath2!,
                width: double.maxFinite,
                //height: 221,
                fit: BoxFit.cover,
                //filterQuality: FilterQuality.high,
              ),
            ),
          ),
          Expanded(
            child: Container(
                width: double.maxFinite,
                decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(32),
                        topRight: Radius.circular(32))),
                child: CustomScrollView(slivers: [
                  SliverFillRemaining(
                    hasScrollBody: false,
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 25, vertical: 25),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Dr. ${doctorInfoObj.name}",
                            style: Theme.of(context)
                                .textTheme
                                .headlineLarge!
                                .copyWith(fontSize: 24),
                          ),
                          Text(
                            doctorInfoObj.type,
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          SizedBox(
                            height: 65,
                            width: double.maxFinite,
                            child: ListView.separated(
                              separatorBuilder: (context, index) =>
                                  const SizedBox(
                                width: 15,
                              ),
                              scrollDirection: Axis.horizontal,
                              itemCount: 3,
                              itemBuilder: (context, index) {
                                return Container(
                                  width: 98.97,
                                  height: 62,
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 12),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.circular(10),
                                      boxShadow: const [
                                        BoxShadow(
                                            offset: Offset(0, 11),
                                            blurRadius: 25,
                                            spreadRadius: 0,
                                            color: Color.fromRGBO(
                                                22, 34, 51, 0.08)),
                                        BoxShadow(
                                            offset: Offset(0, 4),
                                            blurRadius: 8,
                                            spreadRadius: -4,
                                            color: Color.fromRGBO(
                                                22, 34, 51, 0.08))
                                      ]),
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          skillList[index]['name']!,
                                          style: Theme.of(context)
                                              .textTheme
                                              .labelSmall!
                                              .copyWith(color: Colors.black),
                                        ),
                                        Text(
                                          skillList[index]['value']!,
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineLarge!
                                              .copyWith(
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                        )
                                      ]),
                                );
                              },
                            ),
                          ),
                          Text(
                            'About',
                            style: Theme.of(context).textTheme.headlineSmall,
                          ),
                          Text(
                            doctorInfoObj.about,
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          GestureDetector(
                            onTap: () {
                              showCupertinoModalPopup(
                                context: context,
                                builder: (context) {
                                  return Container(
                                    height: MediaQuery.of(context).size.height *
                                        0.45,
                                    width: double.maxFinite,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        borderRadius:
                                            BorderRadius.circular(60)),
                                    alignment: Alignment.center,
                                    child: CupertinoDatePicker(
                                      minimumDate: DateTime.now(),
                                      mode: CupertinoDatePickerMode.monthYear,
                                      initialDateTime: DateTime.now(),
                                      onDateTimeChanged: (DateTime value) {
                                        if (DateTime.now().month ==
                                            value.month) {
                                          _dateTime = DateTime.now();
                                          _dateTime
                                              .add(const Duration(days: 1));
                                        } else {
                                          _dateTime = value;
                                        }
                                        dateList.clear();
                                        //getdates();
                                        //print(dateList);
                                        setState(() {});
                                      },
                                    ),
                                  );
                                },
                              );
                            },
                            child: Row(
                              children: [
                                Text(
                                  'Available Days',
                                  style:
                                      Theme.of(context).textTheme.headlineSmall,
                                ),
                                const Spacer(),
                                Row(
                                  children: [
                                    Icon(
                                      Icons.calendar_today_outlined,
                                      size: 22,
                                      color: Theme.of(context).primaryColor,
                                    ),
                                    Text(
                                        "  ${DateFormat('MMMM , yyyy').format(_dateTime)}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .titleLarge)
                                  ],
                                ),
                                const Spacer()
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 40,
                            width: double.maxFinite,
                            child: ListView.separated(
                              separatorBuilder: (context, index) =>
                                  const SizedBox(
                                width: 5,
                              ),
                              scrollDirection: Axis.horizontal,
                              itemCount: dateList.length,
                              itemBuilder: (context, index) {
                                return ChoiceChip(
                                  label: Text(dateList[index]['date']),
                                  selected: dateList[index]['value'],
                                  onSelected: (bool value) {
                                    dateList[index]['value'] = value;
                                    setState(() {});
                                  },
                                  labelStyle: Theme.of(context)
                                      .textTheme
                                      .labelSmall!
                                      .copyWith(
                                          color: dateList[index]['value']
                                              ? Colors.white
                                              : const Color.fromRGBO(
                                                  49, 29, 14, 1)),
                                  showCheckmark: false,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  side: BorderSide(
                                      color: Theme.of(context).primaryColor),
                                  selectedColor: Theme.of(context).primaryColor,
                                  disabledColor: Colors.white,
                                );
                              },
                            ),
                          ),
                          Text(
                            'Available Time',
                            style: Theme.of(context).textTheme.headlineSmall,
                          ),
                          SizedBox(
                            height: 40,
                            width: double.maxFinite,
                            child: ListView.separated(
                              separatorBuilder: (context, index) =>
                                  const SizedBox(
                                width: 5,
                              ),
                              scrollDirection: Axis.horizontal,
                              itemCount: timeList.length,
                              itemBuilder: (context, index) {
                                return ChoiceChip(
                                  label: Text(timeList[index]['time']),
                                  selected: timeList[index]['value'],
                                  onSelected: (bool value) {
                                    timeList[index]['value'] = value;
                                    setState(() {});
                                  },
                                  labelStyle: Theme.of(context)
                                      .textTheme
                                      .labelSmall!
                                      .copyWith(
                                          color: timeList[index]['value']
                                              ? Colors.white
                                              : const Color.fromRGBO(
                                                  49, 29, 14, 1)),
                                  showCheckmark: false,
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20)),
                                  side: BorderSide(
                                      color: Theme.of(context).primaryColor),
                                  selectedColor: Theme.of(context).primaryColor,
                                  disabledColor: Colors.white,
                                );
                              },
                            ),
                          ),
                          getButton(
                              backGroundColor:
                                  Theme.of(context).primaryColorLight,
                              foreGroundColor: Colors.white,
                              centralWidget: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  const Icon(
                                    Icons.map_outlined,
                                    color: Color.fromRGBO(163, 97, 46, 1),
                                    size: 20,
                                  ),
                                  Text(
                                    '  See Location',
                                    style: Theme.of(context)
                                        .textTheme
                                        .labelSmall!
                                        .copyWith(
                                            color: const Color.fromRGBO(
                                                163, 97, 46, 1)),
                                  )
                                ],
                              )),
                          getButton(
                              backGroundColor: Theme.of(context).primaryColor,
                              foreGroundColor: Colors.white,
                              centralWidget: Text(
                                'Book now',
                                style: Theme.of(context)
                                    .textTheme
                                    .labelSmall!
                                    .copyWith(color: Colors.white),
                              ))
                        ],
                      ),
                    ),
                  ),
                ])),
          ),
        ],
      ),
    );
  }
}
