import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:sqflite/sqflite.dart';
import 'utilites.dart';

class UserInfoEditPage extends StatefulWidget {
  const UserInfoEditPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _UserInfoEditPage();
  }
}

class _UserInfoEditPage extends State {
  bool usernameVerfication(String username) {
    for (int i = 0; i < userDataList.length; i++) {
      if (userDataList[i].username == username &&
          userDataList[i].username != currentUserObj!.username) {
        return false;
      }
    }
    return true;
  }

  bool isEmailPresent(String email) {
    for (int i = 0; i < userDataList.length; i++) {
      if (userDataList[i].email == email &&
          userDataList[i].email != currentUserObj!.email) {
        return false;
      }
    }
    return true;
  }

  bool isNumPresent(String phone) {
    for (int i = 0; i < userDataList.length; i++) {
      if (userDataList[i].phone == phone &&
          userDataList[i].phone != currentUserObj!.phone) {
        return false;
      }
    }
    return true;
  }

  bool flag = true;
  bool pass1 = false;
  bool pass2 = false;
  bool changePassFlag = false;
  String preUsername = currentUserObj!.username;
  List editList = [
    {
      "name": "email",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "name",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "phone",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "username",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "Confirm Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
  ];
  @override
  Widget build(BuildContext constext) {
    if (flag) {
      editList[0]["controller"].text = currentUserObj!.email;
      editList[1]["controller"].text = currentUserObj!.name;
      editList[2]["controller"].text = currentUserObj!.phone;
      editList[3]["controller"].text = currentUserObj!.username;
      editList[4]["controller"].clear();
      editList[5]["controller"].clear();
      flag = false;
    }
    return Scaffold(
      body: LiquidPullToRefresh(
        borderWidth: 5,
        showChildOpacityTransition: true,
        color: const Color.fromARGB(255, 159, 155, 243),
        backgroundColor: const Color.fromARGB(255, 11, 145, 255),
        animSpeedFactor: 2.5,
        height: 200,
        onRefresh: () async {
          flag = true;
          passList = [false, false, false, false, false];
          count = 0;
          await Future.delayed(const Duration(milliseconds: 250));
          setState(() {});
        },
        child: Container(
            color: (logInfoObj!.themeMode == 0)
                ? const Color.fromRGBO(89, 57, 241, 1)
                : Colors.black,
            child: Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                Row(
                  children: [
                    GestureDetector(
                      child: const Icon(Icons.arrow_back_rounded,
                          color: Colors.white),
                      onTap: () {
                        passList = [false, false, false, false, false];
                        count = 0;
                        Navigator.of(context).pop();
                      },
                    ),
                    const Spacer(),
                    Text(
                      "UserInfoEditing page",
                      style: GoogleFonts.quicksand(
                          fontSize: 15,
                          fontWeight: FontWeight.w600,
                          color: Colors.white),
                    ),
                    const Spacer(),
                  ],
                ),
                const SizedBox(
                  height: 15,
                ),
                Expanded(
                    child: Container(
                  padding: const EdgeInsets.only(top: 25, left: 20, right: 20),
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        SizedBox(
                          height: (changePassFlag) ? 605 : 320,
                          child: ListView.separated(
                            separatorBuilder: (context, index) {
                              if (index == 4) {
                                return Column(
                                  children: [
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          !passList[0]
                                              ? Icons.circle_outlined
                                              : Icons.check_circle,
                                          color: passList[0]
                                              ? Colors.green
                                              : Colors.red,
                                        ),
                                        Text(
                                          "At least one lowercase letter",
                                          style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: passList[0]
                                                  ? Colors.green
                                                  : Colors.red),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          !passList[1]
                                              ? Icons.circle_outlined
                                              : Icons.check_circle,
                                          color: passList[1]
                                              ? Colors.green
                                              : Colors.red,
                                        ),
                                        Text(
                                          "At least one uppercase letter",
                                          style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: passList[1]
                                                  ? Colors.green
                                                  : Colors.red),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          !passList[2]
                                              ? Icons.circle_outlined
                                              : Icons.check_circle,
                                          color: passList[2]
                                              ? Colors.green
                                              : Colors.red,
                                        ),
                                        Text(
                                          "At least one number",
                                          style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: passList[2]
                                                  ? Colors.green
                                                  : Colors.red),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          !passList[3]
                                              ? Icons.circle_outlined
                                              : Icons.check_circle,
                                          color: passList[3]
                                              ? Colors.green
                                              : Colors.red,
                                        ),
                                        Text(
                                          "Be at least 8 character",
                                          style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: passList[3]
                                                  ? Colors.green
                                                  : Colors.red),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Icon(
                                          !passList[4]
                                              ? Icons.circle_outlined
                                              : Icons.check_circle,
                                          color: passList[4]
                                              ? Colors.green
                                              : Colors.red,
                                        ),
                                        Text(
                                          "Must contains at least 1 special character",
                                          style: GoogleFonts.quicksand(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w500,
                                              color: passList[4]
                                                  ? Colors.green
                                                  : Colors.red),
                                        )
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                  ],
                                );
                              } else {
                                return const SizedBox(
                                  height: 10,
                                );
                              }
                            },
                            itemCount: changePassFlag
                                ? editList.length
                                : editList.length - 2,
                            itemBuilder: (context, index) {
                              return TextFormField(
                                key: editList[index]["key"],
                                controller: editList[index]["controller"],
                                obscureText: (!pass1 && index == 4)
                                    ? true
                                    : (!pass2 && index == 5)
                                        ? true
                                        : false,
                                validator: (String? val) {
                                  if (index == 0) {
                                    if (val != null) {
                                      emailVaildator =
                                          EmailValidator.validate(val);

                                      if (!emailVaildator) {
                                        return "Please enter vaild email";
                                      } else if (!isEmailPresent(val)) {
                                        return "This email is already regesistered";
                                      }
                                    } else if (count > 0 && val == null) {
                                      return "Please enter vaild email";
                                    }
                                  } else if (index == 1) {
                                    if (val == null || val.isEmpty) {
                                      return "Please enter valid name";
                                    }
                                  } else if (index == 2) {
                                    if ((val != null &&
                                            !phoneNumVerification(val)) ||
                                        (count > 0 && val == null)) {
                                      return "Please enter vaild phone number";
                                    } else if (val != null &&
                                        !isNumPresent(val)) {
                                      return "This number is already regesistered";
                                    }
                                  } else if (index == 3) {
                                    if (val == null || val.isEmpty) {
                                      return "Please enter vaild username";
                                    } else if (val.contains(" ")) {
                                      return "Please enter vaild username";
                                    } else if (val.isNotEmpty) {
                                      int ascii = val.trim().codeUnitAt(0);
                                      if (ascii >= 65 && ascii <= 90) {
                                        return "username must not start with Upperclass letter";
                                      } else if (!usernameVerfication(val)) {
                                        return "User already taken";
                                      }
                                    }
                                  } else if (index == 4) {
                                    if ((val == null || val.isEmpty) &&
                                        count > 0) {
                                      return "please enter vaild password";
                                    }
                                  } else if (index == 5) {
                                    if ((val != null &&
                                            editList[4]["controller"].text !=
                                                val) ||
                                        (count > 0 &&
                                            (val == null || val.isEmpty))) {
                                      return "Password don't match";
                                    }
                                  }

                                  return null;
                                },
                                onChanged: (String val) {
                                  if (index == 4) {
                                    setState(() {
                                      passList[0] = passCritria1(val);
                                      passList[1] = passCritria2(val);
                                      passList[2] = passCritria3(val);
                                      passList[3] = passCritria4(val);
                                      passList[4] = passCritria5(val);
                                    });
                                  }
                                },
                                keyboardType: (index == 0)
                                    ? TextInputType.emailAddress
                                    : (index == 1)
                                        ? TextInputType.name
                                        : (index == 2)
                                            ? TextInputType.phone
                                            : (index == 3)
                                                ? TextInputType.name
                                                : TextInputType.visiblePassword,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                decoration: InputDecoration(
                                    hintText: editList[index]["name"],
                                    suffixIcon: (index == 4)
                                        ? IconButton(
                                            icon: Icon((pass1)
                                                ? Icons.visibility
                                                : Icons.visibility_off),
                                            onPressed: () {
                                              pass1 = !pass1;
                                              setState(() {});
                                            },
                                          )
                                        : (index == 5)
                                            ? IconButton(
                                                icon: Icon((pass2)
                                                    ? Icons.visibility
                                                    : Icons.visibility_off),
                                                onPressed: () {
                                                  pass2 = !pass2;
                                                  setState(() {});
                                                },
                                              )
                                            : null,
                                    labelText: editList[index]["name"],
                                    focusedErrorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30),
                                        borderSide: const BorderSide(
                                            color: Color.fromARGB(
                                                255, 243, 33, 33),
                                            width: 2.4)),
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30),
                                        borderSide: const BorderSide(
                                            color: Colors.blue, width: 2.4)),
                                    errorBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30),
                                        borderSide: const BorderSide(
                                            color: Colors.red, width: 2.4)),
                                    enabledBorder: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(30),
                                        borderSide: const BorderSide(
                                            color: Colors.black, width: 2.4))),
                              );
                            },
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Checkbox(
                              value: changePassFlag,
                              onChanged: (value) {
                                changePassFlag = !changePassFlag;
                                editList[4]["controller"].clear();
                                editList[5]["controller"].clear();
                                setState(() {});
                              },
                              activeColor: Colors.green,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                            Text(
                              "Do you want to change password? ",
                              style: GoogleFonts.quicksand(
                                  fontSize: 15, fontWeight: FontWeight.w600),
                            )
                          ],
                        ),
                        GestureDetector(
                          onTap: () async {
                            int checkCount = 0;
                            count++;
                            int n = (changePassFlag)
                                ? editList.length
                                : editList.length - 2;
                            for (int i = 0; i < n; i++) {
                              if (editList[i]["key"].currentState!.validate()) {
                                checkCount++;
                              }
                            }

                            if (checkCount == editList.length - 2 &&
                                !changePassFlag) {
                              if (preUsername !=
                                  editList[3]["controller"].text) {
                                UserInfo userChange = UserInfo(
                                    username: editList[3]["controller"].text,
                                    email: editList[0]["controller"].text,
                                    password: currentUserObj!.password,
                                    phone: editList[2]["controller"].text,
                                    tableName:
                                        "${editList[3]["controller"].text}TaskList",
                                    name: editList[1]["controller"].text);
                                await database
                                    .delete(currentUserObj!.tableName);

                                if (!await doesTableExits(
                                    userChange.tableName)) {
                                  await database.execute(
                                      '''CREATE TABLE ${userChange.tableName} (taskNum INTEGER PRIMARY KEY,title TEXT,description TEXT,date TEXT,imgIndex INT,imgURL TEXT,taskDone INTEGER)''');
                                }
                                for (int i = 0; i < taskList.length; i++) {
                                  await database.insert(userChange.tableName,
                                      taskList[i].getTaskMap(),
                                      conflictAlgorithm:
                                          ConflictAlgorithm.replace);
                                }
                                taskList =
                                    await getTaskData(userChange.tableName);
                                database.delete('UserLoginIdTable',
                                    where: 'username=?',
                                    whereArgs: [currentUserObj!.username]);

                                await database.delete('CurrentLogin',
                                    where: 'username=?',
                                    whereArgs: [currentUserObj!.username]);

                                await database.insert(
                                    'UserLoginIdTable', userChange.getUserMap(),
                                    conflictAlgorithm:
                                        ConflictAlgorithm.replace);
                                userDataList =
                                    await getUserList('UserLoginIdTable');
                                await database.insert(
                                    'CurrentLogin', userChange.getUserMap(),
                                    conflictAlgorithm:
                                        ConflictAlgorithm.replace);

                                currentUser = await getUserList('CurrentLogin');
                                currentUserObj = currentUser[0];
                              } else {
                                UserInfo changeUser = UserInfo(
                                    username: editList[3]["controller"].text,
                                    email: editList[0]["controller"].text,
                                    password: currentUserObj!.password,
                                    phone: editList[2]["controller"].text,
                                    tableName: currentUserObj!.tableName,
                                    name: editList[1]["controller"].text);
                                await database.update(
                                    'UserLoginIdTable', changeUser.getUserMap(),
                                    where: "username= ?",
                                    whereArgs: [changeUser.username]);
                                userDataList =
                                    await getUserList('UserLoginIdTable');
                                await database.update(
                                    'Currentlogin', changeUser.getUserMap(),
                                    where: "username= ?",
                                    whereArgs: [changeUser.username]);
                                currentUser = await getUserList('Currentlogin');
                                currentUserObj = currentUser[0];
                              }
                              passList = [false, false, false, false, false];
                              count = 0;
                              Navigator.of(context).pop();
                            } else if (checkCount == editList.length &&
                                changePassFlag &&
                                !passList.contains(false)) {
                              if (preUsername !=
                                  editList[3]["controller"].text) {
                                UserInfo userChange = UserInfo(
                                    username: editList[3]["controller"].text,
                                    email: editList[0]["controller"].text,
                                    password: editList[4]["controller"].text,
                                    phone: editList[2]["controller"].text,
                                    tableName:
                                        "${editList[3]["controller"].text}TaskList",
                                    name: editList[1]["controller"].text);
                                await database
                                    .delete(currentUserObj!.tableName);
                                if (!await doesTableExits(
                                    userChange.tableName)) {
                                  await database.execute(
                                      '''CREATE TABLE ${userChange.tableName} (taskNum INTEGER PRIMARY KEY,title TEXT,description TEXT,date TEXT,imgIndex INT,imgURL TEXT,taskDone INTEGER)''');
                                }
                                for (int i = 0; i < taskList.length; i++) {
                                  await database.insert(userChange.tableName,
                                      taskList[i].getTaskMap(),
                                      conflictAlgorithm:
                                          ConflictAlgorithm.replace);
                                }
                                taskList =
                                    await getTaskData(userChange.tableName);
                                database.delete('UserLoginIdTable',
                                    where: 'username=?',
                                    whereArgs: [currentUserObj!.username]);

                                await database.delete('CurrentLogin',
                                    where: 'username=?',
                                    whereArgs: [currentUserObj!.username]);

                                await database.insert(
                                    'UserLoginIdTable', userChange.getUserMap(),
                                    conflictAlgorithm:
                                        ConflictAlgorithm.replace);
                                userDataList =
                                    await getUserList('UserLoginIdTable');
                                await database.insert(
                                    'CurrentLogin', userChange.getUserMap(),
                                    conflictAlgorithm:
                                        ConflictAlgorithm.replace);

                                currentUser = await getUserList('CurrentLogin');
                                currentUserObj = currentUser[0];
                              } else {
                                UserInfo changeUser = UserInfo(
                                    username: editList[3]["controller"].text,
                                    email: editList[0]["controller"].text,
                                    password: editList[4]["controller"].text,
                                    phone: editList[2]["controller"].text,
                                    tableName: currentUserObj!.tableName,
                                    name: editList[1]["controller"].text);
                                await database.update(
                                    'UserLoginIdTable', changeUser.getUserMap(),
                                    where: "username= ?",
                                    whereArgs: [changeUser.username]);
                                userDataList =
                                    await getUserList('UserLoginIdTable');
                                await database.update(
                                    'Currentlogin', changeUser.getUserMap(),
                                    where: "username= ?",
                                    whereArgs: [changeUser.username]);
                                currentUser = await getUserList('Currentlogin');
                                currentUserObj = currentUser[0];
                              }
                              passList = [false, false, false, false, false];
                              count = 0;
                              Navigator.of(context).pop();
                            }
                          },
                          child: Container(
                            height: 50,
                            width: 100,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: const Color.fromRGBO(89, 57, 241, 1),
                            ),
                            child: Center(
                              child: Text(
                                "Save",
                                style: GoogleFonts.quicksand(
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ))
              ],
            )),
      ),
    );
  }
}
