import 'package:flutter/material.dart';

class ToDoListBasic extends StatefulWidget {
  const ToDoListBasic({super.key});
  @override
  State createState() => _ToDoListBasicState();
}

class _ToDoListBasicState extends State {
  List<Map> taskList = [
    {
      "title": "Lorem Ipsum is simply dummy industry. ",
      "description":
          "Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ",
      "date": "10 July 2023"
    },
    {
      "title": "Lorem Ipsum is simply dummy industry. ",
      "description":
          "Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ",
      "date": "10 July 2023"
    },
    {
      "title": "Lorem Ipsum is simply dummy industry. ",
      "description":
          "Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ",
      "date": "10 July 2023"
    },
    {
      "title": "Lorem Ipsum is simply dummy industry. ",
      "description":
          "Simply dummy text of the printing and type setting industry. Lorem Ipsum Lorem Ipsum Lorem. ",
      "date": "10 July 2023"
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        SizedBox(
          height: 45,
        ),
        Text(
          "Good morning",
          style: Theme.of(context).textTheme.displayLarge,
        ),
        Text(
          "Pathum",
          style: Theme.of(context).textTheme.displayMedium,
        ),
        Expanded(
          child: Container(
            margin: const EdgeInsets.only(top: 20),
            width: double.infinity,
            decoration: BoxDecoration(
                color: Color.fromRGBO(217, 217, 217, 1),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(40),
                    topRight: Radius.circular(40))),
            child: Column(
              children: [
                Text("CREATE TO DO LIST"),
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: Container(
                    padding: EdgeInsets.only(top: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40))),
                    child: ListView.builder(
                      itemCount: taskList.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          padding: const EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                  color: Color.fromRGBO(0, 0, 0, 0.5),
                                  width: 0.5)),
                          margin: EdgeInsets.only(bottom: 10),
                          child: Row(
                            children: [
                              Container(
                                height: 52,
                                width: 52,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle, color: Colors.grey),
                                child: Image.asset("images/Group42.png"),
                              ),
                              const SizedBox(
                                width: 15,
                              ),
                              Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Text(taskList[index]["title"]),
                                      ],
                                    ),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Text(taskList[index]["description"]),
                                    const SizedBox(
                                      height: 5,
                                    ),
                                    Row(
                                      children: [
                                        Text(taskList[index]["date"]),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Icon(
                                Icons.check_circle,
                                color: Colors.green,
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    ));
  }
}
