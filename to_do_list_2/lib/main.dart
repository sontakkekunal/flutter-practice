import 'package:flutter/material.dart';
import 'package:sqflite/sqflite.dart';
import 'package:to_do_list_2/loginSiginScreen.dart';
import 'package:to_do_list_2/todoList_basic2.dart';
import 'package:path/path.dart' as path;
import 'utilites.dart';
import 'package:intl/intl.dart';

//import 'todolist_basic.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //deleteDatabase(path.join(await getDatabasesPath(), "ToDoList.db"));

  database = await openDatabase(
    path.join(await getDatabasesPath(), "ToDoList.db"),
    version: 1,
    onCreate: (db, version) async {
      await db.execute(
          '''CREATE TABLE CurrentLogin(username TEXT PRIMARY KEY,name TEXT,email TEXT,phone TEXT,password TEXT,tableName TEXT)''');
      await db.execute(
          '''CREATE TABLE UserLoginIdTable(username TEXT PRIMARY KEY,name TEXT,email TEXT,phone TEXT,password TEXT,tableName TEXT)''');
      await db.execute(
          '''CREATE TABLE LogInfo(primKey INTEGER PRIMARY KEY,themeMode INTEGER,lastLogin TEXT)''');
      LogInfo logInfoObj = LogInfo(primKey: 1, themeMode: 0, lastLogin: "0");
      await db.insert('LogInfo', logInfoObj.getLogInfoMap(),
          conflictAlgorithm: ConflictAlgorithm.replace);
    },
  );
  // database.delete('CurrentLogin');
  // database.delete('UserLoginIdTable');
  List<LogInfo> logInfoList = await getLogInfoData();
  logInfoObj = logInfoList[0];
  currentUser = await getUserList('CurrentLogin');
  if (currentUser.isNotEmpty) {
    currentUserObj = currentUser[0];
  }
  if (logInfoObj!.lastLogin != "0") {
    DateFormat inputFormat = DateFormat('dd-MMM-yyyy , hh:mm a');
    DateTime dt = inputFormat.parse(logInfoObj!.lastLogin);
    int num = DateTime.now().difference(dt).inMinutes;
    if (num > 10) {
      logInfoObj!.lastLogin = "0";
      logInfoObj!.themeMode = 0;
      sessionFlag = true;
      await database.update('LogInfo', logInfoObj!.getLogInfoMap(),
          where: 'primKey= ?', whereArgs: [logInfoObj!.primKey]);
      await database.delete('CurrentLogin',
          where: 'username=?', whereArgs: [currentUserObj!.username]);
    }
  }
  currentUser = await getUserList('CurrentLogin');
  userDataList = await getUserList('UserLoginIdTable');
  if (currentUser.isEmpty) {
    runApp(const MainApp(flag: false));
  } else {
    logInfoObj!.lastLogin =
        DateFormat('dd-MMM-yyyy , hh:mm a').format(DateTime.now());
    await database.update('LogInfo', logInfoObj?.getLogInfoMap(),
        where: 'primKey= ?', whereArgs: [logInfoObj?.primKey]);
    currentUserObj = currentUser[0];
    taskTableName = currentUserObj!.tableName;
    taskList = await getTaskData(taskTableName!);
    runApp(const MainApp(flag: true));
  }
}

class MainApp extends StatelessWidget {
  final bool flag;
  const MainApp({super.key, required this.flag});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: flag ? const ToListUI() : const LoginAndSign(),
      debugShowCheckedModeBanner: false,
      scaffoldMessengerKey: snackBarKey,
    );
  }
}
