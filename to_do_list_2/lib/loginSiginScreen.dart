import "dart:ui";

import 'package:flutter/material.dart';
import "package:email_validator/email_validator.dart";
import "package:google_fonts/google_fonts.dart";
import "package:sqflite/sqflite.dart";
import "package:to_do_list_2/todoList_basic2.dart";
import 'utilites.dart';
import 'package:intl/intl.dart';

class LoginAndSign extends StatefulWidget {
  const LoginAndSign({super.key});
  @override
  State createState() => _LoginAndSignState();
}

class _LoginAndSignState extends State {
  int pageIndex = 0;
  List<Map> loginList = [
    {
      "name": "username",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    },
    {
      "name": "password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
    }
  ];

  List<Map> siginList = [
    {
      "name": "Email",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Name",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Phone",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Username",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    },
    {
      "name": "Confirm Password",
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController()
    }
  ];
  Map? account;
  bool pass = false;
  bool pass1 = false;
  bool pass2 = false;

  snacks() async {
    if (sessionFlag) {
      await Future.delayed(const Duration(milliseconds: 1500));
      snackBarKey.currentState!.showSnackBar(SnackBar(
          duration: const Duration(milliseconds: 1600),
          behavior: SnackBarBehavior.fixed,
          backgroundColor: Colors.red.withOpacity(0.95),
          content: Center(
            child: Text(
              "Session Time Out",
              style: GoogleFonts.quicksand(
                  fontSize: 18,
                  fontWeight: FontWeight.w600,
                  color: Colors.white),
            ),
          )));
      sessionFlag = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    snacks();
    if (pageIndex == 0) {
      return Scaffold(
          body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/loginBg.avif'), fit: BoxFit.cover)),
        height: double.infinity,
        width: double.infinity,
        child: BackdropFilter(
          filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                height: 50,
                width: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Colors.white12.withOpacity(0.55),
                ),
                child: Center(
                  child: Text(
                    "Login page",
                    style: GoogleFonts.quicksand(
                        fontSize: 25, fontWeight: FontWeight.w700),
                  ),
                ),
              ),
              Container(
                height: 270,
                width: 380,
                padding: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white12.withOpacity(0.55)),
                child: ListView.separated(
                  itemCount: loginList.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      height: 20,
                    );
                  },
                  itemBuilder: (BuildContext context, int index) {
                    return TextFormField(
                      controller: loginList[index]["controller"],
                      key: loginList[index]["key"],
                      validator: (value) {
                        if ((value == null || value.isEmpty) && count > 0) {
                          return "Please enter vaild ${loginList[index]["name"]}";
                        }
                        return null;
                      },
                      obscureText: (index == 1) ? !pass : false,
                      obscuringCharacter: "*",
                      keyboardType:
                          (index == 1) ? TextInputType.visiblePassword : null,
                      onChanged: (value) {
                        for (int i = 0;
                            count > 0 && i < loginList.length;
                            i++) {
                          loginList[i]["key"].currentState!.validate();
                        }
                      },
                      autovalidateMode: AutovalidateMode.always,
                      decoration: InputDecoration(
                          suffixIcon: (index == 1)
                              ? IconButton(
                                  onPressed: () {
                                    setState(() {
                                      pass = !pass;
                                    });
                                  },
                                  icon: Icon(!pass
                                      ? Icons.visibility_off
                                      : Icons.visibility))
                              : null,
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 2.5)),
                          labelText: loginList[index]["name"],
                          hintText: (index == 1)
                              ? "Enter ${loginList[index]["name"]}"
                              : "Enter username,email or phone number",
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: const BorderSide(
                                  color: Colors.black87, width: 2.5))),
                    );
                  },
                ),
              ),
              GestureDetector(
                onTap: () async {
                  for (int i = 0; i < loginList.length; i++) {
                    loginList[i]["key"].currentState!.validate();
                  }
                  count++;
                  bool hasUser = false;
                  for (int i = 0; i < userDataList.length; i++) {
                    if ((userDataList[i].username ==
                                loginList[0]["controller"].text ||
                            userDataList[i].email ==
                                loginList[0]["controller"].text ||
                            userDataList[i].phone ==
                                loginList[0]["controller"].text) &&
                        userDataList[i].password ==
                            loginList[1]["controller"].text) {
                      hasUser = true;
                      //account = userData[i];
                      logInfoObj!.lastLogin =
                          DateFormat('dd-MMM-yyyy , hh:mm a')
                              .format(DateTime.now());
                      await database.update(
                          'LogInfo', logInfoObj?.getLogInfoMap(),
                          where: 'primKey= ?',
                          whereArgs: [logInfoObj?.primKey]);
                      await database.insert(
                          'CurrentLogin', userDataList[i].getUserMap(),
                          conflictAlgorithm: ConflictAlgorithm.replace);
                      currentUser = await getUserList('CurrentLogin');
                      currentUserObj = currentUser[0];
                      taskTableName = currentUserObj!.tableName;
                      taskList = await getTaskData(taskTableName!);
                      // Navigator.of(context).pop();

                      // if (!Navigator.of(context).canPop()) {
                      //   Navigator.of(context).push(MaterialPageRoute(
                      //     builder: (context) => const ToListUI(),
                      //   ));
                      // }

                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const ToListUI(),
                          ));

                      setState(() {
                        emailVaildator = false;
                        for (int i = 0; i < loginList.length; i++) {
                          loginList[i]["controller"].clear();
                        }
                        displaySnackBar(
                            context, Colors.green, "Login Succesfully");

                        count = 0;
                      });

                      break;
                    }
                  }
                  if (!hasUser) {
                    displaySnackBar(
                        context, Colors.red, "Inavild username or password");
                  }
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.white12.withOpacity(0.55),
                      borderRadius: BorderRadius.circular(30)),
                  height: 50,
                  width: 200,
                  child: Center(
                      child: Text(
                    "login",
                    style: GoogleFonts.quicksand(
                        fontSize: 18, fontWeight: FontWeight.w600),
                  )),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.white12.withOpacity(0.55)),
                child: TextButton(
                    onPressed: () {
                      setState(() {
                        pageIndex = 1;
                        emailVaildator = false;
                        for (int i = 0; i < loginList.length; i++) {
                          loginList[i]["controller"].clear();
                        }
                        count = 0;
                      });
                    },
                    child: Text(
                      "I don't have account.Sigin?",
                      style: GoogleFonts.quicksand(
                          fontSize: 18,
                          fontWeight: FontWeight.w600,
                          color: const Color.fromARGB(255, 88, 11, 101)),
                    )),
              )
            ],
          ),
        ),
      ));
    } else {
      return Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('images/siginBg.avif'), fit: BoxFit.cover)),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const SizedBox(
                    height: 15,
                  ),
                  Container(
                    height: 40,
                    width: 200,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(30),
                        color: Colors.white12.withOpacity(0.55)),
                    child: Center(
                      child: Text(
                        "Sign in page",
                        style: GoogleFonts.quicksand(
                            fontSize: 25, fontWeight: FontWeight.w700),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 12,
                  ),
                  Container(
                      height: 600,
                      width: 380,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 20, vertical: 5),
                      decoration: BoxDecoration(
                          color: Colors.white12.withOpacity(0.55),
                          borderRadius: BorderRadius.circular(30)),
                      child: ListView.separated(
                        itemCount: siginList.length,
                        separatorBuilder: (BuildContext context, int index) {
                          if (index == 4) {
                            return Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      !passList[0]
                                          ? Icons.circle_outlined
                                          : Icons.check_circle,
                                      color: passList[0]
                                          ? Colors.green
                                          : Colors.red,
                                    ),
                                    Text(
                                      "At least one lowercase letter",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: passList[0]
                                              ? Colors.green
                                              : Colors.red),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      !passList[1]
                                          ? Icons.circle_outlined
                                          : Icons.check_circle,
                                      color: passList[1]
                                          ? Colors.green
                                          : Colors.red,
                                    ),
                                    Text(
                                      "At least one uppercase letter",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: passList[1]
                                              ? Colors.green
                                              : Colors.red),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      !passList[2]
                                          ? Icons.circle_outlined
                                          : Icons.check_circle,
                                      color: passList[2]
                                          ? Colors.green
                                          : Colors.red,
                                    ),
                                    Text(
                                      "At least one number",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: passList[2]
                                              ? Colors.green
                                              : Colors.red),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      !passList[3]
                                          ? Icons.circle_outlined
                                          : Icons.check_circle,
                                      color: passList[3]
                                          ? Colors.green
                                          : Colors.red,
                                    ),
                                    Text(
                                      "Be at least 8 character",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: passList[3]
                                              ? Colors.green
                                              : Colors.red),
                                    )
                                  ],
                                ),
                                Row(
                                  children: [
                                    Icon(
                                      !passList[4]
                                          ? Icons.circle_outlined
                                          : Icons.check_circle,
                                      color: passList[4]
                                          ? Colors.green
                                          : Colors.red,
                                    ),
                                    Text(
                                      "Must contains at least 1 special character",
                                      style: GoogleFonts.quicksand(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w500,
                                          color: passList[4]
                                              ? Colors.green
                                              : Colors.red),
                                    )
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                              ],
                            );
                          } else {
                            return const SizedBox(
                              height: 10,
                            );
                          }
                        },
                        itemBuilder: (BuildContext context, int index) {
                          return TextFormField(
                            controller: siginList[index]["controller"],
                            key: siginList[index]["key"],
                            obscureText: (!pass1 && index == 4)
                                ? true
                                : (!pass2 && index == 5)
                                    ? true
                                    : false,
                            validator: (String? val) {
                              if (index == 0) {
                                if (val != null) {
                                  emailVaildator = EmailValidator.validate(val);

                                  if (!emailVaildator) {
                                    return "Please enter vaild email";
                                  } else if (isEmailPresent(val)) {
                                    return "This email is already regesistered";
                                  }
                                } else if (count > 0 && val == null) {
                                  return "Please enter vaild email";
                                }
                              } else if (index == 1) {
                                if (val == null || val.isEmpty) {
                                  return "Please enter valid name";
                                }
                              } else if (index == 2) {
                                if ((val != null &&
                                        !phoneNumVerification(val)) ||
                                    (count > 0 && val == null)) {
                                  return "Please enter vaild phone number";
                                } else if (val != null && isNumPresent(val)) {
                                  return "This number is already regesistered";
                                }
                              } else if (index == 3) {
                                if (val == null || val.isEmpty) {
                                  return "Please enter vaild username";
                                } else if (val.contains(" ")) {
                                  return "Please enter vaild username";
                                } else if (val.isNotEmpty) {
                                  int ascii = val.trim().codeUnitAt(0);
                                  if (ascii >= 65 && ascii <= 90) {
                                    return "username must not start with Upperclass letter";
                                  } else if (!usernameVerfication(val)) {
                                    return "User already taken";
                                  }
                                }
                              } else if (index == 4) {
                                if ((val == null || val.isEmpty) && count > 0) {
                                  return "please enter vaild password";
                                }
                              } else if (index == 5) {
                                if ((val != null &&
                                        siginList[4]["controller"].text !=
                                            val) ||
                                    (count > 0 &&
                                        (val == null || val.isEmpty))) {
                                  return "Password don't match";
                                }
                              }

                              return null;
                            },
                            onChanged: (String val) {
                              if (index == 4) {
                                setState(() {
                                  passList[0] = passCritria1(val);
                                  passList[1] = passCritria2(val);
                                  passList[2] = passCritria3(val);
                                  passList[3] = passCritria4(val);
                                  passList[4] = passCritria5(val);
                                });
                              }
                            },
                            keyboardType: (index == 0)
                                ? TextInputType.emailAddress
                                : (index == 1)
                                    ? TextInputType.name
                                    : (index == 2)
                                        ? TextInputType.phone
                                        : (index == 3)
                                            ? TextInputType.name
                                            : TextInputType.visiblePassword,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            decoration: InputDecoration(
                                hintText: siginList[index]["name"],
                                suffixIcon: (index == 4)
                                    ? IconButton(
                                        icon: Icon((pass1)
                                            ? Icons.visibility
                                            : Icons.visibility_off),
                                        onPressed: () {
                                          pass1 = !pass1;
                                          setState(() {});
                                        },
                                      )
                                    : (index == 5)
                                        ? IconButton(
                                            icon: Icon((pass2)
                                                ? Icons.visibility
                                                : Icons.visibility_off),
                                            onPressed: () {
                                              pass2 = !pass2;
                                              setState(() {});
                                            },
                                          )
                                        : null,
                                labelText: siginList[index]["name"],
                                focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide: const BorderSide(
                                        color: Color.fromARGB(255, 243, 33, 33),
                                        width: 2.4)),
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide: const BorderSide(
                                        color: Colors.blue, width: 2.4)),
                                errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide: const BorderSide(
                                        color: Colors.red, width: 2.4)),
                                enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(30),
                                    borderSide: const BorderSide(
                                        color: Colors.black, width: 2.4))),
                          );
                        },
                      )),
                  const SizedBox(
                    height: 25,
                  ),
                  GestureDetector(
                    onTap: () async {
                      count++;
                      int checkCount = 0;
                      for (int i = 0; i < siginList.length; i++) {
                        if (siginList[i]["key"].currentState!.validate()) {
                          checkCount++;
                        }
                      }
                      if (checkCount == siginList.length &&
                          !passList.contains(false)) {
                        count = 0;

                        UserInfo addUserObj = UserInfo(
                            username: siginList[3]["controller"].text,
                            email: siginList[0]["controller"].text,
                            password: siginList[4]["controller"].text,
                            phone: siginList[2]["controller"].text,
                            tableName:
                                "${siginList[3]["controller"].text}TaskList",
                            name: siginList[1]["controller"].text);
                        await database.insert(
                            'UserLoginIdTable', addUserObj.getUserMap(),
                            conflictAlgorithm: ConflictAlgorithm.replace);
                        userDataList = await getUserList('UserLoginIdTable');
                        await database.insert(
                            'CurrentLogin', addUserObj.getUserMap(),
                            conflictAlgorithm: ConflictAlgorithm.replace);
                        currentUser = await getUserList('CurrentLogin');
                        currentUserObj = currentUser[0];

                        if (!await doesTableExits(addUserObj.tableName)) {
                          await database.execute(
                              '''CREATE TABLE ${addUserObj.tableName} (taskNum INTEGER PRIMARY KEY,title TEXT,description TEXT,date TEXT,imgIndex INT,imgURL TEXT,taskDone INTEGER)''');
                        }
                        taskList = await getTaskData(addUserObj.tableName);
                        logInfoObj!.lastLogin =
                            DateFormat('dd-MMM-yyyy , hh:mm a')
                                .format(DateTime.now());
                        await database.update(
                            'LogInfo', logInfoObj?.getLogInfoMap(),
                            where: 'primKey= ?',
                            whereArgs: [logInfoObj?.primKey]);
                        Navigator.of(context).pop();
                        if (!Navigator.of(context).canPop()) {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return const ToListUI();
                          }));
                        }
                        displaySnackBar(context, Colors.green,
                            "Account created Succesfully");

                        emailVaildator = false;
                        pass1 = false;
                        pass2 = false;
                        pageIndex = 0;

                        for (int i = 0; i < siginList.length; i++) {
                          siginList[i]["controller"].clear();
                        }
                        passList = [false, false, false, false, false];
                        setState(() {});
                      }
                    },
                    child: Container(
                      height: 45,
                      width: 120,
                      decoration: BoxDecoration(
                          color: Colors.white12.withOpacity(0.55),
                          borderRadius: BorderRadius.circular(30)),
                      child: Center(
                        child: Text(
                          "Sign",
                          style: GoogleFonts.quicksand(
                              fontSize: 18, fontWeight: FontWeight.w600),
                        ),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 25,
                  ),
                  Container(
                    decoration: BoxDecoration(
                        color: Colors.white.withOpacity(0.55),
                        borderRadius: BorderRadius.circular(30)),
                    child: TextButton(
                        onPressed: () {
                          count = 0;
                          pageIndex = 0;
                          for (int i = 0; i < siginList.length; i++) {
                            siginList[i]["controller"].clear();
                          }
                          emailVaildator = false;
                          passList = [false, false, false, false, false];
                          pass1 = false;
                          pass2 = false;
                          setState(() {});
                        },
                        child: Text("I already have account. Login?",
                            style: GoogleFonts.quicksand(
                                fontSize: 16,
                                fontWeight: FontWeight.w600,
                                color:
                                    const Color.fromARGB(255, 88, 11, 101)))),
                  )
                ],
              ),
            ),
          ),
        ),
      );
    }
  }
}
