import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:intl/intl.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';
import 'package:to_do_list_2/loginSiginScreen.dart';
import 'package:to_do_list_2/userInfoEditPage.dart';
import 'utilites.dart';

class ToListUI extends StatefulWidget {
  const ToListUI({super.key});

  @override
  State createState() => _ToDoListState();
}

class _ToDoListState extends State {
  final GlobalKey<FormFieldState> _key1 = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _key2 = GlobalKey<FormFieldState>();
  final GlobalKey<FormFieldState> _key3 = GlobalKey<FormFieldState>();
  Future<List<Task>> getTaskData() async {
    List taskListL = await database.query(currentUserObj!.tableName);

    return List.generate(taskListL.length, (index) {
      return Task(
          taskNum: taskListL[index]['taskNum'],
          title: taskListL[index]['title'],
          description: taskListL[index]['description'],
          date: taskListL[index]['date'],
          imgIndex: taskListL[index]['imgIndex'],
          imgURl: taskListL[index]['imgURL'],
          taskDone: taskListL[index]['taskDone']);
    });
  }

  List<ChipData> chipsList = [
    ChipData(type: 'All', selection: true),
    ChipData(type: 'pending', selection: false),
    ChipData(type: 'Completed', selection: false),
    ChipData(type: 'Incomplete', selection: false),
  ];
  List<Task> subTaskList = [];
  List<Map> inputTask = [
    {
      "taskName": "Title",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Description",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Date",
      "controller": TextEditingController(),
    },
    {
      "taskName": "Network Image URl(Optional)",
      "controller": TextEditingController(),
    }
  ];
  List<String> imgList = [
    "images/profile.png",
    "images/meeting.gif",
    "images/task1.jpg",
    "images/bill.gif",
    "images/task2.jpg",
    "images/car.gif",
    "images/task3.webp",
    "images/doctor.gif",
    "images/task4.png",
    "images/shopping.gif",
    "images/task5.jpg",
  ];
  bool isTaskDone(int taskDone) {
    if (taskDone == 0) {
      return false;
    } else {
      return true;
    }
  }

  String getGreeting() {
    DateTime dt = DateTime.now();
    String dayType = DateFormat("a").format(dt);
    String temp = DateFormat('mm').format(dt);
    temp = '0.$temp';
    double minute = double.parse(temp);

    double hour = double.parse(DateFormat("hh").format(dt));
    hour = hour + minute;
    //print(dayType);
    //print(hour);
    if (dayType == 'PM') {
      if ((hour >= 1.0 && hour < 5.0) || (hour >= 12.0 && hour <= 12.59)) {
        return 'afternoon';
      } else if (hour >= 5.0 && hour < 10.30) {
        return 'evening';
      } else {
        return 'night';
      }
    } else {
      if ((hour >= 12.00 && hour <= 12.59) || hour < 5.0) {
        return 'night';
      } else {
        return 'morning';
      }
    }
  }

  void subListSort() {
    subTaskList.clear();
    if (!chipsList[0].selection ||
        (chipsList[1].selection &&
            chipsList[2].selection &&
            chipsList[3].selection)) {
      Set<Task> subTaskSet = {};
      for (int i = 0; i < taskList.length; i++) {
        if (chipsList[1].selection &&
            taskList[i].taskDone == 0 &&
            !isDateExide(taskList[i].date)) {
          subTaskSet.add(taskList[i]);
        }
        if (chipsList[2].selection && taskList[i].taskDone == 1) {
          subTaskSet.add(taskList[i]);
        }
        if (chipsList[3].selection &&
            taskList[i].taskDone == 0 &&
            isDateExide(taskList[i].date)) {
          subTaskSet.add(taskList[i]);
        }
      }
      subTaskList = subTaskSet.toList();
    } else {
      for (int i = 0; i < taskList.length; i++) {
        subTaskList.add(taskList[i]);
      }
      subTaskList = subTaskList.reversed.toList();
    }
  }

  bool isDateExide(String taskDateTime) {
    DateFormat inputFormat = DateFormat('dd-MMM-yyyy , hh:mm a');
    DateTime taskDT = inputFormat.parse(taskDateTime);
    return taskDT.isBefore(DateTime.now());
  }

  Color getTaskColor(Task taskobj) {
    if (isTaskDone(taskobj.taskDone) && isDateExide(taskobj.date)) {
      return const Color.fromARGB(255, 169, 238, 170).withOpacity(0.55);
    } else if (isTaskDone(taskobj.taskDone)) {
      return const Color.fromARGB(255, 169, 238, 170).withOpacity(0.55);
    } else if (isDateExide(taskobj.date)) {
      return const Color.fromARGB(255, 238, 169, 169).withOpacity(0.55);
    } else {
      if (themeColor) {
        return Colors.white;
      } else {
        return Color.fromARGB(255, 58, 58, 58);
      }
    }
  }

  Image imgChoice(String? imgUrl, int i) {
    if (imgUrl != null && imgUrl.isNotEmpty) {
      return Image.network(
        imgUrl,
        height: 50,
        width: 50,
        fit: BoxFit.cover,
      );
    } else {
      return Image.asset(imgList[subTaskList[i].imgIndex]);
    }
  }

  bool themeColor = (logInfoObj!.themeMode == 0) ? true : false;
  taskModification(bool editable, {Task? taskObj}) {
    int selectedInd = 0;
    int submitCount = 0;
    if (editable == false) {
      for (int i = 0; i < inputTask.length; i++) {
        inputTask[i]["controller"].clear();
      }
    } else if (taskObj != null) {
      inputTask[0]["controller"].text = taskObj.title;
      inputTask[1]["controller"].text = taskObj.description;
      inputTask[2]["controller"].text = taskObj.date;
      if (taskObj.imgURl != null) {
        inputTask[3]["controller"].text = taskObj.imgURl;
      }
      selectedInd = taskObj.imgIndex;
    }

    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        backgroundColor: (themeColor == false) ? Colors.black87 : null,
        builder: (BuildContext context) {
          return SingleChildScrollView(
            child: Padding(
              padding: MediaQuery.of(context).viewInsets,
              child: Container(
                decoration: BoxDecoration(
                  border: Border.all(
                      color: (themeColor) ? Colors.black87 : Colors.white,
                      width: 1),
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      topRight: Radius.circular(20)),
                ),
                width: (FocusScope.of(context).hasPrimaryFocus)
                    ? 340
                    : double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(
                      height: 30,
                      width: double.infinity,
                      child: Icon(
                        Icons.horizontal_rule_rounded,
                        size: 60,
                        color: (themeColor == false) ? Colors.white : null,
                      ),
                    ),
                    Text(
                      (editable == false) ? "Create task" : "Edit task",
                      style: GoogleFonts.quicksand(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: (themeColor == false) ? Colors.white : null),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 310,
                      width: 300,
                      child: ListView.separated(
                        separatorBuilder: (BuildContext context, int index) =>
                            const SizedBox(
                          height: 8,
                        ),
                        itemCount: inputTask.length,
                        itemBuilder: (BuildContext context, int index) {
                          return TextFormField(
                            key: (index == 0)
                                ? _key1
                                : (index == 1)
                                    ? (_key2)
                                    : (index == 2)
                                        ? _key3
                                        : null,
                            controller: inputTask[index]["controller"],
                            maxLength: (index == 0)
                                ? 50
                                : (index == 1)
                                    ? 250
                                    : (index == 2)
                                        ? null
                                        : null,
                            cursorWidth: 2,
                            cursorHeight: 30,
                            onChanged: (submitCount > 0)
                                ? (String val) {
                                    _key1.currentState?.validate();
                                    _key2.currentState?.validate();
                                    _key3.currentState?.validate();
                                  }
                                : null,
                            cursorColor: const Color.fromARGB(255, 105, 9, 122),
                            cursorRadius: const Radius.circular(50),
                            cursorOpacityAnimates: true,
                            textAlign: TextAlign.center,
                            validator: (String? value) {
                              if ((value == null || value.isEmpty) &&
                                  index == 0) {
                                return "Please enter valid title";
                              } else if ((value == null || value.isEmpty) &&
                                  index == 1) {
                                return "Please enter valid desciption";
                              } else if ((value == null || value.isEmpty) &&
                                  index == 2) {
                                return "Please enter valid date";
                              } else {
                                return null;
                              }
                            },
                            textAlignVertical: TextAlignVertical.center,
                            readOnly: (index == 2) ? true : false,
                            onTap: (index == 2)
                                ? () async {
                                    //pick the date from datePicker
                                    DateTime? pickdate = await showDatePicker(
                                        context: context,
                                        initialDate: DateTime.now(),
                                        firstDate: DateTime.now(),
                                        lastDate: DateTime(2050));

                                    //Format the date into the requierd
                                    //format of the date i.e. year month date

                                    TimeOfDay? pickTime = await showTimePicker(
                                      context: context,
                                      initialTime: TimeOfDay.now(),
                                    );

                                    if (pickdate != null && pickTime != null) {
                                      DateTime dateTime = DateTime(
                                          pickdate.year,
                                          pickdate.month,
                                          pickdate.day,
                                          pickTime.hour,
                                          pickTime.minute);
                                      DateFormat df =
                                          DateFormat('dd-MMM-yyyy , hh:mm a');
                                      String formatedDate = df.format(dateTime);
                                      setState(() {
                                        inputTask[index]["controller"].text =
                                            formatedDate;
                                      });
                                    }
                                    if (submitCount > 0) {
                                      _key3.currentState!.validate();
                                    }
                                  }
                                : null,
                            style: GoogleFonts.quicksand(
                                fontSize: 18,
                                color: (themeColor == false)
                                    ? Colors.white
                                    : null),
                            decoration: InputDecoration(
                              hintText: "Enter ${inputTask[index]["taskName"]}",
                              hintStyle: GoogleFonts.quicksand(
                                  fontSize: 14,
                                  color: (themeColor == false)
                                      ? Colors.white
                                      : null),
                              labelText: inputTask[index]["taskName"],
                              labelStyle: GoogleFonts.quicksand(
                                  fontSize: 16,
                                  color: (themeColor == false)
                                      ? Colors.white
                                      : null),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: (index == 3)
                                          ? Colors.yellow
                                          : (submitCount > 0)
                                              ? Colors.purple
                                              : (themeColor)
                                                  ? Colors.black87
                                                  : Colors.white,
                                      width: 2),
                                  gapPadding: 10,
                                  borderRadius: BorderRadius.circular(20)),
                              suffixIcon: (index == 2)
                                  ? const Icon(
                                      Icons.calendar_month_outlined,
                                      color: Color.fromRGBO(2, 167, 177, 1),
                                    )
                                  : null,
                              errorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide:
                                      const BorderSide(color: Colors.red)),
                              focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(30),
                                  borderSide:
                                      const BorderSide(color: Colors.red)),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(
                                      color: (index == 3)
                                          ? Colors.yellow
                                          : (submitCount > 0)
                                              ? Colors.green
                                              : Colors.blue,
                                      width: 2),
                                  gapPadding: 3,
                                  borderRadius: BorderRadius.circular(20)),
                            ),
                          );
                        },
                      ),
                    ),
                    Text("--------provide network path OR----------",
                        style: GoogleFonts.quicksand(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color:
                                (themeColor == false) ? Colors.white : null)),
                    Text("Choose gif :",
                        style: GoogleFonts.quicksand(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                            color:
                                (themeColor == false) ? Colors.white : null)),
                    const SizedBox(
                      height: 10,
                    ),
                    SizedBox(
                      height: 45,
                      width: 300,
                      child: ListView.separated(
                        scrollDirection: Axis.horizontal,
                        itemCount: imgList.length,
                        separatorBuilder: (BuildContext context, int index) {
                          return const SizedBox(
                            width: 25,
                          );
                        },
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              setState(() {
                                selectedInd = index;

                                if (FocusScope.of(context).hasPrimaryFocus ==
                                    false) {
                                  FocusScope.of(context).unfocus();
                                }
                              });
                            },
                            child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    color: Colors.white,
                                    border: (index == selectedInd)
                                        ? Border.all(
                                            color: const Color.fromARGB(
                                                255, 191, 13, 222),
                                            width: 4)
                                        : null,
                                    boxShadow: const [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.7),
                                        blurRadius: 2,
                                      )
                                    ]),
                                child: ClipRRect(
                                    borderRadius: BorderRadius.circular(25),
                                    child: Image.asset(
                                      imgList[index],
                                      height: 40,
                                      width: 40,
                                    ))),
                          );
                        },
                      ),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.center, children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            minimumSize: const Size(90, 50),
                            foregroundColor: Colors.white,
                            backgroundColor:
                                const Color.fromRGBO(0, 139, 148, 1)),
                        onPressed: () async {
                          for (int i = 0; i < inputTask.length; i++) {
                            inputTask[i]["controller"].text =
                                inputTask[i]["controller"].text.trim();
                          }
                          if (inputTask[0]["controller"].text.length > 0 &&
                              inputTask[1]["controller"].text.length > 0 &&
                              inputTask[2]["controller"].text.length > 0) {
                            if (editable == false) {
                              Task taskObj = Task(
                                  title: inputTask[0]["controller"].text,
                                  description: inputTask[1]["controller"].text,
                                  date: inputTask[2]["controller"].text,
                                  imgIndex: selectedInd,
                                  imgURl:
                                      (inputTask[3]["controller"].text.isEmpty)
                                          ? null
                                          : inputTask[3]["controller"].text,
                                  taskDone: 0);
                              //instalizer();
                              await database.insert(currentUserObj!.tableName,
                                  taskObj.getTaskMap(),
                                  conflictAlgorithm: ConflictAlgorithm.replace);
                              taskList = await getTaskData();
                            } else {
                              taskObj?.title = inputTask[0]["controller"].text;
                              taskObj?.description =
                                  inputTask[1]["controller"].text;
                              taskObj?.date = inputTask[2]["controller"].text;
                              taskObj?.imgURl = inputTask[3]["controller"].text;
                              taskObj?.imgIndex = selectedInd;
                              await database.update(currentUserObj!.tableName,
                                  taskObj?.getTaskMap(),
                                  where: 'taskNum= ?',
                                  whereArgs: [taskObj?.taskNum]);
                            }
                            setState(() {});
                            Navigator.of(context).pop();
                          } else {
                            submitCount++;
                            _key1.currentState!.validate();
                            _key2.currentState!.validate();
                            _key3.currentState!.validate();
                          }
                        },
                        child: Text(
                          "Save",
                          style: GoogleFonts.inter(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            minimumSize: const Size(90, 50),
                            foregroundColor: Colors.white,
                            backgroundColor:
                                const Color.fromRGBO(0, 139, 148, 1)),
                        onPressed: () {
                          for (int i = 0; i < inputTask.length; i++) {
                            inputTask[i]["controller"].clear();
                          }
                          if (submitCount > 0) {
                            _key1.currentState!.validate();
                            _key2.currentState!.validate();
                            _key3.currentState!.validate();
                          }
                          //FocusScope.of(context).unfocus();
                        },
                        child: Text(
                          "ClearAll",
                          style: GoogleFonts.inter(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      )
                    ])
                  ],
                ),
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    subListSort();
    return Scaffold(
      backgroundColor: (logInfoObj!.themeMode == 0)
          ? const Color.fromRGBO(111, 81, 255, 1)
          : Colors.black12,
      body: RefreshIndicator.adaptive(
        edgeOffset: -65,
        displacement: 90,
        triggerMode: RefreshIndicatorTriggerMode.anywhere,
        strokeWidth: 2.7,
        color: const Color.fromARGB(255, 11, 145, 255),
        backgroundColor: Colors.white,
        onRefresh: () async {
          Future.delayed(const Duration(milliseconds: 2000));
          setState(() {});
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              margin: const EdgeInsets.only(top: 70, left: 30),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Good ${getGreeting()}",
                    style: GoogleFonts.quicksand(
                        fontSize: 22,
                        fontWeight: FontWeight.w400,
                        color: Colors.white),
                  ),
                  const Spacer(),
                  GestureDetector(
                    child: const Icon(
                      Icons.logout_rounded,
                      color: Colors.red,
                    ),
                    onTap: () async {
                      logInfoObj!.themeMode = 0;
                      logInfoObj!.lastLogin = "0";
                      await database.update(
                          'LogInfo', logInfoObj?.getLogInfoMap(),
                          where: 'primKey= ?',
                          whereArgs: [logInfoObj?.primKey]);
                      await database.delete('CurrentLogin',
                          where: 'username=?',
                          whereArgs: [currentUserObj!.username]);
                      currentUser = await getUserList('CurrentLogin');
                      Navigator.of(context).pop();
                      if (!Navigator.of(context).canPop()) {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => const LoginAndSign(),
                        ));
                      }
                      displaySnackBar(
                          context, Colors.green, "Logout Succesfully");
                    },
                  ),
                  const SizedBox(
                    width: 12,
                  )
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.only(top: 12, left: 30),
                  child: Text(
                    currentUserObj!.name,
                    style: GoogleFonts.quicksand(
                        fontSize: 30,
                        fontWeight: FontWeight.w600,
                        color: Colors.white),
                  ),
                ),
                const Spacer(),
                GestureDetector(
                  onTap: () async {
                    if (logInfoObj!.themeMode == 0) {
                      logInfoObj!.themeMode = 1;
                    } else {
                      logInfoObj!.themeMode = 0;
                    }
                    themeColor = (logInfoObj!.themeMode == 0) ? true : false;

                    setState(() {});
                    await database.update(
                        'LogInfo', logInfoObj?.getLogInfoMap(),
                        where: 'primKey= ?', whereArgs: [logInfoObj?.primKey]);
                  },
                  child: Icon(
                    !themeColor
                        ? Icons.light_mode_outlined
                        : Icons.dark_mode_outlined,
                    color: Colors.white,
                  ),
                ),
                const SizedBox(
                  width: 10,
                ),
                GestureDetector(
                  child: const Icon(
                    Icons.account_circle_rounded,
                    color: Colors.white,
                  ),
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(
                      builder: (context) => UserInfoEditPage(),
                    ))
                        .then((value) {
                      setState(() {});
                    });
                    setState(() {});
                  },
                ),
                const SizedBox(
                  width: 12,
                )
              ],
            ),
            Expanded(
                child: Container(
              margin: const EdgeInsets.only(top: 20),
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(40)),
                  color: (logInfoObj!.themeMode == 0)
                      ? const Color.fromRGBO(217, 217, 217, 1)
                      : const Color.fromRGBO(84, 84, 84, 1)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: Text(
                      "CREATE TO DO LIST",
                      style: GoogleFonts.quicksand(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: themeColor ? Colors.black : Colors.white),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    width: double.infinity,
                    margin: const EdgeInsets.only(top: 20),
                    padding: const EdgeInsets.only(top: 10),
                    decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            topLeft: Radius.circular(40),
                            topRight: Radius.circular(40)),
                        color: (logInfoObj!.themeMode == 0)
                            ? Colors.white
                            : const Color.fromARGB(255, 38, 38, 38)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 10),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: chipsList.map((ChipData chipItr) {
                                return Container(
                                  margin:
                                      const EdgeInsets.symmetric(horizontal: 5),
                                  child: FilterChip(
                                    labelPadding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(40)),
                                    selected: chipItr.selection,
                                    shadowColor:
                                        const Color.fromRGBO(0, 0, 0, 0.6),
                                    showCheckmark: true,
                                    checkmarkColor: Colors.green,
                                    backgroundColor: themeColor
                                        ? const Color.fromRGBO(111, 81, 255, 1)
                                            .withOpacity(0.075)
                                        : const Color.fromRGBO(0, 0, 0, 0.6),
                                    selectedShadowColor:
                                        const Color.fromRGBO(0, 0, 0, 0.8),
                                    selectedColor: themeColor
                                        ? const Color.fromRGBO(111, 81, 255, 1)
                                            .withOpacity(0.275)
                                        : const Color.fromRGBO(111, 81, 255, 1)
                                            .withOpacity(0.45),
                                    label: Text(
                                      chipItr.type,
                                      style: GoogleFonts.inter(
                                          fontSize: 13,
                                          fontWeight: FontWeight.w500,
                                          color: !themeColor
                                              ? Colors.white
                                              : null),
                                    ),
                                    onSelected: (bool selectVal) {
                                      chipItr.selection = selectVal;
                                      if (chipItr == chipsList[0]) {
                                        for (int i = 1;
                                            i < chipsList.length;
                                            i++) {
                                          chipsList[i].selection = false;
                                        }
                                      }
                                      for (int i = 1;
                                          i < chipsList.length;
                                          i++) {
                                        if (chipsList[i].selection) {
                                          chipsList[0].selection = false;
                                          break;
                                        }
                                      }
                                      setState(() {});
                                    },
                                  ),
                                );
                              }).toList(),
                            ),
                          ),
                        ),
                        const SizedBox(
                          height: 7,
                        ),
                        Expanded(
                          child: (subTaskList.isNotEmpty)
                              ? ListView.builder(
                                  itemCount: subTaskList.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Slidable(
                                      //controller: ,
                                      //key: key,
                                      closeOnScroll: true,
                                      endActionPane: ActionPane(
                                          motion: const StretchMotion(),
                                          extentRatio: 0.2,
                                          children: [
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: [
                                                const SizedBox(
                                                  height: 1,
                                                ),
                                                GestureDetector(
                                                  onTap: (!isDateExide(
                                                          subTaskList[index]
                                                              .date))
                                                      ? () {
                                                          taskModification(true,
                                                              taskObj:
                                                                  subTaskList[
                                                                      index]);

                                                          setState(() {});
                                                        }
                                                      : null,
                                                  child: Container(
                                                    height: 31,
                                                    width: 31,
                                                    decoration: BoxDecoration(
                                                        shape: BoxShape.circle,
                                                        color: !isDateExide(
                                                                subTaskList[
                                                                        index]
                                                                    .date)
                                                            ? const Color
                                                                .fromRGBO(
                                                                89, 57, 241, 1)
                                                            : const Color
                                                                .fromARGB(255,
                                                                198, 198, 198)),
                                                    child: const Icon(
                                                      Icons.edit_outlined,
                                                      size: 17,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () async {
                                                    await database.delete(
                                                        currentUserObj!
                                                            .tableName,
                                                        where: 'taskNum=?',
                                                        whereArgs: [
                                                          subTaskList[index]
                                                              .taskNum
                                                        ]);
                                                    taskList =
                                                        await getTaskData();

                                                    setState(() {});
                                                  },
                                                  child: Container(
                                                    height: 31,
                                                    width: 31,
                                                    decoration:
                                                        const BoxDecoration(
                                                            shape:
                                                                BoxShape.circle,
                                                            color:
                                                                Color.fromRGBO(
                                                                    89,
                                                                    57,
                                                                    241,
                                                                    1)),
                                                    child: const Icon(
                                                      Icons.delete_outlined,
                                                      size: 17,
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                                const SizedBox(
                                                  height: 1,
                                                ),
                                              ],
                                            ))
                                          ]),
                                      child: Container(
                                        padding: const EdgeInsets.all(12),
                                        margin: const EdgeInsets.only(
                                            bottom: 14, left: 8, right: 8),
                                        decoration: BoxDecoration(
                                            color: getTaskColor(
                                                subTaskList[index]),
                                            borderRadius:
                                                BorderRadius.circular(22),
                                            boxShadow: [
                                              BoxShadow(
                                                  color: themeColor
                                                      ? const Color.fromARGB(
                                                          255, 236, 236, 236)
                                                      : const Color.fromARGB(
                                                          255, 56, 56, 56),
                                                  blurRadius: 20,
                                                  spreadRadius: 10,
                                                  offset: const Offset(0, 4))
                                            ]),
                                        child: Row(
                                          children: [
                                            Container(
                                                height: 52,
                                                width: 52,
                                                decoration: const BoxDecoration(
                                                    shape: BoxShape.circle,
                                                    color: Color.fromRGBO(
                                                        217, 217, 217, 1)),
                                                child: ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(25),
                                                  child: imgChoice(
                                                      subTaskList[index].imgURl,
                                                      index),
                                                )),
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            Expanded(
                                                child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                Text(
                                                  "• ${subTaskList[index].title}",
                                                  style: GoogleFonts.inter(
                                                      fontSize: 12,
                                                      fontWeight:
                                                          FontWeight.w500,
                                                      color: themeColor
                                                          ? Colors.black
                                                          : Colors.white),
                                                ),
                                                const SizedBox(
                                                  height: 7,
                                                ),
                                                Text(
                                                  "• ${subTaskList[index].description}",
                                                  style: GoogleFonts.inter(
                                                      fontSize: 9,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: themeColor
                                                          ? Colors.black
                                                          : Colors.white),
                                                ),
                                                const SizedBox(
                                                  height: 7,
                                                ),
                                                Text(
                                                  subTaskList[index].date,
                                                  style: GoogleFonts.inter(
                                                      fontSize: 8,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                      color: themeColor
                                                          ? Colors.black
                                                          : Colors.white),
                                                ),
                                              ],
                                            )),
                                            Checkbox(
                                              value: isTaskDone(
                                                  subTaskList[index].taskDone),
                                              onChanged: (!isDateExide(
                                                      subTaskList[index].date))
                                                  ? (bool? val) async {
                                                      if (isTaskDone(
                                                          subTaskList[index]
                                                              .taskDone)) {
                                                        subTaskList[index]
                                                            .taskDone = 0;
                                                      } else {
                                                        subTaskList[index]
                                                            .taskDone = 1;
                                                      }

                                                      await database.update(
                                                          currentUserObj!
                                                              .tableName,
                                                          subTaskList[index]
                                                              .getTaskMap(),
                                                          where: 'taskNum= ?',
                                                          whereArgs: [
                                                            subTaskList[index]
                                                                .taskNum
                                                          ]);
                                                      setState(() {});
                                                    }
                                                  : null,
                                              activeColor: Colors.green,
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    );
                                  },
                                )
                              : Center(
                                  child: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      Image.asset(
                                        'images/Notask.gif',
                                        height: 310,
                                        width: 310,
                                      ),
                                      Text(
                                        'No task available',
                                        style: GoogleFonts.quicksand(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ))
                ],
              ),
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: const Color.fromRGBO(89, 57, 241, 1),
        onPressed: () {
          taskModification(false);
        },
        child: const Icon(
          Icons.add,
          size: 50,
          color: Colors.white,
        ),
      ),
    );
  }
}
