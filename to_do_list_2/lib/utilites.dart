import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

dynamic database;

List<Task> taskList = [];
List<UserInfo> userDataList = [];
List<UserInfo> currentUser = [];
UserInfo? currentUserObj;
String? taskTableName;
Future<bool> doesTableExits(String table) async {
  List tableList = await database
      .query('sqlite_master', where: 'name = ?', whereArgs: [table]);
  return tableList.isNotEmpty;
}

Future<List<Task>> getTaskData(String databaseName) async {
  List taskListL = await database.query(databaseName);

  return List.generate(taskListL.length, (index) {
    return Task(
        taskNum: taskListL[index]['taskNum'],
        title: taskListL[index]['title'],
        description: taskListL[index]['description'],
        date: taskListL[index]['date'],
        imgIndex: taskListL[index]['imgIndex'],
        imgURl: taskListL[index]['imgURL'],
        taskDone: taskListL[index]['taskDone']);
  });
}

Future<List<UserInfo>> getUserList(String tableName) async {
  List usrList = await database.query(tableName);
  return List.generate(usrList.length, (index) {
    return UserInfo(
        username: usrList[index]["username"],
        email: usrList[index]["email"],
        password: usrList[index]["password"],
        phone: usrList[index]["phone"],
        tableName: usrList[index]["tableName"],
        name: usrList[index]['name']);
  });
}

void displaySnackBar(BuildContext context, Color color, String text) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      behavior: SnackBarBehavior.fixed,
      duration: const Duration(milliseconds: 1200),
      backgroundColor: color.withOpacity(0.95),
      content: Center(
        child: Text(
          text,
          style: GoogleFonts.quicksand(
              fontSize: 16, fontWeight: FontWeight.w600, color: Colors.white),
        ),
      )));
}

int count = 0;
bool emailVaildator = false;
bool phoneNumVerification(String str) {
  if (str.length != 10) {
    return false;
  } else {
    for (int i = 0; i < str.length; i++) {
      int ascii = str.codeUnitAt(i);
      if (ascii < 48 || ascii > 57) {
        return false;
      }
    }
    return true;
  }
}

bool usernameVerfication(String username) {
  for (int i = 0; i < userDataList.length; i++) {
    if (userDataList[i].username == username) {
      return false;
    }
  }
  return true;
}

List<bool> passList = [false, false, false, false, false];
bool passCritria3(String str) {
  for (int i = 0; i < str.length; i++) {
    int ascii = str.codeUnitAt(i);
    if (ascii >= 48 && ascii <= 57) {
      return true;
    }
  }
  return false;
}

bool passCritria2(String str) {
  for (int i = 0; i < str.length; i++) {
    int ascii = str.codeUnitAt(i);
    if (ascii >= 65 && ascii <= 90) {
      return true;
    }
  }
  return false;
}

bool passCritria1(String str) {
  for (int i = 0; i < str.length; i++) {
    int ascii = str.codeUnitAt(i);
    if (ascii >= 97 && ascii <= 122) {
      return true;
    }
  }
  return false;
}

bool passCritria4(String str) {
  if (str.length >= 8) {
    return true;
  } else {
    return false;
  }
}

bool passCritria5(String str) {
  if (str.contains("@") ||
      str.contains("!") ||
      str.contains("\$") ||
      str.contains("%") ||
      str.contains("^") ||
      str.contains("&") ||
      str.contains("*")) {
    return true;
  } else {
    return false;
  }
}

bool isEmailPresent(String email) {
  for (int i = 0; i < userDataList.length; i++) {
    if (userDataList[i].email == email) {
      return true;
    }
  }
  return false;
}

bool isNumPresent(String phone) {
  for (int i = 0; i < userDataList.length; i++) {
    if (userDataList[i].phone == phone) {
      return true;
    }
  }
  return false;
}

class Task {
  int? taskNum;
  String title;
  String description;
  String date;
  int imgIndex;
  String? imgURl;
  int taskDone;
  Task(
      {required this.title,
      required this.description,
      required this.date,
      required this.imgIndex,
      this.imgURl,
      this.taskNum,
      required this.taskDone});
  Map<String, dynamic> getTaskMap() {
    return {
      'title': title,
      'description': description,
      'date': date,
      'imgIndex': imgIndex,
      'imgURl': imgURl,
      'taskDone': taskDone
    };
  }
}

class UserInfo {
  String username;
  String name;
  String email;
  String phone;
  String password;
  String tableName;
  UserInfo(
      {required this.username,
      required this.email,
      required this.password,
      required this.phone,
      required this.tableName,
      required this.name});

  @override
  String toString() {
    return "$tableName  $username";
  }

  Map<String, dynamic> getUserMap() {
    return {
      'username': username,
      'name': name,
      'email': email,
      'phone': phone,
      'password': password,
      'tableName': tableName
    };
  }
}

Future<List<LogInfo>> getLogInfoData() async {
  List s = await database.query('LogInfo');
  return List.generate(s.length, (index) {
    return LogInfo(
        primKey: s[index]['primKey'],
        themeMode: s[index]['themeMode'],
        lastLogin: s[index]['lastLogin']);
  });
}

LogInfo? logInfoObj;
GlobalKey<ScaffoldMessengerState> snackBarKey =
    GlobalKey<ScaffoldMessengerState>();
bool sessionFlag = false;

class LogInfo {
  final int primKey;
  int themeMode;
  String lastLogin;
  LogInfo(
      {required this.primKey,
      required this.themeMode,
      required this.lastLogin});
  Map<String, dynamic> getLogInfoMap() {
    return {'primKey': primKey, 'themeMode': themeMode, 'lastLogin': lastLogin};
  }
}

class ChipData {
  String type;
  bool selection;
  ChipData({required this.type, required this.selection});
}
