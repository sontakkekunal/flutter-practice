import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class UIDesign1 extends StatefulWidget {
  const UIDesign1({super.key});

  @override
  State createState() => _UIDesign1State();
}

List<Map> course = [
  {"image": "images/ti0.png", "name": "UI/UX"},
  {"image": "images/ti1.png", "name": "VISUAL"},
  {"image": "images/ti2.png", "name": "ILLUSTRATON"},
  {"image": "images/ti3.png", "name": "PHOTO"},
];
List<Map> yourCourseList = [
  {
    "title": "UX Designer from Scratch.",
    "colorList": const [
      Color.fromRGBO(197, 4, 98, 1),
      Color.fromRGBO(80, 3, 112, 1),
    ],
    "image": "images/p1.png"
  },
  {
    "title": "Design Thinking The Beginner",
    "colorList": const [
      Color.fromRGBO(0, 77, 228, 1),
      Color.fromRGBO(1, 47, 135, 1),
    ],
    "image": "images/p2.png"
  },
];

class _UIDesign1State extends State {
  int pageIndex = -1;
  @override
  Widget build(BuildContext context) {
    if (pageIndex == -1) {
      return Scaffold(
        backgroundColor: Color.fromRGBO(234, 255, 255, 1).withOpacity(0.4),
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const SizedBox(
                height: 60,
              ),
              const Row(
                children: [
                  SizedBox(
                    width: 20,
                  ),
                  Icon(
                    Icons.menu_rounded,
                    size: 32,
                  ),
                  Spacer(),
                  Icon(
                    Icons.notifications_none_outlined,
                    size: 32,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                ],
              ),
              Container(
                padding: const EdgeInsets.only(left: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Welcome to New ",
                      style: GoogleFonts.jost(
                          fontSize: 26, fontWeight: FontWeight.w300),
                    ),
                    Text(
                      "Educourse",
                      style: GoogleFonts.jost(
                          fontSize: 37, fontWeight: FontWeight.w700),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Container(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: TextField(
                  readOnly: true,
                  decoration: InputDecoration(
                    filled: true,
                    fillColor: Colors.white,
                    suffixIcon: const Icon(
                      Icons.search_rounded,
                      size: 30,
                      color: Colors.black,
                    ),
                    hintText: "Enter your keyboard",
                    hintStyle: GoogleFonts.inter(
                        fontSize: 14, fontWeight: FontWeight.w400),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(28),
                        borderSide: const BorderSide(color: Colors.white)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(28),
                        borderSide: const BorderSide(color: Colors.white)),
                  ),
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  padding: const EdgeInsets.only(left: 20),
                  decoration: const BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 1),
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  margin: const EdgeInsets.only(top: 20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const SizedBox(
                        height: 1,
                      ),
                      Text(
                        "Course For You",
                        style: GoogleFonts.jost(
                            fontSize: 18, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                          height: 280,
                          width: double.infinity,
                          child: ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemCount: yourCourseList.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  pageIndex = index;
                                  setState(() {});
                                },
                                child: Container(
                                  height: 270,
                                  width: 220,
                                  margin: const EdgeInsets.symmetric(
                                      horizontal: 7.5),
                                  padding: const EdgeInsets.all(20),
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      gradient: LinearGradient(
                                          colors: yourCourseList[index]
                                              ["colorList"],
                                          begin: Alignment.topCenter,
                                          end: Alignment.bottomCenter,
                                          stops: const [0, 1])),
                                  child: Column(
                                    children: [
                                      Text(
                                        yourCourseList[index]["title"],
                                        style: GoogleFonts.jost(
                                            color: Colors.white,
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500),
                                      ),
                                      SizedBox(
                                        child: Image.asset(
                                          yourCourseList[index]["image"],
                                          height: 160,
                                          width: 160,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          )),
                      Text(
                        "Course By Category",
                        style: GoogleFonts.jost(
                            fontSize: 17, fontWeight: FontWeight.w500),
                      ),
                      SizedBox(
                        height: 70,
                        width: double.infinity,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: course.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              margin:
                                  const EdgeInsets.only(left: 20, right: 20),
                              height: 70,
                              width: 50,
                              child: Column(
                                children: [
                                  Container(
                                    height: 30,
                                    width: 30,
                                    decoration: BoxDecoration(
                                        color: Color.fromRGBO(25, 0, 56, 1),
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                    child: Image.asset(
                                      course[index]["image"],
                                      height: 20,
                                      width: 20,
                                    ),
                                  ),
                                  Text(
                                    course[index]["name"],
                                    style: GoogleFonts.jost(
                                        fontSize: 14,
                                        fontWeight: FontWeight.w400),
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    } else {
      return Scaffold(
          body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              width: double.infinity,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  gradient: LinearGradient(
                      colors: yourCourseList[pageIndex]["colorList"],
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      stops: const [0, 0.4])),
              padding: const EdgeInsets.only(top: 40),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      padding: const EdgeInsets.only(left: 20),
                      child: IconButton(
                        icon: const Icon(
                          Icons.arrow_back,
                          color: Colors.white,
                        ),
                        onPressed: () {
                          pageIndex = -1;
                          setState(() {});
                        },
                      )),
                  Container(
                    height: 260,
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 40),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Text(
                          yourCourseList[pageIndex]["title"],
                          style: GoogleFonts.jost(
                              color: Colors.white,
                              fontSize: 32,
                              fontWeight: FontWeight.w500),
                        ),
                        Text(
                          "Basic guideline & tips & tricks for how to become a UX designer easily.",
                          style: GoogleFonts.jost(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w400),
                        ),
                        Row(children: [
                          Container(
                              decoration: const BoxDecoration(
                                  color: Colors.blue, shape: BoxShape.circle),
                              child: const Icon(
                                Icons.account_circle_outlined,
                                color: Colors.white,
                                size: 34,
                                opticalSize: 1,
                              )),
                          Text(
                            " Auther:",
                            style: GoogleFonts.jost(
                              fontSize: 16,
                              color: Color.fromRGBO(190, 154, 197, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          Text(
                            " Jenny",
                            style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          const Spacer(),
                          Text(
                            "4.8",
                            style: GoogleFonts.jost(
                                fontSize: 16,
                                fontWeight: FontWeight.w500,
                                color: Colors.white),
                          ),
                          const Icon(Icons.star,
                              color: Color.fromRGBO(255, 146, 0, 1)),
                          Text(
                            "(20 review)",
                            style: GoogleFonts.jost(
                              fontSize: 16,
                              color: Color.fromRGBO(190, 154, 197, 1),
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ])
                      ],
                    ),
                  ),
                  Expanded(
                      child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(38),
                            topRight: Radius.circular(38))),
                    child: ListView.builder(
                      itemCount: 5,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          width: 300,
                          height: 70,
                          margin: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 10),
                          padding: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: const Color.fromRGBO(255, 255, 255, 1),
                              boxShadow: const [
                                BoxShadow(
                                    color: Color.fromRGBO(0, 0, 0, 0.15),
                                    offset: Offset(0, 8),
                                    blurRadius: 40)
                              ]),
                          child: Row(
                            children: [
                              Container(
                                height: 60,
                                width: 46,
                                decoration: BoxDecoration(
                                    color: Color.fromRGBO(230, 239, 239, 1),
                                    borderRadius: BorderRadius.circular(12)),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Text("Introduction",
                                      style: GoogleFonts.jost(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500)),
                                  Text("Lorem Ipsum is simply dummy text ... ",
                                      style: GoogleFonts.jost(
                                          color:
                                              Color.fromRGBO(143, 143, 143, 1),
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400)),
                                ],
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ))
                ],
              ),
            ),
          ),
        ],
      ));
    }
  }
}
