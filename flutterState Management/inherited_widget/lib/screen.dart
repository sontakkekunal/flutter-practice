import 'package:flutter/material.dart';
import 'package:inherited_widget/main.dart';

class Screen extends StatefulWidget {
  Screen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ScreenState();
  }
}

class _ScreenState extends State {
  void increment() {}

  @override
  Widget build(BuildContext context) {
    SharedDate sharedDateObj =
        context.getInheritedWidgetOfExactType<SharedDate>()!;
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Card(
            buttonName: 'Button1',
          ),
          const Display(),
          Container(
            height: 200,
            width: double.infinity,
            margin: const EdgeInsets.symmetric(horizontal: 20),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20), color: Colors.amber),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                ElevatedButton(
                    onPressed: () {
                      sharedDateObj.count++;
                    },
                    child: const Text('Button2')),
                Text('${sharedDateObj.count}')
              ],
            ),
          ),
          Container(
            height: 60,
            padding: const EdgeInsets.all(20),
            decoration: BoxDecoration(
                color: Colors.purple[200],
                borderRadius: BorderRadius.circular(20)),
            child: Text("${sharedDateObj.count}"),
          )
        ],
      ),
    );
  }
}

class Card extends StatefulWidget {
  String buttonName;
  Card({super.key, required this.buttonName});

  @override
  State<StatefulWidget> createState() {
    return _CardState();
  }
}

class _CardState extends State<Card> {
  @override
  Widget build(BuildContext context) {
    SharedDate sharedDateobj = SharedDate.of(context);
    return Container(
      height: 200,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.amber),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          ElevatedButton(
              onPressed: () {
                sharedDateobj.count++;
                sharedDateobj.updateShouldNotify(sharedDateobj);
              },
              child: Text(widget.buttonName)),
          Text('${sharedDateobj.count}')
        ],
      ),
    );
  }
}

class Display extends StatefulWidget {
  const Display({super.key});

  @override
  State<StatefulWidget> createState() {
    return _DisplayState();
  }
}

class _DisplayState extends State {
  @override
  Widget build(BuildContext context) {
    SharedDate sharedDateobj =
        context.getInheritedWidgetOfExactType<SharedDate>()!;
    return Container(
      height: 60,
      padding: const EdgeInsets.all(20),
      //alignment: Alignment.center,
      decoration: BoxDecoration(
          color: Colors.purple[200], borderRadius: BorderRadius.circular(20)),
      child: Text("${sharedDateobj.count}"),
    );
  }
}
