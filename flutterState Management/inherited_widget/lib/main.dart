import 'package:flutter/material.dart';
import 'package:inherited_widget/screen.dart';

void main() {
  runApp( MainApp());
}

class SharedDate extends InheritedWidget {
  int count;
  SharedDate({super.key, required this.count, required super.child});
  @override
  bool updateShouldNotify(SharedDate oldWidget) {
    
    if (oldWidget.count != count) {
      return true;
    } else {
      return false;
    }
  }

  static SharedDate of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SharedDate>()!;
  }
}

class MainApp extends StatelessWidget {
  MainApp({super.key});
  int count = 0;

  @override
  Widget build(BuildContext context) {
    return SharedDate(count: count, child:  MaterialApp(home: Screen()));
  }
}
