import 'package:flutter/material.dart';
import 'package:product_info/controller/product_controller.dart';
import 'package:product_info/model/product_model.dart';
import 'package:product_info/view/favrate_product.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class ShowList extends StatefulWidget {
  const ShowList({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ShowListState();
  }
}

class _ShowListState extends State {
  @override
  Widget build(BuildContext context) {
    List<ProductModel> productList = Provider.of<ProductController>(
      context,
    ).getProductList();
    log("In show_all_product build");
    return Scaffold(
      appBar: AppBar(
        title: const Text("All product List"),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(
                      builder: (context) => const FavProduct(),
                    ))
                    .then((value) =>
                        Provider.of<ProductController>(context, listen: false)
                            .refreash());
              },
              icon: const Icon(Icons.favorite))
        ],
      ),
      body: GridView.builder(
        itemCount: productList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 0.9,
            crossAxisCount: 2,
            mainAxisSpacing: 24,
            crossAxisSpacing: 24),
        itemBuilder: (context, index) {
          return Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.6),
                      spreadRadius: 2,
                      blurRadius: 8)
                ]),
            child: Column(
              children: [
                Image.network(
                  productList[index].imgUrl,
                  height: 150,
                  width: 200,
                ),
                Text(productList[index].productName),
                Text('price: ${productList[index].price}'),
                Text('Quantity: ${productList[index].quantity}'),
                Consumer(
                  builder: (context, value, child) {
                    log("In show_all_product consumer");
                    return GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .changeLikeByIndex(index);
                        },
                        child: productList[index].favFlag
                            ? const Icon(
                                Icons.favorite,
                                color: Colors.red,
                              )
                            : const Icon(Icons.favorite_border_rounded));
                  },
                )
              ],
            ),
          );
        },
      ),
    );
  }
}
