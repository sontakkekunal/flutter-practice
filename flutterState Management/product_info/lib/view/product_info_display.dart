import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:product_info/controller/product_controller.dart';
import 'package:product_info/model/product_model.dart';
import 'package:product_info/view/favrate_product.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class ProductInfoDisplay extends StatefulWidget {
  const ProductInfoDisplay({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ProductInfoDisplayState();
  }
}

class _ProductInfoDisplayState extends State {
  @override
  Widget build(BuildContext context) {
    log("ProductInfo Display build");
    ProductModel productObj =
        Provider.of<ProductController>(context).getProduct();
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(
                  builder: (context) => const FavProduct(),
                ))
                    .then((value) {
                  Provider.of<ProductController>(context, listen: false)
                      .refreash();
                });
              },
              icon: const Icon(Icons.favorite))
        ],
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Image.network(
              productObj.imgUrl,
              height: 200,
              width: 200,
            ),
            Text('Product name: ${productObj.productName}'),
            Text("Price: ${productObj.price}"),
            Consumer<ProductController>(
              builder: (context, value, child) {
                log("In consumer of product_info");
                return Row(
                  children: [
                    GestureDetector(
                        onTap: () {
                          Provider.of<ProductController>(context, listen: false)
                              .changeLike();
                        },
                        child: productObj.favFlag
                            ? const Icon(
                                Icons.favorite_rounded,
                                color: Colors.red,
                              )
                            : const Icon(Icons.favorite_border_rounded)),
                    const Spacer(),
                    Container(
                      height: 50,
                      width: 100,
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 211, 211, 211),
                          borderRadius: BorderRadius.horizontal(
                              left: Radius.circular(30),
                              right: Radius.circular(30))),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Provider.of<ProductController>(context,
                                      listen: false)
                                  .incrementQuantity();
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                              alignment: Alignment.center,
                              child: const Icon(Icons.add),
                            ),
                          ),
                          Text(
                              '${Provider.of<ProductController>(context).getProduct().quantity}'),
                          GestureDetector(
                            onTap: () {
                              Provider.of<ProductController>(context,
                                      listen: false)
                                  .deccrementQuantity();
                            },
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle, color: Colors.white),
                              alignment: Alignment.center,
                              child: Container(
                                color: Colors.black,
                                height: 2,
                                width: 16,
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                );
              },
            )
          ],
        ),
      ),
    );
  }
}
