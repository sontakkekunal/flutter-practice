import 'package:flutter/material.dart';
import 'package:product_info/controller/fav_product_controller.dart';
import 'package:product_info/model/product_model.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class FavProduct extends StatefulWidget {
  const FavProduct({super.key});

  @override
  State<StatefulWidget> createState() {
    return _FavProductState();
  }
}

class _FavProductState extends State {
  @override
  Widget build(BuildContext context) {
    Provider.of<FavProductController>(context, listen: false)
        .updateFavProduct(context);

    List<ProductModel> productList =
        Provider.of<FavProductController>(context).favProductList;
    log('In fav Product build');
    return Scaffold(
      appBar: AppBar(
        title: const Text("favrate product list"),
      ),
      body: GridView.builder(
        itemCount: productList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            childAspectRatio: 0.9,
            crossAxisCount: 2,
            mainAxisSpacing: 24,
            crossAxisSpacing: 24),
        itemBuilder: (context, index) {
          return Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.6),
                      spreadRadius: 2,
                      blurRadius: 8)
                ]),
            child: Column(
              children: [
                Image.network(
                  productList[index].imgUrl,
                  height: 150,
                  width: 200,
                ),
                Text(productList[index].productName),
                Text('price: ${productList[index].price}'),
                Text('Quantity: ${productList[index].quantity}'),
                GestureDetector(
                    onTap: () {
                      Provider.of<FavProductController>(context, listen: false)
                          .changeLikeByIndex(index);
                    },
                    child: productList[index].favFlag
                        ? const Icon(
                            Icons.favorite,
                            color: Colors.red,
                          )
                        : const Icon(Icons.favorite_border_rounded))
              ],
            ),
          );
        },
      ),
    );
  }
}
