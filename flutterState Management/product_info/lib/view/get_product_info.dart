import 'package:flutter/material.dart';
import 'package:product_info/controller/product_controller.dart';
import 'package:product_info/model/product_model.dart';
import 'package:product_info/view/product_info_display.dart';
import 'package:product_info/view/show_all_product.dart';
import 'package:provider/provider.dart';

class GetProductInfo extends StatefulWidget {
  const GetProductInfo({super.key});
  @override
  State createState() => _GetProductInfoState();
}

class _GetProductInfoState extends State {
  bool isNum(String str) {
    for (int i = 0; i < str.length; i++) {
      int ascii = str.codeUnitAt(i);
      if (ascii < 48 || ascii > 57) {
        return false;
      }
    }
    return true;
  }

  List<Map<String, dynamic>> inputFeild = [
    {
      'name': 'image url',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    },
    {
      'name': 'Product name',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    },
    {
      'name': 'Price',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              height: 300,
              width: 320,
              child: ListView.separated(
                itemCount: inputFeild.length,
                separatorBuilder: (context, index) => const SizedBox(
                  height: 20,
                ),
                itemBuilder: (context, index) {
                  return TextFormField(
                    key: inputFeild[index]['key'],
                    controller: inputFeild[index]['controller'],
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (index == 2 && !isNum(value!)) {
                        return 'please enter valid price';
                      } else if (index == 0 || index == 1) {
                        if (value == null || value.isEmpty) {
                          return 'Please enter vailid data';
                        }
                      }
                    },
                    decoration: InputDecoration(
                        hintText: inputFeild[index]['name'],
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                            borderSide: const BorderSide(width: 0.3))),
                  );
                },
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  bool valid = true;
                  for (int i = 0; i < inputFeild.length; i++) {
                    if (!inputFeild[i]['key'].currentState!.validate()) {
                      valid = false;
                    }
                  }
                  if (valid) {
                    ProductModel productObj = ProductModel(
                        imgUrl: inputFeild[0]['controller'].text,
                        productName: inputFeild[1]['controller'].text,
                        price: int.parse(inputFeild[2]['controller'].text));
                    Provider.of<ProductController>(context, listen: false)
                        .add(productObj);
                    Provider.of<ProductController>(context, listen: false)
                        .productIndex++;
                    for (int i = 0; i < inputFeild.length; i++) {
                      inputFeild[i]['controller'].clear();
                    }
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const ProductInfoDisplay(),
                    ));
                  }
                },
                child: const Text('Submit')),
            ElevatedButton(
                onPressed: () {
                  for (int i = 0; i < inputFeild.length; i++) {
                    inputFeild[i]['controller'].clear();
                  }
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const ShowList(),
                  ));
                },
                child: const Text("Show"))
          ],
        ),
      ),
    );
  }
}
