import 'package:flutter/material.dart';
import 'package:product_info/controller/fav_product_controller.dart';
import 'package:product_info/controller/product_controller.dart';
import 'package:product_info/view/get_product_info.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => ProductController()),
        ChangeNotifierProvider(create: (context) => FavProductController())
      ],
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: GetProductInfo(),
      ),
    );
  }
}
