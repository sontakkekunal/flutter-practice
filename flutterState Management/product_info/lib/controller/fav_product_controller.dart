import 'package:flutter/material.dart';
import 'package:product_info/controller/product_controller.dart';
import 'package:product_info/model/product_model.dart';
import 'package:provider/provider.dart';

class FavProductController extends ChangeNotifier {
  List<ProductModel> favProductList = [];
  void updateFavProduct(BuildContext context) {
    List<ProductModel> tempList =
        Provider.of<ProductController>(context, listen: false).getProductList();
    favProductList.clear();
    for (int i = 0; i < tempList.length; i++) {
      if (tempList[i].favFlag) {
        favProductList.add(tempList[i]);
      }
    }
  }

  void changeLikeByIndex(int index) {
    favProductList[index].favFlag = !favProductList[index].favFlag;
    favProductList.removeAt(index);
    notifyListeners();
  }
}
