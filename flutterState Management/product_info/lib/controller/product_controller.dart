import 'package:flutter/material.dart';
import 'package:product_info/model/product_model.dart';

class ProductController extends ChangeNotifier {
  int productIndex = -1;
  List<ProductModel> productList = [];
  void add(ProductModel productObj) {
    productList.add(productObj);
  }

  List<ProductModel> getProductList() {
    return productList;
  }

  ProductModel getProduct() {
    return productList[productIndex];
  }

  void changeLikeByIndex(int index) {
    productList[index].favFlag = !productList[index].favFlag;
    notifyListeners();
  }

  void changeLike() {
    productList[productIndex].favFlag = !productList[productIndex].favFlag;
    notifyListeners();
  }

  void refreash() {
    notifyListeners();
  }

  void incrementQuantity() {
    productList[productIndex].quantity++;
    notifyListeners();
  }

  void deccrementQuantity() {
    productList[productIndex].quantity--;
    notifyListeners();
  }
}
