int productIndex = -1;

class ProductModel {
  String imgUrl;
  String productName;
  int price;
  bool favFlag = false;
  int quantity = 0;
  ProductModel({
    required this.imgUrl,
    required this.productName,
    required this.price,
  });
}
