import 'package:emp_info/login_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MainApp());
}

class SharedData extends InheritedWidget {
  int id;
  String name;
  String username;
  List<String> skillList;
  SharedData({
    super.key,
    required this.id,
    required this.name,
    required this.username,
    required this.skillList,
    required super.child,
  });
  @override
  bool updateShouldNotify(covariant SharedData oldWidget) {
    return oldWidget.id != id ||
        oldWidget.name != name ||
        oldWidget.username != username;
  }

  static of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SharedData>()!;
  }
}

class MainApp extends StatelessWidget {
  MainApp({super.key});

  int id = 0;
  String name = 'jj';
  String username = 'wnc';
  @override
  Widget build(BuildContext context) {
    return SharedData(
        skillList: [],
        id: id,
        name: name,
        username: username,
        child: const MaterialApp(home: LoginScreen()));
  }
}
