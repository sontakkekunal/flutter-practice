import 'package:emp_info/emp_info_display.dart';
import 'package:emp_info/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LoginScreenState();
  }
}

class _LoginScreenState extends State {
  List<Map<String, dynamic>> loginInfo = [
    {
      'name': 'login id',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    },
    {
      'name': 'name',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    },
    {
      'name': 'username',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    }
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
                height: 300,
                width: double.infinity,
                child: ListView.separated(
                  itemCount: loginInfo.length,
                  separatorBuilder: (context, index) => const SizedBox(
                    height: 10,
                  ),
                  itemBuilder: (context, index) {
                    return TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'enter valid data';
                        }
                      },
                      controller: loginInfo[index]['controller'],
                      key: loginInfo[index]['key'],
                      decoration: InputDecoration(
                          hintText: loginInfo[index]['name'],
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10))),
                    );
                  },
                )),
            GestureDetector(
              onTap: () {
                bool b1 = loginInfo[0]['key'].currentState.validate();
                bool b2 = loginInfo[1]['key'].currentState.validate();
                bool b3 = loginInfo[2]['key'].currentState.validate();
                if (b1 && b2 && b3) {
                  SharedData obj =
                      context.dependOnInheritedWidgetOfExactType()!;
                  obj.id = int.parse(loginInfo[0]['controller'].text);
                  obj.name = loginInfo[1]['controller'].text;
                  obj.username = loginInfo[2]['controller'].text;
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const EmpInfo(),
                  ));
                }
              },
              child: Container(
                height: 40,
                width: 100,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.purple[200]),
                child: Text('Submit'),
              ),
            )
          ],
        ),
      ),
    );
  }
}
