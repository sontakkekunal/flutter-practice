import 'package:emp_info/main.dart';
import 'package:emp_info/skill.dart';
import 'package:flutter/material.dart';

class EmpInfo extends StatefulWidget {
  const EmpInfo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _EmpInfoState();
  }
}

class _EmpInfoState extends State {
  @override
  Widget build(BuildContext context) {
    SharedData obj = context.dependOnInheritedWidgetOfExactType()!;
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 200),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text('Emp id: ${obj.id}'),
              Text('Emp name: ${obj.name}'),
              Text('Emp username: ${obj.username}'),
              GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => const Skill(),
                  ));
                },
                child: Container(
                  height: 50,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.purple[200]),
                  alignment: Alignment.center,
                  child: const Text('add skill'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
