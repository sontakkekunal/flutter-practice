import 'package:emp_info/main.dart';
import 'package:flutter/material.dart';

class Skill extends StatefulWidget {
  const Skill({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SkillState();
  }
}

class _SkillState extends State {
  final TextEditingController _controller = TextEditingController();
  final GlobalKey<FormFieldState> key = GlobalKey<FormFieldState>();
  @override
  Widget build(BuildContext context) {
    List<String> skillList = SharedData.of(context).skillList;
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            TextFormField(
              controller: _controller,
              key: key,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'enter valid data';
                }
                // return null;
              },
              decoration: InputDecoration(
                  hintText: 'enter skill',
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10))),
            ),
            const SizedBox(
              height: 50,
            ),
            GestureDetector(
              onTap: () {
                if (key.currentState!.validate()) {
                  skillList.add(_controller.text);
                  _controller.clear();
                  setState(() {});
                }
              },
              child: Container(
                height: 40,
                width: 100,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.amber[400]),
                child: Text('add skill'),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Expanded(
                child: ListView.separated(
              itemCount: skillList.length,
              separatorBuilder: (context, index) => const SizedBox(
                height: 20,
              ),
              itemBuilder: (context, index) {
                return Container(
                  height: 50,
                  width: double.infinity,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.orange[300]),
                  child: Text(skillList[index]),
                );
              },
            ))
          ],
        ),
      ),
    );
  }
}
