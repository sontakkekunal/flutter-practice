import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class Login1 extends ChangeNotifier {
  String username;
  String pass;
  Login1({required this.username, required this.pass});
  void changePass(String pass) {
    this.pass = pass;
    notifyListeners();
  }
}

class Employee1 extends ChangeNotifier {
  String username;
  String pass;
  String name;
  int id;
  Employee1(
      {required this.username,
      required this.id,
      required this.name,
      required this.pass});
}

class ChangeNotifierProxyproviderDemo extends StatelessWidget {
  const ChangeNotifierProxyproviderDemo({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) {
          log('changenotifierprovider Login create');
          return Login1(username: 'kunal2004', pass: '12334444');
        }),
        ChangeNotifierProxyProvider<Login1, Employee1>(
          create: (context) {
            log('changenotifierproxyprovider create employee');
            return Employee1(
                username: Provider.of<Login1>(context, listen: false).username,
                id: 15,
                name: 'Kunal',
                pass: Provider.of<Login1>(context, listen: false).pass);
          },
          update: (context, login1, emp1) {
            log('changenotifierproxyprovider update employee');
            return Employee1(
                username: login1.username,
                id: emp1!.id,
                name: 'Kunal',
                pass: Provider.of<Login1>(context, listen: false).pass);
          },
        )
      ],
      child: const MaterialApp(
        home: InfoDisplay(),
      ),
    );
  }
}

class InfoDisplay extends StatefulWidget {
  const InfoDisplay({super.key});

  @override
  State<StatefulWidget> createState() {
    return _InfoDisplayState();
  }
}

class _InfoDisplayState extends State {
  @override
  Widget build(BuildContext context) {
    log("In build");
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('${Provider.of<Employee1>(context).id}'),
            Text(Provider.of<Employee1>(context).name),
            Text(Provider.of<Employee1>(context).username),
            Text(Provider.of<Employee1>(context).pass),
            ElevatedButton(
                onPressed: () {
                  Provider.of<Login1>(context, listen: false).changePass('jam');
                },
                child: const Text('Change pass'))
          ],
        ),
      ),
    );
  }
}
