import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:proxyprovider_demo/changenotifier_proxyprovider.dart';

void main() {
  //runApp(const MyApp());
  runApp(const ChangeNotifierProxyproviderDemo());
}

// class Login {
//   String username;
//   String pass;
//   Login({required this.username, required this.pass});
// }

// class Employee {
//   String username;
//   String pass;
//   String name;
//   int id;
//   Employee(
//       {required this.username,
//       required this.id,
//       required this.name,
//       required this.pass});
// }

// class MyApp extends StatelessWidget {
//   const MyApp({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return MultiProvider(
//       providers: [
//         Provider(
//           create: (context) {
//             log('In create parameter of provider');
//             return Login(username: 'kunal2004', pass: '1234');
//           },
//         ),
//         ProxyProvider<Login, Employee>(
//           create: (context) {
//             log("In create parameter of proxyProvider");
//             return Employee(
//                 username: Provider.of<Login>(context, listen: false).username,
//                 id: 15,
//                 name: 'Kunal',
//                 pass: Provider.of<Login>(context, listen: false).pass);
//           },
//           update: (context, login, previous) {
//             log("In update parameter of proxyProvider");
//             return Employee(
//                 username: Provider.of<Login>(context).username,
//                 id: 7,
//                 name: 'Kunal',
//                 pass: login.pass);
//           },
//         )
//       ],
//       child: const MaterialApp(
//         home: InfoDisplay(),
//       ),
//     );
//   }
// }

// class InfoDisplay extends StatefulWidget {
//   const InfoDisplay({super.key});

//   @override
//   State<StatefulWidget> createState() {
//     return _InfoDisplayState();
//   }
// }

// class _InfoDisplayState extends State {
//   @override
//   Widget build(BuildContext context) {
//     log("In build");
//     return Scaffold(
//       body: SizedBox(
//         height: double.infinity,
//         width: double.infinity,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.spaceAround,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             Text('${Provider.of<Employee>(context).id}'),
//             Text(Provider.of<Employee>(context).name),
//             Text(Provider.of<Employee>(context).username),
//             Text(Provider.of<Employee>(context).pass)
//           ],
//         ),
//       ),
//     );
//   }
// }
