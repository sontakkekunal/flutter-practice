import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log('MainApp build');
    return MultiProvider(
      providers: [
        Provider(create: (context) {
          return Player(playerName: 'Virat', jerNo: 18);
        }),
        ChangeNotifierProvider(
            create: (context) => MatchInfo(runs: 1000, matchPlayed: 150))
      ],
      child: const MaterialApp(
        home: ConsumerDemo(),
      ),
    );
  }
}

class ConsumerDemo extends StatefulWidget {
  const ConsumerDemo({super.key});
  @override
  State createState() => _ConsumerDemoState();
}

class _ConsumerDemoState extends State {
  @override
  Widget build(BuildContext context) {
    log("ConsumerDemo build");
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('playerName: ${Provider.of<Player>(context).playerName}'),
            Text('jerNo:  ${Provider.of<Player>(context).jerNo} '),
            Consumer(
              builder: (context, value, child) {
                log('Call to consumer');
                return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Text('runs: ${Provider.of<MatchInfo>(context).runs}'),
                    Text(
                        'matchPlayed: ${Provider.of<MatchInfo>(context).matchPlayed}'),
                  ],
                );
              },
            ),
            _DisplayInfo(),
            ElevatedButton(
                onPressed: () {
                  Provider.of<MatchInfo>(context, listen: false)
                      .changeData(runs: 1500, matchPlayed: 400);
                },
                child: const Text("change data"))
          ],
        ),
      ),
    );
  }
}

class _DisplayInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    log("In display build");
    return Text("data: ${Provider.of<MatchInfo>(context).matchPlayed}");
  }
}

class Player {
  String playerName;
  int jerNo;
  Player({required this.playerName, required this.jerNo});
}

class MatchInfo extends ChangeNotifier {
  int runs;
  int matchPlayed;
  MatchInfo({required this.runs, required this.matchPlayed});
  void changeData({required int runs, required int matchPlayed}) {
    this.matchPlayed = matchPlayed;
    this.runs = runs;
    notifyListeners();
  }
}
