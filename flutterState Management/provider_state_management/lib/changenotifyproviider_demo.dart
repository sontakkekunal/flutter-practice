import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Company extends ChangeNotifier {
  String compName;
  int empCount;
  Company({required this.compName, required this.empCount});
  void makeChange(String compName, int empCount) {
    this.compName = compName;
    this.empCount = empCount;
    notifyListeners();
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) {
        return Company(compName: 'Google', empCount: 500);
      },
      child: const MaterialApp(
        home: ChangeNotifyProviderDemo(),
      ),
    );
  }
}

class ChangeNotifyProviderDemo extends StatefulWidget {
  const ChangeNotifyProviderDemo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ChangeNotifyProviderDemoState();
  }
}

class _ChangeNotifyProviderDemoState extends State {
  @override
  Widget build(BuildContext context) {
    log('In Main App build');
    Company obj = Provider.of<Company>(context, listen: false);
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(obj.compName),
            const SizedBox(
              height: 20,
            ),
            Text("${Provider.of<Company>(context).empCount}"),
            const SizedBox(
              height: 20,
            ),
            const Display(),
            const SizedBox(
              height: 20,
            ),
            const Display2(),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  obj.makeChange('Microsoft', 1000);
                },
                child: const Text('Change'))
          ],
        ),
      ),
    );
  }
}

class Display extends StatelessWidget {
  const Display({super.key});

  @override
  Widget build(BuildContext context) {
    log('In Display App build');
    return Text('${Provider.of<Company>(context).empCount}');
    //return const Text('Nothing');
  }
}

class Display2 extends StatefulWidget {
  const Display2({super.key});

  @override
  State<StatefulWidget> createState() {
    return _DisplayState();
  }
}

class _DisplayState extends State{
  @override
  Widget build(BuildContext context) {
    log('In Display2 App build');
    return Text(Provider.of<Company>(context).compName);
    //return const Text('Nothing');
  }
}
