import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log('In Main build');
    return Provider(
      create: (context) {
        return Company(compName: 'Google', empCount: 500);
      },
      child: const MaterialApp(home: ProviderDemo()),
    );
  }
}

class Company {
  String compName;
  int empCount;
  Company({required this.compName, required this.empCount});
  // void makeChange(String compName, int empCount) {
  //   this.compName = compName;
  //   this.empCount = empCount;
  //   notifyListeners();
  // }
}

class ProviderDemo extends StatefulWidget {
  const ProviderDemo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _ProviderDemoState();
  }
}

class _ProviderDemoState extends State<ProviderDemo> {
  @override
  Widget build(BuildContext context) {
    log("In provider demo build");
    Company companyObj = Provider.of(context);
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Company name: ${companyObj.compName}'),
            const SizedBox(
              height: 20,
            ),
            Text("empCount: ${companyObj.empCount}"),
            const SizedBox(
              height: 20,
            ),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  // companyObj.empCount = 1000;
                  // companyObj.compName = "Microsoft";
                  // setState(() {});
                },
                child: const Text('Change'))
          ],
        ),
      ),
    );
  }
}
