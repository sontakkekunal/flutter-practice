import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class EmpInfo {
  String empName;
  int empId;
  EmpInfo({required this.empName, required this.empId});
}

class ProjectInfo with ChangeNotifier {
  String projectName;
  String devType;
  ProjectInfo({required this.projectName, required this.devType});
  void changeProjectInfo(String projectName, String devType) {
    this.projectName = projectName;
    this.devType = devType;
    notifyListeners();
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider(create: (context) => EmpInfo(empName: 'Kunal', empId: 7)),
        ChangeNotifierProvider(
          create: (context) =>
              ProjectInfo(devType: 'Backend', projectName: 'EduServer'),
        )
      ],
      child: const MaterialApp(home: MutltiProviderDemo()),
    );
  }
}

class MutltiProviderDemo extends StatefulWidget {
  const MutltiProviderDemo({super.key});
  @override
  State createState() => _MutltiProviderDemoState();
}

class _MutltiProviderDemoState extends State {
  @override
  Widget build(BuildContext context) {
    log("Build call");
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text("EmpName: ${Provider.of<EmpInfo>(context).empName}"),
            Text('EmpId:  ${Provider.of<EmpInfo>(context).empId}'),
            Text(
                'ProjectName: ${Provider.of<ProjectInfo>(context).projectName}'),
            Text('DevType: ${Provider.of<ProjectInfo>(context).devType}'),
            //Text('Object: ${Provider.of(context).devType}'),
            ElevatedButton(
                onPressed: () {
                  EmpInfo obj = Provider.of<EmpInfo>(context, listen: false);
                  Provider.of<ProjectInfo>(context, listen: false)
                      .changeProjectInfo('EduApp', 'FrontEnd');
                },
                child: const Text("Change project"))
          ],
        ),
      ),
    );
  }
}
