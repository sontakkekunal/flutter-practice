import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    log("MainApp build");
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
            create: (context) =>
                StudentEdu(degree: 'Engineering', passYear: 2025)),
        ChangeNotifierProvider(
          create: (context) => StudentVehicle(bike: 'CD delux', car: 'no car'),
        )
      ],
      child: const MaterialApp(home: MutiChangeProvider()),
    );
  }
}

class MutiChangeProvider extends StatefulWidget {
  const MutiChangeProvider({super.key});

  @override
  State<StatefulWidget> createState() {
    return _MutiChangeProviderState();
  }
}

class _MutiChangeProviderState extends State {
  @override
  Widget build(BuildContext context) {
    log('MutilChangeApp build');
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Consumer2<StudentEdu, StudentVehicle>(
              builder:
                  (context, StudentEdu value, StudentVehicle value2, child) {
                log('In consumer 2');
                return Column(
                  children: [
                    Text('degree: ${value.degree}'),
                    Text('passyear: ${value.passYear}'),
                    Text('bike: ${value2.bike}'),
                    Text('car: ${value2.car}')
                  ],
                );
              },
            ),
            ElevatedButton(
                onPressed: () {
                  Provider.of<StudentEdu>(context, listen: false)
                      .changeData(degree: 'ME', passYear: 2027);
                },
                child: const Text(
                  'change student info',
                )),
            ElevatedButton(
                onPressed: () {
                  Provider.of<StudentVehicle>(context, listen: false)
                      .changeData(bike: 'Unicon', car: 'harier');
                },
                child: const Text('change vehicle info'))
          ],
        ),
      ),
    );
  }
}

class StudentVehicle extends ChangeNotifier {
  String bike;
  String car;
  StudentVehicle({required this.bike, required this.car});
  void changeData({required String bike, required String car}) {
    this.bike = bike;
    this.car = car;
    notifyListeners();
  }
}

class StudentEdu extends ChangeNotifier {
  String degree;
  int passYear;
  StudentEdu({required this.degree, required this.passYear});
  void changeData({required String degree, required int passYear}) {
    this.degree = degree;
    this.passYear = passYear;
    notifyListeners();
  }
}
