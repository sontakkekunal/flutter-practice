import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MainApp extends StatefulWidget {
  const MainApp({super.key});

  @override
  State<StatefulWidget> createState() {
    return _MainAppState();
  }
}

class _MainAppState extends State {
  @override
  Widget build(BuildContext context) {
    log('In Main build');
    //Company companyObj = Provider.of(context);
    return Provider(
      create: (context) {
        log('In create of privoder ');
        return Company(compName: 'Google', empCount: 500);
      },
      child: MaterialApp(
          home: Scaffold(
        body: SizedBox(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              //Text('Company name: ${Provider.of<Company>(context).compName}'),
              const SizedBox(
                height: 20,
              ),
              //Text("empCount: ${Provider.of(context).empCount}"),
              const SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () {
                    log("${Provider.of<Company>(context, listen: false)}");
                    setState(() {});
                  },
                  child: Text('Change'))
            ],
          ),
        ),
      )),
    );
  }
}

class Company {
  String compName;
  int empCount;
  Company({required this.compName, required this.empCount}) {
    log('In constructor of Company');
    print('jj');
  }
}
