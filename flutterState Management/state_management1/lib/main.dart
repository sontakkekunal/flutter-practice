import 'package:flutter/material.dart';
import 'package:state_management1/first_page.dart';
import 'package:state_management1/second_page.dart';
import 'package:state_management1/specefic_setState.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false,
        //home: First()
        home: LangCount()
        );
  }
}
