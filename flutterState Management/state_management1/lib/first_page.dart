import 'package:flutter/material.dart';

class First extends StatefulWidget {
  const First({super.key});

  @override
  State<StatefulWidget> createState() {
    return _FirstState();
  }
}

class _FirstState extends State {
  int _javaCount = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          //mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
              onTap: () {
                _javaCount++;
                setState(() {});
              },
              child: Container(
                height: 40,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15),
                  color: const Color.fromARGB(255, 213, 166, 162),
                ),
                child: const Center(child: Text('Java')),
              ),
            ),
            Text('Java count is: $_javaCount')
          ],
        ),
      ),
    );
  }
}
