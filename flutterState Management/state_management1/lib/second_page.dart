import 'package:flutter/material.dart';

class Second extends StatefulWidget {
  const Second({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SecondState();
  }
}

class _SecondState extends State {
  int _javaCount = 0;
  int _pyCount = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            //mainAxisSize: MainAxisSize.min,
            children: [
              GestureDetector(
                onTap: () {
                  _javaCount++;
                  setState(() {});
                },
                child: Container(
                  height: 40,
                  width: 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: const Color.fromARGB(255, 213, 166, 162),
                  ),
                  child: const Center(child: Text('Java')),
                ),
              ),
              Text('Java count is: $_javaCount')
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            //mainAxisSize: MainAxisSize.min,
            children: [
              GestureDetector(
                onTap: () {
                  _pyCount++;
                  setState(() {});
                },
                child: Container(
                  height: 40,
                  width: 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: const Color.fromARGB(255, 134, 213, 223),
                  ),
                  child: const Center(child: Text('Python')),
                ),
              ),
              Text('Python count is: $_pyCount')
            ],
          ),
        ],
      ),
    );
  }
}
