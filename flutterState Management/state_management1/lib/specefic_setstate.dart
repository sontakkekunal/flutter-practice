import 'package:flutter/material.dart';
import 'dart:developer';

class LangCount extends StatefulWidget {
  const LangCount({super.key});
  @override
  State createState() {
    log('In createState main');
    return _LangCountState();
  }
}

class _LangCountState extends State {
  @override
  Widget build(BuildContext context) {
    log("In setState main");
    return Scaffold(
        body: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        LangInfo(
          langName: 'Java',
        ),
        const SizedBox(
          height: 20,
        ),
        LangInfo(langName: "Dart"),
        const SizedBox(
          height: 20,
        ),
        ElevatedButton(
            onPressed: () {
              setState(() {});
            },
            child: const Text('call to setState'))
      ],
    ));
  }
}

class LangInfo extends StatefulWidget {
  String langName;
  LangInfo({super.key, required this.langName});
  @override
  State<StatefulWidget> createState() {
    log('In subState createState ');
    return _LangInfoState();
  }
}

class _LangInfoState extends State<LangInfo> {
  int count = 0;
  @override
  Widget build(BuildContext context) {
    log("In substate of setsate");
    return Container(
      height: 175,
      width: double.infinity,
      margin: const EdgeInsets.symmetric(horizontal: 20),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20), color: Colors.amber),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          GestureDetector(
              onTap: () {
                setState(() {
                  count++;
                });
              },
              child: Text(widget.langName)),
          Text("$count")
        ],
      ),
    );
  }
}
