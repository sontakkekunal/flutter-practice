import 'package:flutter/material.dart';
import 'package:inherited_widget_ex/team_info.dart';
import 'package:inherited_widget_ex/teamchange_screen.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatefulWidget {
  const MainApp({super.key});
  @override
  State createState() => MainAppState();
}

class MainAppState extends State {
  static String countryName = '';
  static String playerName = '';
  static String iplTeamName = '';

  @override
  Widget build(BuildContext context) {
    return PlayerInfo(
        countryName: countryName,
        playerName: playerName,
        iplTeamName: iplTeamName,
        child: const MaterialApp(home: TeamInfo()));
  }
}

class PlayerInfo extends InheritedWidget {
  String countryName;
  String playerName;
  String iplTeamName;
  PlayerInfo(
      {super.key,
      required this.countryName,
      required this.playerName,
      required this.iplTeamName,
      required super.child});
  @override
  bool updateShouldNotify(PlayerInfo oldWidget) {
    return oldWidget.iplTeamName != iplTeamName;
  }

  static PlayerInfo of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<PlayerInfo>()!;
  }
}
