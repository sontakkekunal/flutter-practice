import 'package:flutter/material.dart';
import 'package:inherited_widget_ex/main.dart';

class PlayerDisp extends StatefulWidget {
  const PlayerDisp({super.key});

  @override
  State<StatefulWidget> createState() {
    return _PlayerDispState();
  }
}

class _PlayerDispState extends State {
  int i = -1;
  List<String> iplTeamList = ['MI', 'CSK', 'RCB', 'Gujarat'];
  @override
  Widget build(BuildContext context) {
    PlayerInfo obj = PlayerInfo.of(context);
    return Scaffold(
      body: SizedBox(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Country Name: ${obj.countryName}'),
            const SizedBox(
              height: 20,
            ),
            Text('PlayerName: ${obj.playerName}'),
            const SizedBox(
              height: 20,
            ),
            Text('IPL team name: ${obj.iplTeamName}'),
            const SizedBox(
              height: 20,
            ),
            ElevatedButton(
                onPressed: () {
                  i++;
                  obj.iplTeamName = iplTeamList[i % iplTeamList.length];
                  setState(() {});
                },
                child: Text('Change team'))
          ],
        ),
      ),
    );
  }
}
