import 'package:flutter/material.dart';
import 'package:inherited_widget_ex/main.dart';
import 'package:inherited_widget_ex/teamchange_screen.dart';

class TeamInfo extends StatefulWidget {
  const TeamInfo({super.key});

  @override
  State<StatefulWidget> createState() {
    return _TeamInfoState();
  }
}

class _TeamInfoState extends State {
  List<Map<String, dynamic>> inputList = [
    {
      'name': 'Enter country',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    },
    {
      'name': 'Enter Player name',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    },
    {
      'name': 'Enter Ipl team',
      'controller': TextEditingController(),
      'key': GlobalKey<FormFieldState>()
    }
  ];
  @override
  Widget build(BuildContext context) {
    PlayerInfo obj = PlayerInfo.of(context);
    return Scaffold(
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 300,
              width: 350,
              child: ListView.separated(
                itemCount: inputList.length,
                separatorBuilder: (context, index) => const SizedBox(
                  height: 20,
                ),
                itemBuilder: (context, index) {
                  return TextFormField(
                    key: inputList[index]['key'],
                    controller: inputList[index]['controller'],
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'enter valid data';
                      }
                    },
                    decoration: InputDecoration(
                        hintText: inputList[index]['name'],
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  );
                },
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  bool valid = true;
                  for (int i = 0; i < inputList.length; i++) {
                    if (!inputList[i]['key'].currentState!.validate()) {
                      valid = false;
                    }
                  }
                  if (valid) {
                    obj.countryName = inputList[0]['controller'].text;
                    obj.playerName = inputList[1]['controller'].text;
                    obj.iplTeamName = inputList[2]['controller'].text;
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => const PlayerDisp(),
                    ));
                  }
                },
                child: Text('Next'))
          ],
        ),
      ),
    );
  }
}
