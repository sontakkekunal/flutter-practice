import 'package:flutter/material.dart';

class Trianga extends StatefulWidget {
  const Trianga({super.key});

  @override
  State<StatefulWidget> createState() => _Trianga();
}

class _Trianga extends State<Trianga> {
  bool pipe = false;
  bool orange = false;
  bool white = false;
  bool akshokchakara = false;
  bool green = false;
  int count = 0;
  click() {
    count++;
    if (count == 1) {
      setState(() {
        pipe = true;
      });
    }
    if (count == 2) {
      setState(() {
        orange = true;
      });
    }
    if (count == 3) {
      setState(() {
        white = true;
      });
    }
    if (count == 4) {
      setState(() {
        akshokchakara = true;
      });
    }
    if (count == 5) {
      setState(() {
        green = true;
      });
    }
    
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("Tiranga StatefullWidget")),
      body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              pipe
                  ? Container(
                      height: 600,
                      width: 10,
                      color: Colors.black,
                    )
                  : Container(),
              Column(
                children: [
                  orange
                      ? Container(
                          height: 100,
                          width: 300,
                          color: Colors.orange,
                        )
                      : Container(),
                  white
                      ? Container(
                          height: 100,
                          width: 300,
                          color: Colors.white,
                          margin: const EdgeInsets.only(top:6,bottom: 6),
                          child: akshokchakara
                              ? Image.network(
                                  "https://t3.ftcdn.net/jpg/03/11/13/46/360_F_311134651_RXMvbUB3h089Js0ODvuHrttmsON9Tpik.jpg")
                              : null)
                      : Container(),
                  green
                      ? Container(
                          height: 100,
                          width: 300,
                          color: Colors.green,
                        )
                      : Container()
                ],
              ),
            ],
          )
          ),
      floatingActionButton: FloatingActionButton(
        onPressed: click,
        child: const Icon(Icons.add),
      ),
    );
  }
}
