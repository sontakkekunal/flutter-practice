import 'package:flutter/material.dart';

class Tiranga extends StatefulWidget {
  const Tiranga({super.key});
  @override
  State<StatefulWidget> createState() => _Tiranga();
}

class _Tiranga extends State<Tiranga> {
  int count = 0;
  click() {
    setState(() {
      count++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey,
      appBar: AppBar(
        title: const Text("Tiranga"),
      ),
      body: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
        children: [
          (count >= 1)
              ? Container(
                  height: 600,
                  width: 10,
                  color: Colors.black,
                )
              : Container(),
          Column(
            children: [
              (count >= 2)
                  ? Container(
                      height: 100,
                      width: 400,
                      color: Colors.orange,
                    )
                  : Container(),
              (count >= 3)
                  ? Container(
                      height: 100,
                      width: 400,
                      color: Colors.white,
                      child: (count >= 4)
                          ? Image.network(
                              "https://t3.ftcdn.net/jpg/03/11/13/46/360_F_311134651_RXMvbUB3h089Js0ODvuHrttmsON9Tpik.jpg")
                          : null,
                    )
                  : Container(),
              (count >= 5)
                  ? Container(
                      height: 100,
                      width: 400,
                      color: Colors.green,
                    )
                  : Container(),
            ],
          )
        ],
      ),
      
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: click,
        child: const Text("Add"),
      ),
    );
  }
}
