import 'package:flutter/material.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('Sliding Up Panel with Customizations'),
        ),
        body: SlidingUpPanel(
          slideDirection: SlideDirection.UP,
          collapsed: Container(
            decoration: BoxDecoration(
              color: Colors.blueGrey,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(24.0),
                  topRight: Radius.circular(24.0)),
            ),
            child: Center(child: Text('Drag to open panel')),
          ),
          panel: Center(child: Text('This is the sliding panel')),
          body: Center(child: Text('This is the main body')),
          backdropEnabled: true,
          backdropOpacity: 0.5,
        ),
      ),
    );
  }
}
