import 'package:flutter/material.dart';

class Q5 extends StatefulWidget {
  const Q5({super.key});

  @override
  State createState() => _Q5();
}

class _Q5 extends State {
  bool userNameValidation(String val) {
    if (val.length < 6 || val.length > 20) {
      return false;
    }
    return true;
  }

  bool passValidation(String val) {
    bool flag = false;
    for (int i = 0; i < val.length; i++) {
      int num = val.codeUnitAt(i);
      if (num >= 97 && num <= 122) {
        flag = true;
      }
    }
    if (!flag) {
      return false;
    }
    flag = false;

    for (int i = 0; i < val.length; i++) {
      int num = val.codeUnitAt(i);
      if (num >= 65 && num <= 90) {
        flag = true;
      }
    }
    if (!flag) {
      return false;
    }
    flag = false;
    if (val.contains("@") ||
        val.contains("!") ||
        val.contains("#") ||
        val.contains("\$") ||
        val.contains("%") ||
        val.contains("^") ||
        val.contains("&") ||
        val.contains("*")) {
      flag = true;
    }
    if (!flag) {
      return false;
    } else {
      return true;
    }
  }

  List<Map> list = [
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "userinfo"
    },
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "password"
    }
  ];
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 400,
              width: 300,
              child: ListView.builder(
                itemCount: 2,
                itemBuilder: (context, index) {
                  return TextFormField(
                    key: list[index]["key"],
                    controller: list[index]["controller"],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return "please enter valid data";
                      } else if (index == 0 && !userNameValidation(value)) {
                        return "Enter valid username";
                      } else if (index == 1 && value.length < 8) {
                        return "at minimum 8 length required";
                      } else if (index == 1 && !passValidation(value)) {
                        return "password must contain atleaast 1 captial and 1 small charcater and speical charceter";
                      }
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                        labelText: list[index]["name"],
                        hintText: list[index]["name"]),
                  );
                },
              ),
            ),
            GestureDetector(
              child: Container(
                height: 20,
                width: 20,
                child: Text("submit"),
              ),
              onTap: () {
                list[0]["key"].currentState!.validate();
                list[1]["key"].currentState!.validate();
              },
            ),
          ],
        ),
      ),
    );
  }
}
