import 'package:day13/q1.dart';
import 'package:day13/q4.dart';
import 'package:day13/q5.dart';
import 'package:flutter/material.dart';

import 'q2.dart';
import 'q3.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Q5(),
    );
  }
}
