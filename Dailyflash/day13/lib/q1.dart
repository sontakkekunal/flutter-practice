import 'package:flutter/material.dart';

class Q1 extends StatefulWidget {
  const Q1({super.key});

  @override
  State createState() => _Q1();
}

class _Q1 extends State {
  List<Map> list = [
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "userinfo"
    },
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "password"
    }
  ];
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 400,
              width: 300,
              child: ListView.builder(
                itemCount: 2,
                itemBuilder: (context, index) {
                  return TextFormField(
                    key: list[index]["key"],
                    controller: list[index]["controller"],
                    validator: (String? value) {
                      if (value == null || value.isEmpty) {
                        return "please enter valid data";
                      }
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                        labelText: list[index]["name"],
                        hintText: list[index]["name"]),
                  );
                },
              ),
            ),
            GestureDetector(
              child: Container(
                height: 20,
                width: 20,
                child: Text("submit"),
              ),
              onTap: (){
                list[0]["key"].currentState!.validate();
                list[1]["key"].currentState!.validate();
              },
              ),
          ],
        ),
      ),
    );
  }
}
