import 'dart:convert';

import 'package:flutter/material.dart';

class Q3 extends StatefulWidget {
  const Q3({super.key});

  @override
  State createState() => _Q3();
}

class _Q3 extends State {
  List<Map> list = [
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "email"
    },
  ];
  bool checkEmail(String s) {
    if (s.contains(" ")) {
      return false;
    }
    String str = "@gmail.com";
    int j = str.length - 1;
    for (int i = s.length - 1; j >= 0 && i >= 0; i--) {
      if (s.codeUnitAt(i) != str.codeUnitAt(j)) {
        return false;
      }
      j--;
    }
    if (str.length == s.length) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 400,
              width: 300,
              child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context, index) {
                  return TextFormField(
                    keyboardType: TextInputType.emailAddress,
                    key: list[index]["key"],
                    controller: list[index]["controller"],
                    validator: (String? value) {
                      if ((value == null || value.isEmpty) ||
                          !checkEmail(value)) {
                        return "please enter valid data";
                      }
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                        labelText: list[index]["name"],
                        hintText: list[index]["name"]),
                  );
                },
              ),
            ),
            GestureDetector(
              child: Container(
                height: 20,
                width: 20,
                child: Text("submit"),
              ),
              onTap: () {
                list[0]["key"].currentState!.validate();
              },
            ),
          ],
        ),
      ),
    );
  }
}
