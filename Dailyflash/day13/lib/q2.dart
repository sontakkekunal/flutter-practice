import 'dart:convert';

import 'package:flutter/material.dart';

class Q2 extends StatefulWidget {
  const Q2({super.key});

  @override
  State createState() => _Q2();
}

class _Q2 extends State {
  List<Map> list = [
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "phoneNumber"
    },
  ];
  bool checkNum(String s) {
    if (s.length != 10) {
      return false;
    }
    for (int i = 0; i < s.length; i++) {
      int ascii = s.codeUnitAt(i);
      if (ascii < 48 || ascii > 57) {
        return false;
      }
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 400,
              width: 300,
              child: ListView.builder(
                itemCount: 1,
                itemBuilder: (context, index) {
                  return TextFormField(
                    keyboardType: TextInputType.phone,
                    maxLength: 10,
                    key: list[index]["key"],
                    controller: list[index]["controller"],
                    validator: (String? value) {
                      if ((value == null || value.isEmpty) ||
                          !checkNum(value)) {
                        return "please enter valid data";
                      }
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                        labelText: list[index]["name"],
                        hintText: list[index]["name"]),
                  );
                },
              ),
            ),
            GestureDetector(
              child: Container(
                height: 20,
                width: 20,
                child: Text("submit"),
              ),
              onTap: () {
                list[0]["key"].currentState!.validate();
              },
            ),
          ],
        ),
      ),
    );
  }
}
