import 'package:flutter/material.dart';

class Q4 extends StatefulWidget {
  const Q4({super.key});

  @override
  State createState() => _Q4();
}

class _Q4 extends State {
  List<Map> list = [
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "email"
    },
    {
      "key": GlobalKey<FormFieldState>(),
      "controller": TextEditingController(),
      "name": "phone"
    }
  ];
  bool checkEmail(String s) {
    if (s.contains(" ")) {
      return false;
    }
    String str = "@gmail.com";
    int j = str.length - 1;
    for (int i = s.length - 1; j >= 0 && i >= 0; i--) {
      if (s.codeUnitAt(i) != str.codeUnitAt(j)) {
        return false;
      }
      j--;
    }
    if (str.length == s.length) {
      return false;
    }
    return true;
  }

  bool checkNum(String s) {
    if (s.length != 10) {
      return false;
    }
    for (int i = 0; i < s.length; i++) {
      int ascii = s.codeUnitAt(i);
      if (ascii < 48 || ascii > 57) {
        return false;
      }
    }
    return true;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            SizedBox(
              height: 400,
              width: 300,
              child: ListView.builder(
                itemCount: 2,
                itemBuilder: (context, index) {
                  return TextFormField(
                    key: list[index]["key"],
                    controller: list[index]["controller"],
                    validator: (String? value) {
                      if ((value == null || value.isEmpty)) {
                        return "please enter valid data";
                      } else if (value.isNotEmpty &&
                          index == 0 &&
                          !checkEmail(value)) {
                        return "please enter valid email";
                      } else if (value.isNotEmpty &&
                          index == 1 &&
                          !checkNum(value)) {
                        return "please enter valid phone number";
                      }
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: InputDecoration(
                        labelText: list[index]["name"],
                        hintText: list[index]["name"]),
                  );
                },
              ),
            ),
            GestureDetector(
              child: Container(
                height: 20,
                width: 20,
                child: Text("submit"),
              ),
              onTap: () {
                list[0]["key"].currentState!.validate();
                list[1]["key"].currentState!.validate();
              },
            ),
          ],
        ),
      ),
    );
  }
}
