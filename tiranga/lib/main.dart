import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: const Text("Triranga"),
            ),
            body: Container(
              height: double.infinity,
              width: double.infinity,
              color: Colors.grey,
              child: Column(
                //mainAxisAlignment: MainAxisAlignment.center,
                //crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 10,
                          height: 500,
                          color: Colors.black,
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              height: 100,
                              width: 400,
                              color: Colors.orange,
                            ),
                            Container(
                              height: 100,
                              width: 400,
                              color: Colors.white,
                              padding: const EdgeInsets.only(top: 5, bottom: 5),
                              child: Image.network(
                                  'https://cdn.pixabay.com/photo/2023/04/06/16/29/ashoka-chakra-7904695_1280.png'),
                            ),
                            Container(
                              height: 100,
                              width: 400,
                              color: Colors.green,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 40,
                        width: 300,
                        color: Colors.brown,
                        padding: const EdgeInsets.only(right: 900),
                      )
                    ],
                  ))
                ],
              ),
            )));
  }
}

/*
Container(
                        height: 100,
                        width: 400,
                        color: Colors.orange,
                      ),
                      Container(
                        height: 100,
                        width: 400,
                        color: Colors.white,
                        padding: const EdgeInsets.only(top: 5, bottom: 5),
                        child: Image.network(
                            'https://cdn.pixabay.com/photo/2023/04/06/16/29/ashoka-chakra-7904695_1280.png'),
                      ),
                      Container(
                        height: 100,
                        width: 400,
                        color: Colors.green,
                      ),
*/