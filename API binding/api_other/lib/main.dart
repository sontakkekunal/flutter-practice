import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MainApp());
}

class Data {
  int? id;
  String? employeeName;
  int? employeeSalary;
  int? employeeAge;

  Data(Map<String, dynamic> empinfo) {
    id = empinfo['id'];
    employeeAge = empinfo['employee_age'];
    employeeName = empinfo['employee_name'];
    employeeSalary = empinfo['employee_salary'];
  }
}

class JasonData {
  String? status;
  List<Data>? data;
  String? message;
  JasonData(Map<String, dynamic> infoMap) {
    status = infoMap['status'];
    message = infoMap['message'];
    data = [];
    infoMap['data'].forEach((json) {
      data!.add(Data(json));
    });
    // data = List.generate(infoMap['data'].length, (index) {
    //   return Data(infoMap['data'][index]);
    // });
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: DataPage());
  }
}

class DataPage extends StatefulWidget {
  const DataPage({super.key});
  @override
  State<StatefulWidget> createState() {
    return _DataPageState();
  }
}

class _DataPageState extends State {
  List<Data> empData = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                  onPressed: () async {
                    Uri url = Uri.parse(
                        'https://dummy.restapiexample.com/api/v1/employees');
                    http.Response response = await http.get(url);
                    //log("${jsonDecode(response.body)}");
                    empData = JasonData(json.decode(response.body)).data!;
                    setState(() {});
                  },
                  child: const Text("get data")),
              ElevatedButton(
                  onPressed: () async {
                    Uri url = Uri.parse(
                        'https://dummy.restapiexample.com/api/v1/update/21/');
                    Map<String, dynamic> mapData = {
                      'name': 'Kunal',
                      'salary': '10000',
                      'age': '22'
                    };
                    http.Response response = await http.put(url, body: mapData);
                    log('${response.statusCode}');
                  },
                  child: const Text("put data")),
              ElevatedButton(
                  onPressed: () async {
                    Uri url = Uri.parse(
                        'https://dummy.restapiexample.com/api/v1/delete/2');
                    http.Response response = await http.delete(url);
                    log('${response.statusCode}');
                  },
                  child: const Text("delete"))
            ],
          ),
          Expanded(
              child: ListView.builder(
            itemCount: empData.length,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                    color: Colors.amber,
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  children: [
                    Text("${empData[index].id}"),
                    Text("${empData[index].employeeName}"),
                    Text("${empData[index].employeeAge}"),
                    Text("${empData[index].employeeSalary}"),
                  ],
                ),
              );
            },
          ))
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Uri url = Uri.parse('https://dummy.restapiexample.com/api/v1/create');
          Map<String, dynamic> mapData = {
            'name': 'Kunal',
            'salary': '10000',
            'age': '21'
          };
          http.Response response = await http.post(url, body: mapData);
          log("${response.statusCode}");
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
