import 'package:api_get/entry_class.dart';
import 'package:api_get/muti_emp.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MainApp());
}

class LinkContainerClass extends InheritedWidget {
  bool isMulti;
  int empCount;
  List<dynamic> empList;
  LinkContainerClass(
      {super.key,
      required this.isMulti,
      required this.empCount,
      required this.empList,
      required super.child});

  static LinkContainerClass of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<LinkContainerClass>()!;
  }

  @override
  bool updateShouldNotify(covariant LinkContainerClass oldWidget) {
    return true;
  }
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return LinkContainerClass(
        empList: const [],
        empCount: 0,
        isMulti: false,
        child: MaterialApp(
          routes: {
          'empPage': (context) {
            return Emp();
          }
        }, home: const EntryClass()));
  }
}
