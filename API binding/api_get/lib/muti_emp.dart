import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class Emp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _EmpState();
  }
}

class _EmpState extends State {
  CarouselController carouselController = CarouselController();
  List<Widget> getCont() {
    return List.generate(LinkContainerClass.of(context).empCount, (index) {
      return Container(
        height: 52,
        width: 52,
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.grey,
        ),
        alignment: Alignment.center,
        child: Text('${index + 1}'),
      );
    });
  }

  int activeIndex = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: const Text("EmpInfo page"),
      ),
      body: Column(
        children: [
          (LinkContainerClass.of(context).isMulti)
              ? CarouselSlider(
                  carouselController: carouselController,
                  items: getCont(),
                  options: CarouselOptions(
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (index, reason) {
                        setState(() {
                          activeIndex = index;
                        });
                      },
                      enableInfiniteScroll: false,
                      autoPlay: true,
                      enlargeCenterPage: true,
                      enlargeStrategy: CenterPageEnlargeStrategy.zoom))
              : const SizedBox(),
          Table(
            border: TableBorder.all(color: Colors.black, width: 1),
            defaultVerticalAlignment: TableCellVerticalAlignment.middle,
            children: [
              TableRow(children: [
                const Text('id:'),
                Text(
                    '${LinkContainerClass.of(context).empList[activeIndex]['id']}')
              ]),
              TableRow(children: [
                const Text('employee_name:'),
                Text(
                    '${LinkContainerClass.of(context).empList[activeIndex]['employee_name']}')
              ]),
              TableRow(children: [
                const Text('employee_salary:'),
                Text(
                    '${LinkContainerClass.of(context).empList[activeIndex]['employee_salary']}')
              ]),
              TableRow(children: [
                const Text('employee_age:'),
                Text(
                    '${LinkContainerClass.of(context).empList[activeIndex]['employee_age']}')
              ]),
            ],
          )
        ],
      ),
    );
  }
}
