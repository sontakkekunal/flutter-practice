import 'dart:convert';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'main.dart';

class EntryClass extends StatefulWidget {
  const EntryClass({super.key});
  @override
  State createState() => _EntryClassState();
}

class _EntryClassState extends State {
  final TextEditingController _editingController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextFormField(
              controller: _editingController,
            ),
            const SizedBox(
              height: 25,
            ),
            ElevatedButton(
                onPressed: () async {
                  Uri url = Uri.parse(_editingController.text);
                  http.Response response = await http.get(url);
                  //log(response.body);
                  var package = json.decode(response.body);
                  var data = package['data'];
                  if (data.runtimeType.toString() == 'List<dynamic>') {
                    LinkContainerClass.of(context).isMulti = true;
                    LinkContainerClass.of(context).empCount = data.length;
                    LinkContainerClass.of(context).empList = data;
                  } else {
                    LinkContainerClass.of(context).isMulti = false;
                    LinkContainerClass.of(context).empCount = 1;
                    LinkContainerClass.of(context).empList = [data];
                  }
                  Navigator.of(context).pushNamed('empPage');
                },
                child: const Text("Submit"))
          ],
        ),
      ),
    );
  }
}
