import 'package:flutter/material.dart';
import 'toggleColor.dart';

void main() => runApp(const MyToggleApp());

class MyToggleApp extends StatelessWidget {
  const MyToggleApp({super.key});
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home:Toggle()
    );
  }
}
