// ignore: file_names
import 'package:flutter/material.dart';

class Toggle extends StatefulWidget {
  const Toggle({super.key});

  @override
  State<StatefulWidget> createState() => _Toggle();
}

class _Toggle extends State {
  int numBox1 = 1;
  int numBox2 = 1;
  Color setColorBox1() {
    if (numBox1 > 2) {
      return Colors.blue;
    } else if (numBox1 > 1) {
      return Colors.green;
    } else {
      return Colors.red;
    }
  }

  changeBoxColor1() {
    setState(() {
      numBox1++;
    });
    if (numBox1 > 3) {
      numBox1 = 1;
    }
  }

  Color setColorBox2() {
    if (numBox2 > 2) {
      return Colors.orange;
    } else if (numBox2 > 1) {
      return Colors.yellow;
    } else {
      return Colors.purple;
    }
  }

  changeBoxColor2() {
    setState(() {
      numBox2++;
    });
    if (numBox2 > 3) {
      numBox2 = 1;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("ToggleApp"),
          centerTitle: true,
        ),
        body: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Container(height: 100, width: 100, color: setColorBox1()),
            ElevatedButton(
              onPressed: changeBoxColor1,
              child: const Text("Button1"),
            )
          ]),
          Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
            Container(height: 100, width: 100, color: setColorBox2()),
            ElevatedButton(
                onPressed: changeBoxColor2, child: const Text("Button2"))
          ])
        ]));
  }
}
