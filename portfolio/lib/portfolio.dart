import 'package:flutter/material.dart';

class Portfolio extends StatefulWidget {
  const Portfolio({super.key});
  @override
  State<StatefulWidget> createState() => _Portfolio();
}

class _Portfolio extends State<Portfolio> {
  int count = 0;
  bool flag = true;
  next() {
    if (flag) {
      setState(() {
        count++;
      });
      if (count == 6) {
        flag = false;
      }
    } else {
      setState(() {
        count--;
      });
      if (count == 0) {
        flag = true;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 228, 234, 236),
      appBar: AppBar(
        title: const Text("Portflio",
            style: TextStyle(
              fontSize: 27,
              fontWeight: FontWeight.w600,
            )),
        shadowColor: Colors.black87,
        backgroundColor: const Color.fromARGB(255, 211, 108, 230),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Name: ",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w600,
                        backgroundColor: Color.fromARGB(255, 239, 227, 240)),
                  ),
                  (count >= 1)
                      ? const Text(" Kunal Sontakke ",
                          style: TextStyle(
                              fontSize: 20,
                              backgroundColor:
                                  Color.fromARGB(255, 239, 227, 240)))
                      : const Text(""),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 200,
                width: 200,
                //margin: const EdgeInsets.all(5),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Color.fromARGB(255, 213, 212, 212)),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  child: (count >= 2)
                      ? Image.asset(
                          "images/profile_image.webp",
                          fit: BoxFit.cover,
                        )
                      : null,
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("College: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          backgroundColor: Color.fromARGB(255, 239, 227, 240))),
                  (count >= 3)
                      ? const Text(" Zeal college ",
                          style: TextStyle(
                              fontSize: 20,
                              backgroundColor:
                                  Color.fromARGB(255, 239, 227, 240)))
                      : const Text(""),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 200,
                width: 200,
                margin: const EdgeInsets.all(5),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Color.fromARGB(255, 211, 211, 211)),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  child: (count >= 4)
                      ? Image.network(
                          "https://images.shiksha.com/mediadata/images/1660115784phplJc53T.png",
                          fit: BoxFit.cover)
                      : null,
                ),
              ),
              const SizedBox(
                height: 40,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text("dream Comapany: ",
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 20,
                          backgroundColor: Color.fromARGB(255, 239, 227, 240))),
                  (count >= 5)
                      ? const Text(" Software company ",
                          style: TextStyle(
                              fontSize: 20,
                              backgroundColor:
                                  Color.fromARGB(255, 239, 227, 240)))
                      : const Text(""),
                ],
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 200,
                width: 200,
                margin: const EdgeInsets.all(5),
                decoration: const BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(15)),
                    color: Color.fromARGB(255, 198, 197, 197)),
                child: ClipRRect(
                  borderRadius: const BorderRadius.all(Radius.circular(15)),
                  child: (count >= 6)
                      ? Image.network(
                          "https://img.freepik.com/premium-vector/software-technology-company-logo-with-connecting-dots-illustration_884294-31.jpg",
                          fit: BoxFit.cover)
                      : null,
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton:
          FloatingActionButton(onPressed: next, child: const Text("Next")),
    );
  }
}
