import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:clothes_store/Controller/HomeController.dart';
import 'package:clothes_store/Controller/OnboardController.dart';
import 'package:clothes_store/View/CartScreen.dart';
import 'package:clothes_store/View/CheckOutScreen.dart';
import 'package:clothes_store/View/HomeScreen.dart';
import 'package:clothes_store/View/OnboardScreen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          Provider(create: (create) => OnBoardController()),
          ChangeNotifierProvider(create: (context) => Homecontroller()),
          ChangeNotifierProvider(create: (context) => Deliverycontroller()),

        ],
        child: MaterialApp(
            routes: {
              'homeScreen': (context) => const HomeScreen(),
              'cartScreen': (context) => const CartScreen(),
              'checkOutScreen': (context) => const CheckOutScreen(),
            },
            theme: ThemeData(
                primaryColor: const Color.fromRGBO(255, 122, 0, 1),
                primaryColorDark: const Color.fromRGBO(13, 13, 14, 1),
                scaffoldBackgroundColor: const Color.fromRGBO(255, 255, 255, 1),
                textTheme: TextTheme(
                  bodyMedium: GoogleFonts.imprima(
                      fontSize: 24,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                  displaySmall: GoogleFonts.imprima(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                  displayLarge: GoogleFonts.imprima(
                      fontSize: 42,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                  displayMedium: GoogleFonts.imprima(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                  headlineLarge: GoogleFonts.imprima(
                      fontSize: 36,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                  labelLarge: GoogleFonts.imprima(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(121, 119, 128, 1)),
                  labelMedium: GoogleFonts.imprima(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(121, 119, 128, 1)),
                  labelSmall: GoogleFonts.imprima(
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                  titleMedium: GoogleFonts.imprima(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: const Color.fromRGBO(255, 255, 255, 1),
                  ),
                  bodyLarge: GoogleFonts.imprima(
                    fontSize: 30,
                    fontWeight: FontWeight.w400,
                    color: const Color.fromRGBO(13, 13, 14, 1),
                  ),
                  titleLarge: GoogleFonts.imprima(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(121, 119, 128, 1)),
                  titleSmall: GoogleFonts.imprima(
                      fontSize: 16,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(13, 13, 14, 1)),
                )),
            home: const OnBoardScreen()));
  }
}
