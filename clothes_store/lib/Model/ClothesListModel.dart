import 'ClothesInfoModel.dart';

class ClothesListModel {
  String name;
  List<ClothesInfoModel> clothesList;
  ClothesListModel({
    required this.name,
    required this.clothesList,
  });
}
