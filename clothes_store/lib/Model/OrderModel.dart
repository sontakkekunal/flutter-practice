import 'package:clothes_store/Model/ClothesInfoModel.dart';

class OrderModel {
  int item;
  ClothesInfoModel clothesInfoModel;
  OrderModel({required this.item, required this.clothesInfoModel});
}
