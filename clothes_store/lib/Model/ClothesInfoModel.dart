class ClothesInfoModel {
  List<String> imgColorList;
  String name;
  double price;
  List<String> sizeList;
  bool isSaved;
  int selectedColor;
  int selectedSize;
  List<String> colorSelect;
  double increaseRate;
  int key;
  ClothesInfoModel({
    required this.imgColorList,
    required this.name,
    required this.price,
    required this.sizeList,
    required this.isSaved,
    required this.selectedColor,
    required this.selectedSize,
    required this.colorSelect,
    required this.increaseRate,
    required this.key,
  });
  double getPrice() {
    if (selectedSize == 0) {
      return price;
    } else {
      return double.parse(
          (price + (increaseRate * (selectedSize + 1))).toStringAsFixed(2));
    }
  }

  String getColorName() {
    if (selectedColor == 0) {
      return 'Yellow';
    } else if (selectedColor == 1) {
      return 'Red';
    } else if (selectedColor == 2) {
      return 'Blue';
    } else {
      return 'None';
    }
  }

  ClothesInfoModel copy() {
    return ClothesInfoModel(
        imgColorList: imgColorList,
        name: name,
        price: price,
        sizeList: sizeList,
        isSaved: isSaved,
        selectedColor: selectedColor,
        selectedSize: selectedSize,
        colorSelect: colorSelect,
        increaseRate: increaseRate,
        key: key);
  }
}
