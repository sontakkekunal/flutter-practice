import 'package:clothes_store/Controller/HomeController.dart';
import 'package:clothes_store/Model/ClothesInfoModel.dart';
import 'package:clothes_store/Model/OrderModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:open_street_map_search_and_pick/open_street_map_search_and_pick.dart';
import 'package:provider/provider.dart';

import '../Model/ClothesListModel.dart';

class Deliverycontroller extends ChangeNotifier {
  List<OrderModel> orderList = [];
  int itemCount = 0;
  double totalItemPrice = 0;
  double totalPayment = 0;
  PickedData? pickedData;
  double standardDeliveryPrice = 0;
  List<String> paymentList = [
    'images/onlineImg/visaImg.png',
    'images/onlineImg/amercianExpressImg.png',
    'images/onlineImg/masterCardImg.png',
    'images/onlineImg/payPalImg.png',
    'images/onlineImg/applePayImg.png',
  ];
  void refreash() {
    notifyListeners();
  }

  void priceSetter() {
    itemCount = 0;
    totalItemPrice = 0;
    orderList.forEach((obj) {
      itemCount = itemCount + obj.item;
      totalItemPrice =
          totalItemPrice + (obj.item * obj.clothesInfoModel.getPrice());
    });
    standardDeliveryPrice = itemCount * 3;
    totalPayment = standardDeliveryPrice + totalItemPrice;
  }

  void changeLoction(PickedData pickedData) {
    this.pickedData = pickedData;
    notifyListeners();
  }

  void removeOrder({required int index}) {
    if (orderList[index].item > 1) {
      orderList[index].item--;
    } else {
      orderList.removeAt(index);
    }

    priceSetter();
    notifyListeners();
  }

  void changeFav(
      {required ClothesInfoModel obj, required BuildContext context}) {
    List<ClothesListModel> clothesListData =
        Provider.of<Homecontroller>(context, listen: false).clothesListData;
    for (int i = 0; i < clothesListData.length; i++) {
      List<ClothesInfoModel> clothesList = clothesListData[i].clothesList;
      for (int j = 0;
          j < clothesList.length && clothesListData[i].name != 'All';
          j++) {
        if (clothesList[j].key == obj.key) {
          clothesList[j].isSaved = !clothesList[j].isSaved;
          obj.isSaved = !obj.isSaved;

          break;
        }
      }
    }
    refreash();
  }

  void addOrder(ClothesInfoModel clothInfoModel) {
    bool isPresent = false;
    orderList.forEach((obj) {
      if (obj.clothesInfoModel.key == clothInfoModel.key &&
          obj.clothesInfoModel.selectedColor == clothInfoModel.selectedColor &&
          obj.clothesInfoModel.selectedSize == clothInfoModel.selectedSize) {
        isPresent = true;
        obj.item++;
      }
    });
    if (!isPresent) {
      orderList
          .add(OrderModel(item: 1, clothesInfoModel: clothInfoModel.copy()));
    }
  }
}
