import 'package:clothes_store/Model/ClothesInfoModel.dart';
import 'package:clothes_store/Model/ClothesListModel.dart';
import 'package:flutter/material.dart';

class Homecontroller extends ChangeNotifier {
  void refreah() {
    notifyListeners();
  }

  List<ClothesListModel> clothesListData = [
    ClothesListModel(name: 'Men', clothesList: [
      ClothesInfoModel(
          key: 0,
          imgColorList: [
            'images/onlineImg/tagerineShirtImg1.png',
            'images/onlineImg/tagerineShirtImg_1.png',
            'images/onlineImg/tagerineShirtImg_2.png'
          ],
          name: 'Tagerine Shirt',
          price: 240.32,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          selectedSize: 0,
          increaseRate: 8.765,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
      ClothesInfoModel(
          key: 1,
          imgColorList: [
            'images/onlineImg/leatherCoartImg.png',
            'images/onlineImg/tagerineShirtImg_1.png',
            'images/onlineImg/tagerineShirtImg_2.png'
          ],
          name: 'Leather Coart',
          price: 325.36,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          selectedSize: 0,
          increaseRate: 8.765,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
      ClothesInfoModel(
          key: 2,
          imgColorList: [
            'images/onlineImg/leatherCoartImg2.png',
            'images/onlineImg/tagerineShirtImg_1.png',
            'images/onlineImg/tagerineShirtImg_2.png'
          ],
          name: 'Tagerine Shirt',
          price: 126.47,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          selectedSize: 0,
          increaseRate: 8.765,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
      ClothesInfoModel(
          key: 3,
          imgColorList: [
            'images/onlineImg/tagerineShirtImg2.png',
            'images/onlineImg/tagerineShirtImg_1.png',
            'images/onlineImg/tagerineShirtImg_2.png'
          ],
          name: 'Leather Coart',
          price: 257.85,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 8.765,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
    ]),
    ClothesListModel(name: 'Women', clothesList: [
      ClothesInfoModel(
          key: 4,
          imgColorList: [
            'images/onlineImg/redSaree1.webp',
            'images/onlineImg/redSaree3.webp',
            'images/onlineImg/redSaree2.webp'
          ],
          name: 'Red Saree',
          price: 55.73,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 4.12,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
      ClothesInfoModel(
          key: 5,
          imgColorList: [
            'images/onlineImg/yellowSaree.jpg',
            'images/onlineImg/yellowSaree2.jpg',
            'images/onlineImg/yellowSaree3.jpeg'
          ],
          name: 'Yellow Saree',
          price: 60.24,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 2.9,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
      ClothesInfoModel(
          key: 6,
          imgColorList: [
            'images/onlineImg/orangeSaree1.jpg',
            'images/onlineImg/orangeSaree2.jpg',
            'images/onlineImg/orangeSaree3.jpg'
          ],
          name: 'Orange Saree',
          price: 65.24,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 8.21,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
    ]),
    ClothesListModel(name: 'Kids', clothesList: [
      ClothesInfoModel(
          key: 7,
          imgColorList: [
            'images/onlineImg/kidClothImg1.webp',
            'images/onlineImg/kidClothImg2.jpg',
            'images/onlineImg/kidClothImg3.jpg'
          ],
          name: 'Kids Dress 1',
          price: 30.1,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 1.21,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
      ClothesInfoModel(
          key: 8,
          imgColorList: [
            'images/onlineImg/kidClothImg4.jpg',
            'images/onlineImg/kidClothImg5.jpg',
            'images/onlineImg/kidClothImg6.jpg'
          ],
          name: 'Kids Dress 2',
          price: 37.42,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 1.71,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
    ]),
    ClothesListModel(name: 'Other', clothesList: [
      ClothesInfoModel(
          key: 8,
          imgColorList: [
            'images/onlineImg/underWare1.jpg',
            'images/onlineImg/underWare2.webp',
            'images/onlineImg/underWare3.webp'
          ],
          name: 'Underwear',
          price: 20.41,
          sizeList: ['S', 'M', 'L', 'XL', 'XXL'],
          isSaved: false,
          selectedColor: 0,
          increaseRate: 1.34,
          selectedSize: 0,
          colorSelect: [
            'images/onlineImg/color1.png',
            'images/onlineImg/color2.png',
            'images/onlineImg/color3.png'
          ]),
    ])
  ];

  Homecontroller() {
    List<ClothesInfoModel> get() {
      List<ClothesInfoModel> temp = [];
      for (int i = 0; i < clothesListData.length; i++) {
        temp.addAll(clothesListData[i].clothesList);
      }
      return temp;
    }

    clothesListData.insert(
        0, ClothesListModel(name: 'All', clothesList: get()));
    //clothesListData.sort((a, b) => a.name.compareTo(b.name));
  }
}
