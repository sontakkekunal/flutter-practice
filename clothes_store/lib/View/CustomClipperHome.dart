import 'package:flutter/material.dart';

class Customclipperhome extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    //Path path_0 = Path();
    //size = Size(900, 900);
    Path path_0 = Path();
    path_0.moveTo(size.width * 0.0025400, size.height * 0.0515088);
    path_0.lineTo(size.width * 0.9281976, size.height * 0.0479835);
    path_0.lineTo(size.width * 0.9317675, size.height * 1.0036889);
    path_0.lineTo(size.width * 0.7986196, size.height * 1.0029222);
    path_0.quadraticBezierTo(size.width * 0.8086072, size.height * 0.9073000,
        size.width * 0.6814960, size.height * 0.8985444);
    path_0.quadraticBezierTo(size.width * 0.5760847, size.height * 0.9075333,
        size.width * 0.5678419, size.height * 1.0009556);
    path_0.lineTo(size.width * 0.0118658, size.height * 1.0053602);
    path_0.lineTo(size.width * 0.0025400, size.height * 0.0515088);
    return path_0;
  }

  @override
  bool shouldReclip(covariant CustomClipper oldClipper) {
    return true;
  }
}

class RPSCustomPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    // Layer 1

    Paint paint_fill_0 = Paint()
      ..color = const Color.fromARGB(255, 255, 255, 255)
      ..style = PaintingStyle.fill
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;

    Path path_0 = Path();
    path_0.moveTo(size.width * 0.1633333, size.height * 0.3557143);
    path_0.lineTo(size.width * 0.8333333, size.height * 0.3557143);
    path_0.lineTo(size.width * 0.8333333, size.height * 0.6428571);
    path_0.lineTo(size.width * 0.7465667, size.height * 0.6420143);
    path_0.quadraticBezierTo(size.width * 0.7411000, size.height * 0.6033857,
        size.width * 0.6719333, size.height * 0.6053857);
    path_0.quadraticBezierTo(size.width * 0.6158667, size.height * 0.6058000,
        size.width * 0.6086667, size.height * 0.6419143);
    path_0.lineTo(size.width * 0.1666667, size.height * 0.6414286);
    path_0.lineTo(size.width * 0.1633333, size.height * 0.3557143);
    path_0.close();

    canvas.drawPath(path_0, paint_fill_0);

    // Layer 1

    Paint paint_stroke_0 = Paint()
      ..color = const Color.fromARGB(255, 33, 150, 243)
      ..style = PaintingStyle.stroke
      ..strokeWidth = size.width * 0.00
      ..strokeCap = StrokeCap.butt
      ..strokeJoin = StrokeJoin.miter;

    canvas.drawPath(path_0, paint_stroke_0);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return true;
  }
}
