import 'dart:developer';

import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:clothes_store/Controller/HomeController.dart';
import 'package:clothes_store/Model/ClothesInfoModel.dart';
import 'package:clothes_store/Model/ClothesListModel.dart';
import 'package:clothes_store/View/CustomClipperHome.dart';
import 'package:clothes_store/View/DetailScreen.dart';
import 'package:clothes_store/View/NavBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _HomeScreenState();
  }
}

class _HomeScreenState extends State {
  int _selectedIndex = 0;
  void changeList(int index, BuildContext context) {
    _selectedIndex = index;
    Provider.of<Homecontroller>(context, listen: false).refreah();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    log('In homeScreen build');
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: height * 0.081,
            horizontal: width * 0.065,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Image.asset('images/offlineImg/menuImg.png'),
                  const Icon(Icons.panorama_fisheye_outlined)
                ],
              ),
              SizedBox(
                height: height * 0.042,
              ),
              Text(
                'Explore',
                style: Theme.of(context).textTheme.headlineLarge,
              ),
              SizedBox(
                height: height * 0.0018,
              ),
              Text(
                'Best trendy collection!',
                style: Theme.of(context).textTheme.labelMedium,
              ),
              SizedBox(
                height: height * 0.032,
              ),
              Consumer<Homecontroller>(
                builder: (context, value, child) {
                  List<ClothesListModel> clothesListData =
                      Provider.of<Homecontroller>(context).clothesListData;
                  return SizedBox(
                    height: height * 0.04,
                    width: double.maxFinite,
                    child: ListView.separated(
                      separatorBuilder: (context, index) =>
                          SizedBox(width: width * 0.03),
                      itemCount: clothesListData.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            changeList(index, context);
                          },
                          child: Container(
                            height: height * 0.04,
                            width: width * 0.149,
                            decoration: BoxDecoration(
                              //color: Colors.white,
                              color: (_selectedIndex == index)
                                  ? Theme.of(context).primaryColor
                                  : Colors.transparent,
                              borderRadius: BorderRadius.circular(32),
                            ),
                            alignment: Alignment.center,
                            child: Text(
                              clothesListData[index].name,
                              style: (_selectedIndex == index)
                                  ? Theme.of(context).textTheme.titleMedium
                                  : Theme.of(context).textTheme.titleSmall,
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
              SizedBox(
                height: height * 0.032,
              ),
              Consumer<Homecontroller>(
                builder: (context, value, child) {
                  List<ClothesInfoModel> clothesList =
                      Provider.of<Homecontroller>(context)
                          .clothesListData[_selectedIndex]
                          .clothesList;
                  return Draggable(
                   feedback: Container(
                     height: 35,
                     width: 35,
                     decoration: BoxDecoration(
                       color: Theme.of(context).primaryColor,
                       border: Border.all(color: Colors.black,width: 1.3),
                       shape: BoxShape.circle
                     ),
                   ),
                    childWhenDragging: Center(child: Image.asset('images/offlineImg/logoImg.png',height: 250,width: 250,)),
                    onDragEnd: (drag){
                      if(drag.velocity.pixelsPerSecond.dx<0){//right drag
if(_selectedIndex!=value.clothesListData.length-1){
  changeList(++_selectedIndex, context);
}
                      }else{
                        //left drag
                        if(_selectedIndex!=0){
                          changeList(--_selectedIndex, context);
                        }
                      }
                    },
                    child: SizedBox(
                      width: double.maxFinite,
                      height: height * 0.66,
                      child: MasonryGridView.builder(
                        padding: EdgeInsets.zero,
                        itemCount: clothesList.length,
                        crossAxisSpacing: height * 0.02,
                        mainAxisSpacing: height * 0.02,
                        gridDelegate: SliverSimpleGridDelegateWithFixedCrossAxisCount(crossAxisCount: width~/150),
                        //gridDelegate: RenderSliverMasonryGrid(gridDelegate: SliverSimpleGridDelegateWithFixedCrossAxisCount(crossAxisCount:2 ),crossAxisSpacing: 10,mainAxisSpacing: 10,childManager:RenderSliverBoxChildManager()),
                        // gridDelegate:
                        //     const SliverSimpleGridDelegateWithFixedCrossAxisCount(
                        //         crossAxisCount: 2),
                        itemBuilder: (context, index) {
                          ClothesInfoModel clothesInfoModel = clothesList[index];
                          return GestureDetector(
                            onTap: () {
                              HapticFeedback.heavyImpact();
                              Navigator.of(context)
                                  .push(MaterialPageRoute(
                                builder: (context) => DetailScreen(
                                  listIndex: _selectedIndex,
                                  clotheIndex: index,
                                ),
                              ))
                                  .then((value) {
                                Provider.of<Deliverycontroller>(context,
                                        listen: false)
                                    .refreash();
                              });
                            },
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Stack(
                                  clipBehavior: Clip.none,
                                  children: [
                    
                                    ClipRRect(
                                      borderRadius: BorderRadius.circular(25),
                                      child: Hero(
                                        tag:"img-${clothesInfoModel.key}${clothesInfoModel.imgColorList[0]}",
                                        child: Image.asset(
                                          clothesInfoModel.imgColorList[0],
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                    Positioned(
                                      bottom: -19,
                                      right: width*0.055,
                                      child: Container(
                                        height: height * 0.043,
                                        width: width * 0.094,
                                        decoration: BoxDecoration(
                                          border: Border.all(color: Colors.white,width: 4),
                                            shape: BoxShape.circle,
                                            color: Colors.black),
                                        alignment: Alignment.center,
                                        child: Image.asset(
                                            'images/offlineImg/bagIcon2.png'),
                                      ),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: height * 0.012,
                                ),
                                Text(
                                  "\$${clothesInfoModel.price}",
                                  style:
                                      Theme.of(context).textTheme.displayMedium,
                                ),
                                SizedBox(
                                  height: height * 0.007,
                                ),
                                Text(
                                  clothesInfoModel.name,
                                  style: Theme.of(context).textTheme.titleLarge,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                    ),
                  );
                },
              )
            ],
          ),
        ),
      ),
      bottomNavigationBar: Navbar(
        currentIndex: 0,
      ),
    );
  }
}
