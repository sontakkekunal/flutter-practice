import 'dart:developer';

import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:clothes_store/Model/OrderModel.dart';
import 'package:clothes_store/View/DetailScreen.dart';
import 'package:clothes_store/View/commanWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';

class CartScreen extends StatefulWidget {
  const CartScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CartScreenState();
  }
}

class _CartScreenState extends State {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    log('In card build');
    int itemCount = 0;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: height * 0.051,
            horizontal: width * 0.065,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              getBackBar(title: 'Detail', context: context),
              SizedBox(
                height: height * 0.04,
              ),
              Text(
                'My Order',
                style: Theme.of(context)
                    .textTheme
                    .headlineLarge!
                    .copyWith(fontSize: 40),
              ),
              SizedBox(
                height: height * 0.041,
              ),
              SizedBox(
                height: height * 0.18 * 2.2,
                width: width,
                child: Consumer<Deliverycontroller>(
                  builder: (context, Deliverycontroller value, child) {
                    List<OrderModel> orderList =
                        Provider.of<Deliverycontroller>(context).orderList;
                    itemCount = value.orderList.length;
                    return ListView.separated(
                      padding: EdgeInsets.zero,
                      separatorBuilder: (context, index) => SizedBox(
                        height: height * 0.04,
                      ),
                      itemCount: orderList.length,
                      itemBuilder: (context, index) {
                        return Slidable(
                          endActionPane: ActionPane(
                              extentRatio: 0.456,
                              motion: const DrawerMotion(),
                              children: [
                                Expanded(
                                  child: Container(
                                    margin: EdgeInsets.only(
                                        bottom: height * 0.1096),
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).primaryColor,
                                        borderRadius: BorderRadius.only(
                                            topLeft:
                                                Radius.circular(height * 0.033),
                                            bottomLeft: Radius.circular(
                                                height * 0.033))),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: [
                                        SlidableAction(
                                          onPressed: (context) {
                                            Navigator.of(context)
                                                .push(MaterialPageRoute(
                                              builder: (context) =>
                                                  DetailScreen(
                                                orderIndex: index,
                                              ),
                                            ));
                                          },
                                          icon:
                                              Icons.mode_edit_outline_outlined,
                                          backgroundColor: Colors.transparent,
                                        ),
                                        SlidableAction(
                                          onPressed: (context) {
                                            value.removeOrder(index: index);
                                          },
                                          icon: Icons.delete_outline_sharp,
                                          backgroundColor: Colors.transparent,
                                        ),
                                        SlidableAction(
                                          onPressed: (context) {
                                            value.changeFav(
                                                obj: orderList[index]
                                                    .clothesInfoModel,
                                                context: context);
                                          },
                                          icon: orderList[index]
                                                  .clothesInfoModel
                                                  .isSaved
                                              ? Icons.favorite
                                              : Icons.favorite_border_sharp,
                                          backgroundColor: Colors.transparent,
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ]),
                          child: SizedBox(
                            height: height * 0.17,
                            width: width,
                            child: Row(
                              children: [
                                ClipRRect(
                                  borderRadius: BorderRadius.circular(16),
                                  child: Hero(
                                    tag:"img-${orderList[index].clothesInfoModel.key}${orderList[index].clothesInfoModel.imgColorList[0]}",
                                    child: Image.asset(orderList[index]
                                            .clothesInfoModel
                                            .imgColorList[
                                        orderList[index]
                                            .clothesInfoModel
                                            .selectedColor]),
                                  ),
                                ),
                                SizedBox(
                                  width: width * 0.058,
                                ),
                                Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      width: 120,
                                      child: Text(
                                        "Premium ${orderList[index].clothesInfoModel.name}",
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelSmall,
                                      ),
                                    ),
                                    Text(
                                      orderList[index]
                                          .clothesInfoModel
                                          .getColorName(),
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleLarge!
                                          .copyWith(fontSize: 14),
                                    ),
                                    Text(
                                      "Size: ${orderList[index].clothesInfoModel.sizeList[orderList[index].clothesInfoModel.selectedSize]}",
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleLarge!
                                          .copyWith(fontSize: 14),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          '\$${orderList[index].clothesInfoModel.getPrice()}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!
                                              .copyWith(fontSize: 26),
                                        ),
                                        SizedBox(
                                          width: width * 0.14,
                                        ),
                                        Text(
                                          '${orderList[index].item}',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!
                                              .copyWith(fontSize: 26),
                                        ),
                                        Text(
                                          'x',
                                          style: Theme.of(context)
                                              .textTheme
                                              .bodyMedium!
                                              .copyWith(fontSize: 20),
                                        )
                                      ],
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
              Container(
                height: 2.5,
                width: width,
                decoration: const BoxDecoration(
                    color: Color.fromRGBO(227, 227, 227, 1)),
              ),
              SizedBox(
                height: height * 0.035,
              ),
              getTotalPriceDetails(context: context, height: height),
              SizedBox(
                height: height * 0.04,
              ),
              GestureDetector(
                onTap: () {
                  HapticFeedback.heavyImpact();
                  if (itemCount > 0) {

                    Navigator.of(context).pushNamed('checkOutScreen');
                  } else {
                    Fluttertoast.showToast(
                        webShowClose: true,
                        toastLength: Toast.LENGTH_SHORT,
                        msg: 'Please the add items in before',
                        textColor: Colors.red,
                        fontSize: 24,
                        gravity: ToastGravity.BOTTOM,
                        backgroundColor: Colors.grey.withOpacity(0.2));
                  }
                },
                child: getCont(
                    width: width * 0.5,
                    title: 'Checkout Now',
                    contColor: Theme.of(context).primaryColor,
                    titleColor: Theme.of(context).scaffoldBackgroundColor,
                    context: context),
              )
            ],
          ),
        ),
      ),
    );
  }
}
