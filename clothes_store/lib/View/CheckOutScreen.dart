import 'dart:developer';
import 'dart:io';

import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:clothes_store/View/commanWidget.dart';
import 'package:clothes_store/View/payment_status.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:open_street_map_search_and_pick/open_street_map_search_and_pick.dart';
import 'package:pay/pay.dart';
import 'package:provider/provider.dart';
import 'package:upi_india/upi_india.dart';

import '../Controller/HomeController.dart';

class CheckOutScreen extends StatefulWidget {
  const CheckOutScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _CheckOutScreenState();
  }
}

class _CheckOutScreenState extends State {
  bool addressSelected = false;
  int _selectedPayOpt = 0;
  final UpiIndia _upiIndia = UpiIndia();
  List<UpiApp>? upiApps;

  Future<bool> isNetworkAvaiable() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (se) {
      return false;
    }
    return false;
  }

  @override
  void initState() {
    try {
      _upiIndia
          .getAllUpiApps(
        mandatoryTransactionId: false,
      )
          .then((value) {
        upiApps = value;
      });
    } catch (e) {
      upiApps = [];
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    log('In CheckOutScreen build');
    return Scaffold(
        body: SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(
          vertical: height * 0.051,
          horizontal: width * 0.065,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            getBackBar(title: 'Detail', context: context),
            SizedBox(height: height * 0.024),
            Text(
              'Delivery Address',
              style: Theme.of(context)
                  .textTheme
                  .labelLarge!
                  .copyWith(fontSize: 14),
            ),
            SizedBox(height: height * 0.024),
            Consumer<Deliverycontroller>(
              builder: (BuildContext context, Deliverycontroller value, child) {
                return Row(
                  children: [
                    Image.asset('images/onlineImg/mapImg.png'),
                    SizedBox(
                      width: width * 0.018,
                    ),
                    Expanded(
                      child: Text(
                        value.pickedData != null
                            ? value.pickedData!.addressName
                            : 'Choose location',
                        style: Theme.of(context).textTheme.titleSmall,
                      ),
                    ),
                    const Spacer(),
                    TextButton(
                        onPressed: () async {
                          HapticFeedback.heavyImpact();
                          bool val = await isNetworkAvaiable();

                          if (val == true && (Platform.isWindows || kIsWeb)) {
                            showBottomSheet(
                              elevation: 5,
                              context: context,
                              backgroundColor: Colors.transparent,
                              builder: (context) {
                                return Container(
                                  height: height * 0.80,
                                  width: width,
                                  decoration: BoxDecoration(
                                      border: Border.all(),
                                      color: Colors.white,
                                      borderRadius: const BorderRadius.only(
                                          topLeft: Radius.circular(50),
                                          topRight: Radius.circular(50))),
                                  alignment: Alignment.center,
                                  child: Column(
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.symmetric(
                                            vertical: 15),
                                        child: Text(
                                          'Location Picker',
                                          style: Theme.of(context)
                                              .textTheme
                                              .headlineLarge!
                                              .copyWith(
                                                  color: Theme.of(context)
                                                      .primaryColor),
                                        ),
                                      ),
                                      SizedBox(
                                        height: height * 0.7,
                                        width: width,
                                        child: OpenStreetMapSearchAndPick(
                                          buttonTextStyle: Theme.of(context)
                                              .textTheme
                                              .labelSmall!
                                              .copyWith(color: Colors.white),
                                          locationPinTextStyle:
                                              Theme.of(context)
                                                  .textTheme
                                                  .labelSmall!,
                                          buttonText: 'Select Location',
                                          buttonColor:
                                              Theme.of(context).primaryColor,
                                          locationPinIconColor:
                                              Theme.of(context).primaryColor,
                                          onPicked: (pickedData) {
                                            value.changeLoction(pickedData);
                                            addressSelected = true;
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              },
                            );
                          } else {
                            Fluttertoast.showToast(
                              msg:
                                  'Please check internet connectivity or device compability',
                              textColor: Colors.red,
                              fontSize: 24,
                              gravity: ToastGravity.BOTTOM,
                            );
                          }
                        },
                        child: Text(
                          'change',
                          style: Theme.of(context)
                              .textTheme
                              .labelMedium!
                              .copyWith(fontSize: 14),
                        ))
                  ],
                );
              },
            ),
            SizedBox(
              height: height * 0.029,
            ),
            Row(
              children: [
                const Icon(
                  Icons.access_time,
                  color: Color.fromRGBO(123, 123, 141, 1),
                ),
                SizedBox(
                  width: width * 0.03,
                ),
                Text(
                  'Delivered in next 7 days',
                  style: Theme.of(context).textTheme.titleSmall,
                ),
              ],
            ),
            SizedBox(
              height: height * 0.049,
            ),
            Text(
              'Payment Method',
              style: Theme.of(context).textTheme.titleLarge,
            ),
            SizedBox(
                width: width,
                height: height * 0.022,
                child: Consumer(
                  builder: (context, value, child) {
                    List<String> paymentList =
                        Provider.of<Deliverycontroller>(context).paymentList;
                    return ListView.separated(
                      separatorBuilder: (context, index) => SizedBox(
                        width: width * 0.072,
                      ),
                      scrollDirection: Axis.horizontal,
                      padding: EdgeInsets.only(top: height * 0.024),
                      itemCount: paymentList.length,
                      itemBuilder: (context, index) {
                        return Image.asset(paymentList[index]);
                      },
                    );
                  },
                )),
            SizedBox(
              height: height * 0.049,
            ),
            Container(
              height: height * 0.066,
              width: width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: const Color.fromRGBO(252, 252, 252, 1)),
              alignment: Alignment.center,
              child: Text(
                'Add Voucher',
                style: Theme.of(context)
                    .textTheme
                    .titleLarge!
                    .copyWith(fontSize: 14),
              ),
            ),
            RichText(
                text: TextSpan(children: <TextSpan>[
              TextSpan(
                  text: 'Note : ',
                  style: Theme.of(context)
                      .textTheme
                      .labelLarge!
                      .copyWith(color: Colors.red)),
              TextSpan(
                  text: 'Use your order id at the payment. Your Id ',
                  style: Theme.of(context).textTheme.labelLarge),
              TextSpan(
                  text: '#154619 ',
                  style: Theme.of(context)
                      .textTheme
                      .labelLarge!
                      .copyWith(color: Colors.black)),
              TextSpan(
                  text:
                      'if you forget to put your order id we can’t confirm the payment.',
                  style: Theme.of(context).textTheme.labelLarge)
            ])),
            SizedBox(
              height: height * 0.049,
            ),
            getTotalPriceDetails(context: context, height: height),

            // GooglePayButton(paymentConfiguration: PaymentConfiguration.fromJsonString(defaultGooglePay), paymentItems: [
            //   PaymentItem(amount: '100',label: 'Total')
            // ]),
            SizedBox(
              height: height * 0.12,
              child: ListView.separated(
                padding: EdgeInsets.symmetric(horizontal: width * 0.15),
                itemCount: 2,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      GestureDetector(
                        onTap: () {
                          _selectedPayOpt = index;
                          Provider.of<Homecontroller>(context, listen: false)
                              .refreah();
                        },
                        child: Consumer<Homecontroller>(
                            builder: (context, val1, val2) {
                          return Container(
                            height: height * 0.07,
                            width: width * 0.05,
                            padding: const EdgeInsets.all(3),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              border: Border.all(
                                  color: Theme.of(context).primaryColor),
                            ),
                            child: (_selectedPayOpt == index)
                                ? Container(
                                    decoration: BoxDecoration(
                                        color: Theme.of(context).primaryColor,
                                        shape: BoxShape.circle),
                                  )
                                : null,
                          );
                        }),
                      ),
                      SizedBox(
                        width: width * 0.02,
                      ),
                      Text(
                        index == 0 ? "Upi" : "Card",
                        style: Theme.of(context).textTheme.displayMedium,
                      )
                    ],
                  );
                },
                separatorBuilder: (context, index) => SizedBox(
                  width: width * 0.2,
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  onTap: (_selectedPayOpt == 0)
                      ? () {
                          showModalBottomSheet(
                              context: context,
                              builder: (context) {
                                return SizedBox(
                                    height: height * 0.2,
                                    width: width,
                                    child: ListView.builder(
                                      itemCount: upiApps?.length,
                                      itemBuilder: (context, index) {
                                        return GestureDetector(
                                          onTap: () async {
                                            UpiResponse upiresponse = await _upiIndia
                                                .startTransaction(
                                                    //flexibleAmount: true,
                                                    app: upiApps![index],
                                                    receiverUpiId:
                                                        "BHARATPE90727594880@yesbankltd",
                                                    receiverName: "Anil Goje",
                                                    transactionRefId:
                                                        'Testing Upi India Plugin',
                                                    transactionNote: "Test 1",
                                                    amount: 1);
                                          },
                                          child: Column(
                                            children: [
                                              Image.memory(
                                                upiApps![index].icon,
                                                height: height * 0.1,
                                                width: width * 0.15,
                                              ),
                                              Text(
                                                upiApps![index].name,
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .labelMedium,
                                              )
                                            ],
                                          ),
                                        );
                                      },
                                    ));
                              });
                        }
                      : null,
                  child: Container(
                    height: height * 0.076,
                    width: width * 0.50,
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(90),
                        image: const DecorationImage(
                            image: AssetImage('images/offlineImg/PayNow.png'))),
                    alignment: Alignment.center,
                    child: Visibility(
                      visible: false,
                      maintainSize: true,
                      maintainAnimation: true,
                      maintainState: true,
                      maintainSemantics: false,
                      maintainInteractivity: true,
                      child: Consumer<Homecontroller>(
                        builder: (context, val1, val2) {
                          return (_selectedPayOpt == 1)
                              ? GooglePayButton(
                                  height: height * 0.075,
                                  width: width * 0.48,
                                  onPaymentResult:
                                      (Map<String, dynamic>? value) async {
                                    showDialog(
                                        context: context,
                                        barrierColor:
                                            Colors.black12.withOpacity(0.4),
                                        builder: (context) {
                                          return Center(
                                            child: Container(
                                              height: height * 0.1,
                                              width: width * 0.9,
                                              alignment: Alignment.center,
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(40),
                                                  color: Colors.white),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment
                                                        .spaceEvenly,
                                                children: [
                                                  CircularProgressIndicator(
                                                    color: Theme.of(context)
                                                        .primaryColor,
                                                    strokeWidth: 4.5,
                                                  ),
                                                  Text(
                                                    "Processing.........",
                                                    style: Theme.of(context)
                                                        .textTheme
                                                        .bodyMedium,
                                                  )
                                                ],
                                              ),
                                            ),
                                          );
                                        });
                                    await Future.delayed(
                                        const Duration(milliseconds: 2000));
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    //Navigator.of(context).pop();
                                    Navigator.of(context)
                                        .pushReplacement(MaterialPageRoute(
                                            builder: (context) => PaymentStatus(
                                                  transcationData: value?[
                                                      'paymentMethodData'],
                                                )));
                                  },
                                  paymentConfiguration:
                                      PaymentConfiguration.fromJsonString(
                                          defaultGooglePay),
                                  paymentItems: const [
                                    PaymentItem(
                                        amount: '100',
                                        label: 'Total',
                                        status: PaymentItemStatus.final_price)
                                  ],
                                  onPressed: () {
                                    HapticFeedback.heavyImpact();
                                    if (!addressSelected) {
                                      Fluttertoast.showToast(
                                          webShowClose: true,
                                          toastLength: Toast.LENGTH_SHORT,
                                          msg: '! Address not selected',
                                          textColor: Colors.red,
                                          fontSize: 24,
                                          gravity: ToastGravity.BOTTOM,
                                          backgroundColor:
                                              Colors.grey.withOpacity(0.2));
                                    }
                                  },
                                )
                              : const SizedBox();
                        },
                      ),
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    ));
  }
}
//
// const String defaultGooglePay = '''{
//   "provider": "google_pay",
//   "data": {
//     "environment": "TEST",
//     "apiVersion": 2,
//     "apiVersionMinor": 0,
//     "allowedPaymentMethods": [
//       {
//         "type": "CARD",
//         "tokenizationSpecification": {
//           "type": "PAYMENT_GATEWAY",
//           "parameters": {
//             "gateway": "example",
//             "gatewayMerchantId": "gatewayMerchantId"
//           }
//         },
//         "parameters": {
//           "allowedCardNetworks": ["VISA", "MASTERCARD"],
//           "allowedAuthMethods": ["PAN_ONLY", "CRYPTOGRAM_3DS"],
//           "billingAddressRequired": true,
//           "billingAddressParameters": {
//             "format": "FULL",
//             "phoneNumberRequired": true
//           }
//         }
//       }
//     ],
//     "merchantInfo": {
//       "merchantId": "01234567890123456789",
//       "merchantName": "Example Merchant Name"
//     },
//     "transactionInfo": {
//       "countryCode": "US",
//       "currencyCode": "USD"
//     }
//   }
// }''';

const String defaultGooglePay = '''{
  "provider": "google_pay",
  "data": {
    "environment": "TEST",
    "apiVersion": 2,
    "apiVersionMinor": 0,
    "allowedPaymentMethods": [
      {
        "type": "CARD",
        "tokenizationSpecification": {
          "type": "PAYMENT_GATEWAY",
          "parameters": {
            "gateway": "example",
            "gatewayMerchantId": "gatewayMerchantId"
          }
        },
        "parameters": {
          "allowedCardNetworks": ["VISA", "MASTERCARD"],
          "allowedAuthMethods": ["PAN_ONLY", "CRYPTOGRAM_3DS"],
          "billingAddressRequired": true,
          "billingAddressParameters": {
            "format": "FULL",
            "phoneNumberRequired": true
          }
        }
      }
    ],
    "merchantInfo": {
      "merchantId": "01234567890123456789",
      "merchantName": "Example Merchant Name"
    },
    "transactionInfo": {
      "countryCode": "US",
      "currencyCode": "INR"
    }
  }
}''';

const String basicGooglePayIsReadyToPay = '''{
  "apiVersion": 2,
  "apiVersionMinor": 0,
  "allowedPaymentMethods": [
    {
      "type": "CARD",
      "parameters": {
        "allowedAuthMethods": ["PAN_ONLY", "CRYPTOGRAM_3DS"],
        "allowedCardNetworks": ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"]
      }
    }
  ]
}''';

const String basicGooglePayLoadPaymentData = '''{
  "apiVersion": 2,
  "apiVersionMinor": 0,
  "merchantInfo": {
    "merchantName": "Example Merchant"
  },
  "allowedPaymentMethods": [
    {
      "type": "CARD",
      "parameters": {
        "allowedAuthMethods": ["PAN_ONLY", "CRYPTOGRAM_3DS"],
        "allowedCardNetworks": ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"]
      },
      "tokenizationSpecification": {
        "type": "PAYMENT_GATEWAY",
        "parameters": {
          "gateway": "example",
          "gatewayMerchantId": "exampleGatewayMerchantId"
        }
      }
    }
  ],
  "transactionInfo": {
    "totalPriceStatus": "FINAL",
    "totalPrice": "12.34",
    "currencyCode": "USD"
  }
}''';

const String invalidGooglePayIsReadyToPay = '''{
  "apiVersion": 2,
  "apiVersionMinor": 0,
  "allowedPaymentMethods": [
    {
      "type": "CARD",
      "parameters": {}
    }
  ]
}''';

const String invalidGooglePayLoadPaymentData = '''{
  "apiVersion": 2,
  "apiVersionMinor": 0,
  "merchantInfo": {
    "merchantName": "Example Merchant"
  },
  "allowedPaymentMethods": [
    {
      "type": "CARD",
      "parameters": {
        "allowedAuthMethods": ["PAN_ONLY", "CRYPTOGRAM_3DS"],
        "allowedCardNetworks": ["AMEX", "DISCOVER", "INTERAC", "JCB", "MASTERCARD", "VISA"]
      },
      "tokenizationSpecification": {
        "type": "PAYMENT_GATEWAY",
        "parameters": {
          "gateway": "example",
          "gatewayMerchantId": "exampleGatewayMerchantId"
        }
      }
    }
  ],
  "transactionInfo": {
    "totalPriceStatus": "FINAL",
    "totalPrice": "12.34",
    "currencyCode": "USD"
  }
}''';
