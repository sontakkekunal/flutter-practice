import 'package:clothes_store/Controller/OnboardController.dart';
import 'package:clothes_store/Model/OnboardModel.dart';
import 'package:clothes_store/View/commanWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class OnBoardScreen extends StatefulWidget {
  const OnBoardScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _OnBoardScreenState();
  }
}

class _OnBoardScreenState extends State {
  @override
  Widget build(BuildContext context) {
    OnBoardModel onBoardModel =
        Provider.of<OnBoardController>(context).onBoardModel;
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
              onBoardModel.imgPath,
              width: MediaQuery.of(context).size.width,
              //height: MediaQuery.of(context).size.height * 0.57,
              fit: BoxFit.cover,
            ),
            Padding(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.049,
                left: MediaQuery.of(context).size.width * 0.08,
                right: MediaQuery.of(context).size.width * 0.08,
              ),
              child: Column(
                children: [
                  Text(onBoardModel.title,
                      style: Theme.of(context).textTheme.displayLarge),
                  Padding(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.0073,
                        bottom: MediaQuery.of(context).size.height * 0.037),
                    child: Text(onBoardModel.desciption,
                        style: Theme.of(context).textTheme.labelLarge),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      getCont(
                          width: MediaQuery.of(context).size.width * 0.4,
                          title: 'Sign Up',
                          contColor: Colors.transparent,
                          titleColor: Theme.of(context).primaryColorDark,
                          border: Border.all(),
                          context: context),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .pushReplacementNamed('homeScreen');
                          HapticFeedback.heavyImpact();
                        },
                        child: getCont(
                            width: MediaQuery.of(context).size.width * 0.4,
                            title: 'Sign In',
                            contColor: Theme.of(context).primaryColor,
                            titleColor:
                                Theme.of(context).scaffoldBackgroundColor,
                            context: context),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
