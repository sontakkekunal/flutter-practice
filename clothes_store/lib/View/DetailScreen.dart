import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:clothes_store/Controller/HomeController.dart';
import 'package:clothes_store/Model/ClothesInfoModel.dart';
import 'package:clothes_store/Model/OrderModel.dart';
import 'package:clothes_store/View/commanWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'dart:developer';

class DetailScreen extends StatefulWidget {
  int? orderIndex;
  int? listIndex;
  int? clotheIndex;
  DetailScreen({super.key, this.listIndex, this.clotheIndex, this.orderIndex});
  @override
  State<StatefulWidget> createState() {
    return _DetailScreenState();
  }
}

class _DetailScreenState extends State<DetailScreen> {
  void changeSave(ClothesInfoModel clothesInfoModel, bool isEditing) {
    
    if (isEditing) {
      Provider.of<Deliverycontroller>(context, listen: false)
          .changeFav(obj: clothesInfoModel, context: context);
      Provider.of<Deliverycontroller>(context, listen: false).refreash();
    } else {
      clothesInfoModel.isSaved = !clothesInfoModel.isSaved;
      List<OrderModel> orderList =
          Provider.of<Deliverycontroller>(context, listen: false).orderList;
      for (int i = 0; i < orderList.length; i++) {
        if (orderList[i].clothesInfoModel.key == clothesInfoModel.key) {
          orderList[i].clothesInfoModel.isSaved =
              !orderList[i].clothesInfoModel.isSaved;
          break;
        }
      }
      Provider.of<Homecontroller>(context, listen: false).refreah();
    }
  }

  void changeClotheColor(
      ClothesInfoModel clothesInfoModel, int index, bool isEditing) {
    clothesInfoModel.selectedColor = index;
    isEditing
        ? Provider.of<Deliverycontroller>(context, listen: false).refreash()
        : Provider.of<Homecontroller>(context, listen: false).refreah();
  }

  void changeSize(
      ClothesInfoModel clothesInfoModel, int index, bool isEditing) {
    clothesInfoModel.selectedSize = index;
    //Provider.of<Homecontroller>(context, listen: false).refreah();
    isEditing
        ? Provider.of<Deliverycontroller>(context, listen: false).refreash()
        : Provider.of<Homecontroller>(context, listen: false).refreah();
  }

  @override
  Widget build(BuildContext context) {
    int? listIndex = widget.listIndex;
    int? clotheIndex = widget.clotheIndex;
    int? orderIndex = widget.orderIndex;
    bool isediting = (widget.orderIndex == null) ? false : true;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    log('In DetailScreenState');
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: height * 0.051,
            horizontal: width * 0.065,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                        HapticFeedback.heavyImpact();
                      },
                      icon: const Icon(Icons.arrow_back_ios_new_sharp)),
                  Text(
                    'Detail',
                    style: Theme.of(context).textTheme.labelSmall,
                  ),
                  Consumer(
                    builder: (context, value, child) {
                      ClothesInfoModel clothesInfoModel = isediting
                          ? Provider.of<Deliverycontroller>(context)
                              .orderList[orderIndex!]
                              .clothesInfoModel
                          : Provider.of<Homecontroller>(context)
                              .clothesListData[listIndex!]
                              .clothesList[clotheIndex!];
                      return IconButton(
                          onPressed: () {
                            changeSave(clothesInfoModel, isediting);
                          },
                          icon: Icon(clothesInfoModel.isSaved
                              ? Icons.save_rounded
                              : Icons.save_outlined));
                    },
                  )
                ],
              ),
              SizedBox(
                height: height * 0.035,
              ),
              Consumer(
                builder: (context, value, child) {
                  ClothesInfoModel clothesInfoModel = isediting
                      ? Provider.of<Deliverycontroller>(context)
                          .orderList[orderIndex!]
                          .clothesInfoModel
                      : Provider.of<Homecontroller>(context)
                          .clothesListData[listIndex!]
                          .clothesList[clotheIndex!];
                  // ClothesInfoModel clothesInfoModel =
                  //     Provider.of<Homecontroller>(context)
                  //         .clothesListData[listIndex]
                  //         .clothesList[clotheIndex];
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(40),
                    child: Hero(
                      tag:"img-${clothesInfoModel.key}${clothesInfoModel.imgColorList[0]}",
                      child: Image.asset(
                        clothesInfoModel
                            .imgColorList[clothesInfoModel.selectedColor],
                        //height: 400,
                        width: double.maxFinite,
                        fit: BoxFit.cover,
                      ),
                    ),
                  );
                },
              ),
              SizedBox(
                height: height * 0.024,
              ),
              Consumer(
                builder: (context, value, child) {
                  ClothesInfoModel clothesInfoModel = isediting
                      ? Provider.of<Deliverycontroller>(context)
                          .orderList[orderIndex!]
                          .clothesInfoModel
                      : Provider.of<Homecontroller>(context)
                          .clothesListData[listIndex!]
                          .clothesList[clotheIndex!];
                  // ClothesInfoModel clothesInfoModel =
                  //     Provider.of<Homecontroller>(context)
                  //         .clothesListData[listIndex]
                  //         .clothesList[clotheIndex];
                  return Row(
                    children: [
                      Expanded(
                        child: Text(
                          "Premium ${clothesInfoModel.name}",
                          style: Theme.of(context).textTheme.bodyLarge,
                        ),
                      ),
                      SizedBox(
                        height: height * 0.03,
                        width: width * 0.23,
                        child: ListView.separated(
                          separatorBuilder: (context, index) => SizedBox(
                            width: width * 0.016,
                          ),
                          scrollDirection: Axis.horizontal,
                          itemCount: 3,
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: () {
                                changeClotheColor(
                                    clothesInfoModel, index, isediting);
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: clothesInfoModel.selectedColor ==
                                            index
                                        ? Border.all(
                                            color:
                                                Theme.of(context).primaryColor,
                                            width: 2)
                                        : null),
                                child: Image.asset(
                                    clothesInfoModel.colorSelect[index]),
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  );
                },
              ),
              SizedBox(
                height: height * 0.029,
              ),
              Text(
                'Size',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              SizedBox(
                height: height * 0.012,
              ),
              Consumer(
                builder: (context, value, child) {
                  ClothesInfoModel clothesInfoModel = isediting
                      ? Provider.of<Deliverycontroller>(context)
                          .orderList[orderIndex!]
                          .clothesInfoModel
                      : Provider.of<Homecontroller>(context)
                          .clothesListData[listIndex!]
                          .clothesList[clotheIndex!];
                  // ClothesInfoModel clothesInfoModel =
                  //     Provider.of<Homecontroller>(context)
                  //         .clothesListData[listIndex]
                  //         .clothesList[clotheIndex];
                  return SizedBox(
                    height: height * 0.054,
                    width: double.maxFinite,
                    child: ListView.separated(
                      separatorBuilder: (context, index) => SizedBox(
                        width: width * 0.06,
                      ),
                      itemCount: clothesInfoModel.sizeList.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            changeSize(clothesInfoModel, index, isediting);
                          },
                          child: Container(
                            height: height * 0.074,
                            width: width * 0.125,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12),
                                color: (clothesInfoModel.selectedSize == index)
                                    ? Colors.black
                                    : Colors.transparent),
                            alignment: Alignment.center,
                            child: Text(
                              clothesInfoModel.sizeList[index],
                              style: (clothesInfoModel.selectedSize == index)
                                  ? Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(color: Colors.white)
                                  : Theme.of(context)
                                      .textTheme
                                      .bodyMedium!
                                      .copyWith(
                                          color: const Color.fromRGBO(
                                              121, 119, 128, 1)),
                            ),
                          ),
                        );
                      },
                    ),
                  );
                },
              ),
              SizedBox(
                height: height * 0.034,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Consumer(
                    builder: (context, value, child) {
                      ClothesInfoModel clothesInfoModel = isediting
                          ? Provider.of<Deliverycontroller>(context)
                              .orderList[orderIndex!]
                              .clothesInfoModel
                          : Provider.of<Homecontroller>(context)
                              .clothesListData[listIndex!]
                              .clothesList[clotheIndex!];
                      // ClothesInfoModel clothesInfoModel =
                      //     Provider.of<Homecontroller>(context)
                      //         .clothesListData[listIndex]
                      //         .clothesList[clotheIndex];
                      return Text(
                        "\$${clothesInfoModel.getPrice()}",
                        style: Theme.of(context).textTheme.headlineLarge,
                      );
                    },
                  ),
                  (isediting)
                      ? const SizedBox()
                      : GestureDetector(
                          onTap: () {
                            HapticFeedback.heavyImpact();
                            ClothesInfoModel clothesInfoModel = isediting
                                ? Provider.of<Deliverycontroller>(context)
                                    .orderList[orderIndex!]
                                    .clothesInfoModel
                                : Provider.of<Homecontroller>(context,
                                        listen: false)
                                    .clothesListData[listIndex!]
                                    .clothesList[clotheIndex!];
                            // ClothesInfoModel clothesInfoModel =
                            //     Provider.of<Homecontroller>(context, listen: false)
                            //         .clothesListData[listIndex]
                            //         .clothesList[clotheIndex];
                            Provider.of<Deliverycontroller>(context,
                                    listen: false)
                                .addOrder(clothesInfoModel);
                            Provider.of<Deliverycontroller>(context,
                                    listen: false)
                                .priceSetter();
                            Navigator.of(context).pushNamed('cartScreen');
                          },
                          child: getCont(
                              width: 162,
                              title: 'Add To Cart',
                              contColor: Theme.of(context).primaryColor,
                              titleColor:
                                  Theme.of(context).scaffoldBackgroundColor,
                              context: context),
                        )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
