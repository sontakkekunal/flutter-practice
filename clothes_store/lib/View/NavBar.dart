import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

class Navbar extends StatefulWidget {
  int currentIndex;
  Navbar({super.key, required this.currentIndex});

  @override
  State<StatefulWidget> createState() {
    return _NavBarState();
  }
}

class _NavBarState extends State<Navbar> {
  bool isBadget(
      {required int index,
      required int selectedIndex,
      required Deliverycontroller value}) {
    if (index != selectedIndex) {
      if (index == 0 && value.orderList.isNotEmpty) {
        return true;
      } else if (index == 1) {
        return false;
      } else if (index == 2 && value.orderList.isNotEmpty) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    List<Map<String, String>> bottomList = [
      {'name': 'Home', 'imgPath': 'images/offlineImg/HomeIcon.png'},
      {'name': 'Search', 'imgPath': 'images/offlineImg/searchImg.png'},
      {'name': 'Card', 'imgPath': 'images/offlineImg/bagIcon.png'},
      {'name': 'Setting', 'imgPath': 'images/offlineImg/settingsIcon.png'}
    ];
    int seletedIndex = widget.currentIndex;
    return NavigationBar(
        indicatorColor: Theme.of(context).primaryColor,
        selectedIndex: seletedIndex,

        //backgroundColor: Colors.black,
        backgroundColor: Colors.transparent,
        onDestinationSelected: (int value) {
          if (seletedIndex != value && value == 0) {
            Navigator.of(context).pushNamed('homeScreen');
          } else if (seletedIndex != value && value == 2) {
            Navigator.of(context).pushNamed('cartScreen');
            HapticFeedback.heavyImpact();
          }
        },
        height: MediaQuery.of(context).size.height * 0.09,
        destinations: List.generate(4, (index) {
          return GestureDetector(
            onTap: () {
              if (seletedIndex != index && index == 0) {
                Navigator.of(context).pushNamed('homeScreen');
              } else if (seletedIndex != index && index == 2) {
                Navigator.of(context).pushNamed('cartScreen');
              }
            },
            child: Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.07),
              child: SizedBox(
                  child: Stack(
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.asset(bottomList[index]['imgPath']!),
                      const SizedBox(
                        height: 3,
                      ),
                      Text(
                        bottomList[index]['name']!,
                        style: Theme.of(context)
                            .textTheme
                            .displaySmall!
                            .copyWith(
                                color: (index == seletedIndex)
                                    ? Theme.of(context).primaryColor
                                    : null),
                      )
                    ],
                  ),
                  Positioned(
                      top: 11,
                      right: 20,
                      child: Consumer<Deliverycontroller>(
                        builder: (context, Deliverycontroller value, child) {
                          return Container(
                            height: MediaQuery.of(context).size.height * 0.006,
                            width: MediaQuery.of(context).size.width * 0.013,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: isBadget(
                                        index: index,
                                        selectedIndex: seletedIndex,
                                        value: value)
                                    ? Colors.red
                                    : null),
                          );
                        },
                      ))
                ],
              )),
            ),
          );
        }));
  }
}
