import 'package:clothes_store/Controller/DeliveryController.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

Widget getCont(
    {required double width,
    required String title,
    required Color contColor,
    Border? border,
    required Color titleColor,
    required BuildContext context}) {
  double height = MediaQuery.of(context).size.height * 0.076;
  return Container(
    height: height,
    width: width,
    decoration: BoxDecoration(
      border: border,
      borderRadius: BorderRadius.circular(90),
      color: contColor,
    ),
    alignment: Alignment.center,
    child: Text(
      title,
      style: GoogleFonts.imprima(
          fontSize: 18, fontWeight: FontWeight.w400, color: titleColor),
    ),
  );
}

Widget getBackBar({required String title, required BuildContext context}) =>
    Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        IconButton(
            onPressed: () {
              HapticFeedback.heavyImpact();
              Navigator.of(context).pop();
            },
            icon: const Icon(Icons.arrow_back_ios_new_sharp)),
        const Spacer(),
        Text(
          title,
          style: Theme.of(context).textTheme.labelSmall,
        ),
        const Spacer(),
      ],
    );
Widget getTotalPriceDetails(
    {required BuildContext context, required double height}) {
  return Column(
    children: [
      Consumer(
        builder: (context, value, child) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Total Items (${Provider.of<Deliverycontroller>(context).itemCount})',
                style: Theme.of(context).textTheme.labelMedium,
              ),
              Text(
                '\$${Provider.of<Deliverycontroller>(context).totalItemPrice}',
                style: Theme.of(context).textTheme.displayMedium,
              )
            ],
          );
        },
      ),
      SizedBox(
        height: height * 0.024,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Standard Delivery',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          Consumer(
            builder: (context, value, child) {
              return Text(
                '\$${Provider.of<Deliverycontroller>(context).standardDeliveryPrice}',
                style: Theme.of(context).textTheme.displayMedium,
              );
            },
          )
        ],
      ),
      SizedBox(
        height: height * 0.024,
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Total Payment',
            style: Theme.of(context).textTheme.labelMedium,
          ),
          Consumer(
            builder: (context, value, child) {
              return Text(
                '\$${Provider.of<Deliverycontroller>(context).totalPayment}',
                style: Theme.of(context).textTheme.displayMedium,
              );
            },
          )
        ],
      ),
    ],
  );
}
