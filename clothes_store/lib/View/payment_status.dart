import 'dart:developer';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

import '../Controller/DeliveryController.dart';
import 'commanWidget.dart';

class PaymentStatus extends StatefulWidget {
  Map<String, dynamic> transcationData;
  PaymentStatus({super.key, required this.transcationData});

  @override
  State<StatefulWidget> createState() {
    return _PaymentStatusState();
  }
}

class _PaymentStatusState extends State<PaymentStatus> {
  @override
  void didChangeDependencies() {
    Provider.of<Deliverycontroller>(context, listen: false).orderList.clear();
    Provider.of<Deliverycontroller>(context, listen: false).priceSetter();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> transcationData = widget.transcationData;
    log(transcationData.toString());
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Lottie.asset('assets/tickAnimation.json', repeat: true),
            ],
          ),
          Text("Card detail: ${transcationData['description']}"),
          Text(
              "Address: ${transcationData['info']['billingAddress']['address1']}"),
          Text(
              "Administrative Area: ${transcationData['info']['billingAddress']['administrativeArea']}"),
          Text(
              "Country Code: ${transcationData['info']['billingAddress']['countryCode']}"),
          Text(
              "Locality: ${transcationData['info']['billingAddress']['locality']}"),
          Text("Name: ${transcationData['info']['billingAddress']['name']}"),
          Text(
              "Phone Number: ${transcationData['info']['billingAddress']['phoneNumber']}"),
          Text(
              "Postal Code: ${transcationData['info']['billingAddress']['postalCode']}"),
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: getCont(
                width: 162,
                title: 'Ok',
                contColor: Theme.of(context).primaryColor,
                titleColor: Theme.of(context).scaffoldBackgroundColor,
                context: context),
          ),
        ],
      ),
    );
  }
}
