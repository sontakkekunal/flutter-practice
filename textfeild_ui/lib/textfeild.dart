import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  final String title;
  const MyHomePage({super.key, required this.title});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _namesTextEditingController =
      TextEditingController();
  final FocusNode _nameFocusNode = FocusNode();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text(widget.title),
      ),
      body: Column(
        children: [
          const SizedBox(
            height: 20.0,
          ),
          TextField(
            //textDirection: TextDirection.rtl,
            maxLength: 10,
            textAlign: TextAlign.center,
            style: const TextStyle(
                color: Colors.blue, fontSize: 14, fontWeight: FontWeight.w500),
            textAlignVertical: TextAlignVertical.top,
            keyboardType: TextInputType.name,
            focusNode: _nameFocusNode,
            keyboardAppearance: Brightness.dark,
            textInputAction: TextInputAction.done,
            onChanged: (val) {
              print(_namesTextEditingController.text);
            },
            onSubmitted: (val) {
              print(_namesTextEditingController);
            },
            decoration: InputDecoration(
                icon: const Icon(Icons.emoji_emotions_rounded),
                iconColor: Colors.brown,
                labelText: "Enter name",
                floatingLabelStyle: const TextStyle(fontSize: 20),
                hintTextDirection: TextDirection.rtl,
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: const BorderSide(color: Colors.pink)),
                enabledBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20),
                  borderSide: const BorderSide(color: Colors.blue),
                )),

            //readOnly: true,
            //showCursor: false,
            //obscureText: true,  //for hiding text in textfeild
            cursorColor: Colors.amber,
            cursorWidth: 3,
            controller: _namesTextEditingController,
            autofocus: true,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Text("Add"),
      ),
    );
  }
}
