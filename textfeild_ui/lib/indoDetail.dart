import 'dart:ui';

import 'package:flutter/material.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  static bool flag = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
          appBarTheme: AppBarTheme(
              shadowColor: Colors.blue,
              backgroundColor: (flag == false)
                  ? Color.fromARGB(255, 237, 218, 45)
                  : Colors.black,
              foregroundColor: (flag == false) ? Colors.black : Colors.white,
              toolbarHeight: 85,
              titleTextStyle:
                  const TextStyle(fontWeight: FontWeight.w600, fontSize: 30),
              centerTitle: true),
          colorScheme: ColorScheme.fromSeed(
              background: (flag == false)
                  ? Color.fromARGB(255, 204, 200, 200)
                  : Color.fromARGB(255, 62, 62, 62),
              seedColor: Colors.purple,
              primary: Colors.blue),
          /*
            textTheme: const TextTheme(
                displayLarge: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w500,
                    fontStyle: FontStyle.italic,
                    color: Colors.black26))
                    */
        ),
        home: const DreamCompany());
  }
}

class DreamCompany extends StatefulWidget {
  const DreamCompany({super.key});
  @override
  State createState() => _DreamCompanyState();
}

class _DreamCompanyState extends State<DreamCompany> {
  bool flag = false;
  final List<TextEditingController> _textEditingController = [
    TextEditingController(),
    TextEditingController(),
    TextEditingController(),
    TextEditingController()
  ];
  List<String> textFeildList = [
    "Enter name",
    "Enter Company Name",
    "Enter Company Location",
    "Enter mobile number"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: (flag == false)
          ? Theme.of(context).colorScheme.background
          : Color.fromARGB(255, 36, 35, 35),
      appBar: AppBar(
        leading: IconButton(
          onPressed: () {
            flag = !flag;
            //MyApp.flag = !MyApp.flag;
            setState(() {});
          },
          icon: (flag == false)
              ? const Icon(Icons.sunny)
              : const Icon(Icons.shield_moon_rounded),
        ),
        backgroundColor: (flag == false) ? null : Colors.black,
        foregroundColor: (flag == false) ? null : Colors.white,
        title: Text(
          "Dream Company",
          style: Theme.of(context).appBarTheme.titleTextStyle,
        ),
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                padding: const EdgeInsets.all(25),
                foregroundDecoration: const BoxDecoration(),
                decoration: BoxDecoration(
                    boxShadow: const [
                      BoxShadow(
                          color: Color.fromARGB(255, 241, 188, 205),
                          offset: Offset(-10, 10),
                          blurRadius: 8)
                    ],
                    color: (flag == false)
                        ? Color.fromARGB(255, 226, 223, 210)
                        : Color.fromARGB(255, 89, 88, 88),
                    borderRadius: BorderRadius.circular(20)),
                height: 420,
                width: 350,
                child: ListView.separated(
                  itemCount: textFeildList.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return const SizedBox(
                      height: 20,
                    );
                  },
                  itemBuilder: (BuildContext context, int index) {
                    return TextField(
                      cursorColor: Colors.brown,
                      cursorHeight: 45,
                      cursorWidth: 5,
                      maxLength: (index == 0)
                          ? 20
                          : (index == 1)
                              ? 15
                              : (index == 2)
                                  ? 17
                                  : 10,
                      onTap: () {},
                      keyboardType: (index != 3)
                          ? TextInputType.name
                          : TextInputType.number,
                      keyboardAppearance: Brightness.dark,
                      controller: _textEditingController[index],
                      //focusNode: _focusNode,
                      textAlign: TextAlign.center,
                      textInputAction: TextInputAction.done,
                      decoration: InputDecoration(
                          focusColor: const Color.fromARGB(255, 179, 212, 141),
                          hintText: textFeildList[index],
                          hintStyle: TextStyle(
                              color: (flag == false)
                                  ? Colors.black
                                  : Colors.white),
                          labelText: textFeildList[index],
                          floatingLabelStyle: TextStyle(
                              color: (flag == false) ? null : Colors.white),
                          labelStyle: TextStyle(
                              fontSize: 20,
                              color: (flag == false)
                                  ? const Color.fromARGB(255, 101, 98, 98)
                                  : Color.fromARGB(255, 155, 149, 149)),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 2)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide(
                                  color: (index == 3)
                                      ? (_textEditingController[index]
                                                  .text
                                                  .length ==
                                              10)
                                          ? (Colors.green)
                                          : Colors.red
                                      : (_textEditingController[index]
                                                  .text
                                                  .length >
                                              3)
                                          ? Colors.green
                                          : Colors.red))),
                      onSubmitted: (String val) {
                        setState(() {});
                      },
                    );
                  },
                )),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(90, 60)),
                    onPressed: () {
                      setState(() {});
                    },
                    child: const Text(
                      "Submit",
                      style: TextStyle(fontSize: 18),
                    )),
                const SizedBox(
                  width: 15,
                ),
                ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        minimumSize: const Size(90, 60)),
                    onPressed: () {
                      for (int i = 0; i < _textEditingController.length; i++) {
                        _textEditingController[i].clear();
                      }
                      setState(() {});
                    },
                    child: const Text(
                      "clearAll",
                      style: TextStyle(fontSize: 18),
                    )),
              ],
            ),
            Container(
              height: 200,
              width: 350,
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: (flag == false)
                    ? Color.fromARGB(255, 220, 241, 221)
                    : Color.fromARGB(255, 116, 111, 111),
                boxShadow: const [
                  BoxShadow(
                      color: Color.fromARGB(255, 241, 188, 205),
                      offset: Offset(10, 10),
                      blurRadius: 8)
                ],
              ),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    const Text("Dream Company",
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.w700)),
                    const SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Name: ${_textEditingController[0].text}",
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text("Dream company: ${_textEditingController[1].text}",
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500)),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Location: ${_textEditingController[2].text}",
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    Text(
                      "Phone: ${_textEditingController[3].text}",
                      style: const TextStyle(
                          fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
