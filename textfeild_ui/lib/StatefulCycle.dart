import 'dart:io';

import 'package:flutter/material.dart';

class StateFulMethodX extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(home: StateFulMethod());
  }
}

class StateFulMethod extends StatefulWidget {
  const StateFulMethod({super.key});
  static bool flag = true;
  @override
  State createState() {
    print("In createState");
    return (flag) ? _StateFulMethodState() : _StateFulMethodState2();
  }
}

class _StateFulMethodState extends State<StateFulMethod> {
  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      print("Value: ${_controller.text}");
    });
    print("In initState");
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    print("In didChangeDependencies State");
  }

  bool flag = true;
  bool flag2 = true;
  @override
  void deactivate() {
    super.deactivate();
    print("In deactivate State");
  }

  @override
  void dispose() {
    super.dispose();
    print("In dispose State");
  }

  final TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    print("In build State");
    return Scaffold(
      appBar: AppBar(
        title: const Text("StateFulMethod"),
        centerTitle: true,
        titleSpacing: 0,
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Column(
          children: [
            TextField(
              controller: _controller,
              decoration: const InputDecoration(hintText: "Enter something"),
            ),
            ElevatedButton(
                onPressed: () {
                  flag = !flag;
                  setState(() {
                    StateFulMethod.flag = false;
                    print("In setState");
                  });
                },
                child: const Text("Button 1")),
            ElevatedButton(
                onPressed: () {
                  flag2 = false;

                  didUpdateWidget(StateFulMethod());
                  setState(() {});
                },
                child: const Text("Button 2")),
            (flag) ? const Text("flag is true") : const Text("flag is false"),
          ],
        ),
      ),
    );
  }
}

class _StateFulMethodState2 extends State<StateFulMethod> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Seconf title"),
      ),
    );
  }
}
