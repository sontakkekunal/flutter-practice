import 'package:flutter/material.dart';

class DreamComApp extends StatelessWidget {
  const DreamComApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: DreamCompany());
  }
}

class DreamCompany extends StatefulWidget {
  @override
  State createState() => _DreamCompanyState();
}

class _DreamCompanyState extends State<DreamCompany> {
  List<Map> valList = [];
  List<Map> dreamList = [
    {"name": "Name", "controller": TextEditingController()},
    {"name": "Dream company", "controller": TextEditingController()},
    {"name": "Location", "controller": TextEditingController()},
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Dream Company"),
        centerTitle: true,
      ),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(
                height: 10,
              ),
              Container(
                  height: 300,
                  width: 300,
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: const Color.fromARGB(255, 178, 205, 217)),
                  child: ListView.separated(
                    itemCount: dreamList.length,
                    separatorBuilder: (BuildContext context, int index) {
                      return const SizedBox(
                        height: 20,
                      );
                    },
                    itemBuilder: (BuildContext context, int index) {
                      return TextField(
                        cursorColor: Colors.brown,
                        cursorHeight: 45,
                        cursorWidth: 5,
                        maxLength: (index == 0)
                            ? 20
                            : (index == 1)
                                ? 15
                                : (index == 2)
                                    ? 17
                                    : 10,
                        onTap: () {},
                        keyboardType: (index != 3)
                            ? TextInputType.name
                            : TextInputType.number,
                        keyboardAppearance: Brightness.dark,
                        controller: dreamList[index]["controller"],
                        //focusNode: _focusNode,
                        textAlign: TextAlign.center,
                        textInputAction: TextInputAction.done,
                        decoration: InputDecoration(
                          focusColor: const Color.fromARGB(255, 179, 212, 141),
                          hintText: "Enter ${dreamList[index]["name"]}",
                          labelText: dreamList[index]["name"],
                          labelStyle: const TextStyle(
                            fontSize: 20,
                          ),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                  color: Colors.blue, width: 2)),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: BorderSide(color: Colors.amber)),
                        ),
                        onSubmitted: (String val) {
                          valList.add({
                            "name": dreamList[0]["controller"].text,
                            "compy": dreamList[1]["controller"].text,
                            "location": dreamList[2]["controller"].text,
                          });
                          setState(() {});
                        },
                      );
                    },
                  )),
              const SizedBox(
                height: 10,
              ),
              Container(
                height: 50,
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.green,
                    borderRadius: BorderRadius.circular(20)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    ElevatedButton(
                        onPressed: () {
                          valList.add({
                            "name": dreamList[0]["controller"].text,
                            "compy": dreamList[1]["controller"].text,
                            "location": dreamList[2]["controller"].text,
                          });
                          setState(() {});
                        },
                        child: Text("Submit")),
                    ElevatedButton(
                        onPressed: () {
                          dreamList[0]["controller"].clear();
                          dreamList[1]["controller"].clear();
                          dreamList[2]["controller"].clear();
                          setState(() {});
                        },
                        child: Text("ClearAll"))
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                height: 250,
                width: 300,
                decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(20)),
                child: ListView.builder(
                  itemCount: valList.length,
                  itemBuilder: (context, index) {
                    return Container(
                      height: 100,
                      width: 200,
                      margin: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(20)),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text("Name: ${valList[index]["name"]}"),
                          Text("Company: ${valList[index]["compy"]}"),
                          Text("Location: ${valList[index]["location"]}")
                        ],
                      ),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
