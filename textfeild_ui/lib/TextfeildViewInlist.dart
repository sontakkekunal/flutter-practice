import 'package:flutter/material.dart';

class TextFeildAsList extends StatefulWidget {
  const TextFeildAsList({super.key});
  @override
  State createState() => _TextFeildState();
}

class _TextFeildState extends State<TextFeildAsList> {
  final FocusNode _focusNode = FocusNode();
  final TextEditingController _textEditingController = TextEditingController();
  List<String> str = [];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "ListAdder App",
          ),
          centerTitle: true,
        ),
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              TextField(
                cursorColor: Colors.brown,
                cursorHeight: 45,
                cursorWidth: 5,
                keyboardAppearance: Brightness.dark,
                keyboardType: TextInputType.text,
                focusNode: _focusNode,
                autocorrect: true,
                autofocus: true,
                controller: _textEditingController,
                textInputAction: TextInputAction.done,
                textAlign: TextAlign.center,
                decoration: InputDecoration(
                    hintTextDirection: TextDirection.ltr,
                    floatingLabelAlignment: FloatingLabelAlignment.center,
                    focusColor: Colors.red,
                    hoverColor: Colors.yellow,
                    labelText: "Enter name",
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(color: Colors.pink)),
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(20),
                        borderSide: const BorderSide(color: Colors.cyan))),
                onSubmitted: (val) {
                  str.add(_textEditingController.text);
                  setState(() {});
                },
              ),
              const SizedBox(
                height: 10,
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.blue),
                    color: Color.fromARGB(255, 218, 217, 217)),
                height: 370,
                width: double.infinity,
                child: ListView.builder(
                  scrollDirection: Axis.vertical,
                  itemCount: str.length,
                  itemBuilder: (context, index) {
                    return Container(
                        height: 50,
                        width: 50,
                        margin: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            color: Colors.cyan),
                        child: Center(
                          child: Text("${index + 1}. ${str[index]}",
                              textAlign: TextAlign.center,
                              style: const TextStyle(fontSize: 20)),
                        ));
                  },
                ),
              )
            ],
          ),
        ));
  }
}
