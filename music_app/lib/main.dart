import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:music_app/view/home.dart';
import 'package:provider/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //deleteDatabase(join(await getDatabasesPath(), 'MusicPlayer.db'));
  ErrorWidget.builder = (FlutterErrorDetails details) {
    return SliderTheme(
      data: const SliderThemeData(
          thumbShape: RoundSliderThumbShape(
              pressedElevation: 0, enabledThumbRadius: 0, elevation: 0)),
      child: Slider(
        value: 0,
        onChanged: (value) {},
        allowedInteraction: SliderInteraction.slideOnly,
        inactiveColor: const Color.fromRGBO(217, 217, 217, 0.19),
      ),
    );
  };
  await fetchDd();
  tempVal1 = await getInSong(tableName: 'SongInfo');
  tempVal2 = await getPlayList();
  tempVal3 = await getInSong(tableName: 'LocalSong');
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => MusicController()),
        ChangeNotifierProvider(create: (context) => OperationalController())
      ],
      child: MaterialApp(
          theme: ThemeData(
              primaryColor: const Color.fromRGBO(255, 46, 0, 1),
              secondaryHeaderColor: const Color.fromRGBO(230, 154, 21, 1),
              scaffoldBackgroundColor: const Color.fromRGBO(24, 24, 24, 1),
              textTheme: TextTheme(
                  headlineMedium: GoogleFonts.inter(
                      color: const Color.fromRGBO(230, 154, 21, 1),
                      fontSize: 24,
                      fontWeight: FontWeight.w600),
                  labelMedium: GoogleFonts.inter(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(255, 255, 255, 1)),
                  bodyMedium: GoogleFonts.inter(
                      fontSize: 20,
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(255, 46, 0, 1)),
                  bodySmall: GoogleFonts.inter(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(132, 125, 125, 1)),
                  displaySmall: GoogleFonts.inter(
                      fontSize: 12,
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(203, 200, 200, 1)),
                  labelLarge: GoogleFonts.inter(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 36,
                      fontWeight: FontWeight.w600),
                  labelSmall: GoogleFonts.inter(
                      fontSize: 14,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(248, 162, 69, 1)),
                  displayLarge: GoogleFonts.inter(
                      color: const Color.fromRGBO(255, 255, 255, 1),
                      fontSize: 26,
                      fontWeight: FontWeight.w600),
                  displayMedium: GoogleFonts.inter(
                      fontSize: 20, fontWeight: FontWeight.w600),
                  titleSmall: GoogleFonts.inter(
                      fontSize: 13,
                      fontWeight: FontWeight.w400,
                      color: const Color.fromRGBO(132, 125, 125, 1)),
                  titleMedium: GoogleFonts.inter(
                      fontSize: 17,
                      fontWeight: FontWeight.w600,
                      color: const Color.fromRGBO(203, 200, 200, 1)),
                  titleLarge: GoogleFonts.inter(fontSize: 14, fontWeight: FontWeight.w600, color: const Color.fromRGBO(203, 200, 200, 1)))),
          debugShowCheckedModeBanner: false,
          home: const HomePage()),
    );
  }
}
