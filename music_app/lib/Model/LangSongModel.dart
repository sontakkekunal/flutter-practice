
import 'package:music_app/Model/SongInfoModel.dart';

class LangSongModel {
  String name;
  String imgPath;
  List<SongInfoModel> songList;
  LangSongModel(
      {required this.name, required this.imgPath, required this.songList});
}
