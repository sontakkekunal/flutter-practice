class SongInfoModel {
  int? key;
  String imgPath;
  String songPath;
  String artistName;
  List<String> inPlayList ;
  String lang;
  bool isafav;
  String name;
  bool islocal;
  int year;
  SongInfoModel(
      {required this.imgPath,
      required this.songPath,
      required this.artistName,
      required this.name,
        this.key,
      this.inPlayList=const [],
      this.isafav=false,
      this.islocal = false,
      required this.lang,
      required this.year});

  int getO1(bool val) {
    if (val == true) {
      return 1;
    } else {
      return 0;
    }
  }

  Map<String, dynamic> getMap() {
    return {
      'imgPath': imgPath,
      'songPath': songPath,
      'artistName': artistName,
      'inPlayList': getListAsString(),
      'lang': lang,
      'isafav': getO1(isafav),
      'name': name,
      'islocal': getO1(islocal),
      'year': year,
    };
  }

  String getListAsString() {
    StringBuffer sb = StringBuffer('');
    for (int i = 0; i < inPlayList.length; i++) {
      sb.write(inPlayList[i]);
      if (i != inPlayList.length - 1) {
        sb.write('-');
      }
    }
    return sb.toString();
  }
}
