import 'package:music_app/Model/SongInfoModel.dart';

class PlayListModel {
  int? key;
  String name;

  String? imgPath;
  String? filePath;
  List<SongInfoModel> songList;
  PlayListModel(
      {required this.name,
      this.imgPath,
        this.key,
      this.filePath,
      required this.songList});
  Map<String, dynamic> getMap() {
    return {
      'name': name,
      'imgPath': imgPath,
      'filePath': filePath,
    };
  }
}
