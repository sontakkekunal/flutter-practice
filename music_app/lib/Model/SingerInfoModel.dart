
import 'package:music_app/Model/SongInfoModel.dart';

class SingerInfoModel{
  String imgPath;
  String name;
  String location;
  int year;
  List<SongInfoModel> songList;
  SingerInfoModel(
      {required this.imgPath,
      required this.name,
      required this.location,
      required this.songList,
      required this.year});
}
