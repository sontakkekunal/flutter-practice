import 'package:flutter/material.dart';
import 'package:music_app/Model/LangSongModel.dart';
import 'package:music_app/Model/PlayListModel.dart';
import 'package:music_app/Model/SingerInfoModel.dart';
import 'package:music_app/Model/SongInfoModel.dart';
import 'package:music_app/view/comman_widget.dart';

class MusicController extends ChangeNotifier {
  void refreash() {
    notifyListeners();
  }

  List<PlayListModel> userPlayList = [];
  List<SongInfoModel> localSongList = [];

  List<SongInfoModel> favSong = [];
  List<LangSongModel> langSong = [
    LangSongModel(name: 'Marathi', imgPath: 'assets/ma.gif', songList: []),
    LangSongModel(name: 'Hindi', imgPath: 'assets/hi.gif', songList: []),
    LangSongModel(name: 'English', imgPath: 'assets/e.gif', songList: [])
  ];
  List<SingerInfoModel> singerList = [
    SingerInfoModel(
        imgPath: 'assets/images/arijitSingImg.jpg',
        name: 'Arijit Sing',
        location: 'Mumbai',
        songList: [],
        year: 2009),
    SingerInfoModel(
        imgPath: 'assets/images/vishalImg.jpg',
        name: 'Vishal',
        location: 'Delhi',
        songList: [],
        year: 2016),
    SingerInfoModel(
        imgPath: 'assets/images/shreyaGhospalImg.jpg',
        name: 'Shreya Ghospal',
        location: 'Mumbai',
        songList: [],
        year: 1998),
    SingerInfoModel(
        imgPath: 'assets/images/sambataImg.jpg',
        name: 'Sambata',
        location: 'Pune',
        songList: [],
        year: 2018),
    SingerInfoModel(
        imgPath: 'assets/images/avadhoot_Gupte.jpg',
        name: 'Avdhoot Gupte',
        location: 'Pune',
        songList: [],
        year: 2013),
    SingerInfoModel(
        imgPath: 'assets/images/justin_bieber.jpg',
        name: 'Justin Bieber',
        location: 'Canada',
        songList: [],
        year: 2009),
    SingerInfoModel(
        imgPath: 'assets/images/dj_snakeImg.jpg',
        name: 'DJ Snake',
        location: 'Paris',
        songList: [],
        year: 2011),
  ];

  List<SongInfoModel> songInfoList = [];

  MusicController() {
    songInfoList = tempVal1!;
    userPlayList = tempVal2!;
    localSongList = tempVal3!;
    songInfoList.sort((a, b) => a.name.compareTo(b.name));
    for (int i = 0; i < songInfoList.length; i++) {
      List<String> playList = songInfoList[i].inPlayList;

      for (int z = 0; z < playList.length; z++) {
        for (int w = 0; w < userPlayList.length; w++) {
          if (userPlayList[w].name == playList[z]) {
            userPlayList[w].songList.add(songInfoList[i]);
            break;
          }
        }
      }
      if (songInfoList[i].isafav) {
        favSong.add(songInfoList[i]);
      }
      for (int j = 0; j < singerList.length; j++) {
        if (songInfoList[i].artistName.contains(singerList[j].name)) {
          singerList[j].songList.add(songInfoList[i]);
        }
      }
      for (int j = 0; j < langSong.length; j++) {
        if (songInfoList[i].lang.contains(langSong[j].name)) {
          langSong[j].songList.add(songInfoList[i]);
        }
      }
    }
    for (int i = 0; i < localSongList.length; i++) {
      if (localSongList[i].isafav) {
        favSong.add(localSongList[i]);
      }
    }
  }
}
