// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/view/favorites_screen.dart';
import 'package:music_app/view/library_screen.dart';
import 'package:music_app/view/local_music.dart';
import 'package:music_app/view/search_page.dart';
import 'package:music_app/view/gallery_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class Navbar extends StatefulWidget {
  final int index;
  const Navbar({super.key, required this.index});
  @override
  State<StatefulWidget> createState() {
    return _NavbarState();
  }
}

class _NavbarState extends State<Navbar> {
  @override
  Widget build(BuildContext context) {
    int i = widget.index;
    MusicController musicController =
        Provider.of<MusicController>(context, listen: false);
    return BottomNavigationBar(
        fixedColor: Colors.white,
        iconSize: 28,
        selectedLabelStyle: GoogleFonts.abel(
            fontWeight: FontWeight.w400, fontSize: 12, color: Colors.white),
        unselectedLabelStyle: GoogleFonts.abel(
            fontWeight: FontWeight.w400, fontSize: 12, color: Colors.white),
        unselectedItemColor: Colors.white,
        currentIndex: widget.index,
        onTap: (index) {
          if (index == 0 && i != 0) {
            Navigator.of(context)
                .push(PageTransition(
                    duration: const Duration(milliseconds: 225),
                    child: const FavoritesScreen(),
                    type: PageTransitionType.fade))
                .then((val) {
              musicController.refreash();
            });
          } else if (index == 1 && i != 1) {
            Navigator.of(context)
                .push(PageTransition(
                    duration: const Duration(milliseconds: 225),
                    child: const SearchPage(),
                    type: PageTransitionType.fade))
                .then((val) {
              musicController.refreash();
            });
          } else if (index == 2 && i != 2) {
            Navigator.of(context)
                .push(PageTransition(
                    duration: const Duration(milliseconds: 225),
                    child: const GalleryPage(),
                    type: PageTransitionType.fade))
                .then((val) {
              musicController.refreash();
            });
          } else if (index == 3 && i != 3) {
            Navigator.of(context)
                .push(PageTransition(
                    duration: const Duration(milliseconds: 225),
                    child: const LibraryScreen(),
                    type: PageTransitionType.fade))
                .then((val) {
              musicController.refreash();
            });
          } else if (index == 4 && i != 4) {
            Navigator.of(context)
                .push(PageTransition(
                    duration: const Duration(milliseconds: 225),
                    child: const LocalMusic(),
                    type: PageTransitionType.fade))
                .then((val) {
              musicController.refreash();
            });
          }
        },
        items: [
          BottomNavigationBarItem(
            activeIcon: Icon(Icons.favorite_border_rounded,
                color: Theme.of(context).secondaryHeaderColor),
            icon:
                const Icon(Icons.favorite_border_rounded, color: Colors.white),
            label: 'Favorite',
            backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
          ),
          BottomNavigationBarItem(
              backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
              activeIcon: Icon(Icons.search,
                  color: Theme.of(context).secondaryHeaderColor),
              icon: const Icon(Icons.search, color: Colors.white),
              label: 'Search'),
          BottomNavigationBarItem(
              backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
              activeIcon: Icon(Icons.home_outlined,
                  color: Theme.of(context).secondaryHeaderColor),
              icon: const Icon(Icons.home_outlined, color: Colors.white),
              label: 'Home'),
          BottomNavigationBarItem(
              backgroundColor: const Color.fromRGBO(19, 19, 19, 1),
              activeIcon: Icon(Icons.card_travel_rounded,
                  color: Theme.of(context).secondaryHeaderColor),
              icon: const Icon(Icons.card_travel_rounded, color: Colors.white),
              label: 'Libraries'),
          BottomNavigationBarItem(
              activeIcon: Icon(Icons.local_library_rounded,
                  color: Theme.of(context).secondaryHeaderColor),
              icon:
                  const Icon(Icons.local_library_rounded, color: Colors.white),
              label: 'Local Songs',
              backgroundColor: const Color.fromRGBO(19, 19, 19, 1)),
        ]);
  }
}
