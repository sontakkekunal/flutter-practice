// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'dart:typed_data';
import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:like_button/like_button.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/view/scale_up_down_animation.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:perfect_volume_control/perfect_volume_control.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';

import '../Model/SongInfoModel.dart';
import 'dart:developer' as de;

// ignore: must_be_immutable
class PlayerPage extends StatefulWidget {
  List<SongInfoModel> songList;

  int activeIndex;
  PlayerPage({super.key, required this.songList, required this.activeIndex});

  @override
  State<StatefulWidget> createState() {
    return _PlayerPageState();
  }
}

class _PlayerPageState extends State<PlayerPage> with TickerProviderStateMixin {
  bool play = true;
  double currentVol = 0.2;

  late int suffleIndex;
  int likeChangeIndex = 0;

  final AudioPlayer player = AudioPlayer();
  late AnimationController _animationController;
  Duration duration = Duration.zero;
  Duration position = Duration.zero;
  bool playAgain = false;
  bool isLiked = false;
  int listRepeatCount = 0;
  int suffleCount = 0;
  double volOpacity = 0;
  bool isForward = true;
  List<SongInfoModel> songPlayedList = [];
  int songPlayedIndex = -1;

  IconData getVolIcon() {
    if (currentVol == 0.0) {
      return Icons.volume_off_rounded;
    } else if (currentVol > 0.0 && currentVol < 0.1) {
      return Icons.volume_mute_rounded;
    } else if (currentVol >= 0.1 && currentVol < 0.5) {
      return Icons.volume_down_rounded;
    } else {
      return Icons.volume_up_rounded;
    }
  }

  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    // player.play(AssetSource(_songInfoModel!.songPath));
    player.onDurationChanged.listen((newDuration) {
      duration = newDuration;
    });
    player.onPositionChanged.listen((newPosition) {
      position = newPosition;
      Provider.of<MusicController>(context, listen: false).refreash();
    });

    player.onPlayerStateChanged.listen((state) {
      Provider.of<OperationalController>(context, listen: false).refreash();
    });
    player.onPlayerComplete.listen((state) {
      forwardSong(isUsercall: false);
    });
    PerfectVolumeControl.hideUI = true;
    Future.delayed(Duration.zero, () async {
      currentVol = await PerfectVolumeControl.getVolume();
      Provider.of<OperationalController>(context, listen: false).refreash();
    });

    PerfectVolumeControl.stream.listen((vol) {
      currentVol = vol;
      Provider.of<OperationalController>(context, listen: false).refreash();
    });
    suffleIndex =
        Provider.of<OperationalController>(context, listen: false).suffuleIndex;
  }

  @override
  Future<void> dispose() async {
    _animationController.stop();

    _animationController.dispose();
    await player.stop();
    await player.dispose();
    super.dispose();

    //player.dispose();
  }

  String getFormatedTime(Duration time) {
    return '$time'.split('.')[0].substring(2);
  }

  Future<void> forwardSong({required bool isUsercall}) async {
    await player.stop();
    isForward = true;

    if (!play) {
      play = true;
      _animationController.reverse();
    }
    if (suffleIndex == 0) {
      suffleCount = 0;
      if (widget.activeIndex < widget.songList.length - 1) {
        widget.activeIndex++;
      } else {
        if (listRepeatCount >= widget.songList.length * 1.20.round()) {
          listRepeatCount = 0;
          getFlutterToast(message: 'Automatically suffle started');
          Provider.of<OperationalController>(context, listen: false)
              .suffuleIndex = 2;
          suffleIndex = 2;
          widget.songList =
              Provider.of<MusicController>(context, listen: false).songInfoList;
        } else {
          listRepeatCount++;
          widget.activeIndex = 0;
        }
      }
    }
    if (suffleIndex == 1) {
      if (isUsercall) {
        if (widget.activeIndex < widget.songList.length - 1) {
          widget.activeIndex++;
        } else {
          //
          widget.activeIndex = 0;
        }
      } else {
        suffleIndex = 0;
        Provider.of<OperationalController>(context, listen: false)
            .suffuleIndex = 0;
        if (widget.activeIndex < widget.songList.length - 1) {
        } else {
          //
          widget.activeIndex = 0;
        }
      }
    }
    if (suffleIndex == 2) {
      listRepeatCount = 0;

      if (suffleCount >= widget.songList.length * 1.40.round()) {
        suffleCount = 0;
        getFlutterToast(message: 'Shuffle list changed Automatically');
        widget.songList =
            Provider.of<MusicController>(context, listen: false).songInfoList;
        widget.activeIndex =
            Provider.of<OperationalController>(context, listen: false)
                .random
                .nextInt(widget.songList.length);
      } else {
        suffleCount++;
        widget.activeIndex =
            Provider.of<OperationalController>(context, listen: false)
                .random
                .nextInt(widget.songList.length);
      }
    }
    // if (isUsercall == false && playAgain == true) {
    // } else {
    //   if (widget.activeIndex < widget.songList.length - 1 && suffleIndex==0) {
    //     widget.activeIndex++;
    //   } else {
    //     int randomNum =
    //         Provider.of<OperationalController>(context, listen: false)
    //             .random
    //             .nextInt(Provider.of<MusicController>(context, listen: false)
    //                 .songInfoList
    //                 .length);
    //     widget.activeIndex = randomNum;
    //     widget.songList =
    //         Provider.of<MusicController>(context, listen: false).songInfoList;
    //   }
    //   playAgain = false;
    // }
    setState(() {});
    //await player.resume();
  }

  Future<void> backSong() async {
    if (songPlayedIndex > 0) {
      if (!play) {
        play = true;
        _animationController.reverse();
      }
      await player.stop();
      isForward = false;
      songPlayedIndex--;
      if (!widget.songList.contains(songPlayedList[songPlayedIndex])) {
        List<SongInfoModel> list1 =
            Provider.of<MusicController>(context, listen: false).songInfoList;
        List<SongInfoModel> list2 =
            Provider.of<MusicController>(context, listen: false).localSongList;
        if (list1.contains(songPlayedList[songPlayedIndex])) {
          widget.activeIndex = list1.indexOf(songPlayedList[songPlayedIndex]);
          widget.songList = list1;
        } else {
          widget.activeIndex = list2.indexOf(songPlayedList[songPlayedIndex]);
          widget.songList = list2;
        }
      } else {
        widget.activeIndex =
            widget.songList.indexOf(songPlayedList[songPlayedIndex]);
      }

      // widget.activeIndex =
      //     widget.songList.indexOf(songPlayedList[songPlayedIndex]);
      // if (widget.activeIndex == -1) {
      //   List<SongInfoModel> tempList =
      //       Provider.of<MusicController>(context, listen: false).localSongList;
      //   widget.activeIndex = tempList.indexOf(songPlayedList[songPlayedIndex]);
      //   widget.songList = tempList;
      // }
      songPlayedList.removeRange(songPlayedIndex + 1, songPlayedList.length);
      setState(() {});
    }
    // if (widget.activeIndex > 0) {
    //   widget.activeIndex--;
    //   player.stop();
    //   setState(() {});
    //   player.resume();
    // }
  }

  Color getPreIconColor() {
    if (songPlayedIndex > 0) {
      return Colors.white;
    } else {
      return Colors.white70;
    }
  }

  Widget getSuffleIcon() {
    if (suffleIndex == 0) {
      return const Icon(
        Icons.sync_problem_rounded,
        color: Colors.white,
        size: 50,
      );
      //if (suffleIndex == 1)
    } else {
      return Stack(
        clipBehavior: Clip.none,

        //alignment: Alignment.topRight,
        children: [
          const Icon(
            Icons.shuffle_rounded,
            color: Colors.white,
            size: 45,
          ),
          Positioned(
            top: -5,
            right: -20,
            child: Text(
              suffleIndex == 1 ? '1' : "∞",
              style: Theme.of(context).textTheme.labelMedium,
            ),
          )
        ],
      );
    }
  }

  void getVolumeSlider(
      {required double height,
      required double width,
      required EdgeInsets padding,
      required SliderThemeData sliderThemeData,
      required Color backColor}) {
    showDialog(
      context: context,
      builder: (context) {
        return Dialog(
          backgroundColor: backColor,
          alignment: Alignment.bottomRight,
          elevation: 5,
          insetPadding: padding,
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.only(top: height * 0.02),
            height: height * 0.29,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Consumer<OperationalController>(builder: (context, value, va) {
                  return Text('${(currentVol * 100).round()}',
                      style: Theme.of(context).textTheme.displayMedium);
                }),
                RotatedBox(
                  quarterTurns: 3,
                  child: SliderTheme(
                      data: sliderThemeData,
                      child: Consumer<OperationalController>(
                        builder: (context, value, child) {
                          return Slider(
                            value: currentVol,
                            min: 0,
                            max: 1,
                            onChanged: (vol) async {
                              await PerfectVolumeControl.setVolume(vol);
                              currentVol = vol;
                              value.refreash();
                            },
                          );
                        },
                      )),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  void changeSuffle({required int increment}) {
    suffleIndex += increment;

    suffleIndex = suffleIndex % 3;
    if (suffleIndex == 0) {
      getFlutterToast(message: 'Normal flow started for playlist');
    } else if (suffleIndex == 1) {
      getFlutterToast(message: 'Single song replay');
    } else if (suffleIndex == 2) {
      getFlutterToast(message: 'Shuffle started for playlist');
    }
    Provider.of<OperationalController>(context, listen: false).suffuleIndex =
        suffleIndex;
    Provider.of<OperationalController>(context, listen: false).refreash();
  }

  @override
  Widget build(BuildContext context) {
    de.log('In PlayPage build------||');
    SongInfoModel songInfoModel = widget.songList[widget.activeIndex];
    if (songInfoModel.islocal) {
      player.play(DeviceFileSource(songInfoModel.songPath));
    } else {
      player.play(AssetSource(songInfoModel.songPath));
    }
    if (isForward) {
      if (songPlayedList.isNotEmpty && songPlayedList.last == songInfoModel) {
      } else {
        songPlayedIndex++;
        songPlayedList.insert(songPlayedIndex, songInfoModel);
      }
    }
    if (!play) {
      player.pause();
    }
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.transparent,
          automaticallyImplyLeading: false,
          leading: getBackButton(arrowColor: Colors.white, context: context)),
      body: Column(
        children: [
          Hero(
            tag: (songInfoModel.islocal)
                ? songInfoModel.hashCode
                : songInfoModel.imgPath,
            child: GestureDetector(
              onVerticalDragUpdate: (DragUpdateDetails details) async {
                double? vol = details.primaryDelta;
                if (vol != null && currentVol <= 1 && currentVol >= 0) {
                  double temp = currentVol;
                  if (vol < 0) {
                    temp += 0.0055;
                    if (temp >= 0 && temp <= 1) {
                    } else {
                      temp = temp.round() * 1;
                    }
                    currentVol = temp;
                    await PerfectVolumeControl.setVolume(currentVol);

                    //Provider.of<OperationalController>(context).refreash();
                  } else if (vol > 0) {
                    temp -= 0.0055;
                    if (temp >= 0 && temp <= 1) {
                    } else {
                      temp = 0;
                    }
                    currentVol = temp;
                    await PerfectVolumeControl.setVolume(currentVol);

                    //Provider.of<OperationalController>(context,li).refreash();
                  }
                }
              },
              onVerticalDragStart: (DragStartDetails details) {
                volOpacity = 1;
                Provider.of<OperationalController>(context, listen: false)
                    .refreash();
              },
              onVerticalDragEnd: (DragEndDetails details) {
                volOpacity = 0;
                Provider.of<OperationalController>(context, listen: false)
                    .refreash();
              },
              onHorizontalDragEnd: (DragEndDetails details) {
                double? vel = details.primaryVelocity;
                if (vel != null) {
                  if (vel < -1100) {
                    forwardSong(isUsercall: true);
                  } else if (vel > 1100) {
                    backSong();
                  }
                }
              },
              onDoubleTap: () {
                likeChangeIndex = 1;
                Provider.of<OperationalController>(context, listen: false)
                    .refreash();
              },
              child: AnimatedContainer(
                duration: const Duration(milliseconds: 820),
                height: height * 0.63,
                width: double.infinity,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      fit: BoxFit.fitWidth,
                      image: AssetImage(songInfoModel.imgPath)),
                ),
                child: Container(
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.center,
                          end: Alignment.bottomCenter,
                          stops: const [
                        0.3,
                        0.97
                      ],
                          colors: [
                        Colors.transparent,
                        Theme.of(context).scaffoldBackgroundColor
                      ])),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Column(
                        children: [
                          Consumer<OperationalController>(
                            builder: (context, value, child) {
                              de.log('In Operation call');
                              return Opacity(
                                opacity: volOpacity,
                                child: Card(
                                  color: Colors.transparent,
                                  elevation: volOpacity == 1 ? 12 : 0,
                                  child: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Icon(
                                        getVolIcon(),
                                        color: Colors.white,
                                        size: height * 0.07,
                                      ),
                                      SizedBox(
                                        width: width * 0.05,
                                      ),
                                      Text(
                                        '${(currentVol * 100).round()}',
                                        style: Theme.of(context)
                                            .textTheme
                                            .labelMedium,
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                          SizedBox(
                            height: height * 0.14,
                          ),
                          Consumer<OperationalController>(
                            builder: (context, value, child) {
                              return ScaleUpDownAnimation(
                                  scaleDuration:
                                      const Duration(milliseconds: 120),
                                  afterAnimationDuration:
                                      const Duration(milliseconds: 15),
                                  isAnimation: likeChangeIndex == 1 ||
                                      likeChangeIndex == 2,
                                  onEnd: () async {
                                    if (!songInfoModel.isafav) {
                                      songInfoModel.isafav = true;

                                      Provider.of<MusicController>(context,
                                              listen: false)
                                          .favSong
                                          .add(songInfoModel);
                                      if (songInfoModel.islocal) {
                                        await updateSong(
                                            sonfObj: songInfoModel,
                                            tableName: 'LocalSong');
                                      } else {
                                        await updateSong(
                                            sonfObj: songInfoModel,
                                            tableName: 'SongInfo');
                                      }
                                    }
                                    if (likeChangeIndex != 0) {
                                      likeChangeIndex = 0;
                                      Provider.of<OperationalController>(
                                              context,
                                              listen: false)
                                          .refreash();
                                    }
                                  },
                                  child: Opacity(
                                    opacity: likeChangeIndex == 1 ||
                                            likeChangeIndex == 2
                                        ? 1
                                        : 0,
                                    child: Icon(
                                      Icons.favorite,
                                      color: Colors.white,
                                      size: height * 0.10,
                                    ),
                                  ));
                            },
                          ),
                          SizedBox(
                            height: height * 0.15,
                          ),
                          Text(songInfoModel.name,
                              style:
                                  Theme.of(context).textTheme.headlineMedium),
                          Text(songInfoModel.artistName,
                              style: Theme.of(context)
                                  .textTheme
                                  .displaySmall!
                                  .copyWith(color: Colors.white)),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: width * 0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Consumer<OperationalController>(
                              builder: (context, value, child) {
                                return LikeButton(
                                  // bubblesColor: const BubblesColor(
                                  //     dotPrimaryColor: Colors.red,
                                  //     dotSecondaryColor: Colors.pink),
                                  isLiked: songInfoModel.isafav,
                                  likeBuilder: (isLiked) {
                                    return ScaleUpDownAnimation(
                                        scaleDuration:
                                            const Duration(milliseconds: 120),
                                        afterAnimationDuration:
                                            const Duration(milliseconds: 15),
                                        isAnimation: likeChangeIndex == 1 ||
                                            likeChangeIndex == 2,
                                        onEnd: () {
                                          likeChangeIndex = 0;
                                          Provider.of<OperationalController>(
                                                  context,
                                                  listen: false)
                                              .refreash();
                                        },
                                        child: Icon(
                                          songInfoModel.isafav
                                              ? Icons.favorite
                                              : Icons.favorite_border_rounded,
                                          size: 25,
                                          color: songInfoModel.isafav
                                              ? Theme.of(context)
                                                  .secondaryHeaderColor
                                              : Colors.grey,
                                        ));
                                  },
                                  onTap: (isLiked) async {
                                    songInfoModel.isafav =
                                        !songInfoModel.isafav;

                                    if (songInfoModel.isafav) {
                                      likeChangeIndex = 2;
                                      Provider.of<MusicController>(context,
                                              listen: false)
                                          .favSong
                                          .add(songInfoModel);
                                      Provider.of<OperationalController>(
                                              context,
                                              listen: false)
                                          .refreash();
                                    } else {
                                      likeChangeIndex = 3;
                                      Provider.of<MusicController>(context,
                                              listen: false)
                                          .favSong
                                          .remove(songInfoModel);
                                    }
                                    if (songInfoModel.islocal) {
                                      await updateSong(
                                          sonfObj: songInfoModel,
                                          tableName: 'LocalSong');
                                    } else {
                                      await updateSong(
                                          sonfObj: songInfoModel,
                                          tableName: 'SongInfo');
                                    }

                                    return songInfoModel.isafav;
                                  },
                                );
                              },
                            ),
                            songInfoModel.islocal
                                ? const SizedBox()
                                : GestureDetector(
                                    onTap: () async {
                                      getFlutterToast(
                                          message: "Downloading....");
                                      await Permission.manageExternalStorage
                                          .request();

                                      String path =
                                          '/storage/emulated/0/Download/${songInfoModel.name}.mp3';
                                      File file = File(path);
                                      int index = 1;
                                      String name = '${songInfoModel.name}.mp3';
                                      while (await file.exists()) {
                                        file = File(
                                            '/storage/emulated/0/Download/${songInfoModel.name}($index).mp3');
                                        name =
                                            '${songInfoModel.name}($index).mp3';

                                        index++;
                                      }
                                      final ByteData bytes =
                                          await rootBundle.load(
                                              'assets/${songInfoModel.songPath}');
                                      final Uint8List byteList =
                                          bytes.buffer.asUint8List();
                                      await file.writeAsBytes(byteList);
                                      getFlutterToast(
                                          message:
                                              "$name downloaded at Downloads");
                                    },
                                    child: Icon(Icons.file_download_rounded,
                                        color: Theme.of(context)
                                            .secondaryHeaderColor))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.04),
            child: Column(
              children: [
                SizedBox(
                  height: height * 0.01,
                ),
                Consumer<MusicController>(
                  builder: (context, value, child) {
                    //de.log('in squen call');
                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(getFormatedTime(position),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(color: Colors.white)),
                            Text(getFormatedTime(duration),
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(color: Colors.white)),
                          ],
                        ),
                        SliderTheme(
                          data: const SliderThemeData(
                              thumbShape: RoundSliderThumbShape(
                                  pressedElevation: 0,
                                  enabledThumbRadius: 0,
                                  elevation: 0)),
                          child: Slider(
                            value: position.inSeconds.toDouble(),
                            min: 0,
                            max: duration.inSeconds.toDouble(),
                            autofocus: true,
                            onChanged: (val) async {
                              position = Duration(seconds: val.toInt());
                              await player.seek(position);
                            },
                            activeColor: Theme.of(context).secondaryHeaderColor,
                            allowedInteraction: SliderInteraction.slideOnly,
                            inactiveColor:
                                const Color.fromRGBO(217, 217, 217, 0.19),
                          ),
                        )
                      ],
                    );
                  },
                ),
                // Consumer<OperationalController>(
                //   builder: (context, value, child) {
                //     return;
                //   },
                // ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 12, right: 7),
                      child: LikeButton(
                        postFrameCallback: (state) {},
                        onTap: (like) async {
                          isLiked = !isLiked;
                          changeSuffle(increment: 1);
                          return isLiked;
                        },
                        //isLiked: isLiked,
                        padding: EdgeInsets.zero,
                        likeBuilder: (isLiked) {
                          return Consumer<OperationalController>(
                            builder: (context, value, child) {
                              return getSuffleIcon();
                            },
                          );
                        },
                      ),
                    ),
                    IconButton(onPressed: () {
                      backSong();
                    }, icon: Consumer<OperationalController>(
                      builder: (context, value, child) {
                        return Icon(
                          Icons.skip_previous_rounded,
                          color: getPreIconColor(),
                          size: 50,
                        );
                      },
                    )),
                    IconButton(
                      onPressed: () async {
                        if (play) {
                          await player.pause();

                          _animationController.forward();
                        } else {
                          await player.resume();
                          _animationController.reverse();
                        }
                        play = !play;
                      },
                      icon: AnimatedIcon(
                        icon: AnimatedIcons.pause_play,
                        progress: _animationController,
                        size: 50,
                        color: Colors.white,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        forwardSong(isUsercall: true);
                      },
                      icon: const Icon(
                        Icons.skip_next_rounded,
                        color: Colors.white,
                        size: 50,
                      ),
                    ),
                    IconButton(
                      onPressed: () {
                        getVolumeSlider(
                            height: height,
                            width: width,
                            padding: EdgeInsets.only(
                                left: width * 0.82,
                                right: width * 0.07,
                                bottom: height * 0.17),
                            sliderThemeData: SliderThemeData(
                              activeTrackColor:
                                  Theme.of(context).secondaryHeaderColor,
                              inactiveTrackColor:
                                  const Color.fromRGBO(217, 217, 217, 0.19),
                              allowedInteraction: SliderInteraction.slideOnly,
                              thumbColor:
                                  Theme.of(context).secondaryHeaderColor,
                              thumbShape: const RoundSliderThumbShape(
                                  elevation: 2,
                                  pressedElevation: 2,
                                  enabledThumbRadius: 8),
                            ),
                            backColor: Colors.white);
                      },
                      icon: Consumer<OperationalController>(
                        builder: (context, value, child) {
                          return Icon(
                            getVolIcon(),
                            color: Colors.white,
                            size: 50,
                          );
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
