import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/Model/LangSongModel.dart';
import 'package:music_app/Model/SingerInfoModel.dart';
import 'package:music_app/view/navbar.dart';
import 'package:music_app/view/play_list_screen.dart';
import 'package:music_app/view/show_all_screen.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class GalleryPage extends StatefulWidget {
  const GalleryPage({super.key});
  @override
  State createState() => _GalleryPageState();
}

class _GalleryPageState extends State {
  bool animationOver = false;
  int activeIndex = 0;

  final CarouselSliderController _carouselSliderController =
      CarouselSliderController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            CarouselSlider.builder(
                itemCount: 12,
                itemBuilder: (context, index, realIndex) {
                  return Container(
                    height: height * 0.42,
                    width: double.infinity,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage('assets/images/offer$index.jpg')),
                    ),
                    child: Container(
                      decoration: const BoxDecoration(
                          gradient: LinearGradient(
                              begin: Alignment.center,
                              end: Alignment.bottomCenter,
                              stops: [
                            0.3,
                            0.97
                          ],
                              colors: [
                            Colors.transparent,
                            Color.fromRGBO(24, 24, 24, 1)
                          ])),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 21),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'A.L.O.N.E',
                              style: Theme.of(context).textTheme.labelLarge,
                            ),
                            Container(
                              height: 37,
                              width: 127,
                              margin:
                                  const EdgeInsets.only(bottom: 40, top: 20),
                              decoration: BoxDecoration(
                                color: Theme.of(context).primaryColor,
                                borderRadius: BorderRadius.circular(19),
                              ),
                              alignment: Alignment.center,
                              child: Text(
                                'Subscribe',
                                style: Theme.of(context)
                                    .textTheme
                                    .titleMedium!
                                    .copyWith(color: Colors.black),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                carouselController: _carouselSliderController,
                options: CarouselOptions(
                    height: height * 0.42,
                    enableInfiniteScroll: false,
                    viewportFraction: 1,
                    autoPlay: true,
                    enlargeStrategy: CenterPageEnlargeStrategy.scale,
                    enlargeFactor: 0.55,
                    enlargeCenterPage: true,
                    onPageChanged: (index, reason) {
                      activeIndex = index;
                      Provider.of<OperationalController>(context, listen: false)
                          .refreash();
                    })),
            Consumer<OperationalController>(
              builder: (context, value, child) {
                return AnimatedSmoothIndicator(
                  count: 12,
                  activeIndex: activeIndex,
                  onDotClicked: (index) {
                    _carouselSliderController.animateToPage(index,
                        duration: const Duration(milliseconds: 350));
                  },
                  effect: ExpandingDotsEffect(
                      dotColor: const Color.fromRGBO(159, 159, 159, 1),
                      activeDotColor: Theme.of(context).primaryColor,
                      dotHeight: 10,
                      dotWidth: 10),
                );
              },
            ),
            Padding(
                padding: EdgeInsets.only(
                    top: height * 0.005,
                    left: width * 0.05,
                    right: width * 0.05),
                child: Column(
                  children: [
                    Row(
                      children: [
                        Text('Languages',
                            style: Theme.of(context).textTheme.titleMedium),
                        const Spacer(),
                        TextButton(
                            onPressed: () {
                              List<LangSongModel> langSong =
                                  Provider.of<MusicController>(context,
                                          listen: false)
                                      .langSong;
                              Navigator.of(context).push(PageTransition(
                                  child: ShowAllScreen(
                                    list: langSong,
                                    animationIndex: 0,
                                  ),
                                  type: PageTransitionType.scale,
                                  duration: const Duration(milliseconds: 225),
                                  //alignment: Alignment.centerRight,
                                  alignment: const Alignment(1, 0.035)));
                            },
                            child: Text('See all',
                                style: Theme.of(context).textTheme.labelSmall))
                      ],
                    ),
                    SizedBox(
                        height: height * 0.19,
                        width: double.infinity,
                        child: Consumer<MusicController>(
                          builder: (context, value, child) {
                            List<LangSongModel> langList = value.langSong;
                            return ListView.separated(
                              separatorBuilder: (context, i) {
                                return const SizedBox(
                                  width: 35,
                                );
                              },
                              itemCount: langList.length,
                              scrollDirection: Axis.horizontal,
                              itemBuilder: (context, i) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(PageTransition(
                                        child: PlayListScreen(
                                          playListModel: langList[i],
                                          requiredRefreash: false,
                                        ),
                                        duration:
                                            const Duration(milliseconds: 225),
                                        type: PageTransitionType.scale,
                                        alignment: Alignment.center,
                                        curve: Curves.easeInSine));
                                  },
                                  child: AnimatedContainer(
                                    curve: Curves.easeInOut,
                                    transform: Matrix4.translationValues(
                                        animationOver ? 0 : width, 0, 0),
                                    duration: Duration(
                                        milliseconds: (250 + (i * 150))),
                                    child: Animate(
                                      effects: const [
                                        FadeEffect(
                                            curve: Curves.easeInOut,
                                            begin: 0.1,
                                            end: 0.9,
                                            duration:
                                                Duration(milliseconds: 900))
                                      ],
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            child: Hero(
                                              tag: langList[i].hashCode,
                                              child: Image.asset(
                                                langList[i].imgPath,
                                                fit: BoxFit.cover,
                                                width: width * 0.27,
                                                height: height * 0.15,
                                              ),
                                            ),
                                          ),
                                          SizedBox(
                                            height: height * 0.015,
                                          ),
                                          Text(langList[i].name,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .displaySmall),
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                        )),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    Row(
                      children: [
                        Text('Popular singles',
                            style: Theme.of(context).textTheme.titleLarge),
                        const Spacer(),
                        TextButton(
                            onPressed: () {
                              List<SingerInfoModel> singerList =
                                  Provider.of<MusicController>(context,
                                          listen: false)
                                      .singerList;
                              Navigator.of(context).push(PageTransition(
                                  child: ShowAllScreen(
                                    list: singerList,
                                    animationIndex: 1,
                                  ),
                                  duration: const Duration(milliseconds: 225),
                                  type: PageTransitionType.scale,
                                  alignment: const Alignment(1, 0.45)));
                            },
                            child: Text('See all',
                                style: Theme.of(context).textTheme.labelSmall))
                      ],
                    ),
                    SizedBox(
                      height: height * 0.01,
                    ),
                    SizedBox(
                        width: double.infinity,
                        height: height * 0.2,
                        child: Consumer<MusicController>(
                          builder: (context, value, child) {
                            List<SingerInfoModel> singerList = value.singerList;
                            return ListView.separated(
                              itemCount:
                                  singerList.length > 2 ? 2 : singerList.length,
                              padding: EdgeInsets.zero,
                              separatorBuilder: (context, i) => const SizedBox(
                                height: 25,
                              ),
                              itemBuilder: (context, i) {
                                return GestureDetector(
                                  onTap: () {
                                    Navigator.of(context).push(PageTransition(
                                        child: PlayListScreen(
                                          requiredRefreash: false,
                                          playListModel: singerList[i],
                                        ),
                                        duration:
                                            const Duration(milliseconds: 225),
                                        type: PageTransitionType.scale,
                                        alignment: Alignment.bottomCenter));
                                  },
                                  child: AnimatedContainer(
                                    curve: Curves.easeInOut,
                                    transform: Matrix4.translationValues(
                                        0, animationOver ? 0 : height, 0),
                                    duration:
                                        Duration(milliseconds: 300 + (i * 200)),
                                    height: height * 0.08,
                                    width: double.infinity,
                                    padding: const EdgeInsets.only(right: 20),
                                    child: Animate(
                                      effects: const [
                                        FadeEffect(
                                            curve: Curves.easeInOut,
                                            begin: 0.1,
                                            end: 0.9,
                                            duration:
                                                Duration(milliseconds: 900))
                                      ],
                                      child: Row(
                                        children: [
                                          ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(7),
                                            child: Hero(
                                              tag: singerList[i].hashCode,
                                              child: Image.asset(
                                                singerList[i].imgPath,
                                                height: height * 0.07,
                                                width: width * 0.15,
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                          const SizedBox(
                                            width: 20,
                                          ),
                                          Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(
                                                height: height * 0.01,
                                              ),
                                              Text(singerList[i].name,
                                                  style: Theme.of(context)
                                                      .textTheme
                                                      .titleLarge),
                                              SizedBox(
                                                height: height * 0.01,
                                              ),
                                              Row(
                                                children: [
                                                  Text("${singerList[i].year}",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodySmall),
                                                  Text(" * ",
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .displaySmall),
                                                  Text(singerList[i].location,
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .bodySmall),
                                                ],
                                              ),
                                            ],
                                          ),
                                          const Spacer(),
                                          Icon(
                                            Icons.more_vert_rounded,
                                            color: Colors.white,
                                            size: width * 0.07,
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                );
                              },
                            );
                          },
                        ))
                  ],
                ))
          ],
        ),
      ),
      bottomNavigationBar: const Navbar(index: 2),
    );
  }
}
