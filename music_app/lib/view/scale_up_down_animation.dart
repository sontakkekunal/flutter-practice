import 'package:flutter/material.dart';

class ScaleUpDownAnimation extends StatefulWidget {
  final Duration scaleDuration;
  final Duration afterAnimationDuration;
  final Widget child;
  final bool isAnimation;
  final VoidCallback onEnd;

  const ScaleUpDownAnimation(
      {super.key,
      required this.scaleDuration,
      required this.afterAnimationDuration,
      required this.child,
      required this.isAnimation,
      required this.onEnd});
  @override
  State<StatefulWidget> createState() {
    return _ScaleUpDownAnimationState();
  }
}

class _ScaleUpDownAnimationState extends State<ScaleUpDownAnimation>
    with TickerProviderStateMixin {
  late final AnimationController _animationController;
  late final Animation<double> scale;
  @override
  void initState() {
    super.initState();
    _animationController =
        AnimationController(vsync: this, duration: widget.scaleDuration);
    scale = Tween<double>(begin: 1, end: 1.5).animate(_animationController);
  }

  @override
  void dispose() {
    super.dispose();
    _animationController.dispose();
  }

  @override
  void didUpdateWidget(ScaleUpDownAnimation oldWidget) {
    super.didUpdateWidget(oldWidget);
    doAnimation(oldWidget);
  }

  void doAnimation(ScaleUpDownAnimation oldWidget) async {
    if (oldWidget.isAnimation != widget.isAnimation) {
      await _animationController.forward();
      await _animationController.reverse();
      await Future.delayed(widget.afterAnimationDuration);
      widget.onEnd();
    }
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(scale: scale, child: widget.child);
  }
}
