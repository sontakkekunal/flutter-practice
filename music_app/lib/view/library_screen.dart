// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/Model/PlayListModel.dart';
import 'package:music_app/view/navbar.dart';
import 'package:music_app/view/play_list_screen.dart';
import 'package:music_app/view/scale_up_down_animation.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';
import 'package:readmore/readmore.dart';

import 'favorites_screen.dart';

class LibraryScreen extends StatefulWidget {
  const LibraryScreen({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LibraryScreenState();
  }
}

class _LibraryScreenState extends State {
  final TextEditingController _textController = TextEditingController();
  final GlobalKey<FormFieldState> globalKey = GlobalKey<FormFieldState>();
  final GlobalKey<AnimatedListState> _keyAnimation =
      GlobalKey<AnimatedListState>();
  bool animationOver = false;
  bool _textFeildAnimation = false;

  Future<File?> addImg() async {
    try {
      FilePickerResult? filePickerResult = await FilePicker.platform
          .pickFiles(type: FileType.image, allowMultiple: false);
      if (filePickerResult != null) {
        File file = File(filePickerResult.files.first.path!);
        return file;
      }
    } catch (e) {
      getFlutterToast(message: 'Somerthing went wrong');
    }
    return null;
  }

  void addUserLibrary({bool? isEdit, PlayListModel? playListModel}) {
    File? file;
    String preName = "";
    _textController.clear();
    int selectedIndex = 0;
    if (isEdit == true && playListModel != null) {
      if (playListModel.filePath != null) {
        selectedIndex = -1;
        file = File(playListModel.filePath!);
      }
      if (playListModel.imgPath != null) {
        selectedIndex = imgList.indexOf(playListModel.imgPath!);
      }
      _textController.text = playListModel.name;
      preName = playListModel.name;
    }

    showModalBottomSheet(
      context: context,
      //scrollControlDisabledMaxHeightRatio: 0.2 ,
      isScrollControlled: true,
      backgroundColor: Colors.grey[800],
      builder: (context) {
        double height = MediaQuery.of(context).size.height;
        double width = MediaQuery.of(context).size.width;
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            //width: double.maxFinite,
            height: height * 0.50,
            margin: EdgeInsets.only(
                top: height * 0.03, left: width * 0.08, right: width * 0.08),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(60)),
            child: Column(
              children: [
                Text(isEdit == true ? 'Edit Library' : 'Create Library',
                    style: Theme.of(context)
                        .textTheme
                        .displayMedium!
                        .copyWith(color: Colors.white)),
                SizedBox(
                  height: height * 0.005,
                ),
                Consumer<OperationalController>(
                  builder: (BuildContext context, OperationalController value,
                      val1) {
                    return ScaleUpDownAnimation(
                      scaleDuration: const Duration(milliseconds: 120),
                      afterAnimationDuration: const Duration(milliseconds: 15),
                      isAnimation: _textFeildAnimation,
                      onEnd: () {
                        _textFeildAnimation = false;
                        value.refreash();
                      },
                      child: TextFormField(
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        style: Theme.of(context).textTheme.labelMedium,
                        key: globalKey,
                        validator: (val) {
                          if (val == null || val.isEmpty) {
                            _textFeildAnimation = true;
                            value.refreash();

                            return "";
                          }
                          List<PlayListModel> userPlayList =
                              Provider.of<MusicController>(context,
                                      listen: false)
                                  .userPlayList;
                          if (isEdit == true) {
                            for (int i = 0; i < userPlayList.length; i++) {
                              if (playListModel != userPlayList[i] &&
                                  val == userPlayList[i].name) {
                                _textFeildAnimation = true;
                                value.refreash();
                                return 'Name already taken for playList';
                              }
                            }
                          } else {
                            for (int i = 0; i < userPlayList.length; i++) {
                              if (val == userPlayList[i].name) {
                                _textFeildAnimation = true;
                                value.refreash();
                                return 'Name already taken for playList';
                              }
                            }
                          }
                        },
                        controller: _textController,
                        decoration: InputDecoration(
                          hintText: 'Enter library Name',
                          errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.red,
                                width: 1.3,
                              )),
                          focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.red,
                                width: 1.3,
                              )),
                          focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.orange,
                                width: 1.3,
                              )),
                          enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(20),
                              borderSide: const BorderSide(
                                color: Colors.white12,
                                width: 1.3,
                              )),
                          hintStyle: Theme.of(context)
                              .textTheme
                              .titleLarge!
                              .copyWith(fontSize: 20),
                        ),
                      ),
                    );
                  },
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                Text(
                  'Pick a image for library',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                SizedBox(
                  height: height * 0.02,
                ),
                SizedBox(
                  height: height * 0.075,
                  width: width,
                  child: ListView.separated(
                    separatorBuilder: (context, index) {
                      return const SizedBox(
                        width: 20,
                      );
                    },
                    scrollDirection: Axis.horizontal,
                    itemCount: imgList.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(onTap: () {
                        file = null;
                        selectedIndex = index;
                        Provider.of<OperationalController>(context,
                                listen: false)
                            .refreash();
                      }, child: Consumer<OperationalController>(
                        builder: (context, value, child) {
                          return Container(
                            width: 70,
                            height: 70,
                            padding: const EdgeInsets.all(2),
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: selectedIndex == index
                                    ? Border.all(
                                        color: Theme.of(context).primaryColor,
                                        width: 4)
                                    : null),
                            child: ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: AspectRatio(
                                  aspectRatio: 1,
                                  child: Image.asset(
                                    imgList[index],

                                    alignment: Alignment.center,
                                    //fit: BoxFit.cover,
                                  ),
                                )),
                          );
                        },
                      ));
                    },
                  ),
                ),
                SizedBox(
                  height: height * 0.03,
                ),
                Text(
                  'OR',
                  style: Theme.of(context).textTheme.titleMedium,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Consumer<OperationalController>(
                      builder: (context, value, child) {
                        return (file != null)
                            ? Row(
                                children: [
                                  ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.file(
                                        file!,
                                        height: height * 0.05,
                                        width: width * 0.10,
                                        fit: BoxFit.cover,
                                      )),
                                  SizedBox(
                                    width: width * 0.03,
                                  ),
                                  SizedBox(
                                    width: width * 0.36,
                                    child: ReadMoreText(
                                      file!.path.split('/').last,
                                      style: GoogleFonts.inter(
                                          fontSize: 16,
                                          fontWeight: FontWeight.w600,
                                          color: const Color.fromRGBO(
                                              255, 255, 255, 1)),
                                      // style: Theme.of(context)
                                      //     .textTheme
                                      //     .labelMedium,
                                      trimMode: TrimMode.Line,
                                      trimLines: 2,

                                      trimCollapsedText: '....',
                                      isExpandable: false,
                                      moreStyle: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )
                                ],
                              )
                            : SizedBox(
                                width: width * 0.3,
                              );
                      },
                    ),
                    GestureDetector(
                      onTap: () async {
                        file = await addImg();

                        if (file != null) {
                          selectedIndex = -1;
                        }
                        Provider.of<OperationalController>(context,
                                listen: false)
                            .refreash();
                      },
                      child: Container(
                          height: height * 0.047,
                          width: width * 0.22,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(19),
                              color: const Color.fromRGBO(248, 162, 69, 1)),
                          child: Text('Browse',
                              style:
                                  Theme.of(context).textTheme.displayMedium)),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * 0.002,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () async {
                        _textController.text = _textController.text.trim();
                        if (globalKey.currentState!.validate() &&
                            isEdit != null &&
                            isEdit == true) {
                          if (file != null) {
                            playListModel?.imgPath = null;
                            playListModel?.name = _textController.text;
                            playListModel?.filePath = file!.path;
                          } else {
                            playListModel?.filePath = null;
                            playListModel?.name = _textController.text;
                            playListModel?.imgPath = imgList[selectedIndex];
                          }
                          if (preName != _textController.text) {
                            for (int i = 0;
                                i < playListModel!.songList.length;
                                i++) {
                              playListModel.songList[i].inPlayList
                                  .remove(preName);
                              playListModel.songList[i].inPlayList
                                  .add(_textController.text);
                              await updateSong(
                                  sonfObj: playListModel.songList[i],
                                  tableName: 'SongInfo');
                            }
                          }
                          FocusScope.of(context).unfocus();
                          Navigator.of(context).pop();
                          Provider.of<MusicController>(context, listen: false)
                              .refreash();

                          await updatePlayList(playListModel: playListModel!);
                        }

                        if (globalKey.currentState!.validate() &&
                            isEdit == null) {
                          PlayListModel? playListModel;
                          if (file != null) {
                            playListModel = PlayListModel(
                                name: _textController.text,
                                filePath: file!.path,
                                songList: []);
                          } else {
                            playListModel = PlayListModel(
                                name: _textController.text,
                                imgPath: imgList[selectedIndex],
                                songList: []);
                          }
                          List<PlayListModel> playList =
                              Provider.of<MusicController>(context,
                                      listen: false)
                                  .userPlayList;
                          FocusScope.of(context).unfocus();
                          Navigator.of(context).pop();
                          await insertPlayList(playListModel);
                          playList.removeRange(0, playList.length);
                          playList.addAll(await getPlayList());
                          _keyAnimation.currentState!.insertItem(
                              playList.length - 1,
                              duration: const Duration(milliseconds: 500));

                          // Provider.of<MusicController>(context, listen: false)
                          //     .userPlayList = await getPlayList();

                          Provider.of<MusicController>(context, listen: false)
                              .refreash();
                        }
                      },
                      child: Container(
                          height: height * 0.047,
                          width: width * 0.18,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(19),
                              color: Theme.of(context).primaryColor),
                          child: Text('Save',
                              style:
                                  Theme.of(context).textTheme.displayMedium)),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Widget playListTitle(
      {required PlayListModel playlistmodel,
      required Widget rightCorner,
      required int index,
      required var animation,
      required bool animationOver,
      required int kthLength,
      required double width}) {
    return ScaleTransition(
      scale: animation,
      child: Animate(
        effects: const [
          TintEffect(
            curve: Curves.easeInOutExpo,
            begin: 1,
            end: 0,
            duration: Duration(
              milliseconds: 900,
            ),
          )
        ],
        child: AnimatedContainer(
          curve: Curves.easeInOut,
          duration: Duration(
              milliseconds:
                  (230 + (index * getValDurSlide(kthLength, true))).round()),
          transform: (index % 2 == 0)
              ? Matrix4.translationValues(animationOver ? 0 : -width, 0, 0)
              : Matrix4.translationValues(animationOver ? 0 : width, 0, 0),
          child: Padding(
              padding: const EdgeInsets.all(10),
              child: Row(children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Hero(
                      tag: playlistmodel.hashCode,
                      child: getCoverPhoto(
                          imgPath: playlistmodel.imgPath,
                          filePath: playlistmodel.filePath),
                    )),
                SizedBox(
                  width: width * 0.05,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(playlistmodel.name,
                        style: Theme.of(context).textTheme.displayLarge),
                    Text(
                      '${playlistmodel.songList.length} Songs',
                      style: Theme.of(context).textTheme.labelMedium,
                    )
                  ],
                ),
                const Spacer(),
                rightCorner
              ])),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _textController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(
            top: height * 0.05, left: width * 0.03, right: width * 0.03),
        child: Column(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.of(context)
                    .push(PageTransition(
                        duration: const Duration(milliseconds: 225),
                        child: const FavoritesScreen(),
                        type: PageTransitionType.fade))
                    .then((val) {
                  Provider.of<MusicController>(context, listen: false)
                      .refreash();
                });
              },
              child: Container(
                margin: const EdgeInsets.all(10),
                child: Row(
                  children: [
                    Icon(
                      Icons.favorite,
                      color: Theme.of(context).primaryColor,
                      size: 50,
                    ),
                    SizedBox(
                      width: width * 0.05,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Hero(
                          tag: 'fav',
                          child: Text(
                            'Favorites',
                            style: Theme.of(context).textTheme.displayLarge,
                          ),
                        ),
                        Consumer<MusicController>(
                          builder: (context, value, child) {
                            return Text(
                              '${value.favSong.length} Songs',
                              style: Theme.of(context).textTheme.labelMedium,
                            );
                          },
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Expanded(child: Consumer<MusicController>(
              builder: (context, value, child) {
                List<PlayListModel> userPlayList = value.userPlayList;
                return AnimatedList(
                  key: _keyAnimation,
                  initialItemCount: userPlayList.length,
                  //itemCount: userPlayList.length,
                  padding: EdgeInsets.zero,
                  itemBuilder: (context, index, animation) {
                    return GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(PageTransition(
                            duration: const Duration(milliseconds: 225),
                            child: PlayListScreen(
                              playListModel: userPlayList[index],
                              requiredRefreash: true,
                            ),
                            type: PageTransitionType.fade));
                      },
                      child: playListTitle(
                          playlistmodel: userPlayList[index],
                          width: width,
                          animationOver: animationOver,
                          kthLength: userPlayList.length,
                          index: index,
                          animation: animation,
                          rightCorner: PopupMenuButton(
                            splashRadius: 40,
                            shape: ContinuousRectangleBorder(
                                borderRadius: BorderRadius.circular(25)),
                            icon: const Icon(Icons.more_vert_sharp),
                            iconColor: Colors.white,
                            iconSize: 30,
                            elevation: 10,
                            color: Colors.grey[800],
                            onSelected: (val) async {
                              if (val == 0) {
                                addUserLibrary(
                                    isEdit: true,
                                    playListModel: userPlayList[index]);
                              } else if (val == 1) {
                                PlayListModel playListModel =
                                    userPlayList[index];
                                for (int i = 0;
                                    i < userPlayList[index].songList.length;
                                    i++) {
                                  userPlayList[index]
                                      .songList[i]
                                      .inPlayList
                                      .remove(userPlayList[index].name);
                                  await updateSong(
                                      sonfObj: userPlayList[index].songList[i],
                                      tableName: 'SongInfo');
                                }
                                await deletePlayList(
                                    playListModel: playListModel);

                                _keyAnimation.currentState!.removeItem(index,
                                    duration: const Duration(milliseconds: 200),
                                    (context, val) {
                                  return playListTitle(
                                      animation: animation,
                                      index: index,
                                      animationOver: animationOver,
                                      kthLength: userPlayList.length,
                                      playlistmodel: playListModel,
                                      rightCorner: const Icon(
                                        Icons.more_vert_rounded,
                                        color: Colors.white,
                                        size: 40,
                                      ),
                                      width: width);
                                });
                                userPlayList.remove(userPlayList[index]);
                              }

                              Provider.of<MusicController>(context,
                                      listen: false)
                                  .refreash();
                            },
                            itemBuilder: (context) {
                              return [
                                PopupMenuItem(
                                    value: 0,
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.edit,
                                          color: Colors.white,
                                        ),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        Text('Edit PlayList',
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium),
                                      ],
                                    )),
                                PopupMenuItem(
                                    value: 1,
                                    child: Row(
                                      children: [
                                        const Icon(
                                          Icons.delete_outline_rounded,
                                          color: Colors.white,
                                        ),
                                        const SizedBox(
                                          width: 8,
                                        ),
                                        Text('Delete PlayList',
                                            style: Theme.of(context)
                                                .textTheme
                                                .titleMedium),
                                      ],
                                    )),
                              ];
                            },
                          )),
                    );
                  },
                );
              },
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addUserLibrary();
        },
        backgroundColor: Theme.of(context).primaryColor,
        child: const Icon(
          Icons.add,
          size: 40,
          color: Colors.white,
        ),
      ),
      bottomNavigationBar: const Navbar(index: 3),
    );
  }
}
