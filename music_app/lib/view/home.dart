import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:page_transition/page_transition.dart';
import 'gallery_page.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});
  @override
  State createState() => _HomePageState();
}

class _HomePageState extends State {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      body: Stack(
        //clipBehavior: Clip.none,
        alignment: Alignment.topCenter,
        children: [
          const SizedBox(
            height: double.maxFinite,
            width: double.maxFinite,
          ),
          Container(
            height: height * 0.73,
            width: double.maxFinite,
            decoration: const BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover, image: AssetImage('assets/homeBg.png'))),
            child: Container(
              decoration: const BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.center,
                      end: Alignment.bottomCenter,
                      stops: [
                    0.67,
                    1
                  ],
                      colors: [
                    Colors.transparent,
                    Color.fromRGBO(24, 24, 24, 1)
                  ])),
            ),
          ),
          Positioned(
              top: height * 0.5,
              child: SizedBox(
                  height: height * 0.2,
                  width: width * 0.8,
                  child: Animate(
                    effects: const [
                      FadeEffect(
                        curve: Curves.easeInOut,
                        duration: Duration(milliseconds: 900),
                        begin: 0.3,
                        end: 1,
                      ),
                      SlideEffect(
                        curve: Curves.easeInOut,
                        duration: Duration(milliseconds: 800),
                        begin: Offset(0, 9),
                        end: Offset(0, 0),
                      )
                    ],
                    child: Text(
                      'Dancing between The shadows Of rhythm ',
                      style: Theme.of(context).textTheme.labelLarge,
                      textAlign: TextAlign.center,
                    ),
                  ))),
          Positioned(
            top: height * 0.708,
            child: Animate(
              effects: const [
                FadeEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 1000),
                  begin: 0.3,
                  end: 1,
                ),
                SlideEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 900),
                  begin: Offset(0, 9),
                  end: Offset(0, 0),
                )
              ],
              child: GestureDetector(
                //behavior: HitTestBehavior.translucent,
                onTap: () {
                  Navigator.of(context).push(PageTransition(
                      child: const GalleryPage(),
                      duration: const Duration(milliseconds: 225),
                      type: PageTransitionType.bottomToTop));
                },
                child: Container(
                  height: height * 0.06,
                  width: width * 0.64,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(19),
                      color: Theme.of(context).primaryColor),
                  child: Text('Get started',
                      style: Theme.of(context).textTheme.displayMedium),
                ),
              ),
            ),
          ),
          Positioned(
            top: height * 0.79,
            child: Animate(
              effects: const [
                FadeEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 1100),
                  begin: 0.3,
                  end: 1,
                ),
                SlideEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 1000),
                  begin: Offset(0, 9),
                  end: Offset(0, 0),
                )
              ],
              child: Container(
                height: height * 0.053,
                width: width * 0.64,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(19),
                    border: Border.all(
                        width: 1, color: Theme.of(context).primaryColor)),
                child: Text('Continue with Email',
                    style: Theme.of(context).textTheme.bodyMedium),
              ),
            ),
          ),
          Positioned(
            top: height * 0.87,
            child: Animate(
              effects: const [
                FadeEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 1200),
                  begin: 0.3,
                  end: 1,
                ),
                SlideEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 1100),
                  begin: Offset(0, 9),
                  end: Offset(0, 0),
                )
              ],
              child: SizedBox(
                height: height * 0.040,
                width: width * 0.56,
                child: Text(
                    'by continuing you agree to term of services and  Privacy policy',
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleLarge),
              ),
            ),
          )
        ],
      ),
    );
  }
}
