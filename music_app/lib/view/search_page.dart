// ignore_for_file: file_names

import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Model/SongInfoModel.dart';
import 'package:music_app/view/navbar.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:provider/provider.dart';

class SearchPage extends StatefulWidget {
  const SearchPage({super.key});

  @override
  State<StatefulWidget> createState() {
    return _SearchPageState();
  }
}

class _SearchPageState extends State {
  String? searchData;
  bool animationOver = false;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;

      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    log('In SeachPage');
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(
            top: height * 0.05, left: width * 0.05, right: width * 0.05),
        child: Column(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                getBackButton(arrowColor: Colors.white, context: context),
                const Spacer(),
                SizedBox(
                  width: width * 0.55,
                  height: height * 0.065,
                  child: TextFormField(
                    onChanged: (value) {
                      searchData = value;
                      Provider.of<MusicController>(context, listen: false)
                          .refreash();
                    },
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge!
                        .copyWith(color: Colors.white),
                    decoration: InputDecoration(
                        hintText: 'Search',
                        hintStyle: Theme.of(context).textTheme.titleLarge),
                  ),
                ),
                const Spacer()
              ],
            ),
            Expanded(child: Consumer<MusicController>(
              builder: (context, value, child) {
                List<SongInfoModel> songInfoList = value.songInfoList;
                List<SongInfoModel> displayList = [];
                if (searchData == null || searchData!.isEmpty) {
                  displayList = songInfoList;
                } else {
                  for (int i = 0;
                      i < songInfoList.length && searchData != null;
                      i++) {
                    if (songInfoList[i]
                            .name
                            .toLowerCase()
                            .contains(searchData!.toLowerCase()) ||
                        songInfoList[i]
                            .artistName
                            .toLowerCase()
                            .contains(searchData!.toLowerCase()) ||
                        songInfoList[i]
                            .year
                            .toString()
                            .toLowerCase()
                            .contains(searchData!.toLowerCase()) ||
                        songInfoList[i]
                            .lang
                            .toLowerCase()
                            .contains(searchData!.toLowerCase())) {
                      displayList.add(songInfoList[i]);
                    }

                    for (int j = 0;
                        j < songInfoList[i].inPlayList.length;
                        j++) {
                      if (songInfoList[i]
                          .inPlayList[j]
                          .toLowerCase()
                          .contains(searchData!.toLowerCase())) {
                        displayList.add(songInfoList[i]);
                      }
                    }
                  }
                }
                return getSongList(
                    displayList: displayList,
                    width: width,
                    animationIndex: 0,
                    animationOver: animationOver,
                    context: context,
                    requiredRefreash: false,
                    likeRefeash: false);
              },
            ))
          ],
        ),
      ),
      bottomNavigationBar: const Navbar(index: 1),
    );
  }
}
