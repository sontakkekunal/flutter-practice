// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/view/play_list_screen.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class ShowAllScreen extends StatefulWidget {
  final dynamic list;
  final int animationIndex;

  const ShowAllScreen(
      {super.key, required this.list, required this.animationIndex});

  @override
  State<StatefulWidget> createState() {
    return _ShowAllScreenState();
  }
}

class _ShowAllScreenState extends State<ShowAllScreen> {
  bool animationOver = false;
  Matrix4 getTransition(
      {required int currentIndex,
      required double height,
      required index,
      required double width}) {
    if (index == 0) {
      if (currentIndex % 2 == 0) {
        return Matrix4.translationValues(0, animationOver ? 0 : -height, 0);
      } else {
        return Matrix4.translationValues(0, animationOver ? 0 : height, 0);
      }
    } else {
      if (currentIndex % 2 == 0) {
        return Matrix4.translationValues(
            animationOver ? 0 : -width, animationOver ? 0 : -height, 0);
      } else {
        return Matrix4.translationValues(
            animationOver ? 0 : width, animationOver ? 0 : height, 0);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  Widget build(BuildContext context) {
    dynamic list = widget.list;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.transparent,
        leading: getBackButton(arrowColor: Colors.white, context: context),
      ),
      body: Padding(
        padding: EdgeInsets.only(
            top: height * 0.01, left: width * 0.05, right: width * 0.05),
        child: GridView.builder(
          itemCount: list.length,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisSpacing: 18,
              mainAxisSpacing: 15,
              childAspectRatio: height / width * 0.46,
              crossAxisCount: width ~/ 150),
          itemBuilder: (context, index) {
            return GestureDetector(onTap: () {
              Navigator.of(context).push(PageTransition(
                  duration: const Duration(milliseconds: 225),
                  child: PlayListScreen(
                      playListModel: list[index], requiredRefreash: false),
                  type: PageTransitionType.rightToLeftWithFade));
            }, child: Consumer<MusicController>(
              builder: (context, value, child) {
                return AnimatedContainer(
                  curve: Curves.easeInOut,
                  transform: getTransition(
                      currentIndex: index,
                      height: height,
                      width: width,
                      index: widget.animationIndex),
                  duration: Duration(milliseconds: 250 + (index * 150)),
                  child: Animate(
                    effects: widget.animationIndex == 0
                        ? const [
                            BlurEffect(
                                curve: Curves.elasticInOut,
                                begin: Offset(1.7, 1.7),
                                end: Offset(0, 0),
                                duration: Duration(milliseconds: 1250)),
                            FadeEffect(
                                begin: 0.1,
                                end: 0.9,
                                duration: Duration(milliseconds: 900))
                          ]
                        : [
                            const FlipEffect(
                                curve: Curves.bounceInOut,
                                begin: 1,
                                end: 0,
                                duration: Duration(milliseconds: 800))
                          ],
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(20),
                          child: Hero(
                            tag: list[index].hashCode,
                            child: Image.asset(
                              list[index].imgPath,
                              fit: BoxFit.cover,
                              height: height * 0.15,
                              width: width * 0.48,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: height * 0.01,
                        ),
                        Text(list[index].name,
                            style: Theme.of(context).textTheme.titleMedium)
                      ],
                    ),
                  ),
                );
              },
            ));
          },
        ),
      ),
    );
  }
}
