// ignore_for_file: must_be_immutable

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/Model/LangSongModel.dart';
import 'package:music_app/Model/SingerInfoModel.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:music_app/view/player_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:provider/provider.dart';

class PlayListScreen extends StatefulWidget {
  final dynamic playListModel;
  final bool requiredRefreash;
  const PlayListScreen(
      {super.key, required this.playListModel, required this.requiredRefreash});

  @override
  State<StatefulWidget> createState() {
    return _PlayListScreenState();
  }
}

class _PlayListScreenState extends State<PlayListScreen> {
  bool animationOver = false;
  double? top;
  double? top2;
  ImageProvider getDpyImg(dynamic playListModel) {
    if (playListModel.runtimeType == SingerInfoModel ||
        playListModel.runtimeType == LangSongModel) {
      return AssetImage(playListModel.imgPath);
    } else {
      if (playListModel.filePath != null) {
        return FileImage(File(playListModel.filePath));
      } else {
        return AssetImage(playListModel.imgPath);
      }
    }
  }

  String getTag(dynamic playListModel) {
    if (playListModel.runtimeType == SingerInfoModel ||
        playListModel.runtimeType == LangSongModel) {
      return playListModel.imgPath;
    } else {
      if (playListModel.filePath != null) {
        return playListModel.filePath;
      } else {
        return playListModel.imgPath;
      }
    }
  }

  String getStr(dynamic playListModel) {
    if (playListModel.runtimeType == LangSongModel) {
      return 'Songs';
    } else {
      return "";
    }
  }

  Widget getInfo(dynamic playListModel) {
    if (playListModel.runtimeType == SingerInfoModel) {
      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text("${playListModel.year}",
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(fontSize: 20)),
          Text(" * ",
              style: Theme.of(context)
                  .textTheme
                  .displayMedium!
                  .copyWith(color: const Color.fromRGBO(203, 200, 200, 1))),
          Text(playListModel.location,
              style: Theme.of(context)
                  .textTheme
                  .titleSmall!
                  .copyWith(fontSize: 20)),
        ],
      );
    } else {
      return const SizedBox();
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  Widget build(BuildContext context) {
    dynamic playListModel = widget.playListModel;
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        body: CustomScrollView(
      slivers: [
        SliverAppBar(
            automaticallyImplyLeading: false,
            toolbarHeight: height * 0.07,
            leading: GestureDetector(
                onTap: () {
                  Navigator.of(context).pop();
                },
                child: Padding(
                    padding: EdgeInsets.only(
                        left: width * 0.03, bottom: height * 0.01),
                    child: getBackButton(
                        arrowColor: Colors.black87, context: context))),
            backgroundColor: Colors.transparent,
            pinned: true,
            expandedHeight: height * 0.45,
            flexibleSpace: LayoutBuilder(
              builder: (BuildContext context, BoxConstraints constraints) {
                top = constraints.biggest.height;
                top2 = MediaQuery.of(context).padding.top + (height * 0.07);

                return FlexibleSpaceBar(
                  centerTitle: false,
                  titlePadding: EdgeInsets.only(
                      left: top != top2 ? width * 0.1 : width * 0.18,
                      right: width * 0.018,
                      bottom: top != top2 ? height * 0.072 : height * 0.01),
                  title: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        height: height * 0.06,
                        alignment: Alignment.center,
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        decoration: top == top2
                            ? BoxDecoration(
                                borderRadius: BorderRadius.circular(60),
                                color: Colors.black12.withOpacity(0.25))
                            : null,
                        child: Text(
                          '${playListModel.name} ${getStr(playListModel)}',
                          style: Theme.of(context)
                              .textTheme
                              .titleLarge!
                              .copyWith(fontSize: 30),
                        ),
                      ),
                      top == top2
                          ? Container(
                              height: height * 0.06,
                              alignment: Alignment.center,
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 5),
                              decoration: top == top2
                                  ? BoxDecoration(
                                      borderRadius: BorderRadius.circular(60),
                                      color: Colors.black12.withOpacity(0.25))
                                  : null,
                              child: dropDownWidget(
                                  songlist: playListModel.songList,
                                  context: context,
                                  height: height),
                            )
                          : const SizedBox()
                    ],
                  ),
                  expandedTitleScale: 1,
                  collapseMode: CollapseMode.pin,
                  background: Column(
                    children: [
                      Hero(
                        tag: playListModel.hashCode,
                        child: AnimatedOpacity(
                          opacity: (1 - (top2! / top!).roundToDouble())
                              .roundToDouble(),
                          duration: const Duration(milliseconds: 900),
                          child: Container(
                            height: height * 0.4,
                            width: double.maxFinite,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: getDpyImg(playListModel))),
                            child: Container(
                              padding: EdgeInsets.only(
                                  top: height * 0.03,
                                  left: width * 0.05,
                                  right: width * 0.05),
                              decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                      begin: Alignment.center,
                                      end: Alignment.bottomCenter,
                                      stops: [
                                    0.3,
                                    0.97
                                  ],
                                      colors: [
                                    Colors.transparent,
                                    Color.fromRGBO(24, 24, 24, 1)
                                  ])),
                              alignment: Alignment.bottomLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      const SizedBox(),
                                      IconButton(
                                          onPressed: () {
                                            if (playListModel.songList.length >
                                                0) {
                                              Provider.of<OperationalController>(
                                                      context,
                                                      listen: false)
                                                  .suffuleIndex = 2;
                                              getFlutterToast(
                                                  message:
                                                      'Shuffle started for playlist');
                                              Navigator.of(context).push(PageTransition(
                                                  duration: const Duration(
                                                      milliseconds: 225),
                                                  child: PlayerPage(
                                                      songList: playListModel
                                                          .songList,
                                                      activeIndex: Provider.of<
                                                                  OperationalController>(
                                                              context,
                                                              listen: false)
                                                          .random
                                                          .nextInt(playListModel
                                                              .songList
                                                              .length)),
                                                  type:
                                                      PageTransitionType.scale,
                                                  alignment:
                                                      Alignment.topRight));
                                            } else {
                                              getFlutterToast(
                                                  message:
                                                      'Your Song List is empty');
                                            }
                                          },
                                          icon: getShuffleButton(
                                              height: height,
                                              backGround: Colors.red,
                                              foreground: Colors.black))
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            left: width * 0.135,
                            right: width * 0.05,
                            top: height * 0.015),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            getInfo(playListModel),
                            GestureDetector(
                              onTap: () {
                                if (playListModel.songList.length > 0) {
                                  Navigator.of(context).push(PageTransition(
                                      duration:
                                          const Duration(milliseconds: 225),
                                      child: PlayerPage(
                                          songList: playListModel.songList,
                                          activeIndex: 0),
                                      type: PageTransitionType.scale,
                                      alignment: Alignment.topRight));
                                } else {
                                  getFlutterToast(
                                      message: 'Your Song List is empty');
                                }
                              },
                              child: getPlayAllButton(context: context),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            )),
        Consumer<MusicController>(builder: (context, value, val) {
          return SliverGrid.builder(
              itemCount: playListModel.songList.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisSpacing: 18,
                  mainAxisSpacing: 20,
                  //childAspectRatio: (height / width) * 0.46,
                  crossAxisCount: width ~/ 150),
              itemBuilder: (context, index) {
                return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(PageTransition(
                          duration: const Duration(milliseconds: 225),
                          child: PlayerPage(
                            songList: playListModel.songList,
                            activeIndex: index,
                          ),
                          type: PageTransitionType.rightToLeftWithFade));
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                          left: index % 2 == 0 ? width * 0.05 : 0,
                          right: index % 2 == 1 ? width * 0.05 : 0),
                      child: songRowInfo(
                          context: context,
                          listLength: playListModel.songList.length,
                          animationOver: animationOver,
                          currentIndex: index,
                          animationIndex:
                              playListModel.runtimeType == SingerInfoModel
                                  ? 1
                                  : playListModel.runtimeType == LangSongModel
                                      ? 5
                                      : 2,
                          requiredRefreah: widget.requiredRefreash,
                          songInfoModel: playListModel.songList[index],
                          imgPath: playListModel.songList[index].imgPath,
                          songName: playListModel.songList[index].name,
                          likeRefreah: false,
                          songYear: playListModel.songList[index].year),
                    ));
              });
        }),
      ],
    )
        //
        );
  }
}
