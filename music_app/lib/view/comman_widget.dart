// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:flutter_speed_dial/flutter_speed_dial.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/Model/SongInfoModel.dart';
import 'package:music_app/view/player_page.dart';
import 'package:page_transition/page_transition.dart';
import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:readmore/readmore.dart';
import 'package:sqflite/sqflite.dart';

import '../Controller/MusicController.dart';
import '../Model/PlayListModel.dart';

List<String> imgList = [
  'assets/musicIcon3.png',
  'assets/musicIcon2.jpg',
  'assets/musicIcon1.jpg',
  'assets/musicIcon7.jpg',
  'assets/musicIcon6.webp',
  'assets/musicIcon5.jpg',
  'assets/musicIcon4.jpg',
];
Widget songRowInfo(
    {required String imgPath,
    required String songName,
    required int songYear,
    required BuildContext context,
    required SongInfoModel songInfoModel,
    required bool requiredRefreah,
    required bool likeRefreah,
    required bool animationOver,
    required int listLength,
    required int animationIndex,
    required int currentIndex}) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;

  Matrix4 getMatrix4() {
    if (currentIndex % 2 == 1) {
      return Matrix4.translationValues(animationOver ? 0 : width, 0, 0);
    } else {
      return Matrix4.translationValues(animationOver ? 0 : -width, 0, 0);
    }
  }

  List<Effect<dynamic>>? getEffect() {
    if (animationIndex == 0) {
      return [
        const FadeEffect(
            curve: Curves.easeInOut,
            begin: 0.1,
            end: 1,
            duration: Duration(milliseconds: 900))
      ];
    } else if (animationIndex == 1) {
      return [
        const FadeEffect(
            curve: Curves.easeInOut,
            begin: 0.1,
            end: 1,
            duration: Duration(milliseconds: 900)),
        ScaleEffect(
            curve: Curves.easeInOut,
            begin: const Offset(0, 0),
            end: const Offset(1, 1),
            alignment: currentIndex % 2 == 0
                ? Alignment.centerLeft
                : Alignment.centerRight,
            duration: const Duration(milliseconds: 700))
      ];
    } else if (animationIndex == 2) {
      return [
        const SaturateEffect(
            curve: Curves.easeInQuad,
            begin: 0,
            end: 1,
            duration: Duration(milliseconds: 900))
      ];
    } else if (animationIndex == 3) {
      return [
        const FadeEffect(
            curve: Curves.easeInOut,
            begin: 0.1,
            end: 1,
            duration: Duration(milliseconds: 900)),
        ColorEffect(
            curve: Curves.easeInOutCirc,
            begin: Theme.of(context).scaffoldBackgroundColor,
            end: Colors.transparent,
            duration: const Duration(milliseconds: 600))
      ];
    } else if (animationIndex == 4) {
      return [
        const FadeEffect(
            curve: Curves.easeInOut,
            begin: 0.1,
            end: 1,
            duration: Duration(milliseconds: 900)),
        const FlipEffect(
            curve: Curves.bounceInOut,
            begin: 1,
            end: 0,
            duration: Duration(milliseconds: 850))
      ];
    } else if (animationIndex == 5) {
      return [
        const BlurEffect(
            curve: Curves.elasticInOut,
            begin: Offset(1.35, 1.35),
            end: Offset(0, 0),
            duration: Duration(milliseconds: 1000))
      ];
    }
    return null;
  }

  return Animate(
    effects: getEffect(),
    child: AnimatedContainer(
      curve: Curves.easeInOut,
      transform: getMatrix4(),
      duration: Duration(
          milliseconds: (400 +
              (currentIndex * getValDurSlide(listLength, animationOver)))),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(10),
              child: Hero(
                tag: (songInfoModel.islocal) ? songInfoModel.hashCode : imgPath,
                child: Image.asset(
                  imgPath,
                  fit: BoxFit.cover,
                  height: height * 0.15,
                  width: width * 0.48,
                ),
              )),
          SizedBox(
            //height: height * 0.065,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Hero(
                          tag: songName,
                          child: getProperTitle(
                              songName: songName, context: context)),
                      Hero(
                        tag: songInfoModel,
                        child: Text("$songYear",
                            style: Theme.of(context).textTheme.titleSmall),
                      ),
                    ],
                  ),
                ),
                PopupMenuButton(
                  splashRadius: 20,
                  shape: ContinuousRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  icon: const Icon(Icons.more_vert_sharp),
                  iconColor: Colors.white,
                  iconSize: 25,
                  elevation: 10,
                  color: Colors.grey[800],
                  onSelected: (value) async {
                    if (value == 'Val1') {
                      songInfoModel.isafav = !songInfoModel.isafav;
                      List<SongInfoModel> favList =
                          Provider.of<MusicController>(context, listen: false)
                              .favSong;
                      if (songInfoModel.isafav) {
                        favList.add(songInfoModel);
                      } else {
                        favList.remove(songInfoModel);
                      }
                      if (songInfoModel.islocal) {
                        await updateSong(
                            sonfObj: songInfoModel, tableName: 'LocalSong');
                      } else {
                        await updateSong(
                            sonfObj: songInfoModel, tableName: 'SongInfo');
                      }

                      if (likeRefreah) {
                        Provider.of<MusicController>(context, listen: false)
                            .refreash();
                      }
                    } else if (value == 'Val2') {
                      await showPlayListInfo(
                          context: context, songInfoModel: songInfoModel);
                      await updateSong(
                          sonfObj: songInfoModel, tableName: 'SongInfo');
                      if (requiredRefreah) {
                        Provider.of<MusicController>(context, listen: false)
                            .refreash();
                      }

                      //Provider.of<MusicController>(context,listen: false).refreash();
                    } else if (value == 'Val3') {
                      if (songInfoModel.isafav) {
                        Provider.of<MusicController>(context, listen: false)
                            .favSong
                            .remove(songInfoModel);
                      }
                      Provider.of<MusicController>(context, listen: false)
                          .localSongList
                          .remove(songInfoModel);

                      Provider.of<MusicController>(context, listen: false)
                          .refreash();

                      await database!.delete('LocalSong',
                          where: 'key= ?', whereArgs: [songInfoModel.key]);
                    }
                  },
                  itemBuilder: (context) {
                    return [
                      PopupMenuItem(
                        value: 'Val1',
                        child: Icon(
                          (songInfoModel.isafav)
                              ? Icons.favorite
                              : Icons.favorite_border_sharp,
                          color: Colors.white,
                        ),
                      ),
                      (!songInfoModel.islocal)
                          ? PopupMenuItem(
                              value: 'Val2',
                              child: Row(
                                children: [
                                  Text('Add to library',
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  const Icon(
                                    Icons.library_add_sharp,
                                    color: Colors.white,
                                  )
                                ],
                              ),
                            )
                          : PopupMenuItem(
                              value: 'Val3',
                              child: Row(
                                children: [
                                  Text('Delete',
                                      style: Theme.of(context)
                                          .textTheme
                                          .titleMedium),
                                  const SizedBox(
                                    width: 8,
                                  ),
                                  const Icon(
                                    Icons.delete,
                                    color: Colors.white,
                                  )
                                ],
                              ),
                            )
                    ];
                  },
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}

Widget getProperTitle(
    {required String songName, required BuildContext context}) {
  if (songName.length > 10) {
    return ReadMoreText(
      songName,
      trimLines: 1,
      trimMode: TrimMode.Line,
      trimCollapsedText: '.',
      isExpandable: false,
      moreStyle: const TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
      style: GoogleFonts.inter(
          fontSize: 17,
          fontWeight: FontWeight.w600,
          color: const Color.fromRGBO(203, 200, 200, 1)),
    );
  } else {
    return Text(songName, style: Theme.of(context).textTheme.titleMedium);
  }
}

Widget getBackButton(
    {required Color arrowColor, required BuildContext context}) {
  return GestureDetector(
    onTap: () {
      Navigator.of(context).pop();
    },
    child: Container(
      height: 50,
      width: 50,
      decoration:
          const BoxDecoration(shape: BoxShape.circle, color: Colors.white12),
      alignment: Alignment.center,
      child: Icon(
        Icons.arrow_back,
        color: arrowColor,
      ),
    ),
  );
}

Widget getSongList(
    {required List<SongInfoModel> displayList,
    required double width,
    required bool likeRefeash,
    required bool animationOver,
    required int animationIndex,
    required BuildContext context,
    required bool requiredRefreash}) {
  double height = MediaQuery.of(context).size.height;
  double width = MediaQuery.of(context).size.width;
  return GridView.builder(
    itemCount: displayList.length,
    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: 18,
        mainAxisSpacing: 15,
        childAspectRatio: (height / width) * 0.4,
        crossAxisCount: width ~/ 150),
    itemBuilder: (context, index) {
      return GestureDetector(
        onTap: () {
          Navigator.of(context).push(PageTransition(
              duration: const Duration(milliseconds: 225),
              child: PlayerPage(
                songList: displayList,
                activeIndex: index,
              ),
              type: PageTransitionType.rightToLeftWithFade));
        },
        child: songRowInfo(
            context: context,
            listLength: displayList.length,
            animationOver: animationOver,
            currentIndex: index,
            animationIndex: animationIndex,
            requiredRefreah: requiredRefreash,
            songInfoModel: displayList[index],
            imgPath: displayList[index].imgPath,
            songName: displayList[index].name,
            likeRefreah: likeRefeash,
            songYear: displayList[index].year),
      );
    },
  );
}

Widget getPlayAllButton({required BuildContext context}) {
  return Container(
      height: 40,
      width: 100,
      alignment: Alignment.center,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(19),
          color: Theme.of(context).primaryColor),
      child:
          Text('Play All', style: Theme.of(context).textTheme.displayMedium));
}

Image getCoverPhoto({required String? imgPath, required String? filePath}) {
  if (filePath != null) {
    return Image.file(
      File(filePath),
      height: 50,
      width: 50,
      fit: BoxFit.cover,
    );
  } else {
    return Image.asset(
      imgPath!,
      height: 50,
      width: 50,
      fit: BoxFit.cover,
    );
  }
}

Future<int> showPlayListInfo(
    {required BuildContext context,
    required SongInfoModel songInfoModel}) async {
  await showCupertinoModalPopup(
    useRootNavigator: false,
    context: context,
    builder: (context) {
      double height = MediaQuery.of(context).size.height;
      return Material(
        type: MaterialType.transparency,
        child: Container(
          margin: EdgeInsets.symmetric(vertical: height * 0.30, horizontal: 10),
          height: height * 0.35,
          padding: const EdgeInsets.all(15),
          width: double.maxFinite,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(25), color: Colors.grey[850]),
          child: Column(
            children: [
              Text('Add to playList',
                  style: Theme.of(context).textTheme.displayLarge),
              SizedBox(
                height: height * 0.02,
              ),
              Container(
                width: double.maxFinite,
                height: 2,
                decoration: const BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.horizontal(
                        left: Radius.circular(20), right: Radius.circular(20))),
              ),
              SizedBox(
                height: height * 0.02,
              ),
              Expanded(child: Consumer<MusicController>(
                builder: (context, value, child) {
                  List<PlayListModel> userPlayList = value.userPlayList;
                  return (userPlayList.isNotEmpty)
                      ? ListView.builder(
                          itemCount: userPlayList.length,
                          padding: EdgeInsets.zero,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                              child: Row(
                                children: [
                                  IconButton(onPressed: () async {
                                    if (userPlayList[index]
                                        .songList
                                        .contains(songInfoModel)) {
                                      userPlayList[index]
                                          .songList
                                          .remove(songInfoModel);
                                      songInfoModel.inPlayList
                                          .remove(userPlayList[index].name);
                                    } else {
                                      userPlayList[index]
                                          .songList
                                          .add(songInfoModel);
                                      songInfoModel.inPlayList
                                          .add(userPlayList[index].name);
                                    }

                                    await updateSong(
                                        sonfObj: songInfoModel,
                                        tableName: 'SongInfo');
                                    Provider.of<OperationalController>(context,
                                            listen: false)
                                        .refreash();
                                  }, icon: Consumer<OperationalController>(
                                    builder: (context, value2, child) {
                                      return Icon(
                                        userPlayList[index]
                                                .songList
                                                .contains(songInfoModel)
                                            ? Icons.check_box
                                            : Icons
                                                .check_box_outline_blank_rounded,
                                        color: Theme.of(context).primaryColor,
                                        size: 35,
                                      );
                                    },
                                  )),
                                  Container(
                                    padding: const EdgeInsets.all(20),
                                    child: Row(
                                      children: [
                                        ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(20),
                                            child: Hero(
                                              tag: userPlayList[index],
                                              child: getCoverPhoto(
                                                  imgPath: userPlayList[index]
                                                      .imgPath,
                                                  filePath: userPlayList[index]
                                                      .filePath),
                                            )),
                                        const SizedBox(
                                          width: 20,
                                        ),
                                        Text(
                                          userPlayList[index].name,
                                          style: Theme.of(context)
                                              .textTheme
                                              .displayLarge,
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        )
                      : Padding(
                          padding: EdgeInsets.symmetric(vertical: height * 0.1),
                          child: Text('No PlayList Available',
                              style: Theme.of(context).textTheme.displayLarge),
                        );
                },
              ))
            ],
          ),
        ),
      );
    },
  );
  return 0;
}

Future<List<SongInfoModel>> getInSong({required String tableName}) async {
  bool getTrueFalse(int val) {
    if (val == 1) {
      return true;
    } else {
      return false;
    }
  }

  List<String> getList(String str) {
    return str.split('-');
  }

  List<Map<String, dynamic>> temp = await database!.query(tableName);
  return List.generate(temp.length, (index) {
    return SongInfoModel(
        key: temp[index]['key'],
        isafav: getTrueFalse(temp[index]['isafav']),
        imgPath: temp[index]['imgPath'],
        songPath: temp[index]['songPath'],
        artistName: temp[index]['artistName'],
        inPlayList: getList(temp[index]['inPlayList']),
        islocal: getTrueFalse(temp[index]['islocal']),
        name: temp[index]['name'],
        lang: temp[index]['lang'],
        year: temp[index]['year']);
  });
}

List<SongInfoModel>? tempVal1;
List<PlayListModel>? tempVal2;
List<SongInfoModel>? tempVal3;
Database? database;
Future fetchDd() async {
  database = await openDatabase(
    join(await getDatabasesPath(), 'MusicPlayer.db'),
    version: 1,
    onCreate: (db, version) {
      List<SongInfoModel> songList = [
        SongInfoModel(
            imgPath: 'assets/images/kesariyaImg.jpg',
            songPath: 'audio/Kesariya.mp3',
            artistName: 'Arijit Sing',
            name: 'Kesariya',
            lang: 'Hindi',
            year: 2022),
        SongInfoModel(
            imgPath: 'assets/images/palImg.jpg',
            songPath: 'audio/Pal.mp3',
            artistName: 'Arijit Sing-Shreya Ghospal',
            name: 'Pal',
            lang: 'Hindi',
            year: 2018),
        SongInfoModel(
            imgPath: 'assets/images/pehle_Bhi_MainImg.jpg',
            songPath: 'audio/Pehle_Bhi_Main.mp3',
            artistName: 'Vishal',
            name: 'Pehle Bhi Main',
            lang: 'Hindi',
            year: 2023),
        SongInfoModel(
            imgPath: 'assets/images/shayadImg.jpg',
            songPath: 'audio/Shayad.mp3',
            artistName: 'Arijit Sing',
            name: 'Shayad',
            lang: 'Hindi',
            year: 2019),
        SongInfoModel(
            imgPath: 'assets/images/tujhe_Kitna_Chahne_LageImg.jpg',
            songPath: 'audio/Tujhe_Kitna_Chahne_Lage.mp3',
            artistName: 'Arijit Sing',
            name: 'Tujhe Kitna Chahne Lage',
            lang: 'Hindi',
            year: 2019),
        SongInfoModel(
            imgPath: 'assets/images/tum_Kya_MileImg.jpg',
            songPath: 'audio/Tum_Kya_Mile.mp3',
            artistName: 'Shreya Ghospal-Arijit Sing',
            name: 'Tum Kya Mile',
            lang: 'Hindi',
            year: 2023),
        SongInfoModel(
            imgPath: 'assets/images/yimmy_YimmyImg.jpg',
            songPath: 'audio/yimmy_Yimmy.mp3',
            artistName: 'Shreya Ghospal',
            name: 'Yimmy Yimmy',
            lang: 'Hindi',
            year: 2024),
        SongInfoModel(
            imgPath: 'assets/images/chandraimg.jpg',
            songPath: 'audio/chandra.mp3',
            artistName: 'Shreya Ghospal',
            name: 'Chandra',
            lang: 'Marathi',
            year: 2022),
        SongInfoModel(
            imgPath: 'assets/images/cold_WaterImg.jpg',
            songPath: 'audio/cold_Water.mp3',
            artistName: 'Justin Bieber',
            name: 'Cold Water',
            lang: 'English',
            year: 2018),
        SongInfoModel(
            imgPath: 'assets/images/ghamand_karImg.jpg',
            songPath: 'audio/ghamand_Kar.mp3',
            artistName: 'Avdhoot Gupte',
            name: 'Ghamand Kar',
            lang: 'Marathi-Hindi',
            year: 2020),
        SongInfoModel(
            imgPath: 'assets/images/intentionsImg.jpg',
            songPath: 'audio/intentions.mp3',
            artistName: 'Justin Bieber',
            name: 'Intentions',
            lang: 'English',
            year: 2020),
        SongInfoModel(
            imgPath: 'assets/images/let_Me_Love_Youimg.jpg',
            songPath: 'audio/let_Me_Love_You.mp3',
            artistName: 'Justin Bieber-DJ Snake',
            name: 'Let Me Love You',
            lang: 'English',
            year: 2016),
        SongInfoModel(
            imgPath: 'assets/images/raja_AalaImg.jpg',
            songPath: 'audio/raja_Aala.mp3',
            artistName: 'Avdhoot Gupte',
            name: 'Raja Aala',
            lang: 'Marathi',
            year: 2021),
        SongInfoModel(
            imgPath: 'assets/images/saharaImg.jpg',
            songPath: 'audio/sahara.mp3',
            artistName: 'DJ Snake',
            name: 'Sahara',
            lang: 'English',
            year: 2021),
        SongInfoModel(
            imgPath: 'assets/images/the_Savarkar_Rageimg.jpg',
            songPath: 'audio/the_Savarkar_Rage.mp3',
            artistName: 'Sambata',
            name: 'The Savarkar Rage',
            lang: 'Marathi',
            year: 2024),
        SongInfoModel(
            imgPath: 'assets/images/u_Are_My_HighImg.jpg',
            songPath: 'audio/u_Are_My_High.mp3',
            artistName: 'DJ Snake',
            name: 'U Are My High',
            lang: 'English',
            year: 2021),
        SongInfoModel(
            imgPath: 'assets/images/pinnakImg.jpg',
            songPath: 'audio/pinnak.mp3',
            artistName: 'Sambata',
            name: 'Pinnak',
            lang: 'Marathi',
            year: 2022),
        SongInfoModel(
            imgPath: 'assets/images/kaarteNibaarImg.jpg',
            songPath: 'audio/kaarte_Nibaar.mp3',
            artistName: 'Sambata',
            name: 'Kaarte Nibaar',
            lang: 'Marathi',
            year: 2021),
        SongInfoModel(
            imgPath: 'assets/images/gangsterShit1stimg.jpg',
            songPath: 'audio/gangster_Shit_1st.mp3',
            artistName: 'Sambata',
            name: 'Gangster Shit 1st',
            lang: 'Marathi',
            year: 2024),
        SongInfoModel(
            imgPath: 'assets/images/param_Sundariimg.jpeg',
            songPath: 'audio/param_Sundari.mp3',
            artistName: 'Shreya Ghospal',
            name: 'Param Sundari',
            lang: 'Hindi',
            year: 2021),
      ];
      db.execute(
          'CREATE TABLE SongInfo(key INTEGER PRIMARY KEY,imgPath Text,songPath Text,artistName Text,inPlayList Text,lang Text,isafav INTEGER,name Text,islocal INTEGER,year INTEGER)');
      db.execute(
          'CREATE TABLE PlayListInfo(key INTEGER PRIMARY KEY,name Text,imgPath Text,filePath Text)');
      for (int i = 0; i < songList.length; i++) {
        db.insert('SongInfo', songList[i].getMap(),
            conflictAlgorithm: ConflictAlgorithm.replace);
      }
      db.execute(
          'CREATE TABLE LocalSong(key INTEGER PRIMARY KEY,imgPath Text,songPath Text,artistName Text,inPlayList Text,lang Text,isafav INTEGER,name Text,islocal INTEGER,year INTEGER)');
    },
  );
}

Future updateSong(
    {required SongInfoModel sonfObj, required String tableName}) async {
  await database!.update(tableName, sonfObj.getMap(),
      where: 'key= ?', whereArgs: [sonfObj.key]);
}

Future<void> insertPlayList(PlayListModel playListModel) async {
  await database!.insert('PlayListInfo', playListModel.getMap(),
      conflictAlgorithm: ConflictAlgorithm.replace);
}

Future<void> updatePlayList({required PlayListModel playListModel}) async {
  await database!.update('PlayListInfo', playListModel.getMap(),
      where: 'key= ?', whereArgs: [playListModel.key]);
}

Future<void> deletePlayList({required PlayListModel playListModel}) async {
  await database!
      .delete('PlayListInfo', where: 'key= ?', whereArgs: [playListModel.key]);
}

Future<List<PlayListModel>> getPlayList() async {
  List<Map<String, dynamic>> list = await database!.query('PlayListInfo');
  return List.generate(list.length, (index) {
    return PlayListModel(
        name: list[index]['name'],
        songList: [],
        key: list[index]['key'],
        imgPath: list[index]['imgPath'],
        filePath: list[index]['filePath']);
  });
}

void getFlutterToast({required String message}) {
  Fluttertoast.showToast(
      msg: message,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIosWeb: 1,
      backgroundColor: Colors.black,
      textColor: Colors.white,
      fontSize: 16.0);
}

Widget dropDownWidget(
    {required List<SongInfoModel> songlist,
    required BuildContext context,
    required double height}) {
  return SpeedDial(
    animatedIcon: AnimatedIcons.menu_close,
    overlayOpacity: 0.4,

    spaceBetweenChildren: 10,
    spacing: 7,
    closeDialOnPop: true,
    backgroundColor: Colors.transparent,
    foregroundColor: Colors.white,
    direction: SpeedDialDirection.down,
    closeManually: false,
    shape: InputBorder.none,
    activeBackgroundColor: Colors.transparent,
    activeLabel: null,
    buttonSize: const Size(50, 50),
    label: null,
    animationAngle: BorderSide.strokeAlignInside,
    useRotationAnimation: true,
    //renderOverlay: true,
    overlayColor: Colors.black,
    children: [
      SpeedDialChild(
          onTap: () {
            if (songlist.isNotEmpty) {
              Navigator.of(context).push(PageTransition(
                  duration: const Duration(milliseconds: 225),
                  child: PlayerPage(songList: songlist, activeIndex: 0),
                  type: PageTransitionType.leftToRightWithFade));
            } else {
              getFlutterToast(message: 'Your Song List is empty');
            }
          },
          labelWidget: Animate(effects: const [
            FadeEffect(
              curve: Curves.easeInOut,
              delay: Duration(milliseconds: 150),
              duration: Duration(milliseconds: 300),
              begin: 0.3,
              end: 1,
            ),
            SlideEffect(
                curve: Curves.easeInOut,
                delay: Duration(milliseconds: 150),
                duration: Duration(milliseconds: 200),
                begin: Offset(0, -3),
                end: Offset(0, 0))
          ], child: getPlayAllButton(context: context))),
      SpeedDialChild(
          onTap: () {
            if (songlist.isNotEmpty) {
              Provider.of<OperationalController>(context, listen: false)
                  .suffuleIndex = 2;
              getFlutterToast(message: 'Shuffle started for playlist');
              Navigator.of(context).push(PageTransition(
                  duration: const Duration(milliseconds: 225),
                  child: PlayerPage(
                      songList: songlist,
                      activeIndex: Provider.of<OperationalController>(context,
                              listen: false)
                          .random
                          .nextInt(songlist.length)),
                  type: PageTransitionType.leftToRightWithFade));
            } else {
              getFlutterToast(message: 'Your Song List is empty');
            }
          },
          labelWidget: Animate(
              effects: const [
                FadeEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 300),
                  begin: 0.3,
                  end: 1,
                ),
                SlideEffect(
                  curve: Curves.easeInOut,
                  duration: Duration(milliseconds: 200),
                  begin: Offset(0, -4),
                  end: Offset(0, 0),
                )
              ],
              child: getShuffleButton(
                  height: height,
                  backGround: Theme.of(context).primaryColor,
                  foreground: Colors.white)))
    ],
  );
}

Widget getShuffleButton(
    {required double height,
    required Color backGround,
    required Color foreground}) {
  return Container(
    height: 50,
    width: 50,
    //margin: EdgeInsets.symmetric(horizontal: 25),
    alignment: Alignment.center,
    decoration: BoxDecoration(color: backGround, shape: BoxShape.circle),
    child: Icon(
      Icons.shuffle_rounded,
      color: foreground,
      size: height * 0.04,
    ),
  );
}

int getValDurSlide(int listLength, bool animationOver) {
  if (animationOver) {
    if (listLength < 25) {
      return 130 - listLength;
    } else if (listLength < 50) {
      return 65 - listLength;
    } else if (listLength < 75) {
      return 30 - listLength;
    } else if (listLength < 100) {
      return 15 - listLength;
    } else {
      return 6 - listLength;
    }
  } else {
    if (listLength < 25) {
      return 350;
    } else if (listLength < 50) {
      return 150;
    } else if (listLength < 75) {
      return 75;
    } else if (listLength < 100) {
      return 30;
    } else {
      return 15;
    }
  }
}
