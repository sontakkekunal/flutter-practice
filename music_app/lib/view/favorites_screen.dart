import 'package:flutter/material.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Model/SongInfoModel.dart';
import 'package:music_app/view/navbar.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:provider/provider.dart';

class FavoritesScreen extends StatefulWidget {
  const FavoritesScreen({super.key});
  @override
  State<StatefulWidget> createState() {
    return _FavoritesScreenState();
  }
}

class _FavoritesScreenState extends State {
  bool animationOver = false;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.only(
            top: height * 0.04, left: width * 0.05, right: width * 0.05),
        child: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Hero(
                    tag: 'fav',
                    child: Text('Favorite',
                        style: Theme.of(context).textTheme.labelLarge)),
                dropDownWidget(
                    songlist:
                        Provider.of<MusicController>(context, listen: false)
                            .favSong,
                    context: context,
                    height: height)
              ],
            ),
            Expanded(
              child: Consumer<MusicController>(
                builder: (context, value, child) {
                  List<SongInfoModel> songInfoList = value.favSong;
                  List<SongInfoModel> displayList = songInfoList;

                  return getSongList(
                      displayList: displayList,
                      animationOver: animationOver,
                      width: width,
                      animationIndex: 4,
                      requiredRefreash: false,
                      context: context,
                      likeRefeash: true);
                },
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: const Navbar(index: 0),
    );
  }
}
