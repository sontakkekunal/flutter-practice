// ignore_for_file: use_build_context_synchronously

import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:music_app/Controller/MusicController.dart';
import 'package:music_app/Controller/OperationalController.dart';
import 'package:music_app/Model/SongInfoModel.dart';
import 'package:music_app/view/navbar.dart';
import 'package:music_app/view/scale_up_down_animation.dart';
import 'package:music_app/view/comman_widget.dart';
import 'package:provider/provider.dart';
import 'package:readmore/readmore.dart';
import 'package:sqflite/sqflite.dart';

class LocalMusic extends StatefulWidget {
  const LocalMusic({super.key});

  @override
  State<StatefulWidget> createState() {
    return _LocalMusiState();
  }
}

class _LocalMusiState extends State {
  bool _textFeildAnimation = false;
  bool _buttonAnumation = false;
  final TextEditingController _textController = TextEditingController();
  final GlobalKey<FormFieldState> globalKey = GlobalKey<FormFieldState>();
  bool animationOver = false;
  Future<File?> addSong() async {
    try {
      FilePickerResult? result = await FilePicker.platform
          .pickFiles(type: FileType.audio, allowMultiple: false);
      if (result != null) {
        return File(result.files[0].path!);
      }
    } catch (e) {
      getFlutterToast(message: 'Sorry, Something went wrong');
    }
    return null;
  }

  void addLocalSong() {
    File? file;
    _textController.clear();
    showModalBottomSheet(
      context: context,
      //scrollControlDisabledMaxHeightRatio: 0.2 ,
      isScrollControlled: true,
      backgroundColor: Colors.grey[800],
      builder: (context) {
        double height = MediaQuery.of(context).size.height;
        double width = MediaQuery.of(context).size.width;
        return Padding(
          padding: MediaQuery.of(context).viewInsets,
          child: Container(
            //width: double.maxFinite,
            height: height * 0.35,
            margin: EdgeInsets.only(
                top: height * 0.03, left: width * 0.08, right: width * 0.08),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(60)),
            child: Column(
              children: [
                Text(
                  'Add local Song',
                  style: Theme.of(context)
                      .textTheme
                      .displayMedium!
                      .copyWith(color: Colors.white),
                ),
                SizedBox(
                  height: height * 0.005,
                ),
                Consumer<OperationalController>(
                    builder: (context, valObj, valZ) {
                  return ScaleUpDownAnimation(
                    scaleDuration: const Duration(milliseconds: 120),
                    afterAnimationDuration: const Duration(milliseconds: 15),
                    isAnimation: _textFeildAnimation,
                    onEnd: () {
                      _textFeildAnimation = false;
                      valObj.refreash();
                    },
                    child: TextFormField(
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      style: Theme.of(context).textTheme.labelMedium,
                      key: globalKey,
                      validator: (val) {
                        if (val == null || val.isEmpty) {
                          _textFeildAnimation = true;
                          valObj.refreash();
                          return "";
                        } else {
                          List<SongInfoModel> localSong =
                              Provider.of<MusicController>(context,
                                      listen: false)
                                  .localSongList;
                          for (int i = 0; i < localSong.length; i++) {
                            if (localSong[i].name == val) {
                              _textFeildAnimation = true;
                              valObj.refreash();
                              return "Same song name ";
                            }
                          }
                        }
                      },
                      controller: _textController,
                      decoration: InputDecoration(
                        hintText: 'Enter Song Name',
                        hintStyle: Theme.of(context)
                            .textTheme
                            .titleLarge!
                            .copyWith(fontSize: 20),
                        errorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(
                              color: Colors.red,
                              width: 1.3,
                            )),
                        focusedErrorBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(
                              color: Colors.red,
                              width: 1.3,
                            )),
                        focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(
                              color: Colors.orange,
                              width: 1.3,
                            )),
                        enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20),
                            borderSide: const BorderSide(
                              color: Colors.white12,
                              width: 1.3,
                            )),
                      ),
                    ),
                  );
                }),
                SizedBox(
                  height: height * 0.02,
                ),
                Text('Pick a song form local',
                    style: Theme.of(context).textTheme.titleMedium),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Consumer<OperationalController>(
                      builder: (context, value, child) {
                        return (file != null)
                            ? SizedBox(
                                width: width * 0.5,
                                child: ReadMoreText(
                                  file!.path.split('/').last,
                                  style: GoogleFonts.inter(
                                      fontSize: 16,
                                      fontWeight: FontWeight.w600,
                                      color: const Color.fromRGBO(
                                          255, 255, 255, 1)),
                                  trimMode: TrimMode.Line,
                                  trimLines: 2,
                                  trimCollapsedText: '....',
                                  isExpandable: false,
                                  moreStyle: const TextStyle(
                                      fontSize: 14,
                                      fontWeight: FontWeight.bold),
                                ),
                              )
                            : const SizedBox();
                      },
                    ),
                    GestureDetector(
                      onTap: () async {
                        File? tempFile = await addSong();
                        if (tempFile == null && file != null) {}
                        if (tempFile != null) {
                          file = tempFile;
                          Provider.of<OperationalController>(context,
                                  listen: false)
                              .refreash();
                        }
                      },
                      child: Consumer<OperationalController>(
                        builder: (context, value, val) {
                          return ScaleUpDownAnimation(
                            scaleDuration: const Duration(milliseconds: 120),
                            afterAnimationDuration:
                                const Duration(milliseconds: 15),
                            isAnimation: _buttonAnumation,
                            onEnd: () {
                              _buttonAnumation = false;
                              value.refreash();
                            },
                            child: Container(
                                height: height * 0.047,
                                width: width * 0.22,
                                alignment: Alignment.center,
                                margin: const EdgeInsets.all(10),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(19),
                                    color:
                                        const Color.fromRGBO(248, 162, 69, 1)),
                                child: Text(
                                  'Browse',
                                  style:
                                      Theme.of(context).textTheme.displayMedium,
                                )),
                          );
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: height * 0.002,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    GestureDetector(
                      onTap: () async {
                        _textController.text = _textController.text.trim();
                        if (globalKey.currentState!.validate() &&
                            file != null) {
                          FocusScope.of(context).unfocus();
                          Navigator.of(context).pop();
                          SongInfoModel obj = SongInfoModel(
                              imgPath: 'assets/images/localMusic.gif',
                              songPath: file!.path,
                              islocal: true,
                              artistName: 'Unknown',
                              name: _textController.text,
                              lang: 'Unknown',
                              year: DateTime.now().year);
                          // Provider.of<MusicController>(context, listen: false)
                          //     .localSongList
                          //     .add(obj);
                          Provider.of<MusicController>(context, listen: false)
                              .refreash();
                          await database!.insert('LocalSong', obj.getMap(),
                              conflictAlgorithm: ConflictAlgorithm.replace);
                          Provider.of<MusicController>(context, listen: false)
                                  .localSongList =
                              await getInSong(tableName: 'LocalSong');
                          Provider.of<MusicController>(context, listen: false)
                              .refreash();
                        } else if (file == null) {
                          _buttonAnumation = true;
                          Provider.of<OperationalController>(context,
                                  listen: false)
                              .refreash();
                        }
                      },
                      child: Container(
                          height: height * 0.047,
                          width: width * 0.18,
                          alignment: Alignment.center,
                          margin: const EdgeInsets.all(10),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(19),
                              color: Theme.of(context).primaryColor),
                          child: Text('Save',
                              style:
                                  Theme.of(context).textTheme.displayMedium)),
                    ),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((callback) {
      animationOver = true;
      Provider.of<MusicController>(context, listen: false).refreash();
    });
  }

  @override
  void dispose() {
    super.dispose();
    _textController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(
            vertical: height * 0.05, horizontal: width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Local Song',
                  style: Theme.of(context).textTheme.labelLarge,
                ),
                dropDownWidget(
                    songlist:
                        Provider.of<MusicController>(context, listen: false)
                            .localSongList,
                    context: context,
                    height: height)
              ],
            ),
            Expanded(child: Consumer<MusicController>(
              builder: (context, value, child) {
                List<SongInfoModel> displayList = value.localSongList;
                return getSongList(
                    displayList: displayList,
                    likeRefeash: false,
                    animationIndex: 3,
                    animationOver: animationOver,
                    context: context,
                    width: width,
                    requiredRefreash: true);
              },
            ))
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          addLocalSong();
        },
        backgroundColor: Theme.of(context).primaryColor,
        child: const Icon(
          Icons.add,
          size: 40,
          color: Colors.white,
        ),
      ),
      bottomNavigationBar: const Navbar(index: 4),
    );
  }
}
